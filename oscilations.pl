:- module(
	oscilations,
	[
		% Commands
%		check_directory/1,
		check_oscillations/0
		% Public API
	]
).

:- use_module(doc).
:- use_module(counters).
:- use_module(invariants).

:- devdoc('\\section{Commands}'). 



create_associated_arcs_oscillations(InArcs, OutArcs) :-
	forall(
		member([VertexAId, VertexMid, SpeciesIn_Sorted, ReactionsIn_Sorted, SignIn],InArcs),
		forall(
			member([_, VertexBId, SpeciesOut_Sorted, ReactionsOut_Sorted, SignOut], OutArcs),
			(
				multistability:list_union(ReactionsIn_Sorted, ReactionsOut_Sorted, New_Reactions_Sorted),
				multistability:remove_duplicates(New_Reactions_Sorted, Reactions_Aux),
				(VertexAId == VertexBId
				->
					(
					% Self-Loop
						multistability:list_union(SpeciesIn_Sorted, SpeciesOut_Sorted, New_Species_Sorted_aux),
						(multistability:remove_duplicates(New_Species_Sorted_aux, VertexAId, VertexMid, _New_Species_Sorted)
						->
							(
								multistability:sign_rule(SignIn, SignOut, New_Sign),
								(New_Sign = -
								->
									(
										!,fail	%Negative loop
									);
									(
										true	%Positive loop
									)
								)
							);
							(
								true	%Same Species Twice
							)
						)
					);
					(
					% Not Self-Loop
						multistability:list_union(SpeciesIn_Sorted, SpeciesOut_Sorted, New_Species_Sorted_aux),
						(multistability:remove_duplicates(New_Species_Sorted_aux, VertexMid, New_Species_Sorted)
						->
							(
								multistability:sign_rule(SignIn, SignOut, New_Sign),
								multistability:add_edge_to_multistability_graph(VertexAId, VertexBId, New_Species_Sorted, Reactions_Aux, New_Sign)
							);
							(
								true	%Same Species Twice
							)
						)
					)
				)
			)
		)
	).
		


remove_vertex(VertexId) :-
	get_current_graph(MId),
	item([parent: MId, kind:vertex, item: VertexName, id: VertexId]),
	findall(
		Label,
		(
			item([parent: MId, kind: edge, item: InEdge, id: InEdgeId]),
			InEdge = (_ -> VertexName),
			get_attribute(InEdgeId, label = Label_list),
			member(Label, Label_list)
		),
		InArcs
	),
	findall(
		Label,
		(
			item([parent: MId, kind: edge, item: OutEdge, id: OutEdgeId]),
			OutEdge = (VertexName -> _),
			get_attribute(OutEdgeId, label = Label_list),
			member(Label, Label_list)
		),
		OutArcs
	),
	forall(
		member([[VertexAId|_],_,_], InArcs),
		(
			get_attribute(VertexAId, out_deg = Out),
			Out1 is Out-1,
			set_attribute(VertexAId, out_deg = Out1)
		)
	),
	forall(
		member([Species, _,_], OutArcs),
		(
			append(_,[VertexBId],Species),
			get_attribute(VertexBId, in_deg = In),
			In1 is In-1,
			set_attribute(VertexBId, in_deg = In1)
		)
	),
%	create_associated_arcs(InArcs, OutArcs),
	create_associated_arcs_oscillations(InArcs, OutArcs),
	delete_vertex([VertexName]).


remove_best_vertex :-
	multistability:get_vertex_to_remove(VertexId),
	remove_vertex(VertexId).

check_acyclicity(0) :- !.
check_acyclicity(N) :-
%	write('Graph at state N = '),
%	write(N),
%	write('\n'),
%	list_graph_objects,
	remove_best_vertex,
	N1 is N-1,
	check_acyclicity(N1).

check_acyclicity :-
	multistability:pre_process_multistability_graph,
	multistability:get_number_of_nodes(N),
	check_acyclicity(N).

check_oscillations :-
	biocham_command,
	doc('Checks a necessary condition for the periodic behaviour of the current model.'),
	multistability:multistability_graph,
	get_time(Start_time),
	(check_acyclicity
	->
		(
			get_time(End_time),
			_Diff_time is End_time-Start_time,
			write('Graph does not contain any negative cycle\n')
		);
		(
			get_time(End_time),
			_Diff_time is End_time-Start_time,
			write('Negative loops cannot be removed'),
%			write('\n'),
%			write('Done in '),
%			write(_Diff_time),
%			write('s'),
			write('\n')
		)
	).


check_directory(Path) :-
%	biocham_command,
	directory_files(Path, Entries_unsorted),
	sort(Entries_unsorted,Entries),
	forall(
		(
			member(FileName, Entries),
			file_name_extension(FullName, '.xml', FileName),
			file_name_extension(FullName, '.ode', FileToExport),
			atom_concat(Path,FileName, FullPath),
			write(FullName),
			write('\n'),
			load(FullPath),
			export_ode(FileToExport),
			load_reactions_from_ode(FileToExport),
			delete_file(FileToExport)
		),
		(
			check_oscillations
		)
	).

%profiling_acyc :-
%	load("./library/biomodels/BIOMD0000000270.xml"),
%	export_ode("10.ode"),
%	load_reactions_from_ode("10.ode"),
%	delete_file("10.ode"),
%	reset_profiler,
%	profile(check_oscillations, [cumulative(true)]),


