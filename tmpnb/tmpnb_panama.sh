#!/bin/bash

export TOKEN=$( head -c 30 /dev/urandom | xxd -p )
docker run -d --restart=always --net=host -e CONFIGPROXY_AUTH_TOKEN=$TOKEN --expose 8000 --name=proxy jupyterhub/configurable-http-proxy --default-target http://127.0.0.1:9999
docker run -d --restart=always --net=host -e CONFIGPROXY_AUTH_TOKEN=$TOKEN --name=tmpnb -v /var/run/docker.sock:/docker.sock jupyter/tmpnb python orchestrate.py --image='registry.gitlab.inria.fr/lifeware/biocham:v4.5.15' --mem_limit=1g --pool-size=20 --command='start.sh biocham --notebook --NotebookApp.base_url={base_path} --NotebookApp.token={token} --ip=0.0.0.0 --port {port}'
