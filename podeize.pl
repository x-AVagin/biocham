/** <PODE> Tools to transform an ODE into a PODE
 * 
 * The main predicate of this module is polynomialize_ode that exists as a biocham command
 * (arity 0) and internal predicate (arity 3). It works as a cycle between
 * detect_new_variable/3 that determine the necessary variable to polynomialize the
 * current set of ODE and add_variable/8 that add them.
 *
 * By definition in this module, the free variable is supposed to be the time and denoted
 * as 't'.
 *
@author M. Hemery
@license GNU-GPL v.2
**/

:- module(
  pode,
  [
    %Commands
    compile_function/2,
    polynomialize_ode/0,
    %API
    detect_new_variable/3,
    polynomialize_ode/3,
    complete_derivation/3,
    warning_parameter/1
  ]).

:- use_module(biocham).
:- use_module(counters).
:- use_module(doc).
:- use_module(formal_derivation).
:- use_module(objects).
:- use_module(parameters).
:- use_module(reaction_rules).
:- use_module(util).

:- dynamic initial_transformation/1.

%%%%%%%%%%%%%%%%%%%%%%%
%%% BIOCHAM Command %%%
%%%%%%%%%%%%%%%%%%%%%%%

compile_function(Formula, Input) :-
  biocham_command,
  % type(Formula, object = arithmetic_expression),
  option(
    quadratic_reduction,
    reduction_methods,
    _Reduction,
    'Determine if the quadratic reduction for synthesizing reactions with at most two reactants has to be performed'
  ),
  type(Input, object),
  doc("Compile the desired formula into a CRN by determining a corrsponding IVP, then PIVP and finally a CRN. Typical usage looks like:
  - compile_function(Y=f(X), X). Which set up a CRN such that when parameter(input=X), the final value of Y is f(X). and
  - compile_function(Y=f(Time), Time). Which set up a function of time."),
  add_derivation_to_ode(Formula, Input, Output_list),
  (
    Input = time
  ->
    true
  ;
    add_ode([d('t')/dt = 1])
  ),
  % compilation pipeline
  polynomialize_ode,
  debug(compilation, "ODE is polynomialize", []),
  compile_from_ode(Input, Output_list).


% :- doc('
%   \\begin{example} Compilation of a hill function of order 2:\n
% ').
% :- biocham( compile_function(hill = x^2/(1+x^2), x) ).
% :- biocham( list_model ).
% :- biocham(numerical_simulation(time:10)). %, method:msbdf)).
% :- biocham(plot).
% :- doc('
%   \\end{example}
% ').


polynomialize_ode :-
  biocham_command,
  doc('Starting from the current ODE system, builds an equivalent polynomial ODE system by introducing more variables that compute the non-polynomial terms.'),
  assertz(arithmetic_rules:numbers_may_be_negative),
  % Fetch the old system (TODO: handle the parameters, functions and co.)
  with_current_ode_system((
    get_current_ode_system(Id),
    findall(Deriv, item([parent:Id, kind:ode, item:Deriv]), Input)
  )),
  % Determine the transformation and the new system
  polynomialize_ode(Input, Output, Transf_raw),
  sort(Transf_raw, Transf),
  forall(
    member(Name->Expr, Transf),
    (
      Name = Expr
    ->
      format("~w is a proxy for time.~n", [Name])
    ;
      format("~w = ~w~n", [Name, Expr])
    )
  ),
  catch(
    findall(
      (Var-Value),
      (
        member(d(Var)/dt = _Deriv, Output),
        (
          member(Var->Expr, Transf)
        ->
          true
        ;
          Expr = Var
        ),
        compute_initial_value(Expr, Value)
      ),
      Initial_values
    ),
    error(evaluation_error(zero_divisor), _Context),
    print_message(warning, initialization_impossible(Var))
  ),
  (
    member(Name-Value, Initial_values),
    abs(Value) =:= 1.0Inf
  ->
    print_message(warning, initialization_impossible(Name))
  ;
    true
  ),
  % Construct and select the new system
  new_ode_system(NewId),
  set_ode_system_name(NewId, polynomial_system),
  forall(
    member((d(Var)/dt = Deriv), Output),
    (
      % add_item([parent: NewId, kind:ode, item:(d(Var)/dt = Deriv)]),
      add_ode(NewId, d(Var)/dt = Deriv),
      (
        member(Var-Value, Initial_values)
      ->
        set_ode_initial_value(NewId, Var, Value)
      ;
        true
      )
    )
  ),
  select_ode_system(polynomial_system),
  retractall(arithmetic_rules:numbers_may_be_negative).


:- doc('
  \\begin{example} Polynomialization of an ODE system:\n 
').
:- biocham(d('A')/dt = exp('A')).
:- biocham(polynomialize_ode).
:- biocham(list_ode).
:- doc('
  \\end{example}
').


prolog:message(initialization_impossible(Name)) -->
  ['The initial value of ~p may contains either arithmetic errors or infinite values.' - [Name]].


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Application Progamming Interface %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! polynomialize_ode(+Input, -Output, -Transf) 
%
% Construct the list of variables and the initial substitution list, then delegate
% to polynomialize_ode/6

polynomialize_ode(Input, Output, Transf) :-
  set_counter(variable_id, 0),
  maplist([D,V]>>(D=(d(V)/dt=_Deriv)), Input, List_of_variables),
  findall(
    Trans,
    initial_transformation(Trans),
    In_Transf
  ),
  polynomialize_ode(Input, [], In_Transf, List_of_variables, Transf_raw, Output_raw),
  % Hack to change the name of Time
  new_variable(List_of_variables, TimeName),
  substitute_all(['Time'], [TimeName], Transf_raw, Transf),
  substitute_all(['Time'], [TimeName], Output_raw, Output),
  retractall(initial_transformation(_)).


%! detect_new_variable(+Expr, +Current_variables, -List_of_expression)
%
% detect the expression for variables that needs to be introduced the clause of this
% predicate may be splitted in two, those describing the polynomial structure that do not
% need new variables and the other.

detect_new_variable(E1+E2, LoV, L12) :- !,
  detect_new_variable(E1, LoV, L1),
  detect_new_variable(E2, LoV, L2),
  append(L1, L2, L12).

detect_new_variable(E1-E2, LoV, L12) :- !,
  detect_new_variable(E1, LoV, L1),
  detect_new_variable(E2, LoV, L2),
  append(L1, L2, L12).

detect_new_variable(-Expr, LoV, List) :- !,
  detect_new_variable(Expr, LoV, List).

detect_new_variable(E1*E2, LoV, L12) :- !,
  detect_new_variable(E1, LoV, L1),
  detect_new_variable(E2, LoV, L2),
  append(L1, L2, L12).

detect_new_variable(Expr^N, LoV, L) :-
  integer(N),
  N > 0,!,
  detect_new_variable(Expr, LoV, L).

detect_new_variable(Expr, _LoV, []) :-
  is_numeric(Expr, _Val),
  !.
% end of the polynomial structure

detect_new_variable(E1/E2, LoV, L12) :- !,
  detect_new_variable(E1, LoV, L1),
  (
    is_numeric(E2)
  ->
    L12 = L1
  ;
    (
      E2 = Base^N,
      integer(N)
    ->
      L12 = [1/Base|L1]
    ;
      L12 = [1/E2|L1]
    )
  ).

detect_new_variable(Expr, LoV, []) :-
  member(Expr, LoV),
  !.

detect_new_variable(Expr, LoV, []) :-
  \+((
    member(Var, ['Time'|LoV]),
    contains_term(Var, Expr)
  )),
  !,
  warning_parameter(Expr).

detect_new_variable(sqrt(Expr), _LoV, [Expr^ 0.5]) :- !.
detect_new_variable(Expr, _LoV, [Expr]).


%! complete_derivation(+Expr, +List_Deriv, -Deriv)
%
% Derivate Expr with respect to time and all variable in List_Deriv

complete_derivation(Expr, [], Timeder) :- !,
  derivate(Expr, 'Time', Timeder).

complete_derivation(Expr, [d(Head)/dt = Der_ofH | Tail], Deriv + Der_wrtH*Der_ofH) :-
  derivate(Expr, Head, Der_wrtH), % derivate with respect to Head
  complete_derivation(Expr, Tail, Deriv).


%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Internal functions %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%

%! add_derivation_to_ode(+Formula, +Inputs_list, -[Output])
%
% Used to determine the ODE associated with a set of function, the list of species to be
% considered as input should be specified in arguments
% Formula is of the form: Y1 = f1(X), Y2 = f2(X) 

add_derivation_to_ode(Output = Expression, Input, [Output]) :-
  (
    Input = time
  ->
    substitute_all([Input], ['Time'], Expression, Expr),
    assertz(initial_transformation(Output->Expr)),
    derivate(Expr, 'Time', Der)
  ;
    substitute_all([Input], ['t'], Expression, Expr),
    assertz(initial_transformation(Output->Expr)),
    derivate(Expr, 't', Der)
  ),
  add_ode([d(Output)/dt = Der]),
  % handle the initial conditions
  substitute_all([Input], [0], Expression, Eval_raw),
  is_numeric(Eval_raw, Eval),
  initial_state([Output = Eval]).

add_derivation_to_ode((Formula1,Formula2), Input, Output12) :-
  add_derivation_to_ode(Formula1, Input, Output1),
  add_derivation_to_ode(Formula2, Input, Output2),
  append(Output1, Output2, Output12).


%! polynomialize_ode(+Input, + Accumulator, +In_Transf, +List_of_variables, -List_Transf, -Output) 
%
% Main predicate for the polynomialization, the set of ODE is splited between Input (the
% ODE to polynomialize) and the Accumulator (ODE already polynomial)

polynomialize_ode([], Output, Transf, _LoV, Transf, Output).

% IT/TT/OT => Input, Tempo, Output Transformation
polynomialize_ode([d(Var)/dt = DerivTempo | Tail], Acc, IT, LoV, OT, Output) :-
  % First we scan the current derivative and detect new variable
  substitute_template(IT, Names, Heads),
  substitute_all(Heads, Names, DerivTempo, Deriv),
  detect_new_variable(Deriv, LoV, List_raw),
  sort(List_raw, List), % remove duplicate
  % Then we add necessary variables
  (
    List = []
  ->
    polynomialize_ode(Tail, [d(Var)/dt = Deriv | Acc], IT, LoV, OT, Output)
  ;
    add_variable(List, Acc, [d(Var)/dt = Deriv|Tail], LoV, IT,  [d(Var)/dt = Deriv|NewTail], NewLoV, TT),
    substitute_template(TT, Names2, Heads2),
    substitute_all(Heads2, Names2, Deriv, NewDeriv),
    polynomialize_ode(NewTail, [d(Var)/dt = NewDeriv | Acc], TT, NewLoV, OT, Output)
  ).


%! add_variable(+List, +Acc, +Reminder, +LoV, +Subs
%                       -NewReminder, -NewLoV, -NewSubs).
%
% Add list_of_expression as variables to the current set of ODE, this is described by Acc
% for the already polynomialize derivatives and Reminder for the one that needs to be
% modified, LoV is the list of variable et Subs the substitutions.

add_variable([], _Acc, Rem, LoV, Subs, Rem, LoV, Subs).

add_variable([Expr|Tail], Acc, Rem, LoV, Subs, NewRem, [Name|LoV], NewSubs) :-
  add_variable(Tail, Acc, Rem, LoV, Subs, RemTempo, LoVTempo, SubsTempo),
  (
    Expr = 'Time' -> Name = 'Time'
  ;
    new_variable(LoVTempo, Name)
  ),
  % Take care of the substitute work
  NewSubs = [Name->Expr| SubsTempo],
  substitute_template(NewSubs, Names, Heads),
  append(Acc, RemTempo, List_Deriv),
  complete_derivation(Expr, List_Deriv, Deriv_raw1),
  substitute_all(Heads, Names, Deriv_raw1, Deriv_raw2),
  distribute(Deriv_raw2, Deriv_raw3),
  simplify(Deriv_raw3, DerivExpr),
  append(RemTempo, [d(Name)/dt = DerivExpr], NewRem).

%! substitute_template(+Transformations, -Expressions, -Variables)
%
% Extract the substitute templates from the list of transformations

substitute_template([],[],[]).

substitute_template([Name -> Expr | TTail], NameList, ExprList) :-
  (
    Expr = 1/Den
  ->
    NameList = [A*Name, B*Name^N|NTail],
    ExprList = [A/Den, B/Den^N|ETail]
  ;
    Expr = R^0.5
  ->
    NameList = [Name/R,  Name, Name,    Name^N |NTail],
    ExprList = [R^ -0.5, Expr, sqrt(R), R^(N/2)|ETail]
  ;
    Expr = SubPow^Pow
  ->
    Powm is Pow-1,
    NameList = [Name/SubPow, Name|NTail],
    ExprList = [SubPow^Powm, Expr|ETail]
  ;
    NameList = [Name|NTail],
    ExprList = [Expr|ETail]
  ),
  substitute_template(TTail, NTail, ETail).


%! new_variable(+LoV, -Name)
%
% Generate the name of a new (and not in LoV) variable of the form A,B,C,..,AA,BB, etc.

new_variable(LoV, Name) :-
  count(variable_id, N),
  divmod(N,26,R,C),
  Code is C+65,
  Rep is R+1,
  char_code(Letter, Code),
  util:stutter(Rep, Letter, Try),
  ( % test if the variable already exist
    member(Try, LoV)
  ->
    new_variable(LoV, Name)
  ;
    Name = Try
  ).


%! warning_parameter(Name)
%
% Raise a unique warning for undefined parameter

warning_parameter(Name) :-
  catch(
    nb_getval(troubler, List),
    error(existence_error(variable,troubler),_Context),
    List = []
  ),
  (
    member(Name, List)
  ->
    true
  ;
    append([Name], List, NewList),
    nb_setval(troubler, NewList)
				% print_message(warning, undefined_parameter(Name))
				% FF message supprime car actuellement incoherent avec le code de add_function qui genere d abord les equations puis l etat initial 
  ).

prolog:message(undefined_parameter(Name)) -->
        [ 'The identifier ~w is not defined, it is assumed to be a parameter with positive value (not a function)'-[Name] ].


