:- module(
  arithmetic_rules,
  [
    simplify/2,
    distribute/2,
    additive_normal_form/2,
    always_negative/1,
    always_positive/1,
    determine_sign/2,
    determine_boundary/3,
    normalize_number/2,
    is_null/1,
    zero_like/1,
    modulo/3,
    is_numeric/2,
    is_numeric/1
  ]).

% Insert here for separate compilation and linting
:- use_module(doc).
:- use_module(util).

:- set_prolog_flag(float_overflow, infinity).
:- set_prolog_flag(float_zero_div, infinity).
:- set_prolog_flag(float_undefined, nan).
:- set_prolog_flag(float_underflow, ignore).
:- dynamic(canonical/3).
:- dynamic(numbers_may_be_negative/0).


simplify(In, Out) :-
  (
    simplify_aux(In, Out0)
  ->
    Out = Out0
  ;
    throw(error(simplify_failure))
  ).


simplify_aux(In, 0) :-
  is_null(In),
  !.

simplify_aux(- - In, SIn) :-
  !,
  simplify_aux(In, SIn).

simplify_aux(- In, SIn) :-
  number(In),
  !,
  NIn is -In,
  simplify_aux(NIn, SIn).

simplify_aux(-In, -SIn) :-
  !,
  simplify_aux(In, SIn).

simplify_aux(In, Out) :-
  additive_block(In, Blocks),
  !,
  simplify_blocks(additive_block, additive_index, Blocks, ReducedSubBlocks),
  maplist(rebuild_additive_coef, ReducedSubBlocks, CoefSubBlocks),
  rebuild_additive_blocks(CoefSubBlocks, Out).

simplify_aux(In, Out) :-
  multiplicative_block(In, Blocks),
  !,
  simplify_blocks(multiplicative_block, multiplicative_index, Blocks, ReducedSubBlocks),
  map_blocks(rebuild_multiplicative_coef, ReducedSubBlocks, CoefSubBlocks),
  compute_product(CoefSubBlocks, CoefSubBlocksComputed),
  rebuild_multiplicative_blocks(CoefSubBlocksComputed, OutCoef),
  (
    extract_coefficient(OutCoef, Coef, SubOut),
    Coef < 1
  ->
    (
      Coef < 0
    ->
      CoefAbs is - Coef,
      Out = - OutAbs
    ;
      CoefAbs = Coef,
      Out = OutAbs
    ),
    (
      CoefAbs = 1
    ->
      OutAbs = SubOut
    ;
      insert_coef(SubOut, CoefAbs, OutAbs)
    )
  ;
    Out = OutCoef
  ).

simplify_aux(log(exp(Expr)), Out) :-
  !,
  simplify_aux(Expr, Out).

simplify_aux(exp(log(Expr)), Out) :-
  !,
  simplify_aux(Expr, Out).

simplify_aux(exp(N * log(Expr)), Out) :-
  number(N),
  !,
  simplify_aux(Expr ^ N, Out).

simplify_aux(log(A) + log(B), Out) :-
  !,
  simplify_aux(log(A * B), Out).

simplify_aux(exp(A) * exp(B), Out) :-
  !,
  simplify_aux(exp(A + B), Out).

simplify_aux(Expr ^ N, Out) :-
  (N = 1/2; N=0.5),
  !,
  simplify_aux(sqrt(Expr), Out).

simplify_aux(Expr ^ 1, Out) :-
  !,
  simplify_aux(Expr, Out).

simplify_aux(Expr ^ (- N), Out) :-
  !,
  simplify_aux(1 / Expr ^ N, Out).

simplify_aux(In, Out) :-
  term_morphism(simplify_aux, In, Middle),
  (
    rewrite_eval(Middle, Out)
  ->
    true
  ;
    Out = Middle
  ).


simplify_blocks(Block, Index, Blocks, ReducedSubBlocks) :-
  gather_loop(Block, Blocks, SubBlocks),
  maplist(Index, SubBlocks, IndexedSubBlocks),
  sort(4, @=<, IndexedSubBlocks, SortedSubBlocks),
  with_clean(
    [arithmetic_rules:canonical/3],
    (
      gather_indexed(SortedSubBlocks),
      reduce_blocks(IndexedSubBlocks, ReducedSubBlocks)
    )
  ).


gather_loop(Block, Blocks, SubBlocks) :-
  gather_blocks(Block, Blocks, Blocks0),
  map_blocks(simplify_aux, Blocks0, Blocks1),
  (
    Blocks0 = Blocks1
  ->
    SubBlocks = Blocks0
  ;
    gather_loop(Block, Blocks1, SubBlocks)
  ).


compute_product([], []).

compute_product([B | T], Out) :-
  block_number(B, N),
  !,
  compute_product_with_others(T, PT, Others),
  P is PT * N,
  (
    P = 1
  ->
    Out = Others
  ;
    normalize_number(P, PNorm),
    Out = [+ PNorm | Others]
  ).

compute_product([H | TIn], [H | TOut]) :-
  compute_product(TIn, TOut).


block_number(B, N) :-
  sign(B, Sign, C),
  (
    number(C),
    V = C
  ;
    C = M ^ E,
    number(M),
    V is M ^ E
  ),
  (
    Sign = 1
  ->
    N = V
  ;
    N is 1 / V
  ).


compute_product_with_others([], 1, []).

compute_product_with_others([B | T], P, Others) :-
  block_number(B, N),
  !,
  compute_product_with_others(T, PT, Others),
  P is PT * N.

compute_product_with_others([H | TIn], P, [H | TOut]) :-
  compute_product_with_others(TIn, P, TOut).


reduce_blocks([], []).

reduce_blocks([index(_, _, _, _, Canonical) | TailIn], Out) :-
  retract(canonical(Canonical, Sum, Others)),
  !,
  assert(canonical(Canonical, 0, Others)),
  reduce_block(Sum, Others, Out, TailOut),
  reduce_blocks(TailIn, TailOut).

reduce_blocks([index(Expr, Sign, Coef, _, _) | TailIn], Out) :-
  (
    Coef = 0
  ->
    reduce_blocks(TailIn, Out)
  ;
    reduce_block(Sign, Expr, Out, TailOut),
    reduce_blocks(TailIn, TailOut)
  ).


reduce_block(Coef, Expr, Out, TailOut) :-
  (
    Coef > 0
  ->
    Out = [+ (Coef : Expr) | TailOut]
  ;
    Coef < 0
  ->
    CoefOpp is -Coef,
    Out = [- (CoefOpp : Expr) | TailOut]
  ;
    Out = TailOut
  ).


gather_indexed([]).

gather_indexed([index(_, _, Coef1, Others, Canonical), index(_, _, Coef2, _, Canonical) | Tail]) :-
  !,
  gather_indexed_same(Canonical, Tail, TailSum, TailOthers),
  Sum is Coef1 + Coef2 + TailSum,
  assertz(canonical(Canonical, Sum, Others)),
  gather_indexed(TailOthers).

gather_indexed([_ | Tail]) :-
  gather_indexed(Tail).


gather_indexed_same(Canonical, [index(_, _, Coef, _, Canonical) | Tail], Sum, Others) :-
  !,
  gather_indexed_same(Canonical, Tail, TailSum, Others),
  Sum is Coef + TailSum.

gather_indexed_same(_Canonical, Others, 0, Others).


additive_index(H, index(Expr, Sign, Coef, Others, Canonical)) :-
  sign(H, Sign, Expr),
  (
    extract_coefficient(Expr, SubCoef, Others)
  ->
    true
  ;
    number(Expr)
  ->
    SubCoef = Expr,
    Others = 1
  ;
    SubCoef = 1,
    Others = Expr
  ),
  Coef is Sign * SubCoef,
  canonical_expression(Others, Canonical).


%! extract_coefficient(+Expression, -Numeric, -Variable)
%
% Separate an expression into its numerical constant and variables part

extract_coefficient(C, C, 1) :-
  number(C),
  !.

extract_coefficient(- CA, COpp, A) :-
  extract_coefficient(CA, C, A),
  !,
  COpp is - C.

extract_coefficient(+ CA, C, A) :-
  extract_coefficient(CA, C, A),
  !.

extract_coefficient(A * B, SubCoeff, Others) :-
  extract_coefficient(A, ACoeff, AOthers),
  extract_coefficient(B, BCoeff, BOthers),
  !,
  (
    AOthers = 1
  ->
    Others = BOthers
  ;
    BOthers = 1
  ->
    Others = AOthers
  ;
    Others = AOthers*BOthers
  ),
  SubCoeff is ACoeff*BCoeff.

extract_coefficient(A / B, SubCoeff, Others) :-
  extract_coefficient(A, ACoeff, AOthers),
  extract_coefficient(B, BCoeff, BOthers),
  !,
  (
    BOthers = 1
  ->
    Others = AOthers
  ;
    Others = AOthers/BOthers
  ),
  SubCoeff is ACoeff/BCoeff.

extract_coefficient(A, 1, A).


extract_powers(sqrt(Expr), Power, Sub) :-
  !,
  extract_powers(Expr, SubPower, Sub),
  Power is 0.5 * SubPower.

extract_powers(Expr ^ Pow, Power, Sub) :-
  number(Pow),
  !,
  extract_powers(Expr, SubPower, Sub),
  Power is Pow * SubPower.

extract_powers(Expr, 1, Expr).


multiplicative_index(H, index(Expr, Sign, Coef, Others, Canonical)) :-
  sign(H, Sign, Expr),
  (
    numbers_may_be_negative
  ->
    Others = Expr,
    SubCoef = 1
  ;
    extract_powers(Expr, SubCoef, Others)
  ),
  Coef is Sign * SubCoef,
  canonical_expression(Others, Canonical).


canonical_expression(In, Out) :-
  additive_block(In, Blocks),
  !,
  gather_blocks(additive_block, Blocks, SubBlocks),
  map_blocks(arithmetic_rules:canonical_expression, SubBlocks, CanonicalSubBlocks),
  sort(CanonicalSubBlocks, SortedSubBlocks),
  rebuild_additive_blocks(SortedSubBlocks, Out).

canonical_expression(In, Out) :-
  multiplicative_block(In, Blocks),
  !,
  gather_blocks(multiplicative_block, Blocks, SubBlocks),
  map_blocks(arithmetic_rules:canonical_expression, SubBlocks, CanonicalSubBlocks),
  sort(CanonicalSubBlocks, SortedSubBlocks),
  rebuild_multiplicative_blocks(SortedSubBlocks, Out).

canonical_expression(In, Out) :-
  term_morphism(canonical_expression, In, Out).


rebuild_additive_coef(SignedBlock, Value) :-
  sign(SignedBlock, Sign, Block),
  (
    Block = (Coef : Expr)
  ->
    SignedCoef is Sign * Coef,
    (
      SignedCoef = 1
    ->
      Value = + Expr
    ;
      Expr = 1
    ->
      Value = + SignedCoef
    ;
      (
        extract_coefficient(Expr, OtherCoef, SubExpr)
      ->
        FullCoef is SignedCoef * OtherCoef
      ;
        FullCoef = SignedCoef,
        SubExpr = Expr
      ),
      (
        FullCoef < 0
      ->
        NewSign = -1,
        NewCoef is - FullCoef
      ;
        NewSign = 1,
        NewCoef = FullCoef
      ),
      (
        NewCoef = 1
      ->
        UnsignedValue = SubExpr
      ;
        insert_coef(SubExpr, NewCoef, UnsignedValue)
      ),
      sign(Value, NewSign, UnsignedValue)
    )
  ;
    Value = + Block
  ).


insert_coef(A * B, Coef, Result * B) :-
  !,
  insert_coef(A, Coef, Result).

insert_coef(A, Coef, Result) :-
  (
    Coef > 0,
    Coef < 1,
    CoefInv is 1 / Coef,
    0.0 =:= float_fractional_part(CoefInv)
  ->
    normalize_number(CoefInv, CoefInvNorm),
    Result = A / CoefInvNorm
  ;
    normalize_number(Coef, CoefNorm),
    Result = CoefNorm * A
  ).


rebuild_multiplicative_coef(Block, Value) :-
  (
    Block = (Coef : Expr)
  ->
    (
      Coef = 1
    ->
      Value = Expr
    ;
      Expr = 1
    ->
      Value = 1
    ;
      Coef = 0.5
    ->
      Value = sqrt(Expr)
    ;
      Value = Expr ^ Coef
    )
  ;
    Value = Block
  ).


rebuild_additive_blocks([], 0).

rebuild_additive_blocks([+H | T], Out) :-
  !,
  rebuild_additive_blocks(H, T, Out).

rebuild_additive_blocks([-H | T], Out) :-
  select(+A, T, Others),
  !,
  rebuild_additive_blocks(A, [-H | Others], Out).

rebuild_additive_blocks([-H | T], Out) :-
  rebuild_additive_blocks(-H, T, Out).


rebuild_additive_blocks(A, [], A) :-
  !.

rebuild_additive_blocks(A, [+H | T], Out) :-
  !,
  rebuild_additive_blocks(A + H, T, Out).

rebuild_additive_blocks(A, [-H | T], Out) :-
  rebuild_additive_blocks(A - H, T, Out).


rebuild_multiplicative_blocks([], 1).

rebuild_multiplicative_blocks([+H | T], Out) :-
  !,
  rebuild_multiplicative_blocks(H, T, Out).

rebuild_multiplicative_blocks([-H | T], Out) :-
  select(+A, T, Others),
  !,
  rebuild_multiplicative_blocks(A, [-H | Others], Out).

rebuild_multiplicative_blocks([-H | T], Out) :-
  rebuild_multiplicative_blocks(1 / H, T, Out).


rebuild_multiplicative_blocks(A, [], A) :-
  !.

rebuild_multiplicative_blocks(A, [+H | T], Out) :-
  !,
  rebuild_multiplicative_blocks(A * H, T, Out).

rebuild_multiplicative_blocks(A, [-H | T], Out) :-
  rebuild_multiplicative_blocks(A / H, T, Out).


gather_blocks(P, Blocks, SubBlocks) :-
  map_blocks(maybe(P), Blocks, BlocksOfBlocks),
  flatten_blocks(BlocksOfBlocks, SubBlocks).


maybe(P, A, B) :-
  call(P, A, SubBlocks),
  !,
  gather_blocks(P, SubBlocks, B).

maybe(_P, A, [+A]).


map_blocks(P, Blocks, SubBlocks) :-
  maplist(map_block(P), Blocks, SubBlocks).


map_block(P, +A, +B) :-
  !,
  call(P, A, B).

map_block(P, -A, -B) :-
  call(P, A, B).


flatten_blocks([], []).

flatten_blocks([H | TailIn], Out) :-
  (
    H = +L
  ->
    true
  ;
    H = -LOpp
  ->
    maplist(opp, LOpp, L)
  ),
  append(L, TailOut, Out),
  flatten_blocks(TailIn, TailOut).


opp(+ A, - A).

opp(- A, + A).


sign(+ A, 1, A).

sign(- A, -1, A).



additive_block('_', []). % Notation of empty set in biocham

additive_block(+ A, [+A]).

additive_block(- A, [-A]).

additive_block(A + B, [+A, +B]).

additive_block(A - B, [+A, -B]).

multiplicative_block(+ A, [+A]).

multiplicative_block(- A, [+(-1), +A]).

multiplicative_block(A * B, [+A, +B]).

multiplicative_block(A / B, [+A, -B]).


rewrite_simplify(A + 0, A).

rewrite_simplify(0 + A, A).

rewrite_simplify(A - 0, A).

rewrite_simplify(0 - A, A).

rewrite_simplify(- - A, A).

rewrite_simplify(A + (- B), A - B).

rewrite_simplify(A - (- B), A + B).

rewrite_simplify(- (A + B), - A - B).

rewrite_simplify(- (A - B), - A + B).

rewrite_simplify(A + (B + C), A + B + C).

rewrite_simplify(A + (B - C), A + B - C).

rewrite_simplify(A - (B + C), A - B - C).

rewrite_simplify(A - (B - C), A - B + C).

rewrite_simplify(_A * 0, 0).

rewrite_simplify(0 * _A, 0).

rewrite_simplify(A * (B * C), A * B * C).

rewrite_simplify(0 / _A, 0).

rewrite_simplify(A / A, 1).


distribute(In, Out) :-
  rewrite(rewrite_distribute, In, Out).


rewrite_distribute((A + B) * C, A * C + B * C).

rewrite_distribute(A * (B + C), A * B + A * C).

rewrite_distribute((A - B) * C, A * C - B * C).

rewrite_distribute(A * (B - C), A * B - A * C).

rewrite_distribute((A + B) / C, A / C + B / C).

rewrite_distribute((A - B) / C, A / C - B / C).

rewrite_distribute(A - (B + C), A - B - C).

rewrite_distribute(- (A + B), - A - B).

rewrite_distribute(- (A - B), - A + B).

rewrite_distribute(A^1, A).

rewrite_distribute(A^N, A*A^Nm) :-
  integer(N),
  N > 1,
  Nm is N-1.


%! additive_normal_form(+Expr, -Normal_form)
%
% 

additive_normal_form(In, Out) :-
  rewrite(rewrite_eval, In, Tmp1),
  rewrite(rewrite_distribute, Tmp1, Tmp2),!,
  rewrite_additive_normal_form(Tmp2, Out).


rewrite_additive_normal_form(TailA+TailB, NewTailA+NewTailB) :- !,
  rewrite_additive_normal_form(TailA, NewTailA),
  rewrite_additive_normal_form(TailB, NewTailB).

rewrite_additive_normal_form(Tail-Head, NewTail+NewHead) :- !,
  rewrite_additive_normal_form(-1, Head, 1, Number, Numerator, Denominator),
  gather_element(Number, Numerator, Denominator, NewHead),
  !,
  rewrite_additive_normal_form(Tail, NewTail).

rewrite_additive_normal_form(-Head, NewHead) :- !,
  rewrite_additive_normal_form(-1, Head, 1, Number, Numerator, Denominator),
  gather_element(Number, Numerator, Denominator, NewHead),
  !.

rewrite_additive_normal_form(Head, NewHead) :- !,
  rewrite_additive_normal_form(1, Head, 1, Number, Numerator, Denominator),
  gather_element(Number, Numerator, Denominator, NewHead),
  !.

rewrite_additive_normal_form(N, -A, Den, NNN, NumA, DenA) :-
  !,
  rewrite_additive_normal_form(N, A, Den, NN, NumA, DenA),
  NNN is -NN.

rewrite_additive_normal_form(N, A*B, Den, NN, NumA, NDen) :-
  number(B),!,
  rewrite_additive_normal_form(N, A, Den, NA, NumA, NDen),
  NN is NA*B.

rewrite_additive_normal_form(N, A*B, Den, NN, NumB, NDen) :-
  number(A),!,
  rewrite_additive_normal_form(N, B, Den, NB, NumB, NDen),
  NN is NB*A.

rewrite_additive_normal_form(N, A*B, Den, NN, NumAB, DenAB) :-
  rewrite_additive_normal_form(N, A, Den, NA, NumA, DenA),
  rewrite_additive_normal_form(1, B, 1, NB, NumB, DenB),
  !,
  gather_mult(NumA, NumB, NumAB),
  gather_mult(DenA, DenB, DenAB),
  !,
  NN is NA*NB.

rewrite_additive_normal_form(N, A/B, Den, NN, NA, NDen) :-
  number(B), !,
  rewrite_additive_normal_form(N, A, Den, NN, NA, NDen),
  NN is N/B.

rewrite_additive_normal_form(N, A/B, Den, NN, NDen, Num) :-
  number(A), !,
  rewrite_additive_normal_form(N, B, Den, NN, Num, NDen),
  NN is N*A.

rewrite_additive_normal_form(N, A/B, Den, NN, NumAB, DenAB) :-
  rewrite_additive_normal_form(N, A, Den, NA, NumA, DenA),
  rewrite_additive_normal_form(1, B, 1, NB, NumB, DenB),
  !,
  gather_mult(NumA, DenB, NumAB),
  gather_mult(DenA, NumB, DenAB),
  !,
  NN is NA/NB.

rewrite_additive_normal_form(N, A, Den, N, A, Den) :- !.


%! gather_element(Number, Numerator, Denominator, Element)
%
% Rewrite the number*nominator/denominator according to the normal form standard

gather_element(Number, Numerator, Denominator, Element) :-
  simplify(Numerator/Denominator, SExpr),
  gather_mult(Number, SExpr, Element).

gather_mult(1,1,1).
gather_mult(A,1,A).
gather_mult(1,A,A).
gather_mult(A,B,A*B).


rewrite_eval(In, Out) :-
  arithmetic_operation(In),
  In =.. [_ | Args],
  maplist(number, Args),
  !,
  Out is In.


arithmetic_operation(_ + _).

arithmetic_operation(_ - _).

arithmetic_operation(_ * _).

arithmetic_operation(_ / _).

arithmetic_operation(- _).

arithmetic_operation(_ ^ _).


%! always_negative(+Expr)
%! always_positive(+Expr)
%
% Check if Expr is assure to be always negative/positive, fail if it can change sign
% or if it is impossible to decide

always_negative(Expr) :-
  determine_sign(Expr, zero),!;
  determine_sign(Expr, neg).

always_positive(Expr) :-
  determine_sign(Expr, zero),!;
  determine_sign(Expr, pos).


%always_positive(Expr) :-
%  determine_boundary(Expr, I, _U),
%  I >= 0.

%always_negative(Expr) :-
%  determine_boundary(Expr, _I, U),
%  U =< 0.


%! determine_boundary(+Expr, -Up, -Down)
%
% Try to evaluate the boundary of an expression

determine_boundary(X, N, N) :-
	is_numeric(X, N),
	!.

determine_boundary(X, 0, 1.0Inf) :-
	atom(X),
  ode:is_molecule(X),
	!.

determine_boundary(X, Low, 1.0Inf) :- % undefined parameter
	atom(X),
	!,
  parameters:warning_parameter(X),
  get_option(precision, Pre),
  Low is 10^(- Pre).

determine_boundary(A+B, IAB, UAB) :-
	determine_boundary(A, IA, UA),
	determine_boundary(B, IB, UB),
	IAB is IA+IB,
	UAB is UA+UB.

determine_boundary(A-B, IAB, UAB) :-
	determine_boundary(A, IA, UA),
	determine_boundary(B, IB, UB),
	IAB is IA-UB,
	UAB is UA-IB.

determine_boundary(-A, ImA, UmA) :-
	determine_boundary(A, IA, UA),
	ImA is -UA,
	UmA is -IA.

determine_boundary(A*B, IAB, UAB) :-
	determine_boundary(A, IA, UA),
	determine_boundary(B, IB, UB),
	((IA=0; IB=0) -> T1 = 0; T1 is IA*IB),
	((UA=0; IB=0) -> T2 = 0; T2 is UA*IB),
	((IA=0; UB=0) -> T3 = 0; T3 is IA*UB),
	((UA=0; UB=0) -> T4 = 0; T4 is UA*UB),
	min_list([T1, T2, T3, T4], IAB),
	max_list([T1, T2, T3, T4], UAB).

determine_boundary(A/B, IAB, UAB) :-
	determine_boundary(A, IA, UA),
	determine_boundary(B, IB, UB),
	((IB =:= inf; IB =:= -inf) -> T1 = 0; T1 is IA/IB),
	((UB =:= inf; UB =:= -inf) -> T2 = 0; T2 is IA/UB),
	((IB =:= inf; IB =:= -inf) -> T3 = 0; T3 is UA/IB),
	((UB =:= inf; UB =:= -inf) -> T4 = 0; T4 is UA/UB),
	min_list([T1, T2, T3, T4], IAB),
	max_list([T1, T2, T3, T4], UAB).

determine_boundary(A^N, IAB, UAB) :-
	determine_boundary(A, IA, UA),
  is_numeric(N, NN),
  (
    NN > 0
  ->
	  IAB is IA^NN,
	  UAB is UA^NN
  ;
	  IAB is UA^NN,
	  UAB is IA^NN
  ).

determine_boundary(exp(A), IAB, UAB) :-
	determine_boundary(A, IA, UA),
	IAB is exp(IA),
	UAB is exp(UA).

determine_boundary(sin(_X), -1, 1).

determine_boundary(cos(_X), -1, 1).


%! determine_sign(+Expr, -Sign)
%
% Try to infer the sign of Expr, Sign will be unified to: neg/pos/zero/dnk (do not know)

% TODO : Verify if it needs rewriting
%always_negative(A) :-
%  rewrite(rewrite_simplify, A, B),
%  B \= A,
%  !,
%  always_negative(B).

determine_sign(A, zero) :-
  is_null(A),
  !.

determine_sign(Expr, Sign) :-
  is_numeric(Expr, A),
  !,
  (
    A = 0
  ->
    Sign = zero
  ;
    A < 0
  ->
    Sign = neg
  ;
    Sign = pos
  ).

determine_sign(- A, NSign) :-
  determine_sign(A, Sign),
  !,
  (
    Sign = pos
  ->
    NSign = neg
  ;
    Sign = neg
  ->
    NSign = pos
  ;
    NSign = Sign
  ).

determine_sign(A + B, Sign) :-
  !,
  (
    determine_sign(A, SignAB),
    determine_sign(B, SignAB)
  ->
    Sign = SignAB
  ;
    Sign = dnk
  ).

determine_sign(A - B, Sign) :-
  !,
  determine_sign(A + (- B), Sign).

determine_sign(A * B, Sign) :-
  !,
  determine_sign(A, SignA),
  determine_sign(B, SignB),
  (
    (SignA = zero ; SignB = zero)
  ->
    Sign = zero
  ;
    (SignA = dnk ; SignB = dnk)
  ->
    Sign = dnk
  ;
    SignA = SignB
  ->
    Sign = pos
  ;
    Sign = neg
  ).

determine_sign(A / B, Sign) :-
  !,
  (
    is_null(B)
  ->
    throw(error(division_by_zero, arithmetic_rules_determine_sign))
  ;
    true
  ),
  determine_sign(A * B, Sign).

determine_sign(A, pos) :- % concentrations and parameters are assumed positive
  atom(A),
  !.

determine_sign(_A ^ B, pos) :-
  is_integer(B, Value),
  (
    modulo(Value, 2, 0)
  ->
    !,
    true
  ).

determine_sign(A ^ _B, Sign) :-
  determine_sign(A, Sign).

determine_sign(exp(_A), pos).



normalize_number(N, Norm) :-
  (
    F is float_fractional_part(N),
    (
      F = 0
    ;
      F = 0.0
    )
  ->
    Norm is truncate(N)
  ;
    Norm = N
  ).

%! is_null(+Expr)
%
% Check if an expression is uniformly null

is_null(Expr) :-
  once(is_null_sr(Expr)).
is_null_sr(0).
is_null_sr(0.0).
is_null_sr(-0.0).
is_null_sr(-A) :- is_null_sr(A).
is_null_sr(A*B) :- is_null_sr(A); is_null_sr(B).
is_null_sr(A/_B) :- is_null_sr(A).
is_null_sr(A+B) :- is_null_sr(A), is_null_sr(B).
is_null_sr(A-B) :- is_null_sr(A), is_null_sr(B).
is_null_sr(_A^(-_N)) :- !, false.
is_null_sr(A^_N) :- is_null_sr(A).


%! zero_like(+Expr)
%
% A more drastic version of is_null

zero_like(0).
zero_like(0.0).


%! modulo(+A, +B)
%
% compute C is mod(A,B) but fail instead of raising an error when A or B are not integer

modulo(A, B, C) :-
  catch(
    C is mod(A, B),
    error(type_error(integer,_N), _Context),
    fail
  ).

%! is_numeric(+Expr, -Value)
%! is_numeric(+Expr)
%
% determine if an expression is a simple numeric value (eventualy with parameters)

is_numeric(N) :- is_numeric(N, _V).

is_numeric(Expr, Value) :-
  Expr =.. [Functor|Arguments],
  (
    Arguments = []
  ->
    fail
  ;
    maplist(is_numeric, Arguments, Values),
    Evaluation =.. [Functor|Values],
    Value is Evaluation
  ).

is_numeric(math_pi, V) :- !, V is pi.
is_numeric(math_e, V) :- !, V is e.

is_numeric(N, N) :-
  number(N),!.

is_numeric(N, Value) :-
  item([kind:parameter, key:N, item:parameter(N = Value)]),
  !.

is_numeric(N, Value) :-
  % needed for parameters attached to ode system
  get_current_ode_system(Id),
  item([parent:Id, kind:parameter, item:par(N = Value)]),
  !.

