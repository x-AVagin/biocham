/*
* Module implementing the translation from a classical CRN description to a biocham file
* describing a hybrid simulation (low populations are stochastic and high pop. are
* continuous).
* The main purpose of his module is thus to write a .bc file describing the simulation.
*
* Coders: P. Remondeau, M. Hemery
*/
:- module(hybrid,
  [
    hybrid_static_simulation/2,
    hybrid_static_simulation/5,
    hybrid_dynamic_simulation/3,
    hybrid_dynamic_simulation/6
  ] 
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(biocham).
:- use_module(numerical_simulation).

:- doc('This section describes a hybrid numerical simulation
  method. Some reactions which occurs more often and which have 
  a lot of particles reactants will be seen  as continuous, 
  and other reactions which occurs lesser and with fewer reactants
  particles will be seen as stochastic. Species which appears in at 
  least one continous reaction and one stochastic reaction will be 
  called hybrid species, the total particle count of one species is 
  calculated and will be plot at the end of the simulation.
  The default simulation time is 200, and the  convertion rate between particle 
  and concentration is 1. The simulation method currently used is the
  Rosenbrock method.').

%! hybrid_static_simulation(+ODEFilename, +StochFilename)
%
% Fast front-end for the hybrid_static_simulation/5 predicate

hybrid_static_simulation(ODEFilename, StochFilename) :-
  biocham_command,
  doc('This is a hybrid simulation with a static partitioning of the reactions.
   The first input file contains the informations concerning the continous 
   reactions. The second input file contains the informations concerning the
   stochastic reactions. Please list out all the species initial value 
   (by initial_state(Variable = Value)) in input files.  '),
  hybrid_static_simulation(ODEFilename, StochFilename, 'out_static.bc', 1, 20).


%! hybrid_static_simulation(+ODEFilename, +StochFilename, +OutFilename, +Volume, +Time)
%
% implement the static separation between continuous and stochastic species
% blend the two files in OutFilename and read it to execute a call to the rosenbrock
% integrator.

hybrid_static_simulation(ODEFilename, StochFilename, OutFilename, Volume, Time) :-
  biocham_command,
  %============================%
  %== Preprocessing ODE file ==%
  %============================%
  % 1. Record the ODE species
  nb_setval(volume,Volume),
  load_biocham(ODEFilename),
  all_items([kind: reaction],Reactions1),
  findall(R1,
         (
           member(Reaction,Reactions1),
           reaction(Reaction, [kinetics: _,reactants: Reactants,inhibitors: _,   products: _]),
           member(Reactant,Reactants),
           Reactant = _*R1
         ),
         Reactants1),
  findall(P1,
         ( 
           member(Reaction,Reactions1),
           reaction(Reaction, [kinetics: _,reactants: _,inhibitors: _,   products: Products]),
           member(Product,Products),
           Product = _*P1
         ),
         Products1),  
  append(Reactants1, Products1, ODESpecies),
  sort(Reactants1,SortedReactants), % Used for later hybrid-reactants finding
  sort(ODESpecies,SortedODESpecies),
  clear_model,
  
  %===================================%
  %== Preprocessing Stochastic file ==%
  %===================================%
  % 1. Copy all the parameters.
  % 2. Change all "present" into "parameter", with the same initial value.
  %   Change the species name by appending "_stoch" to the original name   
  % 3. Add two parameters for stochastic simulation: 
  %   parameter (tau, 0).
  %   parameter (ran, 0).
  % 4. Build Macro for the species appearing in both paradigms.
  % 5. User-defined volume.
  % 6. Build hybrid-reactants list.     

  % process the parameters and presents
  load_biocham(StochFilename),
  open(OutFilename, write, Stream),

  format(Stream, "% Hybrid version of ~w and ~w ~n", [ODEFilename, StochFilename]),
  format(Stream, "parameter(tau = 0).~n", []),
  format(Stream, "seed(0).~nparameter(ran = 0).~n", []),
  all_items([kind: parameter],Parameters),
  maplist(format(Stream, "~w.~n"), Parameters),
  
  all_items([kind:initial_state],InitState),
  findall(Hybrid_original,
         (
           member(initial_state(Hybrid_original=_), InitState),
           member(Hybrid_original, SortedODESpecies)
         ),
         Hybrid_original_list),

  get_option(stochastic_conversion, Rate),
  findall(Hybrid_species,
        (
           member(initial_state(Present_pre = Init_value_pre), InitState),
         (
           member(Present_pre, SortedODESpecies)
         ->
           (
             species_to_stoch(Present_pre, Present),
             Init_value = 0,
             species_to_total(Present_pre, Hybrid_species),
             format(Stream,"function(~w = floor((~g*~g*~w) + ~w)).~n",
                           [Hybrid_species, Rate, Volume, Present_pre, Present])
           )
         ;
           (
             species_to_stoch(Present_pre, Present),
             Init_value = Init_value_pre,
             species_to_total(Present_pre, Hybrid_species),
             format(Stream,"function(~w = ~w).~n",
                           [Hybrid_species, Present])
           )
         ),
         format(Stream, "parameter(~w = ~g).~n",
                        [Present, Init_value])
         ),_Hybrid_list),

  clear_model,
  load_biocham(ODEFilename),
  format(Stream, "~n% ODE part~n~n", []),
  write_hybrid_ode(Stream,Hybrid_original_list),
  clear_model,
  format(Stream, "~n% Hybrid event part~n~n", []),

  load_biocham(StochFilename),
  all_items([kind: reaction],Reactions2),
  nb_setval(reactions,Reactions2),
 
  % Add constraints for hybrid species! (28/9/12)
  % For hybrid species, the determinisic part can make the particle number
  % lower than one discrete step needed for stochastic simulation.
  findall(Reactant, 
         (
           member(Reaction,Reactions2),
           reaction(Reaction, [kinetics: _,reactants: Reactants,inhibitors: _,   products: _]),
           (
             Reactants = []
           ->
             Reactant = none 
           ;
             member(Reactant,Reactants)
           )
         ),
         Reactants_by_rules),

  build_constraints_by_rules(Reactants_by_rules, SortedReactants, 1, [], Hybrid_reactants_list),  
  build_constraints_list(Hybrid_reactants_list, Constraints_list),

  findall(Reactant_pre,
         (
           member(Reaction,Reactions2),
           reaction(Reaction, [kinetics: _,reactants: Reactants,inhibitors: _,   products: _]),
           member(Reactant,Reactants),
           Reactant = _*Reactant_pre
         ),
         Reactant_list),
  findall(Sp_stoch,
         (
           member(Sp, Reactant_list),
           species_to_stoch(Sp, Sp_stoch)
         ),
         Reactant_stoch_list),
  
  findall(Product_pre,
         ( 
           member(Reaction,Reactions2),
           reaction(Reaction, [kinetics: _,reactants: _,inhibitors: _,   products: Products]),
           member(Product,Products),
           Product = _*Product_pre
         ),
         Product_list),
  findall(Sp_stoch,
         (
           member(Sp, Product_list),
           species_to_stoch(Sp, Sp_stoch)
         ),
         Product_stoch_list), 
  append(Reactant_list, Product_list, Species_list),
  sort(Species_list, Sorted_Species), % To eliminate duplication.
  append(Reactant_stoch_list, Product_stoch_list, StochSpecies),
  sort(StochSpecies, SortedStochSpecies), % To eliminate duplication.

  %===================================================================%
  %==== Build Data Lists for Reactions and print events in Stream ====%
  %===================================================================%
  prepare_change_list(Reactions2,ODESpecies,List),
  build_reaction_data(List, Sorted_Species, Reaction_data_list),
  print_event(Reaction_data_list, SortedStochSpecies, Hybrid_reactants_list, Constraints_list, Stream),
  
  format(Stream, "~n% Plot part~n~n", []),
  format(Stream,"option(show: {}). % You can choose here which species you want to plot : option(show: {a_total,b_total,c_total,d_total}) to plot species a,b,c and d.~n",[]),

  format(Stream, "option(minimum_step_size:1e-4).~n", []),
  format(Stream, "numerical_simulation(method:rsbk, time: ~g).~n", Time), 
  format(Stream, "plot.~noption(show: {}).~n", []), 
  close(Stream),

  %== Clean Biocham state and call the simulation ==%
  nb_delete(volume),
  nb_delete(reactions),
  retractall(reaction_kinetics(_,_)),
  load_biocham(OutFilename),
  format(" ~n If you want to choose which species are plot, please change the corresponding ~n line in the output file (by default out_static.bc in the biocham directory). ~n ~n",[]),
  !. % FIXME create a dubious choicepoint.

% :- doc('\\begin{example}T7 model using hybrid static simulation:').
% :- biocham_silent(clear_model).
% :- biocham(hybrid_static_simulation('library:examples/hybrid/T7_input_ode.bc','library:examples/hybrid/T7_input_stoch.bc')).
% :- doc('\\end{example}').

%===========================================================%
%================== END of Preprocessor ====================%
%===========================================================%

%! species_to_stoch(+Species, -Species_stoch)
%
% write the stochastic name of a species, include quote to sanitize some model names.

species_to_stoch(Species, Species_stoch) :-
  atomic_list_concat(['\'', Species, '_stoch\''], Species_stoch).


%! species_to_total(+Species, -Species_total)
%
% write the total name of a species, include quote to sanitize some model names.

species_to_total(Species, Species_total) :-
  atomic_list_concat(['\'', Species, '_total\''], Species_total).

%! build_constraints_by_rules(Reactants_by_rules, ODEReactants, Current_counter, Orig_list, Results_list) 
%
% The 'Current_counter' here is used for indexing reactions.

build_constraints_by_rules(Reactants_by_rules, ODEReactants, Current_counter, Orig_list, Results_list) :-
  length(Reactants_by_rules, Number_of_rules),
  nth1(Current_counter, Reactants_by_rules, This_reactants_list),
  findall(Constraints,
         (
           (
             \+(list(This_reactants_list))
           ->
             This_reactants_list = Least_number * Reactant_name,
             member(Reactant_name, ODEReactants)
           ;
             member(Reactants_pair, This_reactants_list),
             Reactants_pair = Least_number * Reactant_name,
             member(Reactant_name, ODEReactants)
           )
         ->
           (
             species_to_total(Reactant_name, Reactant_total),
             atom_concat(Reactant_total, ' >= ', Temp),
             Reactants_pair = Least_number * Reactant_name,
             atom_number(Least_number_atom,Least_number),
             atom_concat(Temp, Least_number_atom, Constraints)
           )
         ;
           Constraints = 0
         ),
         This_constraints_list),

  append(Orig_list, [This_constraints_list], Next_list),
  (
    Current_counter < Number_of_rules
  ->
    (
      Next_counter is Current_counter + 1,
      build_constraints_by_rules(Reactants_by_rules, ODEReactants, Next_counter, Next_list, Results_list)
    )
  ;
    Results_list = Next_list, !
  ).


%! build_constraints_list(+Hybrid_reactants_list, -Constraints_list) 
%
% Construct the lists of lower_level#r atom for later use 

build_constraints_list(Hybrid_list, Constraint_list) :-
  build_constraints_list(Hybrid_list, 1, Constraint_list).

build_constraints_list([], _Counter, []).

build_constraints_list([List| InTail], Counter, [Constraint| OutTail]) :-
  (
    List = [0]
  ->
    Constraint = '0'
  ;
    atom_number(Counter_atom, Counter),
    atom_concat('lower_level', Counter_atom, Constraint)
  ),
  Counterp is Counter + 1,
  build_constraints_list(InTail, Counterp, OutTail).
  

%! write_hybrid_ode(+Stream, +Hybrid_list)
%
% write the ode model in the output file

write_hybrid_ode(Stream, Hybrid_original_list) :-
  list_hybrid_ode(Stream, Hybrid_original_list),
  with_output_to(Stream, models:list_model_initial_state),
  with_output_to(Stream, models:list_model_parameters),
  with_output_to(Stream, models:list_model_events),
  with_output_to(Stream, models:list_model_macros(function)).

%! list_hybrid_ode(+Stream, +Hybrid_original_list)
%
%

list_hybrid_ode(Stream, Hybrid_original_list) :- % Only MA are converted, need to do others
  all_items([kind: reaction], List_Reactions),
  forall(
        member(Reaction, List_Reactions),
        (
          Reaction =.. [Op,_,SecondPart],
          convert_MA(Reaction, List_Reactions, Hybrid_original_list, NewKinetics),
          (
            Op = (=>)
          ->
            NewReaction =.. [for, NewKinetics, Reaction]
          ;
            NewReaction =.. [for, NewKinetics, SecondPart]
          ),
          format(Stream, "~w.~n", NewReaction)
        )
  ).


/*  Can try to get the kinetics function in BIOCHAM 4  directly and translate, but i didn't know how
   Also can direclty adapt the kinetics with the hybrid species list, but need to translate the kinetics first if needed */

%! convert_MA(+Reaction, +List_Reactions, +Hybrid_original_list, -NewKinetics)
%
%

convert_MA(Reaction, List_Reactions, Hybrid_original_list, NewKinetics) :- 
  reaction(Reaction, [kinetics: Kinetics, reactants: Reactants, inhibitors: _,  products: _]),
  Kinetics =.. ['MA',K],
  kinetics:eval_kinetics(Reactants, _, product(in('S'*'M', [reactants]), 'M'^'S'), Value),
  convert_into_hybrid(Value, Hybrid_original_list, std, NewValue),!,
  (
   nb_current(dynamic_parameters,_)
  ->
   nth1(ReactionNumber,List_Reactions,Reaction),
   atom_concat('k',ReactionNumber,Kdiff1),
   atom_concat(Kdiff1,'_diff',Kdiff),
   NewPartialKinetics = K * NewValue,
   assertz(reaction_kinetics(Reaction,NewPartialKinetics)),
   NewKinetics = Kdiff * NewPartialKinetics
  ;
   NewKinetics = K * NewValue
  ).

convert_MA2(Reaction, ODESpecies, NewKinetics, Reactants, Products) :-
  reaction(Reaction, [kinetics: Kinetics, reactants: Reactants, inhibitors: _, products: Products]),
  Kinetics =.. ['MA',K],
  kinetics:eval_kinetics(Reactants, _, product(in('S'*'M', [reactants]), 'M'^'S'), Value),
  convert_into_hybrid(Value, ODESpecies, stoch, NewValue),!,
  NewKinetics = K * NewValue,
  (
   nb_current(dynamic_parameters,_)
  ->
   true
  ;
   assertz(reaction_kinetics(Reaction,NewKinetics))
  ).



%! convert_into_hybrid(+Rate, +List_species, +Mode, -NewRate)
%
% Rewrite the reaction rate using the species_total function
% When Mode = stoch, Species outside Liste_species are convert to Species_stoch
% and are otherwise untouched

convert_into_hybrid(Value * 1, List_species, Mode, NewValue) :-
  convert_into_hybrid(Value, List_species, Mode, NewValue).

convert_into_hybrid(Value ^ 1, List_species, Mode, NewValue) :-
  convert_into_hybrid(Value, List_species, Mode, NewValue).

convert_into_hybrid(Value1 * Value2, List_species, Mode, NewValue1 * NewValue2) :-
  convert_into_hybrid(Value1, List_species, Mode, NewValue1),
  convert_into_hybrid(Value2, List_species, Mode, NewValue2).

convert_into_hybrid(Value1 ^ Value2, List_species, Mode, NewValue1 ^ NewValue2) :-
  convert_into_hybrid(Value1, List_species, Mode, NewValue1),
  convert_into_hybrid(Value2, List_species, Mode, NewValue2).

convert_into_hybrid(Number, _, _Mode, Number) :-
  number(Number),
  !.

convert_into_hybrid(Molecule, List_species, Mode, Out_Molecule) :-
  \+(member(Molecule, List_species)),!,
  (
    Mode = stoch
  ->
    species_to_stoch(Molecule, Out_Molecule)
  ;
    Out_Molecule = Molecule
  ).

convert_into_hybrid(Molecule, _List_species, _Mode, HybridMolecule) :-
  get_option(stochastic_conversion, Rate),
  nb_getval(volume,Volume),
  species_to_total(Molecule, Molecule_total),
  atomic_list_concat(['(',Molecule_total,'/(',Rate,'*',Volume,'))'], HybridMolecule),
  !.


%! prepare_change_list(+Reaction_list, +Species_list, -Change_list)
%
% Extract the reactant, product and kinetics of the reactions
% TODO: Only MA are converted, need to do others

prepare_change_list(Reaction_list, Species_list, Change_list) :- 
  findall([Reactants,Products,Kinetics],
         (
           member(Reaction, Reaction_list),
           convert_MA2(Reaction, Species_list, Kinetics, Reactants, Products)
         ),
         Change_list
  ).


% build_stoechiometry(+Reactant_list, +Product_list, +Species_list, -Stoechiometry)
%
% Construct the stoechiometry of a reaction
% stoech are typed as atom to explicitely display a '+/-' in the output file

build_stoechiometry(_Reactant_list, _Product_list, [], []) :- !.

build_stoechiometry(Reactant_list, Product_list, [Sp|TailSp], [Stoech|TailStoech]) :-
	findall(CR, member(CR*Sp,Reactant_list), CR_list),
	findall(CP, member(CP*Sp,Product_list), CP_list),
	sum_list(CR_list, CRT),
	sum_list(CP_list, CPT),
	Stoech_number is CPT-CRT,
  ( % Explicitely include the sign of the stoechiometric number
    Stoech_number < 0
  ->
    atom_number(Stoech, Stoech_number)
  ;
    atom_number(Stoech_wp, Stoech_number),
    atom_concat('+',Stoech_wp,Stoech)
  ),
	build_stoechiometry(Reactant_list, Product_list, TailSp, TailStoech).


% build_reaction_data(+List_of_reactions, +Species_list, -New_reaction_list)
%
% format the reaction data to the format: [Stoechiometry, Rate]

build_reaction_data([], _Species_list, []) :- !.

build_reaction_data([[RList, PList, Coef]|ReacTail], Species_list, [[Stoech, Coef]|DataTail]) :-
  build_stoechiometry(RList, PList, Species_list, Stoech),
	build_reaction_data(ReacTail, Species_list, DataTail).


% print_event()
%
% write the events in the output file

print_event(Reactions_data, Stoch_list, Hybrid_reactants_list, Constraints_list, Stream) :- 
  get_option(stochastic_conversion,Rate), 
  length(Reactions_data, Number_of_alpha),
  print_alpha_parameters(Number_of_alpha, Stream),
  print_constraints_parameters(Constraints_list, Stream),
  
  % 1. Build Starting String for the event.
  format(Stream, "add_event(Time > tau,~n",[]),
  print_static_alpha(Stream),
  format(Stream,
    "tau = (if alpha_sum <= 0 then inf else Time+((-1 / alpha_sum) * log(random_float))/~w),~n",
    [Rate]),
  format(Stream,"ran = random_float,~n",[]),
  print_hybrid_species_constraint(Hybrid_reactants_list, 1, Stream),

% 2. Build alpha list and alpha subsums.
  build_alphas(Number_of_alpha, Alpha_accumulation_list),

% 3. Print reactions in event
  % Build the print_helper_list.
  build_normal(Reactions_data, Print_helper_list, Alpha_accumulation_list, Constraints_list, Stoch_list, 1),
  % Use print_helper_list to really print out the event part.
  print_change_list(Print_helper_list, Stoch_list, Stream).


%! print_alpha_parameters(+N_alpha, +Stream)
%
% write parameter(alpha1=0,alpha2=0,etc.) in the Stream 

print_alpha_parameters(N_alpha, Stream) :-
  numlist(1,N_alpha,AlphaList),
  format(Stream, "parameter(", []),
  print_alpha_parameters_sr(AlphaList, Stream),
  format(Stream, "alpha_sum = 0).~n", []).

print_alpha_parameters_sr([N| Tail], Stream) :-
  format(Stream, "alpha~d = 0, ", [N]),
  print_alpha_parameters_sr(Tail, Stream).

print_alpha_parameters_sr([], _Stream).


%! print_constraints_parameters(+N_constraints, +Stream)
%
% write parameter(lower_level1=0,lower_level2=0,etc.) in the Stream 

print_constraints_parameters(List_constraints, Stream) :-
  length(List_constraints, N_constraints),
  numlist(1,N_constraints,Constr_List),
  format(Stream, "parameter(", []),
  print_constraints_parameters_sr(Constr_List, Stream),
  format(Stream, ").~n", []).

print_constraints_parameters_sr([N], Stream) :-
  format(Stream, "lower_level~d = 0", [N]),!.

print_constraints_parameters_sr([N|Tail], Stream) :-
  format(Stream, "lower_level~d = 0, ", [N]),
  print_constraints_parameters_sr(Tail, Stream).


%! print_static_alpha(+Stream)
%
% write the expression of the various alphas and their sum

print_static_alpha(Stream) :-
  nb_getval(reactions, Reactions),
  print_static_alpha(Reactions, 1, Stream).

print_static_alpha([Reaction|Tail_Reactions], Counter, Stream) :-
  reaction_kinetics(Reaction, NewKinetics),
  (
    nb_current(dynamic_parameters,_)
  ->
    atomic_list_concat(['k',Counter,'_diff'],Kdiff),
    PrintedKinetics = (1 - Kdiff) * NewKinetics
  ;
    PrintedKinetics = NewKinetics
  ),
  format(Stream, "  alpha~d = ~w,~n", [Counter, PrintedKinetics]),
  Counterp is Counter + 1,
  print_static_alpha(Tail_Reactions, Counterp, Stream).

print_static_alpha([], Counter, Stream) :-
  format(Stream, "  alpha_sum = ", []),
  N_alpha is Counter - 1,
  numlist(1, N_alpha, NumListReac),
  print_alpha_sr(NumListReac, Stream),
  format(Stream, ",~n", []).

print_alpha_sr([N], Stream) :-
  format(Stream, "alpha~d", [N]),!.

print_alpha_sr([N|Tail], Stream) :-
  format(Stream, "alpha~d + ", [N]),
  print_alpha_sr(Tail, Stream).


%========================================%  
%== Build Alpha lists for current event ==%
%========================================%  

%! build_alphas(+N_alpha, -Alpha_accumulation_list)

build_alphas(N_alpha, Alpha_accumulation_list) :-
  numlist(1, N_alpha, NL),
  build_alpha_list(NL, Alpha_list),
  alpha_accumulation(Alpha_list, '', Alpha_accumulation_list).


%! build_alpha_list(+Num_list, -Alpha_list)

build_alpha_list([], []).

build_alpha_list([N|NTail], [Alpha| AlphaTail]) :-
  atom_concat('alpha', N, Alpha),
  build_alpha_list(NTail, AlphaTail).


%! alpha_accumulation(+Alpha_list, +'', -Alpha_accumulation_list)
%
% Accumulate the alphas, the mid argument should be '' to initialize the accumulator

alpha_accumulation([A|Tail_A], '', [A|Tail_Acc]) :- !,
  alpha_accumulation(Tail_A, A, Tail_Acc).

alpha_accumulation([A|Tail_A], Acc, [Accn|Tail_Acc]) :-
  atomic_list_concat([Acc,'+',A], Accn),
  alpha_accumulation(Tail_A, Accn, Tail_Acc).

alpha_accumulation([], _Acc, []).


%=============================%
%== Get Data from Reactions ==%
%=============================%

%! build_normal(Reactions_data, Out_list, Alpha_accumulation_list, Constraints_list, Stoch_list)
%
% Construct the evenment of stochastic reactions

build_normal([[Sp_change|_]|DataTail], Out_list, [Alp1|AlpTail], [Cons|ConsTail], Sp_list, 1) :- 
  build_change(0, Alp1, Cons, Sp_list, Sp_change, Modif_list),
  build_normal(DataTail, Tempo_list, [Alp1|AlpTail], ConsTail, Sp_list, 2),
  append_elem_list(Modif_list, Tempo_list, Out_list).

build_normal([[Sp_change|_]|DataTail], Out_list, [A1,A2|ATail], [Cons|CTail], Sp_list, 2) :-
  build_change(A1, A2, Cons, Sp_list, Sp_change, Modif_list),
  build_normal(DataTail, Tempo_list, [A2|ATail], CTail, Sp_list, 2),
  append_elem_list(Modif_list, Tempo_list, Out_list).

build_normal([], Out_list, _A, _C_list, Sp_list, _N) :-
  length(Sp_list, N_species),
  constant_list(N_species, [], Out_list).


%! build_change(Alpha1, Alpha2, Constraint, Species_list, Change_list, Modif_list)

build_change(_A1, _A2, _Constraint, [], [], []).

build_change(A1, A2, Constraint, [_Species|Sp_tail], ['+0'|Ch_tail], [[]|Out_tail]) :-
  !,
  build_change(A1, A2, Constraint, Sp_tail, Ch_tail, Out_tail).

build_change(A1, A2, Constraint, [Species|Sp_tail], [Change|Ch_tail], [Head|Out_tail]) :-
  atom_concat(Species,Change,NumChange),
  Head = [[A1, A2], NumChange, Constraint],
  build_change(A1, A2, Constraint, Sp_tail, Ch_tail, Out_tail).


%! append_elem_list(+L1, +L2, -L12)
%
% append all the element of the 2 lists

append_elem_list([],[],[]).

append_elem_list([H1|T1],[H2|T2],[H12|T12]) :-
  append(H1,H2,H12),
  append_elem_list(T1,T2,T12).

%======================%
%== Real Output Part ==%
%======================%

%! print_change_list(Modif_list, species_list, Stream)
%
% write the event of a stochastic reaction in the output_file.

print_change_list([], [], Stream) :-
  format(Stream, ").~n", []), !.

print_change_list([Change_list | Helper_tail], [Species | Sp_tail], Stream) :-
  (
    Change_list = [[Lower, Upper], First_change, Constraint | Change_Tail]
  ->
    format(Stream, " ~w = (if alpha_sum*ran>~w and alpha_sum*ran<=~w", [Species, Lower, Upper]),
    (Constraint == '0' -> true ; format(Stream, " and ~w >0", [Constraint])),
    format(Stream, " then ~w", [First_change]),
    print_if_then_else(Change_Tail, Stream),
    format(Stream, "~n else ~w)", [Species]),
    (Sp_tail = [] -> true ; write(Stream,',')),
    nl(Stream)
  ;
    write(Stream, Species)
  ),
  print_change_list(Helper_tail, Sp_tail, Stream).


%! print_if_then_else(+Change_list, +Stream)

print_if_then_else([], _Stream) :- !.

print_if_then_else([[Lower, Upper], Change, Constraint | Change_Tail], Stream) :-
  format(Stream, "~n else if alpha_sum*ran>~w and alpha_sum*ran<=~w", [Lower, Upper]),
  (Constraint == '0' -> true ; format(Stream, " and ~w >0", [Constraint])),
  format(Stream, " then ~w", [Change]),
  print_if_then_else(Change_Tail, Stream).


%! print_single_reaction_constraints(Constraint_list, Stream)

print_single_reaction_constraints([To_print], Stream) :-
  format(Stream, "~w", [To_print]), !.

print_single_reaction_constraints([To_print | Tail], Stream) :-
  format(Stream, "~w and ", [To_print]),
  print_single_reaction_constraints(Tail, Stream).


%! print_hybrid_species_constraint(Hybrid_reactants_list, Stream)

print_hybrid_species_constraint(Constraints_list, Stream) :-
  print_hybrid_species_constraint(Constraints_list, 1, Stream).

print_hybrid_species_constraint([], _Counter, _Stream).

print_hybrid_species_constraint([Constraints|Tail], Counter, Stream) :-
  (
    Constraints \== [0]
  ->
    format(Stream, " lower_level~w = (if " , [Counter]),
    print_single_reaction_constraints(Constraints, Stream),
    format(Stream, " then 1 else 0),~n", [])
  ;
    !
  ),
  Counterp is Counter + 1,
  print_hybrid_species_constraint(Tail, Counterp, Stream).


concat_species([], '').

concat_species([Sp|Sp_tail], Result) :-
  species_to_total(Sp, Sp_tot),
  foldl(concat_species_sr, Sp_tail, Sp_tot, Result).

concat_species_sr(Sp, Acc, Accn) :-
  species_to_total(Sp, Sp_tot),
  atomic_list_concat([Acc, ', ', Sp_tot], Accn).


print_show_option(Hybrid_original_list, Stream) :-
  concat_species(Hybrid_original_list, Sp_list),
  format(Stream, 'option(show: {~w}). % Choose here the species to plot~n', Sp_list).


/************************/
/* DYNAMIC PARTITIONING */
/************************/

%! hybrid_dynamic_simulation(+InputFile, +PropTresh, +PartTresh)
%
% Fast front-end for the hybrid_dynamic_simulation/5 predicate 
% Typical value are PropTresh = 20, PartTresh = 20 

hybrid_dynamic_simulation(InputFile,PropTresh,PartTresh) :-
  biocham_command,
  doc('This is a hybrid simulation with a dynamic partitioning of the reactions.
   The input file, a normal .bc file, contains all the infomation needed. The first
   coefficient will be used in the propencity treshold. The second coefficient
   is for the particle count treshold. If a reaction meets the two conditions
   based on these tresholds anytime during the simulation, the reaction is seen
   as continuous. Otherwise, it is viewed as stochastic. Therefore, if theses 
   numbers are high, the simulation is more precise but slower. On the contrary, 
   the simulation is quicker but less precise if the numbers are low. Please list 
   out all the species initial value (by initial_state(_,_)) in input files.'),
  hybrid_dynamic_simulation(InputFile,'out_dynamic.bc',1,20,PropTresh,PartTresh).


%! hybrid_dynamic_simulation(InputFile,OutFilename,Volume,Time,PropTresh,PartTresh) :-
%
% This predicate work by first writing all the information needed in a .bc file
% this bc_file is then loaded and a rosenbrock numerical simulation takes place

hybrid_dynamic_simulation(InputFile,OutFilename,Volume,Time,PropTresh,PartTresh) :-
  biocham_command,
  nb_setval(volume,Volume),
  load_biocham(InputFile),
  open(OutFilename,write,Stream),
  all_items([kind: reaction],Reactions),
  nb_setval(reactions,Reactions),
  findall(R1,
         (
           member(Reaction,Reactions),
           reaction(Reaction, [kinetics: _,reactants: Reactants,inhibitors: _,   products: _]),
           member(Reactant,Reactants),
           Reactant = _*R1
         ),
         Reactants1),
  findall(P1,
         ( 
           member(Reaction,Reactions),
           reaction(Reaction, [kinetics: _,reactants: _,inhibitors: _,   products: Products]),
           member(Product,Products),
           Product = _*P1
         ),
         Products1),  
  append(Reactants1, Products1, Species1),
  sort(Reactants1,SortedReactants), % Used for later hybrid-reactants finding and constraints lists 
  sort(Species1,SortedSpecies),
  maplist(species_to_stoch, SortedSpecies, SortedStochSpecies),

  format(Stream, "% Hybrid version of ~w~n", InputFile),
  format(Stream, "parameter(tau = 0).~n", []),
  Seed is 0,
  format(Stream, "seed(~d).~nparameter(ran = ~d).~n", [Seed, Seed]),
  format(Stream,"parameter(delta_t = 0).~n~n",[]),

  set_counter(dynamic_parameters,1), % Will count the number of ki_diff parameters
  length(Reactions,NumberOfReactions),
  numlist(1, NumberOfReactions, Numbered_reac),
  maplist(format(Stream, "parameter(k~g_diff = 0).~n"), Numbered_reac),
  format(Stream, "~n~n", []),

  all_items([kind:initial_state],InitState),
  get_option(stochastic_conversion, Rate),
  findall(Hybrid_species,(
         (
           member(initial_state(Present_pre = Init_value_pre),InitState)
         ;
           member(absent(Present_pre), InitState),
           Init_value_pre = 0
         ),
         (
           member(Present_pre, SortedSpecies)
          ->
           (
             species_to_stoch(Present_pre, Present),
             Init_value = 0,
             species_to_total(Present_pre, Hybrid_species),
             format(Stream, "function(~w = floor(~g*~g*~w + ~w)).~n",
                            [Hybrid_species, Rate, Volume, Present_pre, Present])
           )
          ;
           (
             species_to_stoch(Present_pre, Present),
             Init_value = Init_value_pre
           )
         ),
         format(Stream, "parameter(~w = ~g).~n",[Present, Init_value])
       ),_Hybrid_species),
  nl(Stream),
  write_hybrid_ode(Stream,SortedSpecies),

  findall(Reactant, 
           (
             member(Reaction,Reactions),
             reaction(Reaction, [kinetics: _,reactants: Reactants,inhibitors: _,   products: _]),
             (
               Reactants = []
             ->
               Reactant = none 
             ;
               member(Reactant,Reactants)
             )
           ),
          Reactants_by_rules),
  
  build_constraints_by_rules(Reactants_by_rules, SortedReactants, 1, [], Hybrid_reactants_list),  
  build_constraints_list(Hybrid_reactants_list, Constraints_list),

  prepare_change_list(Reactions, SortedSpecies, Reaction_list),
  build_reaction_data(Reaction_list, SortedSpecies, Reactions_data),
  build_hybrid_reactants(Reaction_list, HybridDynamicReactants),
  compute_maximum_change(Reaction_list, MaxParticleChange),
  ParticuleTreshold is MaxParticleChange*PartTresh,

  print_dynamic_event(SortedSpecies,Reactions_data, HybridDynamicReactants,ParticuleTreshold,PropTresh, SortedStochSpecies, Hybrid_reactants_list, Constraints_list, Stream),
  retractall(reaction_kinetics(_,_)),
  
  format(Stream, "~n% Plot part~n~n", []),
  print_show_option(SortedSpecies,Stream),
  format(Stream, "option(minimum_step_size:1e-4).~n", []),
  format(Stream, "numerical_simulation(method:rsbk, time: ~g).~n", [Time]),
  format(Stream, "plot.~noption(show: {}).~n",[]),
  close(Stream),
  
  nb_delete(volume),
  nb_delete(dynamic_parameters),
  nb_delete(reactions),

  % Load the new file and execute the simulation
  load_biocham(OutFilename),
  format(" ~n If you want to choose which species are plot, please change the corresponding ~n line in the output file (by default out_dynamic.bc in the biocham directory). ~n ~n",[]),
  !. % FIXME creates a dubious choice point

% :- doc('\\begin{example}T7 model using hybrid dynamic simulation:').
% :- biocham_silent(clear_model).
% :- biocham(hybrid_dynamic_simulation('library:examples/hybrid/Goutsias_dynamic.bc',20,10)).
% :- doc('\\end{example}').


%! compute_maximum_change(+Reaction_list, -MaxParticleChange) :-
%
% Determine the maximum change that a reaction may provoke in the number of particles

compute_maximum_change(Reaction_list, MaxParticleChange) :-
  extract_stoechiometry_list(Reaction_list, Reactant_list, Product_list),
  min_list(Reactant_list, RMin),
  max_list(Reactant_list, RMax),
  min_list(Product_list, PMin),
  max_list(Product_list, PMax),
  MaxParticleChange is max(abs(PMax-RMin),abs(RMax-PMin)).


%! extract_stoechiometry_list(+Reaction_list, -Reactant_list, -Product_list)
%
% Extract a list of the total stoechiometry of each reaction 
% to be used in compute_maximum_change/2

extract_stoechiometry_list([],[],[]) :- !.

extract_stoechiometry_list([[Reactants, Products, _A]|TReac], [SR|TReactant], [SP|TProduct]) :-
  findall(CoeffR, member(CoeffR*_RSpecies, Reactants), CoeffR_list),
  sum_list(CoeffR_list, SR),
  findall(CoeffP, member(CoeffP*_PSpecies, Products), CoeffP_list),
  sum_list(CoeffP_list, SP),
  extract_stoechiometry_list(TReac, TReactant, TProduct).

% build_hybrid_reactants(+Reaction_list, -HybridDynamicReactants)

build_hybrid_reactants([], []) :- !.

build_hybrid_reactants([[Reactants, _Products, Alpha]|ReacTail], [[ReacName, Alpha]|OutTail]) :-
  build_hybrid_subroutine(Reactants, ReacName),
  build_hybrid_reactants(ReacTail, OutTail).

build_hybrid_subroutine([], []) :- !.

build_hybrid_subroutine([_N*X|InputTail], [Name|OutputTail]) :-
  species_to_total(X, Name),
  build_hybrid_subroutine(InputTail, OutputTail).


%! print_dynamic_event()
%
%

print_dynamic_event(SortedSpecies, Reactions_data, HybridDynamicReactants,MaxParticleChange,PropTresh,Stoch_species_order, Hybrid_reactants_list, Constraints_list, Stream) :-  
  get_option(stochastic_conversion,Rate),
  length(Reactions_data, Number_of_alpha),
  print_alpha_parameters(Number_of_alpha, Stream),
  print_constraints_parameters(Constraints_list, Stream),
  format(Stream, 'add_event(Time > tau,~n', []),
  print_dynamic_event_condition(SortedSpecies,HybridDynamicReactants,PropTresh,MaxParticleChange,Stream),
  print_static_alpha(Stream),
  write(Stream,' delta_t = (if alpha_sum <= 0 then step_size else (-1 / alpha_sum) * log(random_float)),'),nl(Stream),
  format(Stream, " tau = Time+delta_t/~w,~n", [Rate]),
  write(Stream,' ran = random_float,'),nl(Stream),
  print_hybrid_species_constraint(Hybrid_reactants_list, 1, Stream),
  build_alphas(Number_of_alpha, Alpha_accumulation_list),
  build_normal(Reactions_data, Print_helper_list, Alpha_accumulation_list, Constraints_list, Stoch_species_order, 1),
  print_change_list(Print_helper_list, Stoch_species_order, Stream).


print_dynamic_event_condition(Sp_list, Reac_info_list, PropTresh, Max_change, Stream) :-
  nb_getval(reactions, Reac_list),
  print_dynamic_event_condition(Sp_list, Reac_info_list, Reac_list, 1, PropTresh, Max_change, Stream).

print_dynamic_event_condition(_Species, [], [], _Counter, _PropTresh, _Max_change, _Stream).
print_dynamic_event_condition(Species, [[Reactants|_]|Reac_info_tail], [Reac|Reac_tail], Counter,PropTresh,Max_change,Stream) :-
  format(Stream, " k~d_diff = (if ", [Counter]),
  convert_MA2(Reac, Species, NewKinetics, _, _),
  format(Stream, "~w >= 1/(step_size * ~w)", [NewKinetics, PropTresh]),
  print_dynamic_event_condition_ifThenElse(Reactants,Max_change,Stream),
  format(Stream,' then 1 else 0),~n', []),
  NextCounter is Counter + 1,
  print_dynamic_event_condition(Species,Reac_info_tail, Reac_tail, NextCounter,PropTresh,Max_change,Stream).


print_dynamic_event_condition_ifThenElse([],_,_).
print_dynamic_event_condition_ifThenElse([Reactant|Tail],Max_change,Stream) :-
  format(Stream, ' and ~w >= ~w', [Reactant, Max_change]),
  print_dynamic_event_condition_ifThenElse(Tail,Max_change,Stream).

