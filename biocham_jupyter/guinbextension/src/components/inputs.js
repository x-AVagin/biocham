/**
 * @module React_inputs
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Slider from '@material-ui/lab/Slider';
import Tooltip from '@material-ui/core/Tooltip';
import Autocomplete from 'react-autocomplete';
import { commTarget } from 'Constants/gui';
import { createComm,
         handleResult,
         addToNotebook } from 'Utils/comms';
import { format } from 'Utils/formats';


const styles = theme => ({
    root: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: '2px',
        paddingBottom: '2px'
    },
    input: {
        width: '150px',
        boxSizing: 'border-box',
        textAlign: 'center',
    },
    checkboxButton: {
        padding: '0px',
        width: '150px',
        justifyContent: 'center',
    },
    checkboxLabel: {
        display: 'flex',
        margin: '0px',
        width: '100%',
        justifyContent: 'space-between'
    }
});

/** Command title component. */
function Title(props) {
    if (props.doc) {
        return (
            <Tooltip title={props.doc}
                     placement="top-start">
              <div>
                {props.name}:
              </div></Tooltip>
        );
    }
    return (<div>{props.name}:</div>);
}

/** Generic input (textual). */
class BaseInput extends Component {

    /** Handle input change and sends it to Command component to update
     * its arguments.  
     */
    handleChange = (e) => {
        this.props.handleChange(this.props.name,
                                e.target.value,
                                this.props.type);
    }

    /** Executes command on hitting `Ret`. */
    onKeyUp = (e) => {
        if (e.which === 13) {
            this.props.handleEnter();
        }
    }
    
    render () {
        const { classes, doc, value, name, type, ctype } = this.props;
        
        return (
            <div className={classes.root}>
              <Title doc={doc}
                     name={name}/>
              <input className={classes.input}
                     type="text"
                     placeholder={ctype}
                     value={value || value === 0? value: ''}
                     onChange={this.handleChange}
                     onKeyUp={this.onKeyUp}/>
            </div>
        );
    }
}


/** Checkbox input. Used for `yes/no` biocham type. */
class CheckboxInput extends Component {

    /** Converts `true/false` to `yes/no` then sends it to Command component 
     * to update its arguments. 
     */
    handleChange = (e) => {
        const { name, type } = this.props;
        const checked = e.target.checked? 'yes': 'no';
        this.props.handleChange(name, checked, type);
    }

    render() {
        const { classes, name, value, doc } = this.props;
        
        return(
            <div className={classes.root}>
              <FormControlLabel
                className={classes.checkboxLabel}
                control={
                    <Checkbox className={classes.checkboxButton}
                              checked={value === 'yes'? true: false}
                              onChange={this.handleChange}
                              value="checkboxChecked"
                              color="primary"/>
                }
                label={
                    <Title doc={doc}
                           name={name}/>
                }
                labelPlacement="start"
              />
            </div>
        );
    }
}


/** File input. */
class FileInput extends Component {

    state = {
        file: ''
    }

    /** On update, executes handleFile then sends file name to Command component
     * to update its arguments.
     */
    componentDidUpdate(prevProps, prevState) {
        const { file } = this.state;
        if (prevState.file !== file && file != null) {
            this.handleFile(file);
            this.props.handleChange(this.props.name,
                                    file.name,
                                    this.props.type);
        } 
    }

    /** Handle file selection. */
    handleChange = (e) => {
        this.setState({file: e});
    }

    /**
     * Reads file content then sends it to kernel for it to be stored in a file 
     * locally with the same name (file paths are a problem with JS).
     */
    handleFile = (file) => {
        const reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = (evt) => {
            const comm = createComm(commTarget);
            comm.send({file: file.name, content: evt.target.result});
            comm.on_msg((msg) => {
                console.log(msg);
                if (!msg.content.data.saved) {
                    alert(`File didn't load`);
                }
            });
        };
        reader.onerror = () => {
            alert(`There was an error while loading the file : 
            ${reader.error}`);
        };
    }

    render() {
        const { classes, doc, name } = this.props;
        
        return (
            <div className={classes.root}>
              <Title doc={doc}
                     name={name}/>
              <input type="file"
                     dir="rtl"
                     id={this.props.id}
                     onChange={(e) => this.handleChange(e.target.files[0])}/>
            </div>
        );
    }
}


// --------------------------------------------------------------------------- //
const sliderStyles = (theme) => ({
    root: {
        display: 'flex',
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    dotInput: {
        textAlign: 'center',
        marginLeft: '4px',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        minWidth: '72px'
    },
    slider: {
        marginLeft: '6px',
        marginRight: '12px',
        flex: '1 1 148px'
    }
});

const mapStateToProps = state => {
    return {
        numericalSimulation: state.settings.numericalSimulation
    };
};

/**
 * A widget which renders a slider with a text input next to it.
 * The widget only stores its value at all times, all its' other 
 * properties are stored in the Table component (Table.js). The 
 * reason why it works like that is explained in `Table.js`.
 * When the slider value is at its max or above, max is updated 
 * to 10 times the current value. When setting the value to 0 
 * with the slider, the max is divided by 10.
 */
class SliderInput extends Component {
    
    state = {
        value: 0
    }

    /**
     * On mount, set state value from props.
     */
    componentDidMount() {
        const { value } = this.props;
        let val = value;
        if (Number.isNaN(value)) val = val.toString();
        this.setState({ value: val });
    }

    /**
     * Controlled changes in value 
     */
    componentDidUpdate(prevProps) {
        const { value } = this.props;
        let val = value;
        if (Number.isNaN(value)) val = val.toString();
        if (prevProps.value !== val) {
            this.setState({ value: val });
        }
    }
    
    /**
     * Handle text input value changes.
     */
    handleInputChange = (event) => {
        this.setState({value: event.target.value});
    };


    /**
     * Handle slider value changes. The value is rounded to
     * a computed precision passed through props to avoid 
     * floating point problems.
     */
    handleSlider = (event, value) => {
        const { precision } = this.props;
        const rounded = this.round(value, precision);
        this.setState({value: rounded });
    }

    /**
     * Cast value as a number, round it to a precision if needed.
     * @param {number} value - slider value.
     * @param {number} precision - slider precision.
     */
    round = (value, precision) => {
        return precision > 0 ?
            parseFloat(value).toFixed(precision) : parseInt(value);
    }

    /**
     * If event is either key pressed Ret or mouse up, add change to 
     * notebook.
     */
    updateChange = (e) => {
        const { crud, name, numericalSimulation } = this.props;
        const { value } = this.state;
        
        // TODO deuglify this cause it sucks
        if (e.keyCode === 13 || (e.type === 'mouseup' && !isNaN(value))) {
            if (crud.create && crud.commandSep && !isNaN(value)) {
                const create = crud.create.name;
                const replot = 'numerical_simulation. plot.';
                // avoid linebreak with template literals with \ at the end
                const code = `${create}(${name}${crud.commandSep}${value}). \
${numericalSimulation? replot: ''}`;
                addToNotebook(code);
            }
        }
    }

    render() {
        const { classes, min, max, step, precision  } = this.props;
        const { value } = this.state;
        
        return (
            <div className={classes.root}>
              <Slider className={classes.slider}
                      value={isNaN(value)? 0: value}
                      onChange={this.handleSlider}
                      onDragEnd={this.updateChange}
                      min={ min<max ? min : max }
                      max={max>min ? max : min}
                      step={step}
                      disabled={isNaN(value)}/>
              <input className={classes.dotInput}
                     value={value}
                     onChange={this.handleInputChange}
                     onKeyUp={this.updateChange}/>
            </div>
        );
    }
}


/**
 * Input with list of completions component. 
 */
class AutocompleteInput extends Component {
    // style defined here for this one because that's
    // how component's API works :)
    static defaultProps = {
        menuStyle: {
            fontSize: '110%',
            borderRadius: '3px',
            boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
            background: 'rgba(255, 255, 255, 0.9)',
            padding: '2px 0',
            position: 'fixed',
            overflow: 'auto',
            zIndex: 110,
            lineHeight: '1em',
            maxHeight: '10em'
        }
    }
    
    state = {
        completions: [],
        value: ''
    }

    /** When input is focused, show list of possible completions. */
    onFocus = () => {
        const { completionCommand } = this.props;
        const comm = createComm('from_gui');
        comm.send({code: `${completionCommand}.`, silent: true});
        comm.on_msg(msg => {
            const completions = format({str: msg.content.data.output,
                                        type: completionCommand,
                                        display: true});
            this.setState({ completions });
        });
    }

    /** On change update Command component arguments. */
    onChange = (e) => {
        const { name, type } = this.props;
        this.props.handleChange(name,
                                e.target.value,
                                type);
    }

    /** On select, update Command component arguments. */
    onSelect = (value) => {
        const { name, type } = this.props;
        this.props.handleChange(name,
                                value,
                                type);
    }
    
    render() {
        const { classes, doc, menuStyle, name } = this.props;
        const { completions, value } = this.state;
        
        return (
            <div className={classes.root}>
              <Title doc={doc}
                     name={name}/>
              <Autocomplete
                menuStyle={menuStyle}
                getItemValue={(item) => item}
                items={completions}
                renderItem={(item, isHighlighted) =>
                            <div style={{background: isHighlighted?
                                         'lightgray': 'white'}}>
                              {item}
                            </div>
                           }
                shouldItemRender={(item, value) =>
                                  item.toLowerCase()
                                  .indexOf(value.toLowerCase()) > -1}
                value={value}
                inputProps={{onFocus: this.onFocus}}
                onChange={this.onChange}
                onSelect={(val) => this.onSelect(val)}
              />
            </div>
        );
    }
}


BaseInput = withStyles(styles)(BaseInput);
FileInput = withStyles(styles)(FileInput);
CheckboxInput = withStyles(styles)(CheckboxInput);
AutocompleteInput = withStyles(styles)(AutocompleteInput);
SliderInput = connect(mapStateToProps)(
    withStyles(sliderStyles)(SliderInput)
);


export { BaseInput,
         FileInput,
         CheckboxInput,
         SliderInput,
         AutocompleteInput
       };

