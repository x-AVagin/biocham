import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { existingCompletions } from 'Utils/completer';
import { listCommands, getNames } from 'Utils/sections';
import {
    ADD_COMMAND_TO_WORKFLOW,
    REMOVE_COMMAND_FROM_WORKFLOW } from 'Constants/actionTypes';

const styles = theme => ({
    root: {
        display: 'flex',
        flexDirection: 'row',
        height: '70%',
        width: '70%',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        position: 'absolute',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    },
    margin: {
        margin: '24px'
    },
    editor: {
        display: 'flex',
        flexDirection: 'column'
    },
    workflow: {
        marginLeft: '24px',
        minWidth: '30%',
        maxWidth: '30%',
        overflowY: 'scroll'
    },
});


const mapStateToProps = state => {
    return {
        workflows: state.settings.workflows,
        workflowSelected: state.settings.workflowSelected,
    };
};

const mapDispatchToProps = dispatch => ({
    onAddCommand: (wfname, command) =>
        dispatch({ type: ADD_COMMAND_TO_WORKFLOW, wfname, command }),
    onDeleteCommand: (wfname, index) =>
        dispatch({ type: REMOVE_COMMAND_FROM_WORKFLOW, wfname, index })
});


/** Workflow editor container, it appears as a modal in the GUI. */
class WorkflowEditor extends Component {

    state = {
        value: '',
    }

    /** Handle input change (for searching biocham commands. */
    handleChange = (e) => {
        this.setState({ value: e.target.value });
    }

    /**
     * List of current workflow selected commands. 
     * Clicking on a command will remove it from the workflow.
     * @param {Object} wf - the selected workflow.
     */
    editWorkflow = (wf) => {
        const { classes } = this.props;
        return (
            <section className={classes.workflow}>
              <h4>{wf.name}</h4>
              <List>
                { wf.commands.map((cmd, index) => {
                    return (
                        <ListItem key={`item-${index}-${wf.name}`}
                                  onClick={() => this.deleteFromWorkflow(index)}
                                  dense
                                  button>
                          <ListItemText primary={cmd.name}/>
                        </ListItem>
                    );
                })
                }
              </List>
            </section>
        );
    }

    /**
     * Append a biocham command to the currently selected workflow.
     * @param {Object} cmd - biocham command object.
     */
    appendToWorkflow = (cmd) => {
        const { workflows, workflowSelected, onAddCommand } = this.props;
        const wf = workflows[workflowSelected].name;
        onAddCommand(wf, cmd);
    }

    /**
     * Remove a command from currently selected workflow.
     * @param {number} index - the command index.
     */
    deleteFromWorkflow = (cmdIndex) => {
        const { workflows, workflowSelected, onDeleteCommand } = this.props;
        const wf = workflows[workflowSelected].name;
        onDeleteCommand(wf, cmdIndex);
    }

    /**
     * Get a list of biocham commands based on the searched command name.
     * @param {string} name - user input (for searching biocham commands).
     */
    getCommands = (name) => {
        const { classes } = this.props;
        const commandNames = getNames(listCommands);
        const completions = existingCompletions(name, commandNames);
        const commands = listCommands.filter(cmd => completions.includes(cmd.name));
        
        return (
            <List style={{overflowY: 'scroll'}}>
              { commands.map((cmd, index) => {
                  // format display name as cmdName([argNames]).
                  const cmdArgs = cmd.arguments.length > 0?
                        `(${cmd.arguments.map(arg => arg.name).join(', ')})`: '';
                  const displayName = `${cmd.name}${cmdArgs}.`;
                  return (
                      <ListItem key={`item-${cmd.name}-${index}`}
                                onClick={() => this.appendToWorkflow(cmd)}
                                button
                                dense>
                        <ListItemText primary={displayName} secondary={cmd.doc}/>
                      </ListItem>
                  );
              })
              }
            </List>
        );
    }
    
    render() {
        const { classes, workflows, workflowSelected } = this.props;
        const { value } = this.state;
        const wf = workflows[workflowSelected];
        
        return (
            <div className={classes.root}>
              <section className={classes.editor}>
                <TextField className={classes.margin}
                           id="command-wf-search-id"
                           label="Command"
                           value={value}
                           onChange={this.handleChange}
                           InputProps={{
                               startAdornment: (
                                   <InputAdornment position="start">
                                     <SearchIcon />
                                   </InputAdornment>
                               ),
                           }}
                />
                { this.getCommands(value) }
              </section>
              { this.editWorkflow(wf) }
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(
    withStyles(styles)(WorkflowEditor)
);
