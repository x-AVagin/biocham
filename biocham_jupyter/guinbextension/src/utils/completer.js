/**
 * Defines completion methods used in the `Console` component.
 * @module completions
 */

import { listCommands } from './sections';


/**
 * Check the existing completions for a string.
 * @param {string} item - The item to complete.
 * @param {Array} completionArray - The completions array before filtering.
 * @return {Array} The possible completions.
 */
export function existingCompletions(item, completionArray) {
    return completionArray.filter(cmp => {
        if (cmp.trim().substring(0, item.length) == item) return true;
        return false;
    });
}

/**
 * Get the longest common starting substring for all possible completions.
 * @param {Array} arr1 - The completions array.
 * @return {string} the longest common starting string possible.
 */
export function longestCommonStartingSubstring(arr1){
    let arr= arr1.concat().sort(),
          a1= arr[0], a2= arr[arr.length-1], L= a1.length, i= 0;
    while(i< L && a1.charAt(i)=== a2.charAt(i)) i++;
    return a1.substring(0, i);
}


