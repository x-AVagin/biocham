import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Command from './Command';
import { dashingLowercase } from 'Utils/utils';

const styles = theme => ({
    root: {
        overflow: 'auto',
        margin: '20px',
    },
    title: {
        margin: '10px'
    },
    embedded: {
        marginLeft: '10px',
        marginRight: '10px'
    }
});


/**
 * Container for biocham sections.
 */
class Section extends Component {

    render() {
        const { classes, title, children } = this.props;
                
        return (
            <Paper className={classes.root}
                   component="section"
                   id={title && dashingLowercase(title)}>
              { title &&
                <Typography variant="h4"
                            className={classes.title}>
                  {title}
                </Typography>
              }
              { children }
            </Paper>
        );
    }
}

export default withStyles(styles)(Section);
