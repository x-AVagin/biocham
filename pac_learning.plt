:- use_module(library(plunit)).

:- begin_tests(pac_learning, [condition(flag(slow_test, true, true)),
                              setup((clear_model, reset_options))]).


test(
  'Lotka-Volterra pure boolean',
  [
    cleanup(clear_model),
    true(N == 4)
  ]
) :-
  with_output_to(
    atom(Output),
    command('pac_learning(library:examples/lotka_volterra/LVi.bc, 100, 10,
      boolean_simulation: yes)')
  ),
  findall(
    Pos,
    sub_atom(Output, Pos, _, _, 'samples ('),
    Positions
  ),
  length(Positions, N).


test(
  'Lotka-Volterra stochastic simulation with 0, >0 threshold',
  [
    cleanup(clear_model),
    true(N == 4)
  ]
) :-
  with_output_to(
    atom(Output),
    command('pac_learning(library:examples/lotka_volterra/LVi.bc, 100, 10,
      data_transform: threshold(1))')
  ),
  findall(
    Pos,
    sub_atom(Output, Pos, _, _, 'samples ('),
    Positions
  ),
  length(Positions, N).


test(
  'Circadian cycle stochastic simulation with increase/decrease',
  [
    cleanup(clear_model),
    true(Total == 1000)
  ]
) :-
  with_output_to(
    atom(_),
    command('pac_learning(library:examples/circadian_cycle/bernot_comet.bc, 10, 100)')
  ),
  nb_setval(count, 0),
  forall(
    (
      member(Sign, [-1, 1]),
      between(1, 3, Index),
      pac_learning:counter_name(Sign, Index, Counter)
    ),
    (
      peek_count(Counter, C),
      nb_getval(count, CC),
      CCC is CC + C,
      nb_setval(count, CCC)
    )
  ),
  nb_getval(count, Total).


:- end_tests(pac_learning).
