:- module(
  multistability_command,
  [
    check_multistability/0
    ]
  ).

:- doc('
The existence of oscillations and multiple non-degenerate steady states in the differential dynamics of a reaction or influence model
can be checked very efficiently by checking \\emph{necessary conditions} for those properties
in the multistability graph of the model (see \\command{multistability_graph/0}), namely the existence of respectively negative and positive circuits in that graph  \\cite{BFS18jtb}. Some more computationally expensive conditions are optional.
').

:- initial(option(test_permutations: no)).
:- initial(option(test_transpositions: no)).


:- devdoc('\\section{Commands}'). 


check_multistability :-
  biocham_command,
  doc('Checks the possibility of multistability in the continuous semantics of the current model. This commands checks a necessary condition for the existence of multiple non-degenerate (i.e. with no variable equal to 0) steady states.'),
  option(test_transpositions, yesno, _TestTranspo, 'Lazy test of all possible swappings between two species only'),
  option(test_permutations, yesno, _TestSwaps, 'Lazy test of  all possible permutations between species. This option may highly increase the computation time.'),
  check_multistability_graph.

:- doc('\\begin{example}').
:- biocham_silent(clear_model).
:- biocham(load("library:biomodels/BIOMD0000000040.xml")).
:- biocham(multistability_graph).
:- biocham(draw_graph(left_to_right: yes)).
:- biocham(check_multistability).
:- biocham(check_multistability(test_transpositions: yes)).
:- doc('\\end{example}').

