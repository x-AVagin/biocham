:- module(
  sepi_util,
  [
    % Commands
    % SAT variables for core problem
    m_ij/4,
    m_inf_ij/5,
    m_arcs/6,
    is_dummy/6,
    % Writes to CNF file
    implies/5,
    a_and_b_implies/6,
    inf_imp_not_equ/6,
    equ_impl_inf/6,
    inf_impl_inf/6,
    % Useful functions
    nieme/3,
    get_edge_ij/4,
    is_in_arcs/3,
    get_type/3,
    % SAT variables for merge restriction
    m_ij_y/11,
    p_abk/11,
    p_abk1_cdk2/14,
    % SAT variables for old merge restriction
    merge/7,
    tseitin/8
  ]
).


%% Utils
%% Small functions to compute SAT variables

%! m_ij(+I, +J, +N2, -X)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% X is the SAT variable for mu(i) = j with an image graph of N2 vertices
%
% @arg I    Interger representing i in the initial graph
% @arg J    Interger representing j in the image graph
% @arg N2   Number of vertices in the image graph
% @arg X    SAT variable representing mu(i) = j
m_ij(I, J, N2, X) :-
  X is ((N2 + 1) * I + J + 1) .


%! m_inf_ij(+I, +J, +N1, +N2, -X)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% X is the SAT variable for mu(i) < j
%
% @arg I    Interger representing i in the initial graph
% @arg J    Interger representing j in the image graph
% @arg N1   Number of vertices in the initial graph
% @arg N2   Number of vertices in the image graph
% @arg X    SAT variable representing mu(i) < j
m_inf_ij(I, J, N1, N2, X) :-
  m_ij(I, J, N2, X1),
  X is (N1 * (N2 + 1) + X1).


%! m_arcs(+I, +J, +N1, +N2, +Narcs2, -X)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% X is the SAT variable for mu((i, j)) = (i', j')
%
% @arg I      Interger representing i in the initial graph
% @arg J      Interger representing j in the image graph
% @arg N1     Number of vertices in the initial graph
% @arg N2     Number of vertices in the image graph
% @arg Narcs2 Number of arcs in the image graph
% @arg X      SAT variable representing mu((i, j)) = (i', j')
m_arcs(I, J, N1, N2, Narcs2, X) :-
  N1_1 is N1 - 1,
  m_inf_ij(N1_1, N2, N1, N2, X1),
  X is (Narcs2 * I + J + 1 + X1).


%! is_dummy(+I, +N1, +N2, +Narcs1, +Narcs2, -X)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% X is the SAT variable for mu(arc_i) = bottom
%
% @arg I      Interger representing the arc i in the initial graph
% @arg N1     Number of vertices in the initial graph
% @arg N2     Number of vertices in the image graph
% @arg Narcs1 Number of arcs in the initial graph
% @arg Narcs2 Number of arcs in the image graph
% @arg X      SAT variable representing mu(arc_i) = bottom
is_dummy(I, N1, N2, Narcs1, Narcs2, X) :-
  m_arcs(Narcs1, Narcs2, N1, N2, Narcs2, X1),
  X is X1 + I.


%! implies(+A, +B, +Stream, +Extremal_sepi, +Top)
%
% translate 'A=>B' to '-A or B'
%
% @arg A              First SAT variable
% @arg B              Second SAT variable
% @arg Stream         Stream for the CNF file
% @arg Extremal_sepi  no/minimal_deletion/maximal_deletion cf. sepi:extremal_sepi/1
% @arg Top            Clause's weight in the CNF file
implies(A, B, Stream, Extremal_sepi, Top) :-
  A1 is -A,
  (
    Extremal_sepi \= no
  ->
    format(Stream, '~d ~d ~d 0~n', [Top, A1, B])
  ;
    format(Stream, '~d ~d 0~n', [A1, B])
  ).


%! a_and_b_implies(+A, +B, +C, +Stream, +Extremal_sepi, +Top)
%
% translate 'A and B => C' to '-A or -B or C'
%
% @arg A              First SAT variable
% @arg B              Second SAT variable
% @arg C              Third SAT variable
% @arg Stream         Stream for the CNF file
% @arg Extremal_sepi  no/minimal_deletion/maximal_deletion cf. sepi:extremal_sepi/1
% @arg Top            Clause's weight in the CNF file
a_and_b_implies(A, B, C, Stream, Extremal_sepi, Top) :-
  A1 is -A,
  B1 is -B,
  (
    Extremal_sepi \= no
  ->
    format(Stream, '~d ~d ~d ~d 0~n', [Top, A1, B1, C])
  ;
    format(Stream, '~d ~d ~d 0~n', [A1, B1, C])
  ).


%! inf_imp_not_equ(+I, +J, +N1, +N2, +Stream, +Extremal_sepi)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% 'mu(i) < j' implies 'mu(i) != j'
%
% @arg I              Interger representing the vertex i in the initial graph
% @arg J              Interger representing the vertex j in the image graph
% @arg N1             Number of vertices in the initial graph
% @arg N2             Number of vertices in the image graph
% @arg Stream         Stream for the CNF file
% @arg Extremal_sepi  no/minimal_deletion/maximal_deletion cf. sepi:extremal_sepi/1
inf_imp_not_equ(I, J, N1, N2, Stream, Extremal_sepi) :-
  m_inf_ij(I, J, N1, N2, X1),
  m_ij(I, J, N2, X2),
  X3 is -X2,
  Top is N1 + 1,
  implies(X1, X3, Stream, Extremal_sepi, Top).


%! equ_impl_inf(+I, +J, +N1, +N2, +Stream, +Extremal_sepi)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% 'mu(i) = j' implies 'mu(i) < j + 1'
%
% @arg I              Interger representing the vertex i in the initial graph
% @arg J              Interger representing the vertex j in the image graph
% @arg N1             Number of vertices in the initial graph
% @arg N2             Number of vertices in the image graph
% @arg Stream         Stream for the CNF file
% @arg Extremal_sepi  no/minimal_deletion/maximal_deletion cf. sepi:extremal_sepi/1
equ_impl_inf(I, J, N1, N2, Stream, Extremal_sepi) :-
  J1 is J + 1,
  m_ij(I, J, N2, X1),
  m_inf_ij(I, J1, N1, N2, X2),
  Top is N1 + 1,
  implies(X1, X2, Stream, Extremal_sepi, Top).


%! inf_impl_inf(+I, +J, +N1, +N2, +Stream, +Extremal_sepi)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% 'mu(i) < j' implies 'mu(i) < j + 1'
%
% @arg I              Interger representing the vertex i in the initial graph
% @arg J              Interger representing the vertex j in the image graph
% @arg N1             Number of vertices in the initial graph
% @arg N2             Number of vertices in the image graph
% @arg Stream         Stream for the CNF file
% @arg Extremal_sepi  no/minimal_deletion/maximal_deletion cf. sepi:extremal_sepi/1
inf_impl_inf(I, J, N1, N2, Stream, Extremal_sepi) :-
  J1 is J + 1,
  m_inf_ij(I, J, N1, N2, X1),
  m_inf_ij(I, J1, N1, N2, X2),
  Top is N1 + 1,
  implies(X1, X2, Stream, Extremal_sepi, Top).


%! nieme(+I, +List, -Value)
%
% get the Ith element of List
%
% @arg I      Index
% @arg List   List
% @arg Value  Value
nieme(1,[X|_],X) :- !.

nieme(N,[_|R],X) :- 
  N1 is N - 1,
  nieme(N1,R,X).


%! get_edge_ij(+Arcs, +I, +J, -X)
%
% get the Ith element of a list of arcs
% X is one of the vertex of the arc
%
% @arg Arcs   List of arcs
% @arg I      Index of the arc to get
% @arg J      Which vertex to get
% @arg X      Integer representing the vertex
get_edge_ij(Arcs, I, J, X) :-
  nieme(I, Arcs, X1),
  X1 = (A, B),
  (
    J = 0
  ->
    X = A
  ;
    true
  ),
  (
    J = 1
  ->
    X = B
  ;
    true
  ).


%! is_in_arcs(+I, +J, +List)
%
% return true if the arc (I, J) is in the list
%
% @arg I      Interger representing vertex I
% @arg J      Interger representing vertex J
% @arg List   List of arcs
is_in_arcs(I, J, [(I, J)|_]).

is_in_arcs(I, J, [_|T]) :- 
  is_in_arcs(I, J, T).



%% SAT variable for new merge

%! get_type(+A, +ID, -Type)
%
% return 0 if A is a specie 1 if A is a reaction
%
% @arg A      Interger representing vertex A
% @arg ID     Id of the graph
% @arg Type   0/1
get_type(A, ID, Type):-
  species(NameA, A, ID),
  (
    atom_concat("reaction", _, NameA)
  ->
    Type is 1
  ;
    Type is 0
  ).


%! m_ij_y(+I, +A, +B, +N1, +N2, +Ns1, +Ns2, +Type, +Na1, +Na2, -X)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% X is the SAT variable for 'A and B have the same image I through mu'
%
% @arg I    image in graph 2
% @arg A    first vertex mapped to I
% @arg B    second vertex mapped to I
% @arg N1   number of vertices in graph 1
% @arg N2   number of vertices in graph 2
% @arg Ns1  number of species in graph 1
% @arg Ns2  number of species in graph 2
% @arg Type type of A and B (0 for specie or 1 for reaction)
% @arg Na1  number of arcs in graph 1
% @arg Na2  number of arcs in graph 2
% @arg X    SAT variable for m_ab_i
m_ij_y(I, A, B, N1, N2, Ns1, Ns2, 0, Na1, Na2, X_specie) :-
  Na1_1 is Na1 - 1,
  Nr2 is N2 - Ns2,
  is_dummy(Na1_1, N1, N2, Na1_1, Na2, Max_dummy),
  (
    I is N2
  ->
    I2 is (N2 - Nr2 + 1)
  ;
    I2 is I
  ),
  X_specie is (Max_dummy + 1 + A + B * Ns1 + I2 * Ns1 * Ns1),
  debug(merge, "Max_dummy ~w  A ~w B ~w I ~w Type ~w X_specie ~w ", [Max_dummy, A, B, I, 0, X_specie]).

m_ij_y(I, A, B, N1, N2, Ns1, Ns2, 1, Na1, Na2, X_reaction) :-
  Nr1 is N1 - Ns1,
  Ns1_1 is Ns1 - 1,
  m_ij_y(N2, Ns1_1, Ns1_1, N1, N2, Ns1, Ns2, 0, Na1, Na2, Max_specie),
  A_reaction is (A - Ns1),
  B_reaction is (B - Ns1),
  I_reaction is (I - Ns2),
  X_reaction is (Max_specie + 1 + A_reaction + B_reaction * Nr1 + I_reaction * Nr1 * Nr1),
  debug(merge, "Max_specie ~w A ~w B ~w I ~w Type ~w X_reaction ~w ", [Max_specie, A, B, I, 1, X_reaction]).


%! p_abk(+A, +B, +K, +N1, +N2, +Ns1, +Ns2, +Type, +Na1, +Na2, -X)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% X is the SAT variable for 'there exists a good path of length 
%   k between a and b'
%
% @arg A    first vertex
% @arg B    second vertex
% @arg K    length of the path
% @arg N1   number of vertices in graph 1
% @arg N2   number of vertices in graph 2
% @arg Ns1  number of species in graph 1
% @arg Ns2  number of species in graph 2
% @arg Type type of A and B (0 for specie or 1 for reaction)
% @arg Na1  number of arcs in graph 1
% @arg Na2  number of arcs in graph 2
% @arg X    SAT variable for p_abk
p_abk(A, B, K, N1, N2, Ns1, Ns2, 0, Narcs1, Narcs2, X_specie) :-
  N1_1 is N1 - 1,
  m_ij_y(N2, N1_1, N1_1, N1, N2, Ns1, Ns2, 1, Narcs1, Narcs2, Max_mijy),
  X_specie is (Max_mijy + 1 + A + B * Ns1 + K * Ns1 * Ns1),
  debug(merge, "Max_mijy ~w A ~w B ~w K ~w X_specie ~w", [Max_mijy, A, B, K, X_specie]).

p_abk(A, B, K, N1, N2, Ns1, Ns2, 1, Narcs1, Narcs2, X_reaction) :-
  Ns1_1 is Ns1 - 1,
  Ne1 is N1 - Ns1,
  K0 is min(Ns1, Ne1),
  p_abk(Ns1_1, Ns1_1, K0, N1, N2, Ns1, Ns2, 0, Narcs1, Narcs2, Max_specie),
  Nr1 is N1 - Ns1,
  A_reaction is (A - Ns1),
  B_reaction is (B - Ns1),
  X_reaction is (Max_specie + 1 + A_reaction + B_reaction * Nr1 + K * Nr1 * Nr1),
  debug(merge, "Max_specie ~w A ~w B ~w K ~w X_reaction ~w", [Max_specie, A, B, K, X_reaction]).


%! p_abk1_cdk2(+A, +B, +K1, +C, +D, +K2, +N1, +N2, +Ns1, +Ns2, +Type, +Na1, +Na2, -X)
%
% X is the SAT variable for 'p_abk1 = 1 & p_cdk2 = 1'
%
% @arg A    first vertex of the first path
% @arg B    second vertex of the first path
% @arg K1   length of the first path
% @arg C    first vertex of the second path
% @arg D    second vertex of the second path
% @arg K2   length of the second path
% @arg N1   number of vertices in graph 1
% @arg N2   number of vertices in graph 2
% @arg Ns1  number of species in graph 1
% @arg Ns2  number of species in graph 2
% @arg Type type of A and B (0 for specie or 1 for reaction)
% @arg Na1  number of arcs in graph 1
% @arg Na2  number of arcs in graph 2
% @arg X    SAT variable for p_abk1_cdk2
p_abk1_cdk2(A, B, K1, C, D, K2, N1, N2, Ns1, Ns2, 0, Narcs1, Narcs2, X_specie) :-
  N1_1 is N1 - 1,
  Ne1 is N1 - Ns1,
  K is min(Ns1, Ne1),
  p_abk(N1_1, N1_1, K, N1, N2, Ns1, Ns2, 1, Narcs1, Narcs2, Max_pabk),
  (
    A =< B
  ->
    A_specie is A,
    B_specie is B
  ;
    A_specie is B,
    B_specie is A
  ),
  (
    C =< D
  ->
    C1 is C,
    D1 is D
  ;
    C1 is D,
    D1 is C
  ),
  C_reaction is (C1 - Ns1),
  D_reaction is (D1 - Ns1),
  Nr1 is N1 - Ns1,
  X_specie is (Max_pabk + 1 + A_specie + B_specie * Ns1 + C_reaction * Ns1 * Ns1 + D_reaction * Ns1 * Ns1 * Nr1 + K1 * Ns1 * Ns1 * Nr1 * Nr1 + K2 * Ns1 * Ns1 * Nr1 * Nr1 * (K + 1)),
  debug(merge, "Max_pabk ~w A ~w B ~w K1 ~w C ~w D ~w K2 ~w X ~w", [Max_pabk, A_specie, B_specie, K1, C_reaction, D_reaction, K2, X_specie]).

p_abk1_cdk2(A, B, K1, C, D, K2, N1, N2, Ns1, Ns2, 1, Narcs1, Narcs2, X_reaction) :-
  Ne1 is N1 - Ns1,
  K is min(Ns1, Ne1),
  Nr1 is N1 - Ns1,
  Nr1_1 is N1 - 1,
  Ns1_1 is Ns1 - 1,
  p_abk1_cdk2(Ns1_1, Ns1_1, K, Nr1_1, Nr1_1, K, N1, N2, Ns1, Ns2, 0, Narcs1, Narcs2, Max_specie),
  (
    A =< B
  ->
    A1 is A,
    B1 is B
  ;
    A1 is B,
    B1 is A
  ),
  (
    C =< D
  ->
    C_specie is C,
    D_specie is D
  ;
    C_specie is D,
    D_specie is C
  ),
  A_reaction is (A1 - Ns1),
  B_reaction is (B1 - Ns1),
  X_reaction is (Max_specie + 1 + A_reaction + B_reaction * Nr1 + C_specie * Nr1 * Nr1 + D_specie * Nr1 * Nr1 * Ns1 + K1 * Nr1 * Nr1 * Ns1 * Ns1 + K2 * Nr1 * Nr1 * Ns1 * Ns1 * (K + 1)),
  debug(merge, "Max_specie ~w A ~w B ~w K1 ~w C ~w D ~w K2 ~w X ~w", [Max_specie, A_reaction, B_reaction, K1, C_specie, D_specie, K2, X_reaction]).


%% SAT variable for old merge

%! merge(+I, +J, +N1, +N2, +Narcs1, +Narcs2, -X)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% X is the SAT variable for 'I and J have the same image through mu'
%
% @arg I    Interger representing vertex I
% @arg J    Interger representing vertex J
% @arg N1   number of vertices in graph 1
% @arg N2   number of vertices in graph 2
% @arg Na1  number of arcs in graph 1
% @arg Na2  number of arcs in graph 2
% @arg X    SAT variable for I and J have the same image
merge(I, J, N1, N2, Narcs1, Narcs2, X) :-
  Na1_1 is Narcs1 - 1,
  is_dummy(Na1_1, N1, N2, Na1_1, Narcs2, X1),
  X is (X1 + N1 * I + J + 1).


%! merge(+I, +J, +N1, +N2, +Narcs1, +Narcs2, -X)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% X is the SAT variable for 'mu(a) = i & mu(b) = i'
%
% @arg I    Interger representing vertex I in graph 2
% @arg A    Interger representing vertex A in graph 1
% @arg B    Interger representing vertex B in graph 1
% @arg N1   number of vertices in graph 1
% @arg N2   number of vertices in graph 2
% @arg Na1  number of arcs in graph 1
% @arg Na2  number of arcs in graph 2
% @arg X    SAT variable for mu(a) = i & mu(b) = i
tseitin(I, A, B, N1, N2, Narcs1, Narcs2, X) :-
  merge(N1, N1, N1, N2, Narcs1, Narcs2, X1),
  X is (X1 + N1 * A + B + 1 + I * N1 * N1).
