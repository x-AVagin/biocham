/**
 * Defines methods that act on the biocham manual.
 * @module sections
 */

import {
    CHAPTERS_TO_IGNORE,
    CRUD_SECTIONS } from 'Config/config';
import { manual } from 'Kernel/commands';
import * as wf from 'Config/workflows/workflows';


/**
 * Give ids to each chapter and sections in the manual for 
 * further use.
 * @param {Array} arr - The biocham manual.
 * @return {Array} The biocham manual with ids for each object.
 */
function give_id(arr) {
    for (let i = 0; i < arr.length; i++) {
        arr[i].id = i + 1;
    }

    let curr_chapter, curr_section;

    for (let i = 0; i < arr.length; i++) {
        if (arr[i].kind === 'chapter') {
            curr_chapter = arr[i].id;
        }
        else if (arr[i].kind === 'section') {
            curr_section = arr[i].id;
            arr[i].parent_id = curr_chapter;
        }
        else if (arr[i].kind === 'command') {
            // add args for option command for the GUI
            if (arr[i].name === 'option') {
                arr[i].arguments = [{name: 'Option', type: 'option'}];
            }
            if (curr_section > curr_chapter) {
                arr[i].parent_id = curr_section;
            }
            else {
                arr[i].parent_id = curr_chapter;
            }
        }
    }
    return arr;
}


/**
 * Creates a tree from the biocham manual.
 * @param {Array} arr - The biocham manual (with ids).
 * @return {Array} The tree generated from the manual.
 */
function unflatten(arr) {
    let tree = [],
        mapped_arr = {},
        arr_elem,
        mapped_elem;

    // First map the nodes of the array to an object -> create a hash table.
    for (let i = 0, len = arr.length; i < len; i++) {
        arr_elem = arr[i];
        mapped_arr[arr_elem.id] = arr_elem;
        mapped_arr[arr_elem.id].children = [];
    }

    for (let id in mapped_arr) {
        if (mapped_arr.hasOwnProperty(id)) {
            mapped_elem = mapped_arr[id];
            // If the element is not at the root level, add it to its parent array of children.
            if (mapped_elem.parent_id) {
                mapped_arr[mapped_elem['parent_id']].children.push(mapped_elem);
            }
            // If the element is at the root level, add it to first level elements array.
            else {
                tree.push(mapped_elem);
            }
        }
    }
    return tree;
}

/*
function make_menu(tree) {
    let menu = [];
    for (let i=0; i<tree.length; i++) {
        if (tree[i].children.length > 0) {
            menu.push({name: tree[i].name,
                       children: make_menu(tree[i].children)});
        }
        else {
            if (tree[i].kind === 'command') {
                let snippet;
                if (tree[i].arguments.length === 0) {
                    snippet = tree[i].name + '.';
                }
                else {
                    let args = [];
                    for (let k=0; k<tree[i].arguments.length; k++) {
                        // type is undefined if not present for now
                        if (tree[i].arguments.type !== undefined) {
                            args.push('<<' + tree[i].arguments[k].type + ' ' + tree[i].arguments[k].name + '>>');
                        }
                        else {
                            args.push('<<' + tree[i].arguments[k].name + '>>');
                        }
                    }
                    snippet = tree[i].name + '(' + args.join(', ') + ').';
                }
                menu.push({'name': tree[i].name,
                           'snippet': snippet,
                           'container': 'section_' + tree[i].parent_id});
            }
        }
    }
    return menu;
}
*/

function menuBuilder(menu_list) {
    const id_menu = give_id(menu_list);
    return unflatten(id_menu);
}


/**
 * Extract the model (called crud here) sections defined in the config
 * file from the manual, then sort them based on their order in the 
 * config file.
 * @param {Array} manual - The biocham manual (tree).
 * @param {Array} crudSections - The biocham model sections from the config file.
 * @return {Array} The biocham model sections with data extracted from the manual.
 */
export function getCrudSections(manual, crudSections) {
    const crudNames = getNames(crudSections);

    const crud = manual.flatMap(chapter => {
        if (crudNames.includes(chapter.name)) {
            return chapter;
        }
        return chapter.children.filter(section => {
            return crudNames.includes(section.name);
        });
    });

    //sort based on config order
    return crud.sort((a, b) => {
        return crudNames.indexOf(a.name) - crudNames.indexOf(b.name);
    });
};

/**
 * Extract the remaining sections (i.e. not model sections) from the manual,
 * ignoring some chapters that are not useful for the GUI.
 * @param {Array} manual - The biocham manual (tree).
 * @param {Array} crudSections - The biocham model sections from the config file.
 * @param {Array} toIgnore - The chapters names to remove.
 * @return {Array} The rest of the biocham sections (without model sections).
 */
export function getOtherSections(manual, crudSections, toIgnore) {
    const crudNames = getNames(crudSections);

    const others =
          manual.filter(chapter => {
              return !crudNames.includes(chapter.name);
          }).map(chapter => {
              // Only get children (sections) not in crud config object
              return {
                  ...chapter,
                  children: chapter.children.filter(section => {
                      return !crudNames.includes(section.name) &&
                          section.children.length > 0;
                  })
              };
          });
    
    const cleaned = cleanEmpty(others);
    // Only return chapters not included in the ones to ignore
    return cleaned.filter(chapter => {
        return !toIgnore.includes(chapter.name);
    });
};

/**
 * Populate the previously created model sections (in `getCrudSections`) with their
 * information in the config file.
 * Typically it does the following:
 * * Append to each crud command in the sections its information from the manual.
 * * Extract `list_` functions from the section (to display as tables in the GUI).
 * * Leave the rest of the section commands as is.
 * @param {Array} cruds - The previously created crud sections with all their information.
 * @param {Array} config - The model sections config object from the config file.
 * @return {Array} The model sections with correct structure for the GUI.
 */
export function createModelSections(cruds, config) {
    return cruds.map(section => {
        // get crud property from config for particular section
        const crudProp = config.find(obj => obj.name === section.name).crud;
        const { read, create, del } = crudProp;
        //make the names a list to filter in the section commands
        const crudNames = [read, create, del];

        //get the remaining lists to display not present in the config file
        const lists = section.children.filter(cmd => {
            return cmd.name.startsWith('list_') && !crudNames.includes(cmd.name);
        });

        //only get the commands that are not lists to display
        const children = section.children.filter(cmd => {
            return !crudNames.includes(cmd.name) && !lists.includes(cmd);
        });

        if (read) {
            crudProp.read = section.children.find(cmd => cmd.name === read);
        }

        if (create) {
            //needs to filter for command `present` because there are 2 in the manual.
            //we want only the second one (this is duct tape code for now)
            const createList = section.children.filter(cmd => cmd.name === create);
            createList.length === 1 ? crudProp.create = createList[0] : crudProp.create = createList[1];
        }

        if (del) {
            crudProp.del = section.children.find(cmd => cmd.name === del);
        }

        return {
            ...section,
            display: {
                crud: crudProp,
                lists: lists
            },
            children: children
        };
    });
};

/**
 * Extract workflow commands information in the manual commands list to populate
 * the workflow. All crud commands are not appended to the workflow.
 * @param {Array} wfCommands - The array of workflow commands (strings).
 * @param {Array} commandsList - The complete commands list from the manual.
 * @param {Array} crudSections - The model sections build before.
 * @return {Array} Array of workflow commands (same structure as in commandsList).
 */
function getWorkflowCommands(wfCommands, commandsList, crudSections) {
    // this function is used to filter out the different commands that
    // shouldn't be rendered in the workflow section, such as list or crud commands
    let ignore = ['list_model'];
    crudSections.map(section => {
        const { display } = section;
        const { crud, lists } = display;
        crud.read && ignore.push(crud.read.name);
        crud.create && ignore.push(crud.create.name);
        crud.del && ignore.push(crud.del.name);
        lists.map(c => ignore.push(c.name));
    });
    
    const tmp = wfCommands.filter(c => {
        return !ignore.includes(c);
    });
    
    return commandsList.filter(c => {
        return tmp.includes(c.name);
    }).sort((a, b) => {
        return tmp.indexOf(a.name) - tmp.indexOf(b.name);
    });
};

/**
 * Generate a usable workflow array for the GUI.
 * @param {Array} - The array of workflows (automatically generated in config/workflows).
 * @param {}
 */
export function generateWorkflows(workflows, commandsList, crudSections) {
    return workflows.map(wf => {
        return (
            {
                name: wf.name,
                commands: getWorkflowCommands(wf.commands,
                                              commandsList,
                                              crudSections) 
            }
        );
    });
};


export function getNames(list) {
    const names = list.map(c => c.name);
    // remove duplicates
    const s = new Set(names);
    return Array.from(s.values());
};

export function getChapters() {
    return manual.commands_list.filter(current => {
        return current.kind === 'chapter' &&
            !CHAPTERS_TO_IGNORE.includes(current.name);
    });
};

export function cleanEmpty(array) {
    return array.filter(ix => ix);
};

const menu =  menuBuilder(manual);

export const crudSections = getCrudSections(menu,
                                            CRUD_SECTIONS);
export const biochamSections = getOtherSections(menu,
                                                CRUD_SECTIONS,
                                                CHAPTERS_TO_IGNORE);

/** Model sections array with crud commands (used by GUI app)*/
export const modelSectionsWithCruds = createModelSections(crudSections,
                                                          CRUD_SECTIONS);

/** Commands array (extracted from the manual) */
export const listCommands = manual.filter(cmd => cmd.kind === 'command');

/** Workflows (used by GUI app)*/
export const workflows = generateWorkflows(wf.default,
                                           listCommands,
                                           modelSectionsWithCruds);
