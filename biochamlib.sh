#!/usr/bin/bash

# quadratization
swipl --goal=main --stand_alone=true -DSWIPL_LINKER_FLAGS="-static" -DSWIPL_SHARED_LIB=OFF\
  --foreign=save -o quadratization -c biochamlib_quadratization.pl

# ODE2SBML
swipl --goal=main --stand_alone=true -DSWIPL_LINKER_FLAGS="-static" -DSWIPL_SHARED_LIB=OFF\
  --foreign=save -o ODE2SBML -c biochamlib_ode2sbml.pl
