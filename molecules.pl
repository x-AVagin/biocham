:- module(
  molecules,
  [
    % Commands
    list_molecules/0,
    list_locations/0,
    list_neighborhood/0,
    draw_neighborhood/0,
    % Public API
    enumerate_molecules/1
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(objects).


:- devdoc('\\section{Commands}').


list_molecules :-
  biocham_command,
  doc('lists all the molecules of the current model.'),
  enumerate_all_molecules(L),
  write_numbered(L).


:- doc('When a molecule is written as compound@location, it represents the
  given \\emph{compound} as located inside the compartment \\emph{location}').


list_locations :-
  biocham_command,
  doc('lists all the locations of the current model.'),
  enumerate_all_molecules(L),
  maplist(get_location, L, Locs),
  sort([default | Locs], Locations),
  write_numbered(Locations).


list_neighborhood :-
  biocham_command,
  doc('lists all neighborhood relationships inferred from the model.'),
  compute_neighborhood,
  listing(molecules:neighbor/2).


draw_neighborhood :-
  biocham_command,
  doc('draws the graph of all neighborhood relationships inferred from the
    model.'),
  compute_neighborhood,
  new_graph,
  set_graph_name(neighborhood_graph),
  get_current_graph(GraphId),
  forall(
    neighbor(X, Y),
    (
      add_vertex(GraphId, X, XId),
      add_vertex(GraphId, Y, YId),
      add_edge(GraphId, XId, YId, EdgeId),
      set_attribute(EdgeId, dir = 'none')
    )
  ),
  draw_graph(GraphId).


:- devdoc('\\section{Public API}').


enumerate_molecules(Molecules) :-
  single_model(ModelId),
  findall(
    Molecule,
    identifier_kind(ModelId, Molecule, object),
    Mols
  ),
  % keeps order!!!
  list_to_set(Mols, Molecules).


:- devdoc('\\section{Private predicates}').


write_numbered(L) :-
  b_setval(write_numbered_counter, 0),
  write_numbered_aux(L).

write_numbered_aux([]).

write_numbered_aux([H | T]) :-
  b_getval(write_numbered_counter, C),
  CC is C + 1,
  b_setval(write_numbered_counter, CC),
  format('[~d] ~w~n', [C, H]),
  write_numbered_aux(T).


% store neighborhood information
:- dynamic(neighbor/2).


add_neighborhood([]).

add_neighborhood([(A, B, C, K) | L]) :-
  get_locations_from_kinetics(K, D),
  append([A, B, C, D], E),
  % remove stoichiometry if there is some
  maplist(nusmv:reactant_to_name, E, F),
  maplist(get_location, F, G),
  sort(G, H),
  subtract(H, [default], I),
  add_neighboor_pairs(I),
  add_neighborhood(L).


add_neighboor_pairs(L) :-
  findall(
    neighbor(A, B),
    (
      member(A, L),
      member(B, L),
      A @< B
    ),
    Pairs
  ),
  maplist(assertz, Pairs).


get_location(_@L, L) :-
  !.

get_location(_, default).


compute_neighborhood :-
  retractall(neighbor(_, _)),
  enumerate_reactions(Reactions),
  enumerate_influences(Influences),
  append(Reactions, Influences, Rules),
  add_neighborhood(Rules).


enumerate_reactions(Reactions) :-
  findall(
    (Reactants, Inhibitors, Products, Kinetics),
    (
      item([kind: reaction, item: Item]),
      reaction(Item, [
        reactants: Reactants,
        inhibitors: Inhibitors,
        products: Products,
        kinetics: Kinetics
      ])
    ),
    Reactions
  ).


enumerate_influences(Influences) :-
  findall(
    (PositiveInputs, NegativeInputs, [Output], Kinetics),
    (
      item([kind: influence, item: Item]),
      influence(Item, [
        positive_inputs: PositiveInputs,
        negative_inputs: NegativeInputs,
        kinetics: Kinetics,
        output: Output
      ])
    ),
    Influences
  ).


get_locations_from_kinetics(K, L) :-
  findall(
    M@Loc,
    sub_term(M@Loc, K),
    L
  ).
