:- module(
  about,
  [
    version/1,
    copyright/1,
    license/1,
    url/1,
    about/0
  ]).

version('4.5.17').

copyright(
  'Copyright (C) 2003-2020 Inria, EPI Lifeware, Saclay-Île de France, France'
).

license('GNU GPL 2').

url('http://lifeware.inria.fr/biocham4/').

about :-
  biocham_command,
  doc('lists the version, copyright, license and URL of Biocham'),
  version(Version),
  copyright(Copyright),
  license(License),
  url(URL),
  format('Biocham ~a\n', [Version]),
  format('~a,\n', [Copyright]),
  format('license ~a, ~a\n', [License, URL]).
