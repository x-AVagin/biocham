:- module(
  commandline,
  [
    do_arguments/0
  ]
).

:- doc('
\\section{Biocham files}

Biocham file names are suffixed by \\texttt{.bc}. In a Biocham file, everything following the character percent is a comment.
Some other file formats are used.').

:- doc('
Biocham models can be imported from, and exported to, other file formats using the following suffixes:
\\begin{itemize}
\\item \\texttt{.xml} for Systems Biology Markup Language (SBML) files, more precisely SBML2 files for reaction networks and SBML3qual files for influence networks;
\\item \\texttt{.ode} for Ordinary Differential Equation files (ODEs in XXPAUT format), allowing us to infer and import a reaction network from ODEs using a heuristic inference algorithm described in \\cite{FGS15tcs}.
\\item \\texttt{.ipynb} for Jupyter notebook files.
\\end{itemize}
Biocham numerical data time series can be imported/exported in 
\\begin{itemize}
\\item \\texttt{.csv} comma-separated values format (spreadsheet format).
\\end{itemize}
The following files can also be used to export some Biocham objects:
\\begin{itemize}
\\item \\texttt{.tex} LaTeX format for exporting ODEs and graphs;
\\item \\texttt{.dot} for exporting graphs;
\\item \\texttt{.plot} for exporting numerical data time series;
\\item \\texttt{.smv} for exporting boolean transition systems and Computation Tree Logic (CTL) queries for the NuSMV model-checker;
\\item \\texttt{.dot} for exporting graphs.
\\end{itemize}
').

:- doc('\\section{Biocham call options}').

do_arguments :-
  current_prolog_flag(os_argv, [_Exec | Args]),
  do_arguments(Args).


:- dynamic(file/1).


do_arguments(Argv) :-
  retractall(file(_)),
  parse_arguments(Argv),
  load_files.


load_files :-
  \+ (
    file(File),
    \+ (
      load(File)
    )
  ).


parse_arguments([]).

parse_arguments([Arg | Argv]) :-
  (
    Arg = '--'
  ->
    add_files(Argv)
  ;
    atom_concat('--', _, Arg)
  ->
    (
      option(Arg, Argv, ArgTail)
    ->
      parse_arguments(ArgTail)
    ;
      throw(error(unknown_option(Arg)))
    )
  ;
    add_file(Arg),
    parse_arguments(Argv)
  ).


option('--trace', Arg, Arg) :-
  leash(-all),
  trace.

option('--version', Arg, Arg) :-
   quit.

option('--jupyter', [], []) :-
  set_counter(graph_png, 0),
  set_counter(plot_csv, 0),
  set_counter(plot_png, 0),
  set_plot_driver(gnu_plot_csv),
  set_image_viewer_driver(img_tag),
  nb_setval(ode_viewer, latex),
  set_draw_graph_driver(graph_png).

option('--list-commands', [], []) :-
  forall(
    (
      current_functor(Functor, Arity),
      predicate_info(Functor/Arity, _ArgumentTypes, _, BiochamCommand, _),
      member(BiochamCommand, [yes, variantargs])
    ),
    (
      % Pred =.. [Functor | ArgumentTypes],
      format('~a~n', [Functor])
    )
  ),
  quit.

:- doc('\\texttt{biocham [file]}
launches Biocham and optionnally loads the file given as argument.').

:- doc('\\texttt{biocham --trace --version --list-commands} are possible options (prefixed by two dashes).').

:- doc('\\texttt{biocham_debug} is the command to use to launch Biocham with the Prolog toplevel.').


:- doc('
  \\section{Biocham kernel Jupyter notebook}
  \\texttt{biocham --notebook [notebook.ipynb]}  launches Jupyter (see
  \\url{http://jupyter-notebook.readthedocs.io/}) with a biocham kernel, and
  loads the corresponding notebook (ipynb suffix).').

:- doc('
 If no notebook is provided,
  a biocham notebook can be created with the Jupyter menu "new".').

:- doc('
 The
  \\texttt{--lab} option will try to launch JupyterLab instead of the
  traditional notebook.').

:- doc('
\\texttt{--console} will use the terminal console
  of Jupyter.').

:- doc('
  The environment variable \\texttt{BIOCHAM_TIMEOUT} can be used to change the
  default notebook-kernel communication timeout of 120 (seconds).
').

:- doc('
To execute a Biocham command enter Shift-Return.
All shortcuts are described in the \\emph{keyboard menu}.').

:- doc('
 The following video
  \\url{https://www.youtube.com/watch?v=jZ952vChhuI} is a quite nice
  introduction to the Jupyter notebook''s concepts, all can be adapted to
  Biocham as a kernel.
\\html{<div><iframe width="560" height="315" src="https://www.youtube.com/embed/jZ952vChhuI?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>}
').

:- doc('
  The only \\emph{magic commands} available in the notebook and not in Biocham are ').

:- doc('
\\texttt{%load file.bc} which creates a cell for each command contained in a Biocham file,
and').

:- doc('
  \\texttt{%slider k1 k2 …}, which  creates sliders to change the given
  parameters'' value, and run \\command{numerical_simulation} followed by
  \\command{plot} at each change.
').


:- doc('
\\section{Biocham GUI}

The graphical user interface of Biocham-4 is based on the Jupyter notebook.
It provides a static view of the current state with buttons for all Biocham commands.
A button allows you to switch between the notebook timeline view and the GUI spatial view.
').


option(Notebook, Args, []) :-
  member(Notebook, ['--notebook', '--lab', '--console']),
  atom_concat('--', Subcommand, Notebook),
  (
    Notebook == '--console'
  ->
    NArgs = ['--kernel=biocham' | Args]
  ;
    NArgs = Args
  ),
  Cmd =.. [
    jupyter,
    Subcommand |  % ,
    % '--KernelSpecManager.whitelist', 'biocham' |
    NArgs
  ],
  library_path(Lib),
  file_directory_name(Lib, Dir),
  % Same to find the proper biocham in PATH
  (
    getenv('PATH', Path)
  ->
    true
  ;
    Path = ''
  ),
  atomic_list_concat([Dir, Path], ':', NewPath),
  setenv('PATH', NewPath),
  exec(Cmd),
  quit.


add_files(Files) :-
  \+ (
    member(File, Files),
    \+ (
      add_file(File)
    )
  ).


add_file(File) :-
  assertz(file(File)).
