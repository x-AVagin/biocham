/**
 * Defines global and generic Jupyter functions for kernel
 * communication and biocham code execution.
 * @module comms
 */

import { format } from './formats';
import { modelSectionsWithCruds, listCommands } from './sections';
import { commTarget } from '../constants/gui';


// Global and generic jupyter functions //
/**
 * @return {Object} global Jupyter 
 */
export function getJupyter() {
    return window.Jupyter;
};

/**
 * @return {Object} global Mathjax
 */
export function getMathJax() {
    return window.MathJax;
};

/**
 * Create a Jupyter comm.
 * @param {string} target - the back-end target (comm receiver)
 * @param {Object} [init={}] - object to init the comm with (always empty)
 * @return {Object} a Jupyter comm 
 */
export function createComm(target, init={}) {
    const Jupyter = getJupyter();
    return Jupyter && Jupyter.notebook.kernel.comm_manager.new_comm(target, init);
};


/**
 * Create a notebook cell, fill it with code and execute it.
 * @param {string} code - the code to execute
 */
export function addToNotebook(code) {
    const notebook = getJupyter().notebook;
    const ncells = notebook.ncells();
    const lastCell = notebook.get_cell(ncells-1);
    if (lastCell.get_text() === '') {
        fillAndExecuteCell(lastCell, code);
    } else {
        const newCell = notebook.insert_cell_at_bottom('code');
        fillAndExecuteCell(newCell, code);
    }
};


/**
 * Helper method to fill a cell with code, then executing it.
 * @param {Object} cell - notebook cell object
 * @param {string} code - the code to execute
 */
function fillAndExecuteCell(cell, code) {
    cell.set_text(code);
    cell.execute();
};


// Biocham specific jupyter functions //

/**
 * Get biocham global options, format the values and 
 * uses a callback to update Redux store (in App component).
 * @param {function} callback - reducer function called to update
 * the Redux store.
 * @return {Array} either callback with option values or an empty array.
 */
// This is the only way I found to get data from the 
// kernel when creating a comm outside a component.
// I pass a callback function directly to handle the
// data (a reducer in this case which will update the 
// state)
export function getOptionsValues(callback) {
    const comm = createComm(commTarget);
    const code = 'list_options';
    comm.send({code: `${code}.`});
    comm.on_msg((msg) => {
        return msg.content.data.status === 'ok' &&
            typeof callback === 'function' &&
            callback(format({str: msg.content.data.output,
                             type: code}));
    });
    return [];
}


/**
 * Clear previous model and load model and options from string.
 * @param {string} modelStr - biocham model
 * @param {string} optionsStr - biocham options
 */
export function loadModel(modelStr, optionsStr) {
    // first we need to format options as some need an
    // empty string as param (e.g. logscale)
    const options = optionsStr.split('  \n')
          .map(opt => {
              // if option value is empty insert empty string
              if (!opt.match(/option\((.*?):(.+?)\)./g))
                  return opt.replace(':)', ":'')");
              return opt;
          })
          .join();
    
    const comm = createComm(commTarget);
    comm.send({ code: `clear_model. ${modelStr}. ${optionsStr}` });
    comm.on_msg((msg) => {
        if (msg.content.data.status === 'ok') {
            console.log('Model loaded from metadata');
        }
    });
}


/**
 * Function used to extract all the option properties (type, doc)
 * from the command list, as they are not available directly.
 * It is used in the settings part of the GUI when changing options
 * as we just get their value from the kernel.
 * @param {Array} commands - list of biocham commands (complete objects 
 * from manual).
 * @param {string} key - used to remove object duplicates.
 */
export function getOptionsProps(commands, key) {
     return commands.flatMap(cmd => cmd.options)
        .map(opt => ({ name: opt.name, type: opt.type }))
        .filter((opt, index, arr) => {
            return arr.map(mapObj => mapObj[key]).indexOf(opt[key]) === index;
        });
}

/**
 * Merge option props and their values (init them to empty string 
 * if it doesn't find anything.
 * @param {Array} props - options with props (e.g. doc).
 * @param {Array} values - options with values
 * @return {Array} options props with value property.
 */ 
export function mergeOptionPropsAndValues(props, values=[]) {
    return props.map(opt => {
        const mappedOpt = values.find(o => o.name === opt.name);
        let val = '';

        if (mappedOpt && (mappedOpt.value || mappedOpt.value === 0))
            val = mappedOpt.value;
        
        return {
            ...opt,
            value: val
        };
    });
}



