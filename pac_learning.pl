:- module(
  pac_learning,
  [
    pac_learning/1,
    pac_learning/3,
    generate_traces/3,
    % grammar
    transform/1
    %Public API
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(biocham).

:- dynamic(state/1).

:- doc('Implementation of Leslie Valiant''s PAC-Learning \\doi{10.1215/0961754X-2872666} algoritm for k-CNF formulae \\doi{10.1145/1968.1972}, for learning influence networks from Boolean traces \\cite{CFS17cmsb}.').


:- doc('Boolean traces can be generated from Boolean simulations of a reaction or influence model, or from the stochastic simulations.
Threshold values can be specified to transform a stochastic trace in a Boolean trace with the following syntax:').


:- grammar(transform).


transform(none).

transform(threshold(T)) :-
  integer(T).




generate_traces(NInitialStates, TimeHorizon, FilePrefix) :-
  biocham_command,
  option(data_transform, transform, Transform,
    'transformation of the traces before saving'
  ),
  option(boolean_simulation, yesno, Boolean,
    'use a boolean simulation (sbn) instead of the default stochastic one'
  ),
  doc(
    'Randomly choses \\argument{NInitialStates} initial states and for each,
    runs a numerical simulation with \\texttt{method: spn}, \\texttt{method:
    ssa} or \\texttt{method: sbn} and
    \\argument{TimeHorizon} as \\texttt{time} option. All results will be saved
    in \\texttt{.csv} files with the given \\argument{FilePrefix} as prefix
    and indexed by the run number (unless the prefix is empty).'
  ),
  type(NInitialStates, integer),
  type(TimeHorizon, time),
  type(FilePrefix, output_file),
  (
    Boolean == no
  ->
    get_option(method, M),
    (
      member(M, [spn, ssa])
    ->
      Method = M
    ;
      Method = spn
    )
  ;
    Method = sbn
  ),
  assertz(markov:nomake_state_table),
  retractall(state(_)),
  statistics(walltime, _),
  repeat,
  (
    enumerate_molecules(Molecules),
    between(1, NInitialStates, N),
    randomize_initial_state(Molecules, Boolean),
    with_option([time: TimeHorizon, method: Method], numerical_simulation),
    atomic_list_concat([FilePrefix, N, '.csv'], OutputFile),
    transform_data(Transform),
    (
      FilePrefix == ''
    ->
      copy_predicate_clauses(markov:state/1, state/1)
    ;
      markov:export_state(OutputFile)
    ),
    fail
  ;
    !
  ),
  statistics(walltime, [_, MilliTime]),
  retractall(markov:nomake_state_table),
  retractall(markov:cache_conditions(_, _)),
  retractall(markov:initial_state(_)),
  debug(pac_learning, 'Data generation time: ~p ms', [MilliTime]).


randomize_initial_state(Molecules, Boolean) :-
  get_option(stochastic_conversion, Rate),
  retractall(markov:initial_state(_)),
  findall(
    Molecule-X,
    (
      member(Molecule, Molecules),
      (
        Boolean == yes
      ->
        X is random(2)
      ;
        X is round(-log(random_float)*random(2)*Rate)
      )
    ),
    L
  ),
  assertz(markov:initial_state(L)).


transform_data(none).

transform_data(threshold(T)) :-
  forall(
    retract(state(Data)),
    (
      Data = ['#Time' | _]
    ->
      assertz(state(Data))
    ;
      Data = [Time | Values],
      maplist(threshold(T), Values, NewValues),
      NewData = [Time | NewValues],
      assertz(state(NewData))
    )
  ).


threshold(T, N, TN) :-
  number(N),
  !,
  (
    N >= T
  ->
    TN = 1
  ;
    TN = 0
  ).


pac_learning(TimeSeriesFiles) :-
  biocham_command,
  type(TimeSeriesFiles, {input_file}),
  doc(
    'Uses every time-series \\texttt{.csv} file in \\argument{TimeSeriesFiles}
    as source of samples to learn an influence network.'
  ),
  option(cnf_clause_size, integer, _, 'Maximum size of CNF clauses learnt.'),
  retractall(state(_)),
  forall(
    (
      member(TimeSeriesPattern, TimeSeriesFiles),
      filename(TimeSeriesPattern, Filename),
      csv_read_file(Filename, Data, [strip(true), convert(true)]),
      member(Row, Data),
      Row =.. [row | State]
    ),
    assertz(state(State))
  ),
  pac_learning.


:- initial(option(data_transform: none)).


:- initial(option(cnf_clause_size: 3)).


:- initial(option(boolean_simulation: no)).


pac_learning(ModelFile, NInitialStates, TimeHorizon) :-
  biocham_command,
  type(ModelFile, input_file),
  type(NInitialStates, integer),
  type(TimeHorizon, integer),
  doc(
    'Loads \\argument{ModelFile} and runs \\command{generate_traces/3} with
    the provided arguments and then loads the generated files for
    \\command{pac_learning/1}.
\\begin{example}
Learning Lotka-Volterra model from Boolean traces and comparison to the hidden original model.
\\trace{
biocham: pac_learning(library:examples/lotka_volterra/LVi.bc, 200, 10, boolean_simulation: yes).
biocham: list_model.
biocham: pac_learning(library:examples/circadian_cycle/bernot_comet.bc, 100, 10).
biocham: list_model.
}
\\end{example}
\\clearmodel'
  ),
  option(cnf_clause_size, integer, _, 'Maximum size of CNF clauses learnt.'),
  option(data_transform, transform, Transform,
    'transformation of the traces before learning'
  ),
  option(boolean_simulation, yesno, Boolean,
    'use a boolean simulation (sbn) instead of the default stochastic one
    (spn) to generate the data'
  ),
  load(ModelFile),
  with_option(
    [data_transform: Transform, boolean_simulation: Boolean],
    generate_traces(NInitialStates, TimeHorizon, '')
  ),
  statistics(walltime, _),
  pac_learning,
  % TODO maybe we should clear_model here
  statistics(walltime, [_, MilliTime]),
  debug(pac_learning, 'Clause learning time: ~p ms', [MilliTime]).


pac_learning :-
  state([_ | MList]),
  !,
  length(MList, NbMolecules),
  learn_k_cnf(NbMolecules).



:- dynamic(clauses/3).


learn_k_cnf(N) :-
  get_option(cnf_clause_size, K),
  KK is min(K, N),
  initialize_clauses(N),
  forall(
    sample(Index, Sign, Sample),
    (
      counter_name(Sign, Index, Counter),
      count(Counter, _),
      filter_clauses(Index, Sign, Sample, KK)
    )
  ),
  remove_subsumed_clauses,
  find_max_clause_size(KK, Max),
  format('% Maximum K used: ~w~n', [Max]),
  write_clauses(N, Max).


find_max_clause_size(N, N) :-
  length(L, N),
  clauses(_, _, L),
  !.

find_max_clause_size(N, Max) :-
  N > 1,
  !,
  NN is N - 1,
  find_max_clause_size(NN, Max).

find_max_clause_size(1, 0).


counter_name(Sign, Index, Counter) :-
  atomic_list_concat(['samples_', Sign, Index], Counter).


initialize_clauses(N) :-
  retractall(clauses(_, _, _)),
  forall(
    (
      between(1, N, Index),
      member(Sign, [-1, 1])
    ),
    (
      % we assert the most general clause, we will refine with samples
      assertz(clauses(Index, Sign, [])),
      counter_name(Sign, Index, Counter),
      set_counter(Counter, 0)
    )
  ).


filter_clauses(Index, Sign, Sample, K) :-
  clauses(Index, Sign, Clause),
  (
    implied_by(Clause, Sample)
  ->
    true
  ;
    retract(clauses(Index, Sign, Clause)),
    length(Clause, N),
    (
      N < K
    ->
      refine_clause(Index, Sign, Clause, Sample)
    ;
      true
    )
  ),
  fail.

filter_clauses(_, _, _, _).


implied_by([H | T], L) :-
  % a clause is implied if one of its litterals is in the sample
  (
    ord_memberchk(H, L)
  ->
    true
  ;
    implied_by(T, L)
  ).


refine_clause(Index, Sign, Clause, Sample) :-
  forall(
    (
      member(Literal, Sample),
      NotLiteral is -Literal,
      \+ member(NotLiteral, Clause)
    ),
    (
      % TODO maybe check for subsumption
      sort([Literal | Clause], NewClause),
      (
        clauses(Index, Sign, NewClause)
      ->
        % nothing to do
        true
      ;
        assertz(clauses(Index, Sign, NewClause))
      )
    )
  ).


sample(Index, Sign, Sample) :-
  findall(State, state(State), Data),
  sample_data(Data, Index, Sign, Sample).


sample_data([R1, R2 | _Data], Index, Sign, Sample) :-
  R1 = [Time1 | S1],
  Time1 \= '#Time',
  R2 = [Time2 | S2],
  Time2 \= '#Time',
  state_to_clause(S1, Sample),
  nth1(Index, S1, I1),
  nth1(Index, S2, I2),
  Sign is sign(I2 - I1),
  Sign \= 0.


sample_data([_, R | Data], Index, Sign, Sample) :-
  sample_data([R | Data], Index, Sign, Sample).


state_to_clause(State, Clause) :-
  foldl(state_to_literal, State, C, 1, _),
  sort(C, Clause).


state_to_literal(S, L, N, M) :-
  M is N + 1,
  (
    S =:= 0
  ->
    L is -N
  ;
    L is N
  ).


remove_subsumed_clauses :-
  clauses(X, Y, L1),
  clauses(X, Y, L2),
  L1 \= L2,
  ord_subset(L1, L2),
  retract(clauses(X, Y, L2)),
  fail.

remove_subsumed_clauses.


write_clauses(N, K) :-
  % samples = L(h, S) =< 2h(S+ln(h))
  % with S =< 2N^(K+1) total number of possible clauses of size =< K
  % for some edge cases (typically N < 12) 3^N is a better bound for S
  TwoS is 2*min((2*N)^(K+1), (3^N)),
  format('% minimum number of samples for h=1: ~w~n', [TwoS]),
  forall(
    (
      between(1, N, Index),
      member(Sign, [-1, 1])
    ),
    (
      counter_name(Sign, Index, Counter),
      peek_count(Counter, Samples),
      format('~n% ~w samples', [Samples]),
      % we simply ignore ln(h) for our estimation
      H is Samples/TwoS,
      format(' (max h ~~ ~w)~n', [H]),
      findall(
        Clause,
        clauses(Index, Sign, Clause),
        Cnf 
      ),
      debug(pac_learning, 'CNF: ~w', [Cnf]),
      cnf_dnf(Cnf, Dnf),
      maplist(dnf_to_influence(Index, Sign), Dnf)
    )
  ).


cnf_dnf([[]], [[]]) :-
  !.

cnf_dnf(Cnf, Dnf) :-
  findall(
    S,
    (
      cnf_dnf(Cnf, L, [], []),
      % avoid repetitions
      sort(L, S)
    ),
    Dnfs
  ),
  filter_subsumed(Dnfs, [], Dnf).


cnf_dnf([], Y, _, Y).

cnf_dnf([H | T], Y, Forbidden, Seen) :-
  % HH is sorted since clauses are
  (
    ord_disjoint(H, Seen)
  ->
    % select(X, H, HH),
    member(X, H),
    \+ ord_memberchk(X, Forbidden),
    XX is -X,
    % ord_add_element(HH, XX, HHH),
    % ord_union(Forbidden, HHH, FForbidden),
    ord_add_element(Forbidden, XX, FForbidden),
    ord_add_element(Seen, X, SSeen)
  ;
    FForbidden = Forbidden,
    SSeen = Seen
  ),
  cnf_dnf(T, Y, FForbidden, SSeen).


filter_subsumed([], Dnf, Dnf).

filter_subsumed([H | T], X, Dnf) :-
  member(I, T),
  ord_subset(I, H),
  !,
  filter_subsumed(T, X, Dnf).

filter_subsumed([H | T], X, Dnf) :-
  filter_subsumed(T, [H | X], Dnf).


dnf_to_influence(Index, Sign, Conjunct) :-
  state(['#Time' | MList]),
  !,
  nth1(Index, MList, Target),
  (
    Sign == 1
  ->
    Arrow = '+'
  ;
    Arrow = '-'
  ),
  split_conjunct(Conjunct, Activators, Inhibitors),
  maplist(name_molecule(MList), Activators, Activ),
  maplist(name_molecule(MList), Inhibitors, Inhib),
  influence(Influence, [positive_inputs: Activ, negative_inputs: Inhib,
    output: Target, sign: Arrow]),
  (
    Conjunct == []
  ->
    % false
    write('% ')
  ;
    true
  ),
  print_influence(_, Influence),
  nl.


split_conjunct([], [], []).

split_conjunct([H | T], A, I) :-
  (
    H < 0
  ->
    II is -H,
    I = [II | III],
    split_conjunct(T, A, III)
  ;
    % we know cnf_dnf sorts conjuncts
    I = [],
    A = [H | T]
  ).


name_molecule(MList, Index, Molecule) :-
  nth1(Index, MList, Molecule).
