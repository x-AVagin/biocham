:- module(
  initial_state,
  [
    % Commands
    list_initial_state/0,
    clear_initial_state/0,
    present/1,
    present/2,
    absent/1,
    initial_state/1,
    undefined/1,
    make_absent_not_present/0,
    make_present_not_absent/0,
    % Public API
    get_initial_state/2,
    set_initial_state/2,
    get_initial_concentration/2,
    set_initial_concentration/2,
    list_model_initial_state/0,
    check_aliased_states/1,
    undefined_object/1,
    set_initial_values/2,
    get_initial_values/2,
    compute_initial_value/2
  ]
).

% Only for separate compilation/linting
:- use_module(arithmetic_rules).
:- use_module(doc).
:- use_module(objects).


:- devdoc('\\section{Commands}').


list_initial_state :-
  biocham_command,
  doc('
    lists the objects which are present (including their initial concentration)
    and absent from the initial state.'),
  list_items([kind: initial_state]).


clear_initial_state :-
  biocham_command,
  doc('
    makes undefined all objects possibly present or absent in the initial
    state.'),
  delete_items([kind: initial_state]).

%! present/1 or present/2
%
% old commands to set the initial concentration of species

present(ObjectSet) :-
  biocham_command,
  type(ObjectSet, {object}),
  doc('
    Every object in \\argument{ObjectSet} is made present in the initial
    state with initial concentration equal to 1.'),
  set_state(ObjectSet, initial_state(_Object = 1)).


present(ObjectSet, Concentration) :-
  biocham_command,
  type(ObjectSet, {object}),
  type(Concentration, concentration),
  doc('
    Every object in \\argument{ObjectSet} is initialized
    with the given initial \\argument{Concentration}, which can be a parameter
    name to indicate that the parameter value will be used.
    An initial value equal to 0 is equivalent to absent.'),
  set_state(ObjectSet, initial_state(_Object = Concentration)).


absent(ObjectSet) :-
  biocham_command,
  type(ObjectSet, {object}),
  doc('
    Every object in \\argument{ObjectSet} is made absent in the initial
    state.'),
  set_state(ObjectSet, absent(_Object)).


undefined(ObjectSet) :-
  biocham_command,
  type(ObjectSet, {object}),
  doc('
    Every object in \\argument{ObjectSet} is made possibly present or absent
    in the initial state. This is the default.'),
  \+ (
    member(Object, ObjectSet),
    \+ (
      undefined_object(Object)
    )
  ).


%! initial_state(+Initial_state_list)
%
% Set the initial concentration for the molecules
% The argument is of the form A=1, B=2, C=0, D=3.4

initial_state(PresentList) :-
  biocham_command(*),
  type(PresentList, '*'(object = concentration)),
  doc('Sets the value of initial concentration.'),
  forall(
    member(Data, PresentList),
    (
      Data = (Species = Concentration)
    ->
      (
        is_null(Concentration)
      ->
        set_state([Species], absent(_Object0))
      ;
        set_state([Species], initial_state(_Object1 = Concentration))
      )
    ;
      set_state([Data], initial_state(_Object2 = 1))
    )
  ).


make_present_not_absent :-
  biocham_command,
  doc('
    makes all objects (appearing in the instances of the current set of rules)
    which are not declared absent, present in the initial state.'),
  enumerate_molecules(Objects),
  \+ (
    member(Object, Objects),
    \+ defined(Object),
    \+ (
      present([Object])
    )
  ).


make_absent_not_present :-
  biocham_command,
  doc('
    makes all objects (appearing in the instances of the current set of rules)
    which are not declared present, absent in the initial state.'),
  enumerate_molecules(Objects),
  \+ (
    member(Object, Objects),
    \+ defined(Object),
    \+ (
      absent([Object])
    )
  ).


:- devdoc('\\section{Public API}').


get_initial_state(Object, State) :-
  canonical(Object, Canonical),
  (
    item([kind: initial_state, key: Canonical, item: Item])
  ->
    (
      Item = initial_state(Canonical = Concentration)
    ->
      ( % replace input by its value if necessary
         parameter_value(input, Input_Value)
      ->
         substitute([input], [Input_Value], Concentration, ConcentrationUpd)
      ;
         ConcentrationUpd = Concentration
      ),
      (
        % if we have a parameter, keep it as present(param)
        parameter_value(ConcentrationUpd, _Value)
      ->
        State = initial_state(Canonical = ConcentrationUpd)
      ;
        ConcentrationUpd =:= 0
      ->
        State = absent
      ;
        State = initial_state(Canonical = ConcentrationUpd)
      )
    ;
      Item = absent(Canonical)
    ->
      State = absent
    )
  ;
    State = undefined
  ).


set_initial_state(Object, State) :-
  (
    State == undefined
  ->
    undefined_object(Object)
  ;
    (State == absent; State == absent(_O1))
  ->
    set_state([Object], absent(_O2))
  ;
    State = initial_state(_O3 = Value)
  ->
    set_state([Object], initial_state(_O4=Value))
  ).


get_initial_concentration(Object, Concentration) :-
  get_initial_state(Object, State),
  (
    State = initial_state(_Name = Present)
  ->
    (
      % get the actual value for parameters
      parameter_value(Present, Concentration)
    ->
      true
    ;
      Concentration = Present
    )
  ;
    Concentration = 0
  ).


list_model_initial_state :-
  devdoc('
    lists the initial state in a loadable syntax
    (auxiliary predicate of list_model).
  '),
  \+ (
    item([no_inheritance, kind: initial_state, item: InitialState]),
    \+ (
      format('~w.\n', [InitialState])
    )
  ).


check_aliased_states(EquivalenceClassList) :-
  EquivalenceClassList = [Canonical | _],
  findall(
    (Id, State),
    (
      member(Object, EquivalenceClassList),
      item([kind: initial_state, key: Object, id: Id, item: State])
    ),
    List
  ),
  (
    (
      List = []
    ;
      List = [(_, State)],
      rename_state(_, Canonical, State)
    )
  ->
    true
  ;
    (
      select((Id, State), List, Others),
      rename_state(_, Canonical, State)
    ;
      List = [(Id, State) | Others]
    )
  ->
    rename_state(State, Canonical, CanonicalState),
    findall(
      ConflictingState,
      (
        member((_, ConflictingState), Others),
        \+ rename_state(ConflictingState, Canonical, CanonicalState)
      ),
      ConflictingStates
    ),
    (
      ConflictingStates = []
    ->
      true
    ;
      print_message(warning, conflicting_states(State, ConflictingStates))
    ),
    \+ (
      member((OtherId, _), List),
      \+ (
        delete_item(OtherId)
      )
    ),
    add_item([kind: initial_state, key: Canonical, item: CanonicalState])
  ).


prolog:message(conflicting_states(State, ConflictingStates)) -->
  ['Conflicting states ~p. ~p kept.'-[ConflictingStates, State]].


:- devdoc('\\section{Private predicates}').


rename_state(absent(_), Object, absent(Object)).

rename_state(initial_state(_ = Concentration), Object, initial_state(Object = Concentration)) :- !.

rename_state(initial_state(_), Object, initial_state(Object = 1)).



set_state(ObjectSet, State) :-
  forall(
    member(Object, ObjectSet),
    (
      canonical(Object, Canonical),
      rename_state(State, Canonical, CanonicalState),
      undefined_object(Canonical),
      add_item([kind: initial_state, key: Canonical, item: CanonicalState])
    )
  ).


undefined_object(Object) :-
  (
    item([kind: initial_state, key: Object, id: Id])
  ->
    delete_item(Id)
  ;
    true
  ).


defined(Object) :-
  once(item([kind: initial_state, key: Object])).


set_initial_values(Variables, Values) :-
  maplist(set_initial_concentration, Variables, Values).


get_initial_values(Variables, Values) :-
  maplist(get_initial_concentration, Variables, Values).


% compute_initial_value(+Expression, -Value)
%
% Compute the initial value of an expression, undefined Molecule or Parameter are supposed
% to be null.

compute_initial_value(Expression, Value) :-
  is_numeric(Expression, Value),!.

compute_initial_value(Expr, Value) :-
  Expr =.. [Functor|Tail],
  \+ Tail = [],
  !,
  maplist(compute_initial_value, Tail, Tail_rec),
  Expr_rec =.. [Functor|Tail_rec],
  Value is Expr_rec.

compute_initial_value(Expression, Value) :-
  get_initial_concentration(Expression, Value),
  !.

compute_initial_value(_Expr, 0).


set_initial_concentration(X, Y) :-
  present([X], Y).
