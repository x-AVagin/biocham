import React, { Component, createRef } from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = ({
    root: {
        overflowY: 'auto',
        position: 'absolute',
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
});


/** Plot container. It uses the React ref mechanism to define an HTML element which is used
 * to rerender the plot in the container (e.g. when user selects another output). This is 
 * just a more 'react' way instead of creating the element with vanilla JS. 
 */
class Plot extends Component {

    ref = createRef();

    /** On mount, calls handlePlot.*/
    componentDidMount() {
        this.handlePlot();
    }
    
    /** On update and if plot is not the same (e.g. when creating new outputs), 
     * calls handlePlot. 
     */
    componentDidUpdate(prevProps) {
        if (prevProps.plot.div !== this.props.plot.div) this.handlePlot();
    }

    /** Empty the HTML node which contains the output, then create a Range object 
     * with a contextual fragment that will be appended to the HTML node. 
     * This way of doing things enables the HTML and JS script from the plot to be 
     * executed (i.e. otherwise Jupyter sanitize things a lot :) )
     */
    handlePlot = () => {
        const { plot } = this.props;

        this.ref.current.innerHTML = '';

        if (plot.script && plot.div) {
            const range = document.createRange();
            range.setStart(this.ref.current, 0);
            this.ref.current.appendChild(
                range.createContextualFragment(plot.div + plot.script));
        }
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root} ref={this.ref}>
            </div>
        );
    }
}

export default withStyles(styles)(Plot);
