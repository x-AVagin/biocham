:- module(
  doc,
  [
    doc/1,
    devdoc/1,
    devcom/1,
    doc_biocham/1,
    devdoc_biocham/1,
    grammar/1,
    grammar_doc/1,
    biocham/1,
    biocham_silent/1,
    generate_doc/0,
    generate_devdoc/0,
   biocham_reexecute/1,
   escape_tex/2
  ]
).

doc(_) :- % this predicate must just succeed in prolog
  !,
  true.

doc(_) :-
  doc('
    Writes in the documentation (both the Reference Manual
    and the Developer Manual).
  '),
  devdoc(' The basic transformation is write_doc(atom) --> parse_latex(atom,chars,term) --> format_doc(term)  --> html and tex ').

doc_biocham(_) :-
    doc('Writes the trace of a Biocham execution by running it.').

devdoc(_) :-
  doc('Writes in the Developer Manual.').


devcom(_) :-
  doc('Writes a comment in the Developer Manual.').

grammar_doc(_) :-
  doc('Writes documentation annotation for a grammar.').


devdoc_biocham(_) :-
    doc('Writes the trace of a Biocham execution in the Developer Manual.').


grammar(_) :-
  doc('Declares a grammar predicate to be written in the documentation.').


biocham(_).
:- devdoc('Calls a biocham command in a test file .plt. The argument may be either an atom parsed by the Biocham parser or a Prolog term.').


biocham_silent(_).
:- devdoc('Silent biocham call in test file.').

environment(remark, 'Remark', 'border: solid 1pt black; padding: 1em;').

environment(todo, 'To do', 'border: solid 1pt gray; padding: 1em; color: gray;').

:- devdoc('
\\begin{remark}
Environments may have optional arguments. The contents of the optional arguments
appear between parentheses after the environment name and is used as additional
classes in the HTML.
\\end{remark}
').

:- devcom('\\texttt{format_doc_html} and \\texttt{format_doc_tex} are specific and executed once for html, once for tex. \\texttt{write_command} is executed once for both.').


format_doc(Format) :- % specific clauses executed 
  (
    nb_getval(doc_html_stream, HtmlStream),
    with_output_to(
        HtmlStream,
        format_doc_html(Format)
%        ),
% Tex generation is not finished 
%    !,
%    nb_getval(doc_tex_stream, TeXStream),
%    with_output_to(
%        TeXStream,
%        format_doc_tex(Format)
      )
  ->
    true
  ;
    throw(failed_to_format(Format))
  ).


format_doc_html(document_opening(Title)) :-
  !,
  url(Url),
  format('\c
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>~a</title>
<style>
  h5 {font-family: "Courier New"; font-size: 1.1em; font-weight: normal; }
  div.indented {margin-left: 1cm; }
  div.example {margin-left: 1cm; }
  em.right {margin-left:1cm; color:gray; }
', [Title]),
  \+ (
    environment(Name, _, Style),
    \+ (
      format('  .~a {~a}\n', [Name, Style])
    )
  ),
  format('\c
  blockquote {color : gray; }
  blockquote.FF {color : red; }
  blockquote.SS {color : orange; }
  blockquote.TM {color : green; }
</style>
</head>
<body>
<header>
  <h1><img src="logo.png" alt="logo.png">~a</h1>
  <h2>Inria <a href="~a">~a</a></h2>
</header>
', [Title, Url, Url]).

format_doc_html(table_of_contents) :-
  !,
  format('  <nav>~n  <h2 id="Contents">Contents</h2>\n', []).

format_doc_html(end_table_of_contents) :-
  !,
  format('  </nav>~n  <main>', []).

format_doc_html(toc_entry(Id, Title)) :-
  !,
  format('<li>', []),
  write_counters,
  format(' <a href="#~a">~a</a></li>\n', [Id, Title]).

format_doc_html(index_letter(Letter)) :-
  !,
  format('<h4>~a</h4>\n', [Letter]).

format_doc_html(index_entry(Key, Sections)) :-
  !,
  format('<li>~a ', [Key]),
  \+ (
    member(Section - Id, Sections),
    \+ (
      format('<a href="#~a">', [Id]),
      write_counters(Section),
      format('</a>', [])
    )
  ),
  format('</li>\n', []).

format_doc_html(open_level) :-
  !,
  format('<ul>\n', []).

format_doc_html(close_level) :-
  !,
  format('</ul>\n', []).

format_doc_html(toc_part(Id, Title)) :-
  !,
  format(' <br><a href="#~a">~a</a><br><br>', [Id, Title]).

format_doc_html(part(Id,Title)) :-
  !,
  format('<br><hr><h1 id="~a">~a</h1>', [Id, Title]).

format_doc_html(chapter(Id, Counter, Title)) :-
  !,
  nb_getval(doc_commands_stream, CommandsStream),
  (
    Counter > 1
  ->
    write(CommandsStream, ',\n')
  ;
    true
  ),
  format(CommandsStream, '   {kind: "chapter", name: "~w"}', [Title]),
  format('<h2 id="~a">Chapter ~d<br>~a</h2>', [Id, Counter, Title]).

format_doc_html(section(HLevel, Id, Title)) :-
  !,
  nb_getval(doc_commands_stream, CommandsStream),
  format(CommandsStream, ',~n   {kind: "section", name: "~w"}', [Title]),
  format('<h~d id="~a">', [HLevel, Id]),
  write_counters,
  format(' ~a</h~d>\n', [Title, HLevel]).

format_doc_html(local_section(HLevel)) :-
  !,
  format('</div>\n<h~d>', [HLevel]).

format_doc_html(end_local_section(HLevel)) :-
  !,
  format('</h~d>\n<div>', [HLevel]).

format_doc_html(begin(itemize)) :-
  !,
  format('\c
</div>
<ul>
', []).

format_doc_html(close_item) :-
  !,
  format('\c
</li>
', []).

format_doc_html(item) :-
  !,
  format('\c
<li>
', []).

format_doc_html(end(itemize)) :-
  !,
  format('\c
</ul>
<div>
', []).

format_doc_html(begin(enumerate)) :-
  !,
  format('\c
</div>
<ol>
', []).

format_doc_html(end(enumerate)) :-
  !,
  format('\c
</ol>
<div>
', []).

format_doc_html(begin(example)) :-
  !,
  format('\c
</div>
<div class="example">
  <p></p><div><strong>Example.</strong>
', []).

format_doc_html(end(example)) :-
  !,
  format('\c
  </div>
</div>
<div>
', []).

format_doc_html(begin(emph)) :-
  !,
  format('<em>', []).

format_doc_html(end(emph)) :-
  !,
  format('</em>', []).

format_doc_html(begin(emphright)) :-
  !,
  format('<em class="right">', []).

format_doc_html(end(emphright)) :-
  !,
  format('</em>', []).

format_doc_html(begin(paragraph)) :-
  !,
  format('<div>', []).

format_doc_html(end(paragraph)) :-
  !,
  format('</div>', []).

format_doc_html(begin(Env)) :-
  environment(Env, Title, _),
  !,
  format('<div class="~a">\n<strong>~a.</strong> ', [Env, Title]).

format_doc_html(begin(Env, Name)) :-
  environment(Env, Title, _),
  !,
  format('<div class="~a ~a">\n<strong>~a (~a).</strong> ', [Env, Name, Title, Name]).

format_doc_html(end(Env)) :-
  environment(Env, _Title, _),
  !,
  format('</div>', []).

format_doc_html(begin(quote)) :-
  !,
  format('<blockquote><em>', []).

format_doc_html(quoteAuthor(Author)) :- % patch for author color
  !,
  format('</em></blockquote><blockquote class="~a"><em><strong>Comment. </strong>', [Author]).

format_doc_html(end(quote)) :-
  !,
  format('</em></blockquote>', []).

format_doc_html(begin(texttt)) :-
  !,
  format('<code>', []).

format_doc_html(end(texttt)) :-
  !,
  format('</code>', []).

format_doc_html(url(Url)) :-
  !,
  escape_html(Url, EscapedUrl),
  format('<a href="~a">~a</a>', [EscapedUrl, EscapedUrl]).

format_doc_html(html(Html)) :-
  !,
  write(Html).

format_doc_html(document_closing) :-
  !,
  format('
  </main>
</body>
</html>
', []).

format_doc_html(begin_grammar(Id, Grammar)) :-
  format('<div><table id="~a"><tr><td>~a ::= </td><td>\n', [Id, Grammar]).

% FIXME operators are not escaped and it is a nightmare to fix
format_doc_html(grammar_item(Item)) :-
  format('<div>| <code>~w</code></div>', [Item]).

format_doc_html(end_grammar) :-
  format('</td></tr></table></div>\n', []).

format_doc_html(begin_command(Id, Command)) :-
  nb_getval(doc_commands_stream, CommandsStream),
  format(CommandsStream, ',~n   {kind: "command", name: "~w", ',
    [Command]),
  format('<h5 id="~a"><code>~a', [Id, Command]).

format_doc_html(end_command) :-
  format('.</code></h5>\n', []).

format_doc_html(begin_command_description) :-
  format('<div class="indented">', []).

format_doc_html(end_command_description) :-
  format('\n</div>\n', []).

format_doc_html(begin_options) :-
  format('\n<div class="indented">\n<h6>Options.</h6>\n<dl>', []).

format_doc_html(begin_option(Option, Id)) :-
  format('\n<dt><var id="~a">~a</var>: ', [Id, Option]).

format_doc_html(option_description) :-
  format('</dt>\n<dd>', []).

format_doc_html(end_option) :-
  format('</dd>\n', []).

format_doc_html(end_options) :-
  format('\n</dl>\n</div>\n', []).

format_doc_html(type_multi_eqs(NameId, Name, ValueId, Value)) :-
  format(
      '<a href="#~a">~a</a><sub>1</sub> = <a href="#~a">~a</a><sub>1</sub>,
       ...,
       <a href="#~a">~a</a><sub><var>n</var></sub> =
       <a href="#~a">~a</a><sub><var>n</var></sub>',
      [NameId, Name, ValueId, Value, NameId, Name, ValueId, Value]
    ).

format_doc_html(type_multi_colons(NameId, Name, ValueId, Value)) :-
  format(
      '<a href="#~a">~a</a><sub>1</sub>: <a href="#~a">~a</a><sub>1</sub>,
       ...,
       <a href="#~a">~a</a><sub><var>n</var></sub>:
       <a href="#~a">~a</a><sub><var>n</var></sub>',
      [NameId, Name, ValueId, Value, NameId, Name, ValueId, Value]
    ).

format_doc_html(type_multi_lists(NameId, Name)) :-
  format(
      '[<a href="#~a">~a</a><sub>1</sub>],
       ...,
       [<a href="#~a">~a</a><sub><var>n</var></sub>]',
      [NameId, Name, NameId, Name]
    ).

format_doc_html(type_multi(NameId, Name)) :-
  format(
      '<a href="#~a">~a</a><sub>1</sub>,
       ...,
       <a href="#~a">~a</a><sub><var>n</var></sub>',
      [NameId, Name, NameId, Name]
    ).

format_doc_html(type_eqs(Id, ItemType)) :-
  format(
      '<a href="#~a">~a</a><sub>1</sub> = ... =
       <a href="#~a">~a</a><sub><var>n</var></sub>',
      [Id, ItemType, Id, ItemType]
    ).

format_doc_html(type_set(Id, ItemType)) :-
  format(
      '{<a href="#~a">~a</a><sub>1</sub>, ...,
        <a href="#~a">~a</a><sub><var>n</var></sub>}',
      [Id, ItemType, Id, ItemType]
    ).

format_doc_html(type_list(Id, ItemType)) :-
  format(
      '[<a href="#~a">~a</a><sub>1</sub>, ...,
        <a href="#~a">~a</a><sub><var>n</var></sub>]',
      [Id, ItemType, Id, ItemType]
    ).

format_doc_html(type(Id, Type)) :-
  format('<a href="#~a">~a</a>', [Id, Type]).

format_doc_html(type_suffix(Id, Type, Suffix)) :-
  format(
      '<a href="#~a">~a</a><sub><var>~a</var></sub>',
      [Id, Type, Suffix]
    ).

format_doc_html(var_type(Var, Id, Type)) :-
  format(
      '<var>~a</var>: <a href="#~a">~a</a>',
      [Var, Id, Type]
    ).

format_doc_html(var(Var)) :-
  format('<var>~a</var>', [Var]).

format_doc_html(prompt(Prompt, Command, Output)) :-
  escape_html(Command, EscapedCommand),
  escape_html(Output, EscapedOutput),
  format(
    '</div>
<div><code>~a</code><kbd>~w.</kbd></div>
<pre>~a</pre>
<div>',
    [Prompt, EscapedCommand, EscapedOutput]
  ).

format_doc_html(command(Command)) :-
  escape_html(Command, EscapedCommand),
  format(
    '</div>
<div><kbd>~w.</kbd></div>
<div>',
    [EscapedCommand]
  ).

format_doc_html(symbol(backslash)) :-
  format('\\', []).

format_doc_html(symbol(frasl)) :-
  format('&frasl;', []).

format_doc_html(symbol(mdash)) :-
  format('&mdash;', []).

format_doc_html(symbol(ndash)) :-
  format('&ndash;', []).

format_doc_html(symbol(lt)) :-
  format('&lt;', []).

format_doc_html(symbol(nbsp)) :-
  format('&nbsp;', []).

format_doc_html(symbol('(')) :-
  format('(', []).

format_doc_html(symbol(',')) :-
  format(', ', []).

format_doc_html(symbol(')')) :-
  format(')', []).

format_doc_html(char(C)) :-
  escape_html(C, EscapedC),
  format('~a', [EscapedC]).

format_doc_html(command(Id, Command)) :-
  format('<a href="#~a"><code>~a</code></a>', [Id, Command]).

format_doc_html(label(Key)) :-
  format('<a id="~a" />', [Key]).

format_doc_html(cite(Key)) :-
  format('<a href="http://lifeware.inria.fr/wiki/Main/Publications#~a">[~a]</a>', [Key, Key]).


format_doc_html(doi(Key)) :-
  format('<a href="http://doi.org/~a">[doi.org/~a]</a>', [Key, Key]).

:- devdoc('cite{key} will point to a bibliographical reference of the Lifeware group at Inria.').
:- devdoc('doi{key} will point to an external reference given by the doi key.').

format_doc_tex(document_opening(Title)) :-
  !,
  url(Url),
  format('\c
\\documentclass{book}
\\usepackage{fontspec,amsmath,hyperref}
\\usepackage[a4paper]{geometry}
\\newtheorem{example}{Example}
\\newcommand\\Coloneqq{\\mathbin{{:}{:}{=}}}
\\newcommand\\var[1]{\\emphright{\\color{gray}\\hfill #1\color{black}}}
'),
  \+ (
    environment(Env, Title, _),
    \+ (
      format('\\newenvironment{~a}{~a}\n', [Env, Title])
    )
  ),
  format('\c
\\begin{document}
\\title{~a}
\\institute{Inria \\url{~a}}
\\date{\\today}
\\begin{center}
\\includegraphics{"../logo.png"}
\\end{center}

', [Title, Url]).

format_doc_tex(table_of_contents) :-
  !,
  format('  \\tableofcontents\n', []).

format_doc_tex(end_table_of_contents) :-
  !.

format_doc_tex(toc_entry(_Id, _Title)) :-
  !.

format_doc_tex(index_letter(_Letter)) :-
  !.

format_doc_tex(index_entry(_Key, _Sections)) :-
  !.

format_doc_tex(open_level) :-
  !.

format_doc_tex(close_level) :-
  !.

format_doc_tex(toc_part(_Id, Title)) :-
  !,
  format('\\part{~a}\n', [Title]).

format_doc_tex(part(_Id, Title)) :-
  !,
  format('\\part{~a}\n', [Title]).

format_doc_tex(chapter(_Id, _Counter, Title)) :-
  !,
  format('\\chapter{~a}\n', [Title]).

format_doc_tex(section(Level, _Id, Title)) :-
  !,
  tex_section(Level, Command),
  format('\\~a{~a}\n', [Command, Title]).

format_doc_tex(local_section(Level)) :-
  !,
  tex_section(Level, Command),
  format('\\~a{', [Command]).

format_doc_tex(end_local_section(_Level)) :-
  !,
  format('}\n', []).

format_doc_tex(begin(itemize)) :-
  !,
  format('\c
\\begin{itemize}
', []).

format_doc_tex(close_item) :-
  !.

format_doc_tex(item) :-
  !,
  format('\c
\\item
', []).

format_doc_tex(end(itemize)) :-
  !,
  format('\c
\\end{itemize}
', []).

format_doc_tex(begin(enumerate)) :-
  !,
  format('\c
\\begin{enumerate}
', []).

format_doc_tex(end(enumerate)) :-
  !,
  format('\c
\\end{enumerate}
', []).

format_doc_tex(begin(example)) :-
  !,
  format('\c
\\begin{example}
', []).

format_doc_tex(end(example)) :-
  !,
  format('\c
\\end{example}
', []).

format_doc_tex(begin(emph)) :-
  !,
  format('\\emph{', []).

format_doc_tex(end(emph)) :-
  !,
  format('}', []).

format_doc_tex(begin(emphright)) :-
  !,
  format('\\emphright{', []).

format_doc_tex(end(emphright)) :-
  !,
  format('}', []).

format_doc_tex(begin(quote)) :-
  !,
  format('\\color{gray}\\begin{quotation}', []).

format_doc_tex(quoteAuthor(Author)) :-
  !,
  format('\\color{~a}\\emph{\\bold{Comment. }}', [Author]).

format_doc_tex(end(quote)) :-
  !,
  format('\\end{quotation}\\color{black}', []).

format_doc_tex(begin(paragraph)) :-
  !,
  format('\n\n', []).

format_doc_tex(end(paragraph)) :-
  !,
  format('\n\n', []).

format_doc_tex(begin(Env)) :-
  environment(Env, _, _),
  !,
  format('\n\\begin{~a}\n', [Env]).

format_doc_tex(begin(Env, Name)) :-
  environment(Env, _, _),
  !,
  format('\n\\begin{~a}[~a]\n', [Env, Name]).

format_doc_tex(end(Env)) :-
  environment(Env, _, _),
  !,
  format('\n\\end{~a}\n', [Env]).

format_doc_tex(begin(texttt)) :-
  !,
  format('\\texttt{', []).

format_doc_tex(end(texttt)) :-
  !,
  format('}', []).

format_doc_tex(url(Url)) :-
  !,
  format('\\url{~a}', [Url]).

format_doc_tex(html(_)) :-
  !.

format_doc_tex(document_closing) :-
  !,
  format('
\\end{document}
', []).

format_doc_tex(begin_grammar(Id, Grammar)) :-
  escape_tex(Grammar, GrammarEscaped),
  format(
      '\\label{~a}\\begin{tabular}{ll}\n~a \\(\\Coloneqq\\) &',
      [Id, GrammarEscaped]).

format_doc_tex(grammar_item(Item)) :-
  format('\n\\&\\(\\mid\\)', [Item]).

format_doc_tex(end_grammar) :-
  format('\n\\end{tabular}\n', []).

format_doc_tex(begin_command(Id, Command)) :-
  escape_tex(Command, CommandEscaped),
  format('\\label{~a}\\texttt{~a', [Id, CommandEscaped]).

format_doc_tex(end_command) :-
  format('.}\n', []).

format_doc_tex(begin_command_description) :-
  format('', []).

format_doc_tex(end_command_description) :-
  format('', []).

format_doc_tex(begin_options) :-
  format('\\paragraph{Options.} \\begin{description}', []).

format_doc_tex(begin_option(Id, Option)) :-
  escape_tex(Option, EscapedOption),
  format('\\label{~a}\\item[~a: ', [Id, EscapedOption]).

format_doc_tex(option_description) :-
  format(']', []).

format_doc_tex(end_option) :-
  format('\n', []).

format_doc_tex(end_options) :-
  format('\\end{description}', []).

format_doc_tex(type_multi_eqs(NameId, Name, ValueId, Value)) :-
  escape_tex(Name, EscapedName),
  escape_tex(Value, EscapedValue),
  format(
      '\\hyperref[~a]{~a\\(_1\\)} = \\hyperref[~a]{~a\\(_1\\)}, \\dots,
       \\hyperref[~a]{~a\\(_n\\)} = \\hyperref[~a]{~a\\(_n\\)}',
      [NameId, EscapedName, ValueId, EscapedValue,
       NameId, EscapedName, ValueId, EscapedValue]
    ).

format_doc_tex(type_multi_colons(NameId, Name, ValueId, Value)) :-
  escape_tex(Name, EscapedName),
  escape_tex(Value, EscapedValue),
  format(
      '\\hyperref[~a]{~a\\(_1\\)}: \\hyperref[~a]{~a\\(_1\\)}, \\dots,
       \\hyperref[~a]{~a\\(_n\\)}: \\hyperref[~a]{~a\\(_n\\)}',
      [NameId, EscapedName, ValueId, EscapedValue,
       NameId, EscapedName, ValueId, EscapedValue]
    ).

format_doc_tex(type_multi_lists(NameId, Name)) :-
  escape_tex(Name, EscapedName),
  format(
      '[\\hyperref[~a]{~a\\(_1\\)}], \\dots,
       [\\hyperref[~a]{~a\\(_n\\)}]',
      [NameId, EscapedName, NameId, EscapedName]
    ).

format_doc_tex(type_multi(NameId, Name)) :-
  escape_tex(Name, EscapedName),
  format(
      '\\hyperref[~a]{~a\\(_1\\)}, \\dots,
       \\hyperref[~a]{~a\\(_n\\)}',
      [NameId, EscapedName, NameId, EscapedName]
    ).

format_doc_tex(type_eqs(Id, ItemType)) :-
  escape_tex(ItemType, EscapedItemType),
  format(
      '\\hyperref[~a]{~a\\(_1\\)} = \\dots =
       \\hyperref[~a]{~a\\(_n\\)}',
      [Id, EscapedItemType, Id, EscapedItemType]
    ).

format_doc_tex(type_set(Id, ItemType)) :-
  escape_tex(ItemType, EscapedItemType),
  format(
      '\\{\\hyperref[~a]{~a\\(_1\\)}, \\dots,
          \\hyperref[~a]{~a\\(_n\\)}\\}',
      [Id, EscapedItemType, Id, EscapedItemType]
    ).

format_doc_tex(type_list(Id, ItemType)) :-
  escape_tex(ItemType, EscapedItemType),
  format(
      '[\\hyperref[~a]{~a\\(_1\\)}, \\dots,
        \\hyperref[~a]{~a\\(_n\\)}]',
      [Id, EscapedItemType, Id, EscapedItemType]
    ).

format_doc_tex(type(_Id, Type)) :-
  escape_tex(Type, EscapedType),
  format('~a', [EscapedType]).
%  format('\\hyperref[~a]{~a}', [Id, EscapedType]).

format_doc_tex(type_suffix(Id, Type, Suffix)) :-
  escape_tex(Type, EscapedType),
  format(
      '\\hyperref[~a]{~a\\(_{~a}\\)}',
      [Id, EscapedType, Suffix]
    ).

format_doc_tex(var_type(Var, Id, Type)) :-
  escape_tex(Type, EscapedType),
  format(
      '\\var{~a}: \\hyperref[~a]{~a}',
      [Var, Id, EscapedType]
    ).

format_doc_tex(var(Var)) :-
  format('\\var{~a}', [Var]).

format_doc_tex(prompt(Prompt, Command, Output)) :-
  escape_tex(Command, EscapedCommand),
  escape_tex(Output, EscapedOutput),
  format(
    '\\textbf{\\texttt{~a.}}\\texttt{~a}\\\\\\texttt{~a}',
    [Prompt, EscapedCommand, EscapedOutput]
  ).

format_doc_tex(command(Command)) :-
  escape_tex(Command, EscapedCommand),
  format(
    '\\texttt{~a}.',
    [EscapedCommand]
  ).

format_doc_tex(symbol(backslash)) :-
  format('\\(\\backslash\\)', []).

format_doc_tex(symbol(mdash)) :-
  format('----', []).

format_doc_tex(symbol(ndash)) :-
  format('--', []).

format_doc_tex(symbol(lt)) :-
  format('<', []).

format_doc_tex(symbol(nbsp)) :-
  format('~', []).

format_doc_tex(symbol('(')) :-
  format('(', []).

format_doc_tex(symbol(',')) :-
  format(', ', []).

format_doc_tex(symbol(')')) :-
  format(')', []).

format_doc_tex(char(C)) :-
  escape_tex(C, EscapedC),
  format('~a', [EscapedC]).

format_doc_tex(command(Id, Command)) :-
  escape_tex(Command, EscapedCommand),
  format('\\hyperref[~a]{~a}', [Id, EscapedCommand]).

format_doc_tex(label(Key)) :-
  format('\\label{~a}', [Key]).

format_doc_tex(cite(Key)) :-
  format('\\cite{~a}', [Key]).


format_doc_tex(doi(Key)) :-
  format('\\doi{~a}', [Key]).


tex_section(3, section).

tex_section(4, subsection).

tex_section(5, subsubsection).

tex_section(6, paragraph).


setup_doc :-
  set_random(seed(0)), % for reproducibility
  set_plot_driver(gnu_plot_png),
  set_image_viewer_driver(img_tag),
  set_draw_graph_driver(graph_svg).


generate_doc :-
  setup_doc,
  generate_doc_file(doc),
  check_doc(doc, quiet),
  !.


generate_devdoc :-
  setup_doc,
  generate_doc_file(devdoc),
  check_doc(devdoc, quiet),
  !.


generate_doc_file(Type) :-
  catch(
    make_directory(Type),
    error(existence_error(directory, _), _),
    true
  ),
  set_counter(plot_png, 0),
  set_counter(graph_svg, 0),
  working_directory(OldDir, Type),
  format(atom(HtmlFilename), 'index.html', []),
  % format(atom(TeXFilename), 'index.tex', [Type]),
  format(atom(CommandsFilename), 'commands.js', []),
  setup_call_cleanup(
    open(HtmlFilename, write, HtmlStream),
    setup_call_cleanup(
      % open(TeXFilename, write, TeXStream),
      open(CommandsFilename, write, CommandsStream),
      (
        write(CommandsStream,
          '// Auto generated file, please do not edit manually\n'),
        write(CommandsStream, 'define(function() {\n'),
        write(CommandsStream, '   "use strict"; \n'),
        write(CommandsStream, '   var commands_from_manual = [\n'),
        nb_setval(doc_html_stream, HtmlStream),
        % nb_setval(doc_tex_stream, TeXStream),
        nb_setval(doc_commands_stream, CommandsStream),
        generate_doc(Type),
        write(CommandsStream, '   \n];\nvar commands = new RegExp ("(?:" +\n'),
        write(CommandsStream, '   commands_from_manual.filter(\n'),
        write(CommandsStream, '   function(doc){return doc.kind == "command";}).map(\n'),
        write(CommandsStream, '   function(cmd){return cmd.name}).sort().filter(\n'),
        write(CommandsStream, '   function(elt, idx, arr){return (idx == arr.indexOf(elt));}).join("|") + ")\\\\b");\n'),
        write(CommandsStream, '   return {"manual": commands_from_manual,\n'),
        write(CommandsStream, '           "commands": commands};\n'),
        write(CommandsStream, '});')
      ),
      % close(TeXStream)
      close(CommandsStream)
    ),
    close(HtmlStream)
  ),
  working_directory(_OldDir, OldDir).


generate_doc(Type) :-
  version(Version),
  doc_title(Type, DocTitle),
  format(atom(Title), 'Biocham ~a ~a', [Version, DocTitle]),
  format_doc(document_opening(Title)),
  generate_toc(Type),
  generate_body(Type),
  format_doc(document_closing).


doc_title(doc, 'Reference Manual').

doc_title(devdoc, 'Developer Manual').


:- dynamic(toc/1).


generate_toc(Type) :-
  retractall(toc(_)),
  nb_setval(last_level, 0),
  read_toc(Type),
  format_doc(table_of_contents),
  nb_setval(current_counters, []),
  \+ (
    toc(section(Level, Title)),
    \+ (
      generate_toc_section(Level, Title)
    )
  ),
  goto_toc_level(0),
  format_doc(end_table_of_contents).


generate_toc_section(1, Title) :-
  sub_atom(Title,0,_,_,'Part '),
  !,
  goto_toc_level(1),
  indent(0),
  make_id(Title, Id),
  format_doc(toc_part(Id,Title)).

generate_toc_section(Level, Title) :-
  goto_toc_level(Level),
  indent(Level),
  increment_counter,
  make_id(Title, Id),
  format_doc(toc_entry(Id, Title)).


goto_toc_level(Level) :-
  nb_getval(current_counters, OldCounters),
  length(OldCounters, OldLevel),
  (
    OldLevel > Level
  ->
    LevelDiff is OldLevel - Level,
    \+ (
      between(1, LevelDiff, I),
      \+ (
        CurrentLevel is OldLevel - I,
        indent(CurrentLevel),
        format_doc(close_level)
      )
    )
  ;
    OldLevel = Level
  ->
    true
  ;
    OldLevel is Level - 1
  ->
    indent(OldLevel),
    format_doc(open_level)
  ;
    throw(error(bad_toc_structure))
  ),
  goto_level(Level).


goto_level(Level) :-
  nb_getval(current_counters, OldCounters),
  length(OldCounters, OldLevel),
  (
    OldLevel > Level
  ->
    LevelDiff is OldLevel - Level,
    length(FormerCounters, LevelDiff),
    append(NewCounters, FormerCounters, OldCounters),
    nb_setval(current_counters, NewCounters)
  ;
    OldLevel = Level
  ->
    true
  ;
    OldLevel is Level - 1
  ->
    append(OldCounters, [0], NewCounters),
    nb_setval(current_counters, NewCounters)
  ;
    throw(error(bad_toc_structure))
  ).


increment_counter :-
  nb_getval(current_counters, OldCounters),
  append(HigherCounters, [LastCounter], OldCounters),
  NewCounter is LastCounter + 1,
  append(HigherCounters, [NewCounter], NewCounters),
  nb_setval(current_counters, NewCounters).


write_counters :-
  nb_getval(current_counters, Counters),
  write_counters(Counters).

write_counters(Counters) :-
  \+ (
    member(Counter, Counters),
    \+ (
      format('~d.', [Counter])
    )
  ).


make_id(Title, Id) :-
  atom_chars(Title, Chars),
  filter_id(Chars, CharsId),
  atom_chars(Id, CharsId).


filter_id([], []).

filter_id([Char | Tail], [Char | TailId]) :-
  (
    alphanumeric_char(Char)
  ;
    Char = '_'
  ;
    Char = '-'
  ;
    Char = '.'
  ),
  !,
  filter_id(Tail, TailId).

filter_id([' ' | Tail], ['_' | TailId]) :-
  !,
  filter_id(Tail, TailId).

filter_id(['/' | Tail], ['-' | TailId]) :-
  !,
  filter_id(Tail, TailId).

filter_id([_ | Tail], Id) :-
  filter_id(Tail, Id).


read_toc(Type) :-
  setup_call_cleanup(
    open('../toc.org', read, Stream),
    read_toc(Stream, Type),
    close(Stream)
  ).


read_toc(Stream, Type) :-
  \+ (
    repeat,
    read_line_to_codes(Stream, Line),
    (
      Line = end_of_file
    ->
      !
    ;
      atom_codes(Atom, Line),
      (
        atom_concat('%', _, Atom)
      ->
        true
      ;
        atom_concat('* ', Section, Atom)
      ->
        add_section(1, Section)
      ;
        atom_concat('** ', Section, Atom)
      ->
        add_section(2, Section)
      ;
        atom_concat('*** ', Section, Atom)
      ->
        add_section(3, Section)
      ;
        Atom = '- index'
      ->
        assertz(toc(index))
      ;
        Atom = '- biocham.bib'
      ->
        assertz(toc(bibliography))
      ;
        atom_concat('- ', File, Atom)
      ->
        (
          Type = devdoc,
          atom_concat(_, '.pl', File)
        ->
          nb_getval(last_level, Level),
          LevelSucc is Level + 1,
          add_section_nolevel(LevelSucc, File)
        ;
          true
        ),
        assertz(toc(file(File)))
      )
    ),
    fail
  ).


add_section(Level, Section) :-
  add_section_nolevel(Level, Section),
  nb_setval(last_level, Level).

add_section_nolevel(Level, Section) :-
  assertz(toc(section(Level, Section))).


generate_body(Type) :-
  retractall(index_contents(_, _)),
  nb_setval(current_counters, []),
  \+ (
    toc(Item),
    \+ (
      generate_body_item(Item, Type)
    )
  ).


generate_body_item(section(Level, Title), _Type) :-
    sub_atom(Title,0,_,_,'Part '),
    !,
    goto_level(Level),
    make_id(Title, Id),
    format_doc(part(Id, Title)).

generate_body_item(section(Level, Title), _Type) :-
  goto_level(Level),
  increment_counter,
  make_id(Title, Id),
  (
    Level = 1
  ->
      nb_getval(current_counters, [Counter]),
      format_doc(chapter(Id, Counter, Title))
  ;
    HLevel is Level + 1,
    format_doc(section(HLevel, Id, Title))
  ).


generate_body_item(file(File), Type) :-
  format(user_error, 'File: ~a\n', [File]),
  atom_concat('../', File, ParentFile),
  setup_call_cleanup(
    open(ParentFile, read, Stream),
    generate_body_item_stream(Stream, Type),
    close(Stream)
  ).

generate_body_item(index, _Type) :-
  \+ (
    bagof(Entry, index_entry(Letter, Entry), Entries),
    \+ (
      format_doc(index_letter(Letter)),
      format_doc(open_level),
      sort(1, @=<, Entries, SortedEntries),
      \+ (
        member((Key: Sections), SortedEntries),
        \+ (
          format_doc(index_entry(Key, Sections))
        )
      ),
      format_doc(close_level)
    )
  ).


generate_body_item(bibliography, _Type) :-
  true.
%  setup_call_cleanup(
%    open('biocham.bib', read, Stream),
%    generate_body_item_stream(Stream, Type),
%    close(Stream)
%  ).


generate_body_item_stream(SourceStream, Type) :-
  nb_setval(current_grammar, none),
  \+ (
    repeat,
    read_term(
      SourceStream,
      Clause,
      [variables(Variables), variable_names(VariableNames)]
    ),
    (
      Clause = end_of_file
    ->
      close_grammar,
      !
    ;
      generate_clause(Clause, Variables, VariableNames, Type)
    ->
      true
    ;
      throw(error(generate_doc(Clause), generate_body_item_stream))
    ),
    fail
  ).


generate_clause(Clause, Variables, VariableNames, Type) :-
  (
    (
      Clause = (:- doc(Content)),
      atom_concat('\\begin{paragraph}',Content,A),
      atom_concat(A,'\\end{paragraph}',Contents)
    ;
      Clause = (:- devdoc(Content)),
      atom_concat('\\begin{paragraph}',Content,A),
      atom_concat(A,'\\end{paragraph}',Contents),
      Type = devdoc
    ;
      Clause = (:- devcom(Comment)),
      (
        atom_chars(Comment,[A1,A2|_]),
        uppercase_char(A1),
        uppercase_char(A2)
      ->
        atom_chars(Author,[A1,A2])
      ;
        Author='gray'
      ),
      atom_concat('\\begin{quote}\\quoteAuthor{',Author,A),
      atom_concat(A,'}',B),
      atom_concat(B,Comment,C),
      atom_concat(C,'\\end{quote}',Contents),
      Type = devdoc
    )
  ->
    close_grammar,
    write_doc(Contents)
  ;
    Clause = (:- grammar_doc(Comment))
  ->
    write_doc(Comment)
  ;
    Clause = (:- Predicate),
    (
      Predicate = biocham(_)
    ;
      Predicate = biocham_silent(_)
    )
  ->
    close_grammar,
    write_doc_item(Predicate, Type)
  ;
    Clause = (:- grammar(Grammar))
  ->
    close_grammar,
    nb_setval(current_grammar, Grammar),
    make_id(Grammar, Id),
    format_doc(begin_grammar(Id, Grammar))
  ;
    nb_getval(current_grammar, Grammar),
    Grammar \= none,
    (
      Clause = (Head :- Body)
    ;
      Clause = Head,
      Body = true
    ),
    Head =.. [Grammar, Item]
  ->
    instantiate_grammar_body(Body),
    format_doc(grammar_item(Item))
  ;
    Clause = (:- initial(Command))
  ->
    close_grammar,
    format_doc(command(Command))
  ;
    Clause = (:- _)
  ->
    true
  ;
    close_grammar,
    name_variables_and_anonymous(Variables, VariableNames),
    generate_body_item_clause(Clause, Type)
  ).


close_grammar :-
  nb_getval(current_grammar, Grammar),
  (
    Grammar = none
  ->
    true
  ;
    nb_setval(current_grammar, none),
    format_doc(end_grammar)
  ).


instantiate_grammar_body((G0, G1)) :-
  !,
  instantiate_grammar_body(G0),
  instantiate_grammar_body(G1).

instantiate_grammar_body(true) :-
  !.

instantiate_grammar_body(list(Grammar, NonTerminal)) :-
  !,
  make_id(Grammar, Id),
  format(
    atom(NonTerminal), '
      [</code> <a href="#~a">~a</a>
       <code>,</code> ... <code>,</code>
       <a href="#~a">~a</a> <code>] ', [Id, Grammar, Id, Grammar]
  ).

instantiate_grammar_body(function_application(Grammar, NonTerminal)) :-
  !,
  make_id(Grammar, Id),
  format(
    atom(NonTerminal), '
      </code> <a href="#name">name</a><code>(</code><a href="#~a">~a</a>
       <code>,</code> ... <code>,</code>
       <a href="#~a">~a</a><code>) ', [Id, Grammar, Id, Grammar]
  ).

instantiate_grammar_body(Item) :-
  Item =.. [Grammar, NonTerminal],
  make_id(Grammar, Id),
  format(
    atom(NonTerminal), ' </code> <a href="#~a">~a</a> <code> ', [Id, Grammar]
  ).


:- dynamic(argument_type/2).


generate_body_item_clause(Clause, Type) :-
  (
    predicate_info(Clause, ArgumentTypes, Options, BiochamCommand, Doc),
    (
      BiochamCommand = yes
    ;
      BiochamCommand = variantargs
    ;
      Type = devdoc,
      Doc \= []
    )
  ->
    Clause = (Head :- _),
    Head =.. [Command | Arguments],
    retractall(argument_type(_, _)),
    \+ (
      nth0(Index, Arguments, Argument),
      nth0(Index, ArgumentTypes, ArgumentType),
      nonvar(ArgumentType),
      \+ (
        assertz(argument_type(Argument, ArgumentType))
      )
    ),
    (
      BiochamCommand = variantargs
    ->
      format(atom(Key), '~a/*', [Command])
    ;
      length(Arguments, Arity),
      format(atom(Key), '~a/~w', [Command, Arity])
    ),
    make_id(Key, Id),
    index_use_id(Key, Id),
    format_doc(begin_command(Id, Command)),
    nb_getval(doc_commands_stream, CommandsStream),
    write(CommandsStream, 'arguments: ['),
    write_arguments(Arguments),
    format_doc(end_command),
    format_doc(begin_command_description),
    \+ (
      member(DocItem, Doc),
      \+ (
        write_doc_item(DocItem, Type)
      )
    ),
    write(CommandsStream, '], doc: ['),
    write_doc_to_json(Doc, CommandsStream),
    format_doc(end_command_description),
    write(CommandsStream, '], options: ['),
    (
      Options = []
    ->
      true
    ;
      format_doc(begin_options),
      \+ (
        nth1(OptionIndex, Options, option(Option, OptionType, OptionDoc)),
        \+ (
          format(atom(OptionAtom), "~w (option)", [Option]),
          index_new_id(OptionAtom, OptionId),
          format_doc(begin_option(Option, OptionId)),
          write_type(OptionType, ''),
          format_doc(option_description),
          write_doc(OptionDoc),
          format_doc(end_option),
          (
            OptionIndex == 1
          ->
            true
          ;
            write(CommandsStream, ', ')
          ),
          clean_doc(OptionDoc, CleanDoc),
          format(
            CommandsStream,
            '{name: "~w", type: "~w", doc: "~w"}',
            [Option, OptionType, CleanDoc]
          )
        )
      ),
      format_doc(end_options)
    ),
    write(CommandsStream, ']}')
  ;
    true
  ).


write_arguments([]) :-
  !,
  true.

write_arguments([First | Arguments]) :-
  format_doc(symbol('(')),
  write_argument(First),
  nb_getval(doc_commands_stream, CommandsStream),
  \+ (
    member(Argument, Arguments),
    \+ (
      format_doc(symbol(',')),
      write(CommandsStream, ', '),
      write_argument(Argument)
    )
  ),
  format_doc(symbol(')')).


write_argument(Argument) :-
  nb_getval(doc_commands_stream, CommandsStream),
  (
    argument_type(Argument, Type)
  ->
    format(CommandsStream, '{name: "~w", type: "~w"}', [Argument, Type]),
    write_type(Type, Argument)
  ;
    format(CommandsStream, '{name: "~w"}', [Argument]),
    write_var(Argument)
  ).


write_type(Type, Argument) :-
  (
    Type = '*'(Name = Value)
  ->
    make_id(Name, NameId),
    make_id(Value, ValueId),
    format_doc(type_multi_eqs(NameId, Name, ValueId, Value))
  ;
    Type = '*'(Name: Value)
  ->
    make_id(Name, NameId),
    make_id(Value, ValueId),
    format_doc(type_multi_colons(NameId, Name, ValueId, Value))
  ;
    Type = '*'([Name])
  ->
    make_id(Name, NameId),
    format_doc(type_multi_lists(NameId, Name))
  ;
    Type = '*'(Name)
  ->
    make_id(Name, NameId),
    format_doc(type_multi(NameId, Name))
  ;
    Type = '='(ItemType)
  ->
    make_id(ItemType, Id),
    format_doc(type_eqs(Id, ItemType))
  ;
    Type = {ItemType}
  ->
    make_id(ItemType, Id),
    format_doc(type_set(Id, ItemType))
  ;
    Type = [ItemType]
  ->
    make_id(ItemType, Id),
    format_doc(type_list(Id, ItemType))
  ;
    camel_case(Type, CamelCaseType),
    make_id(Type, Id),
    (
      (
        Argument = ''
      ;
        Argument = CamelCaseType
      )
    ->
      make_id(Type, Id),
      format_doc(type(Id, Type))
    ;
      atom_concat(CamelCaseType, Suffix, Argument)
    ->
      format_doc(type_suffix(Id, Type, Suffix))
    ;
      format_doc(var_type(Argument, Id, Type))
    )
  ).


write_var(Var) :-
  format_doc(var(Var)).


refer_argument(Argument) :-
  (
    argument_type(Argument, Type)
  ->
    (
      (
        Type = '='(ItemType)
      ;
        Type = '*'(ItemType)
      ;
        Type = [ItemType]
      ;
        Type = {ItemType}
      )
    ->
      make_id(ItemType, Id),
      format_doc(type_list(Id, ItemType))
    ;
      camel_case(Type, CamelCaseType),
      (
        Argument = CamelCaseType
      ->
        make_id(Type, Id),
        format_doc(type(Id, Type))
      ;
        atom_concat(CamelCaseType, Suffix, Argument)
      ->
        make_id(Type, Id),
        format_doc(type_suffix(Id, Type, Suffix))
      ;
        format_doc(var(Argument))
      )
    )
  ;
    write_var(Argument)
  ).


write_doc_item(doc(DocBody), _Type) :-
  write_doc(DocBody).


write_doc_item(devdoc(DocBody), Type) :-
  (
    Type = devdoc
  ->
    write_doc(DocBody)
  ;
    true
  ).

write_doc_item(biocham_silent(Command), _Type) :-
  echo_command(Command).

write_doc_item(biocham(Command), _Type) :-
  with_output_to(
    atom(Output),
    echo_command(Command)
  ),
  prompt(Prompt),
  format_doc(prompt(Prompt, Command, Output)).




escape_html(Term, EscapedAtom) :-
  (
    atom(Term)
  ->
    Atom = Term
  ;
    term_to_atom(Term, Atom)
  ),
  atom_chars(Atom, AtomChars),
  escape_html_chars(AtomChars, EscapedAtomChars),
  atom_chars(EscapedAtom, EscapedAtomChars).


escape_html_chars([], []).

escape_html_chars(List, EscapedList) :-
  atom_chars('<img ', ImgChars),
  append(ImgChars, Tail, List),
  !,
  append(ImgChars, EscapedTail, EscapedList),
  escape_html_chars(Tail, EscapedTail).

escape_html_chars(['<' | Tail], EscapedList) :-
  !,
  atom_chars('&lt;', EntityChars),
  append(EntityChars, EscapedTail, EscapedList),
  escape_html_chars(Tail, EscapedTail).

escape_html_chars(['&' | Tail], EscapedList) :-
  !,
  atom_chars('&amp;', EntityChars),
  append(EntityChars, EscapedTail, EscapedList),
  escape_html_chars(Tail, EscapedTail).

escape_html_chars([H | T0], [H | T1]) :-
  escape_html_chars(T0, T1).


escape_tex(Expr, EscapedExpr) :-
  (
    atom(Expr)
  ->
    atom_chars(Expr, AtomChars),
    escape_tex_chars(AtomChars, EscapedAtomChars),
    atom_chars(EscapedExpr, EscapedAtomChars)
  ;
    Expr =.. [Functor|ListTerm],
    maplist(escape_tex, ListTerm, EscapedListTerm),
    EscapedExpr =.. [Functor|EscapedListTerm]
  ).


escape_tex_chars([], []).

escape_tex_chars(List, EscapedList) :-
  atom_chars('<img src="', ImgChars),
  append(ImgChars, Tail, List),
  !,
  append(Filename, ['"', '>' | Others], Tail),
  format(chars(IncludeGraphicsChars), '\\includegraphics{~s}', [Filename]),
  append(IncludeGraphicsChars, EscapedTail, EscapedList),
  escape_tex_chars(Others, EscapedTail).

escape_tex_chars(['$' | Tail], EscapedList) :-
  !,
  atom_chars('\\$', TeXChars),
  append(TeXChars, EscapedTail, EscapedList),
  escape_tex_chars(Tail, EscapedTail).

escape_tex_chars(['~' | Tail], EscapedList) :-
  !,
  atom_chars('\\(\sim\\)', TeXChars),
  append(TeXChars, EscapedTail, EscapedList),
  escape_tex_chars(Tail, EscapedTail).

escape_tex_chars(['_' | Tail], EscapedList) :-
  !,
  atom_chars('\\_', TeXChars),
  append(TeXChars, EscapedTail, EscapedList),
  escape_tex_chars(Tail, EscapedTail).

escape_tex_chars(['\\' | Tail], EscapedList) :-
  !,
  atom_chars('\\(\\backslash\\)', TeXChars),
  append(TeXChars, EscapedTail, EscapedList),
  escape_tex_chars(Tail, EscapedTail).

escape_tex_chars([H | T0], [H | T1]) :-
  escape_tex_chars(T0, T1).


write_doc(Atom) :-
  watch_error(
    (
      atom_chars(Atom, Chars),
      doc:write_doc_chars(Chars)
    ),
    format(user_error, 'Error while formatting: ~a\n', [Atom])
  ).


write_doc_chars([]).

write_doc_chars(['\\', '\\' | Tail]) :-
  !,
  format_doc(symbol(backslash)),
  write_doc_chars(Tail).

write_doc_chars(['\\' | Tail]) :-
  !,
  parse_latex_command(Tail, Command, Argument, TailDoc),
  write_command(Command, Argument),
  write_doc_chars(TailDoc).

write_doc_chars(['-', '-', '-' | Tail]) :-
  !,
  format_doc(symbol(mdash)),
  write_doc_chars(Tail).

write_doc_chars(['-', '-' | Tail]) :-
  !,
  format_doc(symbol(ndash)),
  write_doc_chars(Tail).

write_doc_chars(['<' | Tail]) :-
  !,
  format_doc(symbol(lt)),
  write_doc_chars(Tail).

write_doc_chars(['/' | Tail]) :-
  !,
  format_doc(symbol(frasl)),
  write_doc_chars(Tail).

write_doc_chars(['~' | Tail]) :-
  !,
  format_doc(symbol(nbsp)),
  write_doc_chars(Tail).

write_doc_chars([Char | Tail]) :-
  format_doc(char(Char)),
  write_doc_chars(Tail),
  !.

write_doc_chars(Argument):- 
  throw(error(doc_parsing(Argument))).


parse_latex_command(Chars, Command, Argument, Tail) :-
  parse_latex_command_name(Chars, CommandChars, Tail0),
  parse_latex_optional_arguments(Tail0, OptionalArgumentBeforeChars, Tail1),
  parse_latex_command_arguments(Tail1, ArgumentChars, Tail2),
  parse_latex_optional_arguments(Tail2, OptionalArgumentAfterChars, Tail),
  atom_chars(Command, CommandChars),
  atom_chars(ArgumentBody, ArgumentChars),
  (
    OptionalArgumentBeforeChars = none
  ->
    ArgumentBefore = ArgumentBody
  ;
    atom_chars(OptionalArgumentBefore, OptionalArgumentBeforeChars),
    ArgumentBefore = [OptionalArgumentBefore]-ArgumentBody
  ),
  (
    OptionalArgumentAfterChars = none
  ->
    Argument = ArgumentBefore
  ;
    atom_chars(OptionalArgumentAfter, OptionalArgumentAfterChars),
    Argument = ArgumentBefore-[OptionalArgumentAfter]
  ).



parse_latex_command_name([Head | Tail], CommandChars, Others) :-
  alphabetic_char(Head),
  !,
  CommandChars = [Head | TailCommandChars],
  parse_latex_command_name(Tail, TailCommandChars, Others).

parse_latex_command_name(Tail, [], Tail).


parse_latex_optional_arguments(List, Optional, Tail) :-
  parse_latex_optional_arguments_aux(List, Optional, Tail),
  !,
  true.

parse_latex_optional_arguments(List, none, List).


parse_latex_optional_arguments_aux([Head | Tail], ArgumentChars, Others) :-
  (
    Head = ' '
  ;
    Head = '\n'
  ;
    Head = '\t'
  ),
  !,
  parse_latex_optional_arguments_aux(Tail, ArgumentChars, Others).

parse_latex_optional_arguments_aux(['[' | Tail], ArgumentsChars, Others) :-
  !,
  read_latex_command_arguments(Tail, ArgumentsChars, [], Others).


parse_latex_command_arguments([Head | Tail], ArgumentChars, Others) :-
  (
    Head = ' '
  ;
    Head = '\n'
  ;
    Head = '\t'
  ),
  !,
  parse_latex_command_arguments(Tail, ArgumentChars, Others).

parse_latex_command_arguments(['{' | Tail], ArgumentsChars, Others) :-
  !,
  read_latex_command_arguments(Tail, ArgumentsChars, [], Others).

parse_latex_command_arguments(Tail, [], Tail).


read_latex_command_arguments(
  [C | Tail], [C | ArgumentsChars], End, Others
) :-
  (
    C = '{'
  ->
    C0 = '}'
  ;
    C = '['
  ->
    C0 = ']'
  ),
  !,
  read_latex_command_arguments(Tail, ArgumentsChars, [C0 | End0], Others0),
  read_latex_command_arguments(Others0, End0, End, Others).

read_latex_command_arguments(
  [C | Tail], ArgumentsChars, ArgumentsChars, Tail
) :-
  (C = '}' ; C = ']'),
  !.

read_latex_command_arguments(
  [Char | Tail], [Char | ArgumentsChars], End, Others
) :-
  read_latex_command_arguments(Tail, ArgumentsChars, End, Others).

read_latex_command_arguments([], _ArgumentChars, _End, _Others) :-
  throw(error(missing_closing_curly_braces)).


write_command(texttt, Argument) :- % generic clauses executed once for both html and tex
  !,
  format_doc(begin(texttt)),
  atom_chars(Argument, ArgumentChars),
  write_doc_chars(ArgumentChars),
  format_doc(end(texttt)).

write_command(command, Command) :-
  !,
  make_id(Command, Id),
  format_doc(command(Id, Command)).

write_command(clearmodel, _Argument) :-
  !,
  clear_model.

write_command(trace, Trace) :-
  !,
  biocham_reexecute(Trace).

write_command(argument, Argument) :-
  !,
  format_doc(begin(texttt)),
  refer_argument(Argument),
  format_doc(end(texttt)).

write_command(emph, Argument) :-
  !,
  format_doc(begin(emph)),
  atom_chars(Argument, ArgumentChars),
  write_doc_chars(ArgumentChars),
  format_doc(end(emph)).

write_command(emphright, Argument) :-
  !,
  format_doc(begin(emphright)),
  atom_chars(Argument, ArgumentChars),
  write_doc_chars(ArgumentChars),
  format_doc(end(emphright)).


write_command(url, Argument) :-
  !,
  format_doc(url(Argument)).


write_command(html, Argument) :-
  !,
  format_doc(html(Argument)).


write_command(index, Key) :-
  !,
  index(Key),
  atom_chars(Key, KeyChars),
  write_doc_chars(KeyChars).

write_command(begin, itemize) :-
  !,
  nb_setval(opened_li, false),
  format_doc(begin(itemize)).

write_command(begin, enumerate) :-
  !,
  nb_setval(opened_li, false),
  format_doc(begin(enumerate)).

write_command(begin, example) :-
  !,
  format_doc(begin(example)).

write_command(begin, quote) :-
  !,
  format_doc(begin(quote)).

write_command(quoteAuthor, Author) :-
  !,
  format_doc(quoteAuthor(Author)).

write_command(begin, paragraph) :-
  !,
  format_doc(begin(paragraph)).

write_command(begin, Env) :-
  environment(Env, _, _),
  !,
  format_doc(begin(Env)).

write_command(begin, Env-[Name]) :-
  environment(Env, _, _),
  !,
  format_doc(begin(Env, Name)).

write_command(item, Argument) :-
  !,
  close_opened_li,
  format_doc(item),
  atom_chars(Argument, ArgumentChars),
  write_doc_chars(ArgumentChars).

write_command(end, itemize) :-
  !,
  close_opened_li,
  format_doc(end(itemize)).

write_command(end, enumerate) :-
  !,
  close_opened_li,
  format_doc(end(enumerate)).

write_command(end, example) :-
  !,
  format_doc(end(example)).

write_command(end, quote) :-
  !,
  format_doc(end(quote)).

write_command(end, paragraph) :-
  !,
  format_doc(end(paragraph)).

write_command(end, Env) :-
  environment(Env, _, _),
  !,
  format_doc(end(Env)).

write_command(section, Argument) :-
  !,
  nb_getval(current_counters, Counters),
  length(Counters, CounterCount),
  SectionLevel is CounterCount + 2,
  format_doc(local_section(SectionLevel)),
  write_doc(Argument),
  format_doc(end_local_section(SectionLevel)).

:- devcom('
\\begin{todo}
Bibliography handling.
\\end{todo}
').

write_command(cite, Argument) :-
  !,
  format_doc(cite(Argument)).

write_command(doi, Argument) :-
  !,
  format_doc(doi(Argument)).

write_command(Command, Argument) :-
  throw(error(unknown_command((Command,Argument)))).


biocham_reexecute(Trace) :- % the commands are supposed to be written on one single line
  prompt(Prompt),
  atom_chars(Prompt,PromptChars),
  atom_chars(Trace, TraceChars),
  biocham_reexecute(TraceChars, PromptChars).


biocham_reexecute(Trace, Prompt) :-
  append(Head,Tail,Trace), 
  append(_Lost,Prompt,Head),
  !, % go to first biocham prompt skipping whatever before
  (
    append(Head2,Tail2,Tail),
    append(Execution,Prompt,Head2)
  -> % second prompt
    command_result(Command,Execution),
    write_command_chars(Command),
    append(Prompt,Tail2,PromptTail2),
    biocham_reexecute(PromptTail2, Prompt)
  ; % was last prompt
    command_result(Command,Tail),
    write_command_chars(Command)
  ).

command_result(Command,Execution):-
  append(Command,Result,Execution),
  (append(['.','\n'],_,Result);append(['.',' ','\n'],_,Result);append(['.',' ',' ','\n'],_,Result)).


write_command_chars(Command) :- 
%  (
%    append(CommandTrimmed, ['.', '\n'], Command) % not general enough (if blank prints double dots)
%  ->
%    true
%  ;
    CommandTrimmed = Command,
%  ),
  atom_chars(Biocham,CommandTrimmed),
  write_doc_item(biocham(Biocham),_Type).

echo_command(Command) :-
  watch_error(
    command(Command),
    format(user_error, 'Error while executing ~p\n', [Command])
  ).


close_opened_li :-
  (
    nb_getval(opened_li, true)
  ->
    format_doc(close_item),
    nb_setval(opened_li, false)
  ;
    true
  ).

:- dynamic(index_contents/2).


index_entry(UppercaseLetter, Key: Sections) :-
  index_contents(Key, Sections),
  atom_chars(Key, [Letter | _]),
  to_upper(Letter, UpperCode),
  atom_codes(UppercaseLetter, [UpperCode]).


index(Key) :-
  index_new_id(Key, Id),
  format_doc(label(Id)).


index_new_id(Key, Id) :-
  nb_getval(current_counters, Counters),
  (
    retract(index_contents(Key, List))
  ->
    length(List, Count),
    SuccCount is Count + 1,
    format(atom(Name), '~a_~d', [Key, SuccCount])
  ;
    List = [],
    Name = Key
  ),
  make_id(Name, Id),
  append(List, [Counters - Id], NewList),
  assertz(index_contents(Key, NewList)).


index_use_id(Key, Id) :-
  nb_getval(current_counters, Counters),
  (
    retract(index_contents(Key, List))
  ->
    true
  ;
    List = []
  ),
  append(List, [Counters - Id], NewList),
  assertz(index_contents(Key, NewList)).

:- devcom('\\begin{todo}Add to the developper manual all the commands exported in the API.\\end{todo}').


write_doc_to_json(Docs, Stream) :-
  % convlist with library(yall) would be easier to write, but not available on
  % Ubuntu 16.04's version of SWI
  include(=(doc(_)), Docs, DocObj),
  maplist(undoc, DocObj, DocObjects),
  forall(
    nth1(DocIndex, DocObjects, Doc),
    (
      (
        DocIndex == 1
      ->
        true
      ;
        write(Stream, ', ')
      ),
      clean_doc(Doc, Clean),
      format(Stream, '"~w"', [Clean])
    )
  ).


undoc(doc(X), Y) :-
  undoc_aux(X, Y).


undoc_aux(X, Y) :-
  sub_atom(X, Before, 1, After, "\\"),
  !,
  sub_atom(X, 0, Before, _, BAtom),
  sub_atom(X, _, After, 0, AAtom),
  undoc_atom(AAtom, CleanAtom),
  undoc_aux(CleanAtom, UndocAtom),
  atom_concat(BAtom, UndocAtom, Y).

undoc_aux(X, X).


undoc_atom(X, Y) :-
  (
    (
      sub_atom(X, 0, _, After, 'begin{example}')
    ;
      sub_atom(X, 0, _, After, 'end{example}')
    ;
      sub_atom(X, 0, _, After, 'clearmodel')
    )
  ->
    sub_atom(X, _, After, 0, Y)
  ;
    (
      sub_atom(X, 0, _, After, 'texttt{')
    ;
      sub_atom(X, 0, _, After, 'command{')
    ;
      sub_atom(X, 0, _, After, 'argument{')
    )
  ->
    sub_atom(X, _, After, 0, YY),
    once(sub_atom(YY, Before, 1, AAfter, '}')),
    sub_atom(YY, 0, Before, _, YYY),
    sub_atom(YY, _, AAfter, 0, YYYY),
    atomic_list_concat(['`', YYY, '`', YYYY], Y)
  ;
    sub_atom(X, 0, _, After, 'trace{')
  ->
    sub_atom(X, _, After, 0, YY),
    once(sub_atom(YY, _, 1, AAfter, '}')),
    sub_atom(YY, _, AAfter, 0, Y)
  ;
    Y = X
  ).


clean_doc(Doc, Clean) :-
  atom_string(Doc, String),
  split_string(String, "\"", "", List1),
  atomics_to_string(List1, "\\\"", QuotedString),
  split_string(QuotedString, "\s\t\n", "\s\t\n", List),
  atomics_to_string(List, ' ', Clean).


check_doc(Dir, Errors) :-
  directory_file_path(Dir, 'index.html', Path),
  load_html(Path, Dom, [syntax_errors(Errors)]),
  % images
  forall(
    (
      sub_term(element(img, Attrs, []), Dom),
      memberchk(src=Link, Attrs)
    ),
    (
      directory_file_path(Dir, Link, File),
      access_file(File, read)
    )
  ),
  % links
  forall(
    (
      sub_term(element(a, Attrs, _), Dom),
      memberchk(href=Link, Attrs),
      \+ atom_concat('#', _, Link),
      \+ atom_concat('http', _, Link)
    ),
    (
      directory_file_path(Dir, Link, File),
      access_file(File, read)
    )
  ).
