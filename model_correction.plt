:- use_module(library(plunit)).

:- begin_tests(correction, [setup((clear_model, reset_options))]).

test('detect_catalysts', []) :-
  correction:detect_modifiers([a,b,c,d], a/(1+d), [a], _Dummy1),
  correction:detect_modifiers([a,b,c,d], a*exp(b)/(1+b+d^2), [a,b], _Dummy2).

test('detect_inhibitor', []) :-
  correction:detect_modifiers([a,b,c,d], a/(1+d), _Dummy1, [d]),
  correction:detect_modifiers([a,b,c,d], a/(1+b+d^2), _Dummy2, [b,d]).

test('add_catalysts', []) :-
  correction:add_catalysts([a], b+a=>a+c, b+a=>a+c),
  correction:add_catalysts([a,d], b+a=>a+c, b+a+d=>a+c+d),
  correction:add_catalysts([a,b], c=>d, c+b+a=>d+b+a),
  correction:add_catalysts([a], b=>c, b+a=>c+a).

test('add_inhibitors', []) :-
  correction:add_inhibitors([a], b/a=>c, b/a=>c),
  correction:add_inhibitors([a,d], b/a=>c, b/(d,a)=>c),
  correction:add_inhibitors([a], b=>c, b/a=>c).

test('remove_molecule', []) :-
  correction:remove_molecule(2*a+b, a, a+b),
  correction:remove_molecule(1*a+b+c, a, b+c),
  correction:remove_molecule(a+b, a, b).

:- end_tests(correction).
