struct search_parameter {
    double min;
    int index;
    double max;
    double init;
};


struct variable_objective {
    int index;
    double value;
};
