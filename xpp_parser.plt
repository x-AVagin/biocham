:- use_module(library(plunit)).

:- use_module(xpp_parser).

:- begin_tests(xpp_parser, [setup((clear_model, reset_options))]).

test(read_xpp, [nondet]) :-
  read_xpp("library/examples/tests/test_xpp.ode"),
  get_current_ode_system(Id),
  item([parent:Id, type:ode, item: d(x)/dt = y]),
  item([parent:Id, type:ode, item: d(y)/dt = -k*f(x*y)]),
  item([parent:Id, type:parameter, item: par(k1=2.5)]),
  item([parent:Id, type:parameter, item: par(k2=20.5)]),
  item([parent:Id, type:function, item: func(f(x)=sqrt(x))]),
  write_xpp("library/examples/tests/test_xpp_out.ode"),
  clear_model,
  read_xpp("library/examples/tests/test_xpp_out.ode"),
  get_current_ode_system(Id2),
  item([parent:Id2, type:ode, item: d(x)/dt = y]),
  item([parent:Id2, type:ode, item: d(y)/dt = -k*f(x*y)]),
  item([parent:Id2, type:parameter, item: par(k1=2.5)]),
  item([parent:Id2, type:parameter, item: par(k2=20.5)]),
  item([parent:Id2, type:function, item: func(f(x)=sqrt(x))]).

:- end_tests(xpp_parser).
