:- module(
  graph_editor,
  [
    % Grammars
    attribute/1,
    edge/1,
    edgeref/1,
    % Commands
    new_graph/0,
    delete_graph/1,
    set_graph_name/1,
    list_graphs/0,
    select_graph/1,
    add_vertex/1,
    delete_vertex/1,
    add_edge/1,
    delete_edge/1,
    list_edges/0,
    list_isolated_vertices/0,
    list_graph_objects/0,
    graph_object/1,
    set_attribute/2,
    delete_attribute/2,
    list_attributes/1,
    place/1,
    transition/1,
    % Public API
    new_graph/1,
    set_graph_name/2,
    get_current_graph/1,
    set_current_graph/1,
    get_graph_name/2,
    get_attribute/2,
    get_attribute/3,
    set_attribute/3,
    place/2,
    place/3,
    transition/2,
    transition/3,
    kind/3,
    kind/4,
    add_vertex/3,
    add_edge/3,
    add_edge/4,
    proper_source/2
  ]
).


:- use_module(doc).


:- devdoc('\\section{Grammars}').

:- doc('Different graphs can be created from Biocham models and manipulated and more importantly visualized using the third-party Graphviz visualization tool.').


:- grammar(attribute).


attribute(Key = Value) :-
  name(Key),
  term(Value).

attribute(Name) :-
  name(Name).


:- grammar(edge).


edge(From -> To) :-
  name(From),
  name(To).



:- grammar(edgeref).


edgeref(Edge) :-
  edge(Edge).



:- devdoc('\\section{Commands}').


new_graph :-
  biocham_command,
  doc('Creates a new graph.'),
  new_graph(Id),
  set_current_graph(Id).


delete_graph(Name) :-
  biocham_command,
  type(Name, name),
  doc('Deletes a graph.'),
  delete_items([kind: graph, key: Name]).


set_graph_name(Name) :-
  biocham_command,
  type(Name, name),
  doc('Sets the name of the current graph.'),
  get_current_graph(Id),
  set_graph_name(Id, Name).


list_graphs :-
  biocham_command,
  doc('Lists the graph of the current model.'),
  list_items([kind: graph]).


select_graph(Name) :-
  biocham_command,
  type(Name, name),
  doc('Selects a graph'),
  find_item([kind: graph, key: Name, id: Id]),
  set_current_graph(Id).


add_vertex(NameList) :-
  biocham_command(*),
  type(NameList, '*'(name)),
  doc('Adds new vertices to the current graph.'),
  get_current_graph(GraphId),
  \+ (
    member(Name, NameList),
    \+ (
      add_vertex(GraphId, Name, _VertexId)
    )
  ).


delete_vertex(NameList) :-
  biocham_command(*),
  type(NameList, '*'(name)),
  doc('Deletes a set of vertices from the current graph.'),
  get_current_graph(GraphId),
  \+ (
    member(Name, NameList),
    \+ (
      delete_item([parent: GraphId, kind: vertex, key: Name])
    )
  ).


add_edge(EdgeList) :-
  biocham_command(*),
  type(EdgeList, '*'(edge)),
  doc('
    Adds the given set of edges to the current graph.
    The vertices are added if needed.
  '),
  get_current_graph(GraphId),
  \+ (
    member(Edge, EdgeList),
    \+ (
      add_edge(GraphId, Edge, _EdgeId)
    )
  ).


delete_edge(EdgeRefList) :-
  biocham_command(*),
  type(EdgeRefList, '*'(edgeref)),
  doc('Deletes a set of edges from the current graph.'),
  get_current_graph(GraphId),
  \+ (
    member(EdgeRef, EdgeRefList),
    \+ (
      delete_item([parent: GraphId, kind: edge, key: EdgeRef])
    )
  ).


list_edges :-
  biocham_command,
  doc('Lists the edges of the current graph.'),
  get_current_graph(GraphId),
  list_items([parent: GraphId, kind: edge]).


list_isolated_vertices :-
  biocham_command,
  doc('Lists the isolated vertices of the current graph.'),
  get_current_graph(GraphId),
  isolated_vertices(GraphId, VerticeIds),
  list_ids(VerticeIds).


list_graph_objects :-
  biocham_command,
  doc('
    Lists the edges and the isolated vertices of the current graph,
    and their attributes if any.
  '),
  get_current_graph(GraphId),
  all_ids([parent: GraphId, kind: edge], EdgeIds),
  isolated_or_attributed_vertices(GraphId, VerticeIds),
  append(EdgeIds, VerticeIds, AllIds),
  list_ids([recursive, kind: attribute], AllIds).


:- grammar(graph_object).


graph_object(Edge) :-
  edge(Edge).

graph_object(Name) :-
  name(Name).


set_attribute(Id, Attribute) :-
  integer(Id),
  !,
  (
    Attribute = (Key = _Value)
  ->
    true
  ;
    Key = Attribute
  ),
  (
    item(
      [parent: Id, kind: attribute, key: Key, item: Item, id: AttributeId]
    )
  ->
    (
      Item = Attribute
    ->
      true
    ;
      replace_item(AttributeId, attribute, Key, Attribute)
    )
  ;
    add_item([parent: Id, kind: attribute, key: Key, item: Attribute])
  ).


set_attribute(GraphObjectSet, Attribute) :-
  biocham_command,
  type(GraphObjectSet, {graph_object}),
  type(Attribute, attribute),
  doc('
    Adds an attribute to every vertex or edge in the given set.
    The vertices and the edges are added if needed.
  '),
  get_current_graph(GraphId),
  set_attribute(GraphId, GraphObjectSet, Attribute).


place(NameList) :-
  biocham_command(*),
  type(NameList, '*'(name)),
  doc('
    Sets that the vertices \\argument{NameList} are places.
  '),
  get_current_graph(Id),
  place(Id, NameList).


transition(NameList) :-
  biocham_command(*),
  type(NameList, '*'(name)),
  doc('
    Sets that the vertices \\argument{NameList} are transitions.
  '),
  get_current_graph(Id),
  transition(Id, NameList).


delete_attribute(Id, Attribute) :-
  integer(Id),
  !,
  delete_item([parent: Id, kind: attribute, key: Attribute]).

delete_attribute(GraphObject, Attribute) :-
  biocham_command,
  type(GraphObject, graph_object),
  type(Attribute, name),
  doc('Removes an attribute from \\argument{GraphObject}.'),
  get_current_graph(GraphId),
  find_graph_object(GraphId, GraphObject, Id),
  delete_attribute(Id, Attribute).


list_attributes(GraphObject) :-
  biocham_command,
  type(GraphObject, graph_object),
  doc('List the attributes of \\argument{GraphObject}.'),
  get_current_graph(GraphId),
  find_graph_object(GraphId, GraphObject, Id),
  list_items([parent: Id, kind: attribute]).


:- devdoc('\\section{Public API}').


new_graph(Id) :-
  add_item([kind: graph, key: new_graph, id: Id]).


set_graph_name(Id, Name) :-
  replace_item(Id, graph, Name, Name).


get_current_graph(Id) :-
  get_selection(current_model, current_graph, [Id]).


set_current_graph(Id) :-
  set_selection(current_model, current_graph, [Id]).


get_graph_name(Id, Name) :-
  find_item([id: Id, item: Name]).


get_attribute(Id, Attribute) :-
  integer(Id),
  !,
  (
    Attribute = (Key = _Value)
  ->
    true
  ;
    Key = Attribute
  ),
  item([parent: Id, kind: attribute, key: Key, item: Attribute]).


get_attribute(GraphId, GraphObject, Attribute) :-
  find_graph_object(GraphId, GraphObject, Id),
  get_attribute(Id, Attribute).


set_attribute(GraphId, GraphObjectSet, Attribute) :-
  list(GraphObjectSet),
  !,
  \+ (
    member(GraphObject, GraphObjectSet),
    \+ (
      set_attribute(GraphId, GraphObject, Attribute)
    )
  ).

set_attribute(GraphId, GraphObject, Attribute) :-
  add_graph_object(GraphId, GraphObject, Id),
  set_attribute(Id, Attribute).


place(GraphId, NameList) :-
  kind(GraphId, NameList, place).


place(GraphId, Name, VertexId) :-
  kind(GraphId, Name, place, VertexId).


transition(GraphId, NameList) :-
  kind(GraphId, NameList, transition).


transition(GraphId, Name, VertexId) :-
  kind(GraphId, Name, transition, VertexId).


kind(GraphId, NameList, Kind) :-
  list(NameList),
  !,
  \+ (
    member(Name, NameList),
    \+ (
      kind(GraphId, Name, Kind)
    )
  ).

kind(GraphId, Name, Kind) :-
  kind(GraphId, Name, Kind, _).


kind(GraphId, Name, Kind, VertexId) :-
  add_vertex(GraphId, Name, VertexId),
  set_attribute(VertexId, kind = Kind).


add_vertex(GraphId, Name, VertexId) :-
  (
    item([parent: GraphId, kind: vertex, key: Name, id: VertexId])
  ->
    true
  ;
    add_item([parent: GraphId, kind: vertex, key: Name, id: VertexId])
  ).


add_edge(GraphId, Edge, EdgeId) :-
  Edge = (From -> To),
  add_vertex(GraphId, From, FromId),
  add_vertex(GraphId, To, ToId),
  add_edge(GraphId, Edge, FromId, ToId, EdgeId).


add_edge(GraphId, FromId, ToId, EdgeId) :-
  item([id: FromId, item: From]),
  item([id: ToId, item: To]),
  Edge = (From -> To),
  add_edge(GraphId, Edge, FromId, ToId, EdgeId).


:- devdoc('\\section{Private predicates}').


add_edge(GraphId, Edge, FromId, ToId, EdgeId) :-
  (
    item([parent: GraphId, kind: edge, key: Edge, id: EdgeId])
  ->
    true
  ;
    add_item([parent: GraphId, kind: edge, key: Edge, id: EdgeId]),
    add_dependency(EdgeId, FromId),
    add_dependency(EdgeId, ToId)
  ).


find_vertex(GraphId, Name, Id) :-
  find_item([parent: GraphId, kind: vertex, key: Name, id: Id]).


find_edge(GraphId, Edge, Id) :-
  find_item([parent: GraphId, kind: edge, key: Edge, id: Id]).


find_graph_object(GraphId, GraphObject, Id) :-
  (
    GraphObject = (_From->_To)
  ->
    find_edge(GraphId, GraphObject, Id)
  ;
    find_vertex(GraphId, GraphObject, Id)
  ).


add_graph_object(GraphId, GraphObject, Id) :-
  (
    GraphObject = (_From->_To)
  ->
    add_edge(GraphId, GraphObject, Id)
  ;
    add_vertex(GraphId, GraphObject, Id)
  ).


isolated_vertices(GraphId, VerticesId) :-
  findall(
    VertexId,
    (
      item([parent: GraphId, kind: vertex, id: VertexId, item: Vertex]),
      \+ item([parent: GraphId, kind: edge, item: (Vertex -> _)]),
      \+ item([parent: GraphId, kind: edge, item: (_ -> Vertex)])
    ),
    VerticesId
  ).


isolated_or_attributed_vertices(GraphId, VerticesId) :-
  findall(
    VertexId,
    (
      item([parent: GraphId, kind: vertex, id: VertexId, item: Vertex]),
      (
        \+ item([parent: GraphId, kind: edge, item: (Vertex -> _)]),
        \+ item([parent: GraphId, kind: edge, item: (_ -> Vertex)])
      ->
        true
      ;
        once(item([parent: VertexId, kind: attribute]))
      )
    ),
    VerticesId
  ).


proper_source(GraphId, VerticesId) :-
  findall(
    VertexId,
    (
      item([parent: GraphId, kind: vertex, id: VertexId, item: Vertex]),
      \+ item([parent: GraphId, kind: edge, item: (_ -> Vertex)]),
      once(item([parent: GraphId, kind: edge, item: (Vertex -> _)]))
    ),
    VerticesId
  ).
