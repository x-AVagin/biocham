:- use_module(hybrid).

example_reactions([[[1*gen],[1*tem],c1*'gen_stoch'],[[1*tem],[],c2*('tem_total'/(100*1))],[[1*tem],[1*tem,1*gen],c3*('tem_total'/(100*1))],[[1*gen,1*struc],[1*virus],c4*('gen_stoch'*('struc_total'/(100*1)))]]).

:- begin_tests(hybrid, [setup((clear_model, reset_options))]).

test(build_constraints_list) :-
  hybrid:build_constraints_list([[a],[0],[b,c]], [lower_level1, '0', lower_level3]).

test(build_reaction_data) :-
	Sorted_species = [gen,struc,tem,virus],
  example_reactions(List),
	hybrid:build_reaction_data(List, Sorted_species, Data),
  Data = [[['-1','+0','+1','+0'],c1*'gen_stoch'],[['+0','+0','-1','+0'],c2*('tem_total'/(100*1))],[['+1','+0','+0','+0'],c3*('tem_total'/(100*1))],[['-1','-1','+0','+1'],c4*('gen_stoch'*('struc_total'/(100*1)))]].

test(compute_maximum_change) :-
  example_reactions(List),
  hybrid:compute_maximum_change(List,2).

:- end_tests(hybrid).
