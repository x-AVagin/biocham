:- module(
  tables,
  [
    load_table/1,
    export_table/1,
    select_table/1,
    rename_table/1,
    list_tables/0,
    delete_table/1,
    list_rows/0,
    list_columns/0,
    column/1,
    delete_column/1,
    rename_column/2,
    delete_row/1,
    list_last_state/0,
    set_initial_last_state/0,
    %Public API
    add_table/2,
    get_table_data/1,
    get_current_table/1,
    get_table_headers/1,
    columns/3,
    translate_data/3,
    get_last_value/2,
    get_last_state/2
  ]
).

% Only for separate compilation/linting
:- use_module(doc).


:- doc('Numerical data time series, produced by biological experiments or by simulations, can be stored in \\texttt{.csv} files, and loaded.
They can also be modified by the following commands.').


:- devdoc('\\section{Commands}').


load_table(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    loads the given \\texttt{.csv} file as a table.'),
  \+ (
    filename(InputFile, Filename),
    \+ (
      add_table_file(Filename)
    )
  ).


export_table(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('
    exports the current table into a \\texttt{.csv} file.'),
  get_table_data(Data),
  once(filename(OutputFile, Filename)),
  automatic_suffix(Filename, '.csv', write, FilenameCsv),
  Data = [FirstRow | _],
  FirstRow =.. [row | Columns],
  length(Columns, ColumnCount),
  ColumnMax is ColumnCount - 1,
  (
    \+ (
      between(0, ColumnMax, ColumnIndex),
      get_column_name(ColumnIndex, ColumnName),
      \+ (
        default_column_name(ColumnIndex, ColumnName)
      )
    )
  ->
    Table = Data
  ;
    findall(
      ColumnName,
      (
        get_column_name(0, ColumnName0),
        format(atom(ColumnName), '#~a', ColumnName0)
      ;
        between(1, ColumnMax, ColumnIndex),
        get_column_name(ColumnIndex, ColumnName)
      ),
      ColumnNames
    ),
    TitleRow =.. [row | ColumnNames],
    Table = [TitleRow | Data]
  ),
  working_directory(Dir, Dir),
  absolute_file_name(FilenameCsv, AbsoluteFilenameCsv, [relative_to(Dir)]),
  csv_write_file(AbsoluteFilenameCsv, Table).


get_table_headers(Headers) :-
  get_table_data(Data),
  Data = [FirstRow | _],
  FirstRow =.. [row | Columns],
  length(Columns, ColumnCount),
  ColumnMax is ColumnCount - 1,
  findall(
    ColumnName,
    (
      between(0, ColumnMax, ColumnIndex),
      get_column_name(ColumnIndex, ColumnName)
    ),
    Headers
  ).


list_tables :-
  biocham_command,
  doc('lists the current set of tables.'),
  list_items([model: current_model, kind: table]).


select_table(Table) :-
  biocham_command,
  type(Table, name),
  doc('selects \\argument{Table} to be the current table.'),
  find_item([kind: table, key: Table, id: Id]),
  set_current_table(Id).


rename_table(Name) :-
  biocham_command,
  type(Name, name),
  doc('renames the current table.'),
  get_current_table(Id),
  replace_item(Id, table, Name, Name).


delete_table(TableSet) :-
  biocham_command(*),
  type(TableSet, '*'(name)),
  doc('deletes some tables.'),
  \+ (
    member(Table, TableSet),
    \+ (
      find_item([kind: table, key: Table, id: Id]),
      delete_item(Id)
    )
  ).


list_rows :-
  biocham_command,
  doc('lists the rows of the current table.'),
  get_current_table(Id),
  list_items([parent: Id, kind: row]).


list_columns :-
  biocham_command,
  doc('lists the column names of the current table.'),
  get_current_table(Id),
  list_items([parent: Id, kind: column]).



:- devdoc('\\section{Grammar}').

:- doc('Columns can be designated by their index or their name.').

:- grammar(column).


column(Index) :-
  integer(Index).

column(Name) :-
  name(Name).

delete_column(Columns) :-
  biocham_command(*),
  type(Columns, '*'(column)),
  doc('deletes the given columns from the current table.'),
  get_current_table(Id),
  \+ (
    member(Column, Columns),
    \+ (
      (
        integer(Column)
      ->
        find_item([parent: Id, kind: column, key: Column, id: ColumnId])
      ;
        find_item([parent: Id, kind: column, item: Column, id: ColumnId])
      ),
      delete_item(ColumnId)
    )
  ).


rename_column(Column, Name) :-
  biocham_command,
  type(Column, column),
  type(Name, name),
  doc('renames the given column of the current table.'),
  get_current_table(Id),
  (
    integer(Column)
  ->
    ColumnIndex = Column
  ;
    find_item([parent: Id, kind: column, item: Column, id: ColumnId]),
    get_annotation(ColumnId, index, ColumnIndex)
  ),
  delete_items([parent: Id, kind: column, key: ColumnIndex]),
  declare_column(Id, ColumnIndex, Name).


delete_row(Rows) :-
  biocham_command(*),
  type(Rows, '*'(number)),
  doc('deletes the given rows from the current table.'),
  get_current_table(Id),
  \+ (
    member(Row, Rows),
    \+ (
      find_item([parent: Id, kind: column, key: Row, id: RowId]),
      delete_item(RowId)
    )
  ).


:- devdoc('\\section{Public API}').


add_table(Name, Table) :-
  add_item([kind: table, key: Name, item: Name, id: Id]),
  Table = [FirstRow | Others],
  FirstRow =.. [row | Columns],
  Columns = [FirstColumn | OtherColumns],
  (
    atom_concat('#', FirstColumnName, FirstColumn)
  ->
    unquote(FirstColumnName, UnquotedFirstColumnName),
    declare_column(Id, 0, UnquotedFirstColumnName),
    \+ (
      nth1(ColumnIndex, OtherColumns, ColumnName),
      \+ (
        unquote(ColumnName, UnquotedColumnName),
        declare_column(Id, ColumnIndex, UnquotedColumnName)
      )
    ),
    Data = Others
  ;
    length(Columns, ColumnCount),
    \+ (
      ColumnMax is ColumnCount - 1,
      between(0, ColumnMax, ColumnIndex),
      \+ (
        default_column_name(ColumnIndex, ColumnName),
        declare_column(Id, ColumnIndex, ColumnName)
      )
    ),
    Data = Table
  ),
  \+ (
    nth0(RowIndex, Data, Row),
    \+ (
      add_item([parent: Id, kind: row, key: RowIndex, item: Row, id: RowId]),
      set_annotation(RowId, index, RowIndex),
      at_delete(RowId, tables:perform_row_delete(RowId))
    )
  ),
  set_current_table(Id).


unquote(QuotedColumnName, UnquotedColumnName) :-
  atom_concat('"', ColumnNameQuote, QuotedColumnName),
  atom_concat(UnquotedColumnName, '"', ColumnNameQuote),
  !.

unquote(QuotedColumnName, UnquotedColumnName) :-
  atom_concat('\'', ColumnNameQuote, QuotedColumnName),
  atom_concat(UnquotedColumnName, '\'', ColumnNameQuote),
  !.

unquote(UnquotedColumnName, UnquotedColumnName).


get_table_data(Data) :-
  devdoc('unifies Data to the list of rows of the current table.'),
  get_current_table(Id),
  set_counter(row, 0),
  findall(
    Row,
    (
      repeat,
      (
        count(row, RowIndex),
        item([parent:Id, kind: row, key: RowIndex, item: Row])
      ->
        true
      ;
        !,
        fail
      )
    ),
    Data
  ).


get_column_name(Index, Name) :-
  get_current_table(Id),
  find_item([parent: Id, kind: column, key: Index, item: Name]).


get_current_table(Id) :-
  (
    get_selection(current_model, current_table, [Id])
  ->
    true
  ;
    throw(error(no_current_table))
  ).


prolog:message(error(no_current_table)) -->
   ['No current table. Consider running numerical_simulation.'].


columns(Id, ColumnIndex, Name) :-
  item([parent: Id, kind: column, item: Name, id: ColumnId]),
  get_annotation(ColumnId, index, ColumnIndex).


:- devdoc('\\section{Private predicates}').


models:add_file_suffix('csv', tables:add_table_file).


add_table_file(Filename) :-
  automatic_suffix(Filename, '.csv', read, FilenameCsv),
  csv_read_file(FilenameCsv, Table),
  add_table(FilenameCsv, Table).


declare_column(Id, ColumnIndex, Name) :-
  add_item(
    [parent: Id, kind: column, key: ColumnIndex, item: Name, id: ColumnId]
  ),
  set_annotation(ColumnId, index, ColumnIndex),
  at_delete(ColumnId, tables:perform_column_delete(ColumnId)).


perform_column_delete(ColumnId) :-
  get_parent(ColumnId, TableId),
  (
    deleting(TableId)
  ->
    true
  ;
    get_annotation(ColumnId, index, ColumnIndex),
    \+ (
      item([parent:TableId, kind: row, item: Row, id: RowId]),
      \+ (
        get_annotation(RowId, index, RowIndex),
        Row =.. [row | RowItems],
        nth0(ColumnIndex, RowItems, _Deleted, NewRowItems),
        NewRow =.. [row | NewRowItems],
        replace_item(RowId, row, RowIndex, NewRow)
      )
    ),
    \+ (
      item([parent:TableId, kind: column, item: Column0, id: ColumnId0]),
      get_annotation(ColumnId0, index, ColumnIndex0),
      ColumnIndex0 > ColumnIndex,
      \+ (
        ColumnIndex1 is ColumnIndex0 - 1,
        replace_item(ColumnId0, column, ColumnIndex1, Column0),
        set_annotation(ColumnId0, index, ColumnIndex1)
      )
    )
  ).


perform_row_delete(RowId) :-
  get_parent(RowId, TableId),
  (
    deleting(TableId)
  ->
    true
  ;
    get_annotation(RowId, index, RowIndex),
    \+ (
      item([parent:TableId, kind: row, item: Row0, id: RowId0]),
      get_annotation(RowId0, index, RowIndex0),
      RowIndex0 > RowIndex,
      \+ (
        RowIndex1 is RowIndex0 - 1,
        replace_item(RowId0, row, RowIndex1, Row0),
        set_annotation(RowId0, index, RowIndex1)
      )
    )
  ).


default_column_name(ColumnIndex, Name) :-
  format(atom(Name), 'column~d', [ColumnIndex]).


set_current_table(Id) :-
  set_selection(current_model, current_table, [Id]).


translate_data(Add, Data, NewData) :-
  maplist(add_to_first_column(Add), Data, NewData).


add_to_first_column(Add, Row, NewRow) :-
  Row =.. [row, H | T],
  HH is H + Add,
  NewRow =.. [row, HH | T].




list_last_state:-
    biocham_command,
    doc('lists the last state of the current simulation trace.'),
    get_last_state(Variables, Values),
    maplist(pretty_present, Variables, Values).

pretty_present(M, V):-
    print(initial_state(M = V)),nl.


get_last_state(Variables, Values):-
    devdoc('returns the lists of variables and of values in the last state of the current simulation trace.'),
    get_table_headers(['Time' | Variables]),
    get_table_data(Data),
    last(Data, LastRow),
    LastRow=..[row, _ | Values].

  
get_last_value(Variable, Value):-
    devdoc('returns the value of a variable in the last state of the current simulation trace.'),
    get_last_state(Variables, Values),
    nth0(I, Variables, Variable),
    !,
    nth0(I, Values, Value).


set_initial_last_state:-
    biocham_command,
    doc('sets the last state as new initial state.'),
    get_last_state(Variables, Values),
    clear_initial_state,
    set_initial_values(Variables, Values).

