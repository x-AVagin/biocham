
[![pipeline status](https://gitlab.inria.fr/lifeware/biocham/badges/develop/pipeline.svg)](https://gitlab.inria.fr/lifeware/biocham/commits/develop)
[![coverage report](https://gitlab.inria.fr/lifeware/biocham/badges/develop/coverage.svg)](https://gitlab.inria.fr/lifeware/biocham/commits/develop)
[![License](https://img.shields.io/badge/license-GPLv2-orange)](https://gitlab.inria.fr/lifeware/biocham/raw/develop/LICENSE)

# Biocham

This README file is mostly aimed at people *contributing* to Biocham

## Getting Started

After `git clone` you might need to run once the [install.sh](./install.sh)
script in order to install all build dependencies and to try to compile the
project.

After that `make` will build the whole project, including documentation,
tests, debug version, etc., which might be quite slow.
You might want to use `make biocham`, `make biocham_debug` (a SWI-Prolog
toplevel with all biocham files loaded) or `make test_<unit>` (to run a single
test unit) for faster feedback.

Note that `make test` will give you, if available, some test coverage
estimation, and that `make test_<unit>` will attempt to get into more details
about which clauses were not covered (but this highly depends on the SWIPL
version).

## Documentation

The file [toc.org](./toc.org) is used for three things:

- for creating the Biocham runtime from the source code,
  by defining the order in which the source files have to be loaded;
- for generating the Biocham documentation in the directories [doc](./doc/)
  and [devdoc](./devdoc/)
  (from annotations in the source code using the runtime for running the examples),
  by defining the order of the sections and their title;
- for generating the Biocham notebook and web interface
  with the same structure as the documentation.

It is also recommended to use
[PlDoc](https://www.swi-prolog.org/pldoc/doc_for?object=section(%27packages/pldoc.html%27)) comments, the resulting documentation is
in [doc/pldoc](./doc/pldoc/).

## Documentation example

In the [odefunction.pl](./odefunction.pl) file:

- a module is defined for the `add_function` command that is then exported;

- the `devdoc` goal creates a new section in the developer's documentation (in
  the loading order fixed by `toc.org`)

- the predicate `add_function` is then defined:
```prolog
add_function(FunctionList) :-
  % the biocham command has a variable number of arguments treated like a list
  biocham_command(*),
  type(FunctionList, '*'(term = term)),
  % it will appear in the `doc/index.html` file with an example that will be
  % executed and inserted
  doc('
    adds reactions to compute the result of a function of the current variables in the concentration of a new variable at steady state.
    \\begin{example}
  '),
  biocham_silent(clear_model),
  biocham(present(x,1)),
  biocham(present(y,3)),
  biocham(add_function(z=x+y)),
  biocham(list_reactions),
  biocham(list_ode),
  doc('
    \\end{example}
  ‘),
  % now comes the proper Prolog implementation
  new_ode_system(OdeSystem),
  export_ode(FunctionList, OdeSystem),
  import_reactions_from_ode_system(OdeSystem),
  delete_item(OdeSystem).
```

## Tests

All `.plt` files are executed as test programs. See for instance
[odefunction.plt](./odefunction.plt).

## General stuff

There is an [EditorConfig](https://editorconfig.org) file that specifies
indentation settings and such: [.editorconfig](./.editorconfig), please make
your editor aware of it.

It might be a good idea to follow [Prolog programming
guidelines](https://lifeware.inria.fr/~soliman/post/prolog_guidelines/).

We tend to use GitFlow as our branching model, see [our internal
Wiki](https://lifeware.inria.fr/wiki/Internal/WorkingOnBiocham) for more
details.
