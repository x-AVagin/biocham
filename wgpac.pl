:- module(
  wgpac,
  [
    % Grammars
    wgpac/1,
    wgpac_box/1,
    wgpac_named/1,
    wgpac_name/2,
    op(600, xfy, ::),
    op(550, fy, integral),
    % Commands
    compile_wgpac/1
  ]).

:- doc('Biocham can compile a GPAC circuit (Shannon\'s General Purpose Analog Computer) into an abstract CRN using positive as well as negative concentration values. Only weak GPACs, in which the integration gate is with respect to time and not a variable, are considered. The variables associated to the different points of the circuits can be named with the "::" operator. Dy default they are named x0, x1,... The syntax of weak GPAC circuits is as follows:').

:- grammar(wgpac).

wgpac(Y :: B) :-
  object(Y),
  wgpac_box(B).

wgpac(B) :-
  wgpac_box(B).

:- grammar(wgpac_box).

wgpac_box(W1 + W2) :-
  wgpac(W1),
  wgpac(W2).

wgpac_box(W1 * W2) :-
  wgpac(W1),
  wgpac(W2).

wgpac_box(integral W) :-
  wgpac(W).

wgpac_box(K) :-
  concentration(K).

:- grammar(wgpac_named).

wgpac_named(Y :: B) :-
  object(Y),
  wgpac_box(B).

wgpac_named(Y) :-
  object(Y).


wgpac_name(Y :: _, Y) :-
  object(Y).

wgpac_name(Y, Y) :-
  object(Y).

wgpac_free(Y, _) :-
  object(Y),
  !.

wgpac_free(Y :: B, Z) :-
  !,
  Y \== Z,
  wgpac_box_free(B, Z).

wgpac_free(B, Z) :-
  wgpac_box_free(B, Z).

wgpac_box_free(K, _) :-
  concentration(K),
  !.

wgpac_box_free(W1 + W2, Z) :-
  wgpac_free(W1, Z),
  wgpac_free(W2, Z).

wgpac_box_free(W1 * W2, Z) :-
  wgpac_free(W1, Z),
  wgpac_free(W2, Z).

wgpac_box_free(integral W, Z) :-
  wgpac_free(W, Z).

wgpac_well_formed(Y) :-
  object(Y).

wgpac_well_formed(Y :: B) :-
  object(Y),
  wgpac_box_free(B, Y).

:- doc('The option fast rate (defaulting to 1000) is intended to define a high rate constant for reactions faster than the other reactions.
    It is used \\begin{itemize}\\item in the reactions for computing the results of the sum and product GPAC blocks, 
    \\item and in the annihilation reactions between the molecular species for the positive and negative values of a real valued variable.  \\end{itemize}  
  ').

:- initial(option(fast_rate: 1000)).


compile_wgpac(WgpacSet) :-
  biocham_command,
  type(WgpacSet, {wgpac}),
  doc('compiles a set of weak GPAC circuits into a reaction network.'),
  set_counter(fresh, 0),
  option(fast_rate, arithmetic_expression, Rate, ''),
  set_parameter(fast, Rate),
  \+ (
    member(Wgpac, WgpacSet),
    \+ (
      compile_wgpac_impl(Wgpac)
    )
  ).


compile_wgpac_impl(Y) :-
  object(Y),
  !,
  debug(wgpac, 'Start object compilation of: ~w', [Y]).

compile_wgpac_impl(Wgpac) :-
  Wgpac = Y :: B,
  wgpac_well_formed(Wgpac),
  !,
  debug(wgpac, 'Start named compilation of: ~w', [Wgpac]),
  (
    B = K,
    number(K) %concentration(K)
  ->
    present([Y], K)
  ;
    B = W1 + W2
  ->
    debug(wgpac, 'Found Sum box', []),
    (
      wgpac_named(W1)
    ->
      debug(wgpac, 'Found named at position 1: ~w', [W1]),
      wgpac_name(W1, X1),
      compile_wgpac_impl(W1)
    ;
      debug(wgpac, 'Found anonymous at position 1: ~w', [W1]),
      count(fresh, N1),
      atom_concat('x', N1, X1),
      compile_wgpac_impl(X1 :: W1)
    ),
    (
      wgpac_named(W2)
    ->
      debug(wgpac, 'Found named at position 2: ~w', [W2]),
      wgpac_name(W2, X2),
      compile_wgpac_impl(W2)
    ;
      debug(wgpac, 'Found anonymous at position 2: ~w', [W2]),
      count(fresh, N2),
      atom_concat('x', N2, X2),
      compile_wgpac_impl(X2 :: W2)
    ),

    add_reaction(fast * [X1] for _ =[X1]=> Y),
    add_reaction(fast * [X2] for _ =[X2]=> Y),
    add_reaction(fast * [Y] for Y => _)
  ;
    B = W1 * W2
  ->
    debug(wgpac, 'Found Product box', []),
    (
      wgpac_named(W1)
    ->
      debug(wgpac, 'Found named at position 1: ~w', [W1]),
      wgpac_name(W1, X1),
      compile_wgpac_impl(W1)
    ;
      debug(wgpac, 'Found anonymous at position 1: ~w', [W1]),
      count(fresh, N1),
      atom_concat('x', N1, X1),
      compile_wgpac_impl(X1 :: W1)
    ),
    (
      wgpac_named(W2)
    ->
      debug(wgpac, 'Found named at position 2: ~w', [W2]),
      wgpac_name(W2, X2),
      compile_wgpac_impl(W2)
    ;
      debug(wgpac, 'Found anonymous at position 2: ~w', [W2]),
      count(fresh, N2),
      atom_concat('x', N2, X2),
      compile_wgpac_impl(X2 :: W2)
    ),

    add_reaction(fast * [X1] * [X2] for _ =[X1 + X2]=> Y),
    add_reaction(fast * [Y] for Y => _)
  ;
    B = integral W1
  ->
    (
      wgpac_named(W1)
    ->
      wgpac_name(W1, X1),
      compile_wgpac_impl(W1)
    ;
      count(fresh, N),
      atom_concat('x', N, X1),
      compile_wgpac_impl(X1 :: W1)
    ),
    add_reaction(_ =[X1]=> Y)
  ).

compile_wgpac_impl(Wgpac) :-
  debug(wgpac, 'Start anonymous compilation of: ~w', [Wgpac]),
  count(fresh, N),
  atom_concat('x', N, Y),
  wgpac_well_formed(Y :: Wgpac),
  compile_wgpac_impl(Y :: Wgpac).

:- doc('\\begin{example}\n
  Compilation of a GPAC circuit generating cosine as a function of time (using positive and negative concentrations) :\n
  ').
:- biocham_silent(clear_model).
:- biocham(compile_wgpac(f :: integral integral (-1 * f))).
:- biocham(present(f,1)).
:- biocham(list_model).
:- biocham(numerical_simulation(time:10)). %, method:msbdf)).
:- biocham(plot).
:- doc('
  \\end{example}
  ').

debug_iswgpac(W) :-
    (wgpac(W) -> write('WGPAC') ; write('not WGPAC')),
    nl.

debug_iswgpacbox(B) :-
    (wgpacbox(B) -> write('WGPAC BOX') ; write('not WGPAC BOX')),
    nl.


prolog:message(found(Object)) -->
  ['Found ~w'-[Object]].


% redefine the way things are printed by prolog
% necessary since we go through portray to avoid quoting capital letters 'A'

user:portray(integral(Thing)) :-
    format("integral ~p",[Thing]).
