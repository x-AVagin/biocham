:- use_module(library(plunit)).


:- begin_tests(reaction_editor, [setup((clear_model, reset_options))]).


test(
  'compound',
  [Reactions == ['MA'(1) for 2 * a + 2 * b => 2 * 'a-b']]
) :-
  clear_model,
  command(add_reaction(2 * a + 2 * b => 2 * a-b)),
  all_items([kind: reaction], Reactions).

test(
  'catalyst',
  [Reactions == ['MA'(1) for a+c+2*b => a+b+c, 'MA'(1) for a+b+c => a+c+2*b]]
) :-
  clear_model,
  add_reaction(a + b + c <=[ b ]=> a + c),
  simplify_all_reactions,
  all_items([kind: reaction], Reactions).

test(
  'reversible reaction',
  [true(Reactions == [1 for a => b, 2 for b => a])]
) :-
  clear_model,
  command((1, 2) for a <=> b),
  all_items([kind: reaction], Reactions).

test(
'michaelis menten reduction',
  [Reactions == ['E'*'S'*'C' for 'C'+'E'+'S'=> 'C'+'E'+'P']]
) :-
  command(load('library:examples/michaelis-menten/mm.bc')),
  command(delete_reaction('C'=>'E'+'S')),
  command(merge_reactions('E'+'S'=>'C','C'=>'E'+'P')),
  all_items([kind: reaction], Reactions).

test(
'michaelis menten reduction C',
  [Reactions == ['E'*'S'*'C' for 'C'+'E'+'S'=> 'C'+'E'+'P']]
) :-

  command(load('library:examples/michaelis-menten/mm.bc')),
  command(merge_reactions('E'+'S'=>'C','C'=>'E'+'P')),
  command(delete_reaction('C'=>'E'+'S')),
  all_items([kind: reaction], Reactions).

:- end_tests(reaction_editor).
