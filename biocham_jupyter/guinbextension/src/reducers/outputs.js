import { ADD_GRAPHICAL_OUTPUT,
         ADD_STRING_OUTPUT,
         DELETE_GRAPHICAL_OUTPUT,
         CLEAR_CONSOLE } from '../constants/actionTypes';
import { getMetadata } from 'Utils/utils';

const initialState = {
    graphicalOutputs: getMetadata('outputs', 'graphicalOutputs', []),
    consoleOutputs: getMetadata('outputs', 'consoleOutputs', [])
};

export default (state = initialState, action) => {
    switch (action.type) {
    case ADD_GRAPHICAL_OUTPUT:
        return {
            ...state,
            graphicalOutputs: [
                ...state.graphicalOutputs,
                action.data
            ]
        };
    case DELETE_GRAPHICAL_OUTPUT:
        const items = state.graphicalOutputs;
        return {
            ...state,
            graphicalOutputs: 
            items.filter((g, index) => index !== action.index)
        };
    case ADD_STRING_OUTPUT:
        return {
            ...state,
            consoleOutputs: [
                ...state.consoleOutputs,
                {
                    code: action.data.code,
                    output: action.data.output
                }
            ]
        };
    case CLEAR_CONSOLE:
        return {
            ...state,
            consoleOutputs: []
        };
    default:
        return state;
    }
};
