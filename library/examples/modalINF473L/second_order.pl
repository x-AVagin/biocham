/** <second_order> second-order moment closure
 *
 *  example of use of the derivatives module to compute second-order ODEs
 *
 * @author Sylvain Soliman
 * @license GPL2
 */

:- module(second_order, [second_order/0]).
:- reexport(minibiocham).
:- use_module(symbolic).

%! second_order is det.
%
% using the current CRN create the ODE system corresponding to the second_order
% moment closure
second_order :-
  new_ode,
  forall(           % iterate some side-effect
    species(S),     % for all species
    second_order(S) % call the predicate second_order/1
  ),
  forall(           % map the initial state in a trivial way
    initial(S, I),
    assertz(ode_init(S, I))
  ),
  forall(           % map the parameters in a trivial way
    parameter(K, V),
    assertz(ode_par(K, V))
  ).                % no function to add with ode_func


%! second_order(-Species:atom) is det
%
% adds the dx/dt part for x=Species corresponding to the second-order moment
% closure with respect to current CRN
second_order(S) :-
  % first ensure there's some existing dx/dt
  (
    ode_dxdt(S, _)
  ->
    true
  ;
    assertz(ode_dxdt(S, 0))
  ),
  % now iterate an update on this dx/dt
  forall(
    reaction(K, C),       % iterate for all reactions with rate K and change C
    add_change(C, K, S)   % add to dx/dt the corresponding variation of S
  ),
  % postprocessing: remove the ugly sum, simplify it, add it back
  retract(ode_dxdt(S, E)),
  simplify(E, SE),
  assertz(ode_dxdt(S, SE)).


%! add_change(-ChangeVector:list, -KineticLaw, -Species) is det
%
% iterate on the ChangeVector list to find the change for Species and add the
% corresponding KineticLaw*Change to dSpecies/dt

% No change, nothing to do
add_change([], _, _).

add_change([(S1, _) | L], K, S2) :-
  % the change is on another species, ignore and continue the change vector
  S1 \= S2,
  add_change(L, K, S2).

add_change([(S, C) | _], K, S) :-
  % the change is on S, get the current dS/dt, replace it with its sum with
  % C*K
  retract(ode_dxdt(S, E)),
  assertz(ode_dxdt(S, E + C*K)).
