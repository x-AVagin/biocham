:- module(rosenbrock,
   [
      ode_solver/1
   ] 
).

% Only for separate compilation and linting
:- use_module(arithmetic_rules).
:- use_module(counters).
:- use_module(doc).
:- use_module(util).

%! rosenbrock_init
%
% Initialize coefficients for the rosenbrock method, the errors and the minimal step size
% for events handling
 
rosenbrock_init(Epsilon_abs) :-
  nb_setval(not_zero_error, Epsilon_abs),
  nb_setval(k_method,stiff),
  nb_setval(event_step_size, 1e-3),
  nb_setval(step_doubling_error, Epsilon_abs), % Control the method's precision
  SAFE = 0.9, GROW = 2.5, PGROW = -0.25,
  SHRINK = 0.5, PSHRINK is -1.0/3.0, ERRCON = 0.1296,
  GAM is 1.0/2.0, A21=2.0, A31 is 48.0/25.0, A32 is 6.0/25.0, C21 = -8.0,
  C31 is 372.0/25.0, C32 is 12.0/5.0, C41 is -112.0/125.0,
  C42 is -54.0/125.0, C43 is -2.0/5.0,
  B1 is 19.0/9.0, B2 is 1.0/2.0, B3 is 25.0/108.0, B4 is 125.0/108.0,
  E1 is 17.0/54.0, E2 is 7.0/36.0, E3 is 0.0, E4 is 125.0/108.0,
  nb_setval(k_rbk_safe,SAFE),
  nb_setval(k_rbk_grow,GROW),
  nb_setval(k_rbk_pgrow,PGROW),
  nb_setval(k_rbk_shrink,SHRINK),
  nb_setval(k_rbk_pshrink,PSHRINK),
  nb_setval(k_rbk_errcon,ERRCON),
  nb_setval(k_rbk_gam,GAM),
  nb_setval(k_rbk_a21,A21),
  nb_setval(k_rbk_a31,A31),
  nb_setval(k_rbk_a32,A32),
  nb_setval(k_rbk_c21,C21),
  nb_setval(k_rbk_c31,C31),
  nb_setval(k_rbk_c32,C32),
  nb_setval(k_rbk_c41,C41),
  nb_setval(k_rbk_c42,C42),
  nb_setval(k_rbk_c43,C43),
  nb_setval(k_rbk_b1,B1),
  nb_setval(k_rbk_b2,B2),
  nb_setval(k_rbk_b3,B3),
  nb_setval(k_rbk_b4,B4),
  nb_setval(k_rbk_e1,E1),
  nb_setval(k_rbk_e2,E2),
  nb_setval(k_rbk_e3,E3),
  nb_setval(k_rbk_e4,E4).

:- dynamic(k_fail/0).

:- dynamic(rsbk_continue/0).


%! ode_solver(+Options)
%
% Start of the ode solver, cf numerical_simulation.pl for Options

ode_solver([
    fields: Fields,
    equations: Equations,
    initial_values: InitialState,
    initial_parameter_values: InitialParameters,
    events: Events,
    method: _,
    error_epsilon_absolute: Epsilon_abs,
    error_epsilon_relative: _,
    initial_step_size: _,
    maximum_step_size: MaxStSz,
    minimum_step_size: MinStSz,
    precision: _,
    filter: _,
    time_initial: InitialTime,
    time_final: Duration,
    jacobian: Jacobian
  ]) :- 
  debug(rosenbrock, "begin ode_solver", []),
  set_vector(InitialParameters, Parameters),
  set_vector(Equations, EquationsVec),
  set_matrix(Jacobian, JacobianMat),
  initialize_functions(Functions),
  initialize_names(Fields, Names),
  (
    nb_current(rsbk_continue,yes)
  ->
    get_last_row(InitialTime2, TrueInitialState),
    prepare_rosenbrock_events(TrueInitialState, Parameters, InitialTime2),
    prepare_conditional_events(TrueInitialState, Parameters, InitialTime2),
    TrueInitialTime is InitialTime2
  ;
    assertz(saved_row(Names)), %initialize the label of the numerical table
    set_vector(InitialState, VectorState),
    eval_vector(VectorState, _S, Parameters, State),
    split_events(Events, RegularEvents, TimeEvents),
    nb_setval(events_list,RegularEvents),
    nb_setval(time_events_list,TimeEvents),
    nb_setval(last_time_event, -1),
    prepare_rosenbrock_events(State, Parameters, InitialTime),
    prepare_conditional_events(State, Parameters, InitialTime),
    log_current_row(InitialTime, State, Parameters, Functions, Fields),
    TrueInitialTime is InitialTime
  ),
  nb_setval(max_step_size, MaxStSz),
  nb_setval(min_step_size, MinStSz),
  rosenbrock_init(Epsilon_abs),
  rosenbrock(EquationsVec,TrueInitialTime,Duration,Parameters,Functions,JacobianMat,Fields),
  retractall(k_fail).


%! initialize_functions(-Functions)
%
% Constructs a vector which contains all the functions defined in the model

initialize_functions(Functions) :-
  all_items([kind: function], AllFunctions),
  initialize_functions_list(AllFunctions, FunctionsList),
  length(FunctionsList, Number),
  nb_setval(functions_number,Number),
  set_vector(FunctionsList, Functions).

initialize_functions_list([],[]) :- !.

initialize_functions_list([Function|Functions], FunctionList) :- 
  Function =.. [function,TrueFunction],
  TrueFunction =.. [=,Head,Expression],
  initialize_functions_list(Functions,NextList),
  ( % skip over kinetic functions
    is_kinetics_function(Head)
  ->
    FunctionList = NextList
  ;
    numerical_simulation:convert_expression(Expression,IndexedExpression),
    NewFunction = function(Head,IndexedExpression),
    append(NextList,[NewFunction],FunctionList)
  ).


%! is_kinetics_function(F)
%
% Manually check if F is a kinetic function
% Rq: It'll have to be updated if others kinetics are added

is_kinetics_function(Head) :-
  Head = 'MA'(k);
  Head = 'MAI'(k);
  Head = 'MM'('Vm', 'Km');
  Head = 'Hill'('Vm', 'Km', n);
  Head = 'HillI'('Vm', 'Km', n).


%! add_functions_names(+VarName, -VarFuncName)
%
% Add the name of all functions to construct the FullNames list

add_functions_names(VarName, VarFuncName) :-
  (
    nb_current(functions_list,_)
  ->
    nb_getval(functions_list, Functions),
    maplist(give_function_name, Functions, FuncName),
    append(VarName, FuncName, VarFuncName)
  ;
    VarFuncName = VarName
  ).

give_function_name(function(Name, _Expr), Name).


%! initialize_names(+Fields, -Names)
%
% Extract the name of the different fields to be plotted

initialize_names_int([], []).

initialize_names_int([Name:_|FTail], [Name|NTail]) :-
  initialize_names_int(FTail, NTail).

initialize_names([Name:_|FTail], [NName|NTail]) :-
  atom_concat('#', Name, NName),
  initialize_names_int(FTail, NTail).


%! log_current_row(+Time, +State, +Parameters, +Functions, +Fields)
%
% evaluate the elements of list Fields and store them

log_current_row(Time, State, Parameters, Functions, Fields) :-
  prepare_full_state(State, Parameters, Functions, Time, FullState),
  nb_setval(last_row, FullState),
  prepare_row(Time, State, Parameters, Fields, ToSave),
  assertz(saved_row(ToSave)).

prepare_row(_Time, _State, _Param, [], []) :- !.

prepare_row(Time, State, Parameters, [_N:t|FTail], [Time|VTail]) :-
  !,
  prepare_row(Time, State, Parameters, FTail, VTail).

prepare_row(Time, State, Parameters, [_N:x(N)|FTail], [Val|VTail]) :-
  !,
  Np is N+1, % shift of 2 for start at index 1 and skip the Time row
  get_value(State, Np, Val),
  prepare_row(Time, State, Parameters, FTail, VTail).

prepare_row(Time, State, Parameters, [_N:expression(Expr)|FTail], [Val|VTail]) :-
  !,
  eval_coeff(Expr, State, Parameters, Time, Val),
  prepare_row(Time, State, Parameters, FTail, VTail).


%! prepare_full_state(+State, +Parameters, +Functions, +Time, -FullState)
%
% Add the time and the value of all functions to construct the FullState vector

prepare_full_state(State, Parameters, Functions, Time, FullState) :-
  get_vector_as_list(State, State_list),
  get_vector_as_list(Functions, Functions_list),
  maplist(eval_function(State, Parameters, Time), Functions_list, Values),
  append([[Time], State_list, Values], FullState).

eval_function(State, Parameters, Time, function(_Name, Expr), Value) :-
  eval_coeff(Expr, State, Parameters, Time, Value).


%! eval_matrix(+Matrix, +States, +Parameters, -Evaluated_Matrix)
%
% insert all the numerical value in a symbolic matrix
% States and Parameters are vectors

eval_matrix(Matrix, State, Parameters, MatrixEval) :-
  findall(
    EvalVec,
    (
      get_row(Matrix, _Index, Vector),
      eval_vector(Vector, State, Parameters, EvalVec)
    ),
    List
  ),
  set_matrix(List, MatrixEval).


eval_vector(Vector, State, Parameters, VectorEval) :-
  findall(
      Eval,
      (
        get_value(Vector, _Index, Element),
        eval_coeff(Element, State, Parameters, _, Eval)
      ),
      List
  ),
  set_vector(List, VectorEval).


%! eval_coeff(+Expr, +States, +Parameters, +Time, -Evaluation)
%
% Evaluate an expression by rewritting the occurence of the concentration and parameters,
% simplyfying the resulting expression and calling is.
% The rewritting is delegated to rw_coeff


eval_coeff(Expr, States, Parameters, Time, Evaluation) :-
  rw_coeff(Expr, States,  Parameters,  Time,  Rw_expr),!,
  Evaluation is Rw_expr.


rw_coeff(Element, _, _, _, Element) :-
  number(Element),!.

rw_coeff([IndexVariable], State, _, _, ElementEvaluated) :- !,
  IVp is IndexVariable+1,
  get_value(State, IVp, ElementEvaluated).

rw_coeff(p(ParameterIndex), State, Parameters, Time, ElementEvaluated) :- !,
  PIp is ParameterIndex+1,
  get_value(Parameters, PIp, Element_tempo),
  rw_coeff( Element_tempo, State,  Parameters,  Time,  ElementEvaluated).

rw_coeff(A+B, State, Parameters, Time, EA+EB) :- !,
  rw_coeff(A, State, Parameters, Time, EA),
  rw_coeff(B, State, Parameters, Time, EB).

rw_coeff(A-B, State, Parameters, Time, EA-EB) :- !,
  rw_coeff(A, State, Parameters, Time, EA),
  rw_coeff(B, State, Parameters, Time, EB).

rw_coeff(A*B, State, Parameters, Time, EA*EB) :- !,
  rw_coeff(A, State, Parameters, Time, EA),
  rw_coeff(B, State, Parameters, Time, EB).

rw_coeff(A/B, State, Parameters, Time, EA/EB) :- !,
  rw_coeff(A, State, Parameters, Time, EA),
  rw_coeff(B, State, Parameters, Time, EB).

rw_coeff(random, _, _, _, Result) :-
   !,
   Result is random_float.

rw_coeff(infinity, _, _, _, Result) :-
  !,
  catch( % old SWIPL (7.2.3) doesn't have inf/0
    Result is inf,
    error(type_error(evaluable, inf/0), _),
    current_flag(max_tagged_integer, Result)
  ).

rw_coeff(t, _, _, Time, Time) :-
   !.

rw_coeff(step_size, _, _, _, Result) :-
   !,
   nb_getval(hdid,Result).

rw_coeff(Function, State,  Parameters,  Time,  RWFunction) :-
  Function =.. [Functor|Arguments],
  !,
  rw_coeff_list(Arguments, State, Parameters, Time, RWArguments),
  RWFunction =.. [Functor|RWArguments].

rw_coeff_list([], _States, _Parameters, _Time, []) :- !.
rw_coeff_list([Head|Tail], States, Parameters, Time, [EHead|ETail]) :-
  rw_coeff(Head, States, Parameters, Time, EHead),
  rw_coeff_list(Tail, States, Parameters, Time, ETail).


%! update_state(+Old, +StepSize, +Grad, -New) 
%
% Compute New = Old + StepSize x Grad
% This a tool for the rosenbrock method

update_state(CurrentVector, Stepsize, GradVector, Next) :-
  findall(
    NV,
    (
      get_value(CurrentVector, Index, CV),
      get_value(GradVector, Index, GV),
      NV is CV + Stepsize*GV
    ),
    List
  ),
  set_vector(List, Next).


%! get_last_row(-Time, -CurrentState)
%
% 

get_last_row(Time,CurrentState):-
  nb_getval(last_row, [Time|CurrentList]),
  set_vector(CurrentList, CurrentState).


/*********************/
/* Rosenbrock method */
/*********************/

%! rosenbrock(+Equations, +InitialTime, +Duration, +Parameters, +Variables, +Functions, +Jacobian, +Fields)
%
% Parameters are stored within the global variable parameters_list
% The current state of the simulation if obtained in the final row of the global
% variable numerical_table

rosenbrock(Equations, InitialTime, Duration, Parameters, Functions, Jacobian, Fields) :-
  debug(rosenbrock, "begin rosenbrock", []),
  FinalTime is InitialTime + Duration,
  nb_getval(max_step_size, MaxStSz),
  nb_getval(min_step_size, MinStSz),
  StepSizeMax is Duration*MaxStSz,
  StepSizeMin is Duration*MinStSz,
  nb_setval(event_step_size, StepSizeMin),
  (
    StepSizeMax =:= 0.0,
    !,
    debug(error, "Simulation length cannot be 0", []),
    fail
  ;
    true
  ),
  nb_setval(hmax,StepSizeMax),
  (
    nb_current(h, _) % same as nb_getval but fail if 'h' do not exist instead of crashing
  ->
    true
  ;
    StepSize is StepSizeMax/5,
    nb_setval(h,StepSize),
    nb_setval(hdid,StepSize),
    nb_setval(h_init,StepSize),
    nb_setval(k_time_units,2)
  ),
  nb_setval(event_handling, false),% flag to know if we are currently dealing with events 
  repeat,
    get_last_row(Time, FullState),
    debug(rosenbrock, "entering step for ~p", [Time]),
    shape(Equations, NumberTrueVariable), % Cut the function values at the end of FullState 
    slice_element(FullState, 1, NumberTrueVariable, CurrentState),
    handle_time_event(CurrentState, Parameters, Time, NextEventTime),
    % Tailor cutting for the integration time step
    nb_getval(event_step_size, SmallestStep),
    EventStep is max((NextEventTime - Time), SmallestStep),
    FinalStep is (FinalTime - Time),
    MaxStep is min(EventStep, FinalStep),
    % Here is the true rosenbrock step
    rosenbrock_step(Equations, Jacobian, Time, CurrentState, Parameters, MaxStep, Next_state),
    nb_getval(hdid,NewStepSize), 
    Time2 is Time+NewStepSize,
    log_current_row(Time2, Next_state, Parameters, Functions, Fields),
    nb_setval(last_time,Time2),
  Time2 >= FinalTime,!,
  %Collect the data for numerical table
  findall(Row, (saved_row(Data), Row =.. [row|Data]), RowList),
  retractall(saved_row(Data)),
  ( % patch to append the simulation when continue
    nb_current(rsbk_continue,yes)
  ->
    nb_getval(numerical_table, PreviousList),
    append(PreviousList, RowList, Complete_list)
  ;
    Complete_list = RowList
  ),
  nb_setval(numerical_table, Complete_list).


%! rosenbrock_step(+Equations, +Jacobian, +Time, +CurrentState, +Parameters, +MaxStep, -NextState)
%
% perform one step of rosenbrock integration, take also care of adjusting the step_size
% (stored in global variable nb_getval(hdid, _))
% MaxStep avoid overtaking the final step
% The next stepsize being stored in the 'h' global variable

rosenbrock_step(Equations,Jacobian,Time,CurrentState,Parameters,MaxStep,NewCurrentState) :-
  nb_getval(k_rbk_safe,SAFE),
  nb_getval(k_rbk_grow,GROW),
  nb_getval(k_rbk_pgrow,PGROW),
  nb_getval(k_rbk_shrink,SHRINK),
  nb_getval(k_rbk_pshrink,PSHRINK),
  nb_getval(k_rbk_errcon,ERRCON),
  nb_getval(k_rbk_gam,GAM),
  nb_getval(k_rbk_a21,A21),
  nb_getval(k_rbk_a31,A31),
  nb_getval(k_rbk_a32,A32),
  nb_getval(k_rbk_c21,C21),
  nb_getval(k_rbk_c31,C31),
  nb_getval(k_rbk_c32,C32),
  nb_getval(k_rbk_c41,C41),
  nb_getval(k_rbk_c42,C42),
  nb_getval(k_rbk_c43,C43),
  nb_getval(k_rbk_b1,B1),
  nb_getval(k_rbk_b2,B2),
  nb_getval(k_rbk_b3,B3),
  nb_getval(k_rbk_b4,B4),
  nb_getval(k_rbk_e1,E1),
  nb_getval(k_rbk_e2,E2),
  nb_getval(k_rbk_e3,E3),
  nb_getval(k_rbk_e4,E4),
  repeat,
    % evaluate Jacobian at current time point
    debug(rosenbrock,"State evaluation:~w, ~w, ~n", [CurrentState, Parameters]),
    eval_vector(CurrentState, _S, Parameters, TrueCurrentState),
    debug(rosenbrock,"Jacobian evaluation",[]),
    eval_matrix(Jacobian, TrueCurrentState, Parameters, JacobianCurrentState),
    debug(rosenbrock,"Jacobian evaluated",[]),
    debug(rosenbrock,"Jacobian = ~w     CurrentState = ~w",[Jacobian, TrueCurrentState]),
    debug(rosenbrock,"JacobianCurrentState = ~w",[JacobianCurrentState]),

    nb_getval(h,StepSizeRaw),
    StepSize is min(StepSizeRaw, MaxStep),
    nb_setval(hdid,StepSize),
    shape(Equations,Length),

    % fill the left hand side of AX=B
    GGAM is 1.0/(GAM*StepSize),
    diag_matrix(Length, GGAM, Diagonal),
    set_matrix(JacobianCurrentState, JacobianMatrix),
    matrix_substraction(Diagonal, JacobianMatrix, StoredMatrix),

    % LU decompose A
    ludcmp_matrix(StoredMatrix, DcmpMatrix, Index),
    debug(rosenbrock,"Left side evaluated",[]), 

    % fill the right hand side, G1
    eval_vector(Equations,TrueCurrentState,Parameters,G1),
    debug(rosenbrock,"Right side evaluated",[]),
    debug(rosenbrock,"Equations = ~w     CurrentState = ~w",[Equations, TrueCurrentState]),
    debug(rosenbrock,"G1 = ~w",[G1]),

    % compute by backsubstitution
    lusubst(DcmpMatrix, Index, G1, G1X),
    debug(rosenbrock,"G1X = ~w",[G1X]),

    % get the new values at time + A2X * StepSize
    update_state(TrueCurrentState,A21,G1X,CurrentState1),
    debug(rosenbrock,"CurrentState1 = ~w",[CurrentState1]),
    eval_vector(Equations,CurrentState1,Parameters,Equations1),
    debug(rosenbrock,"Equations at +A2X = ~p",[Equations1]),
    % fill rhs again for G2
    C21H is C21/StepSize,
    update_state(Equations1,C21H,G1X,G2),
    debug(rosenbrock,"Right side evaluated",[]),

    % compute by backsubstitution
    lusubst(DcmpMatrix, Index, G2,G2X),
    debug(rosenbrock,"G2=~p",[G2X]),

    % get the new values at time + (A2X+A3X) * StepSize
    update_state(TrueCurrentState,A31,G1X,CurrentState21),
    update_state(CurrentState21,A32,G2X,CurrentState2),
    eval_vector(Equations,CurrentState2,Parameters,Equations2),
    debug(rosenbrock,"Equations at +A2X+A3X = ~p",[Equations2]),
    % fill rhs again for G3
    C31H is C31/StepSize,
    C32H is C32/StepSize,
    update_state(Equations2,C31H,G1X,G31),
    update_state(G31,C32H,G2X,G3),
    debug(rosenbrock,"Right side evaluated",[]),

    % compute by backsubstitution
    lusubst(DcmpMatrix, Index, G3,G3X),
    debug(rosenbrock,"G3=~p",[G3X]),

    % for G4 no need to recompute CurrentState and Equations
    C41H is C41/StepSize,
    C42H is C42/StepSize,
    C43H is C43/StepSize,
    update_state(Equations2,C41H,G1X,G41),
    update_state(G41,C42H,G2X,G42),
    update_state(G42,C43H,G3X,G4),
    debug(rosenbrock,"Right side evaluated",[]),

    % compute by backsubstitution
    lusubst(DcmpMatrix, Index, G4,G4X),
    debug(rosenbrock,"G4=~p",[G4X]),

    % now get 4th order estimate for CurrentState
    update_state(TrueCurrentState,B1,G1X,CurrentState41),
    update_state(CurrentState41,B2,G2X,CurrentState42),
    update_state(CurrentState42,B3,G3X,CurrentState43),
    update_state(CurrentState43,B4,G4X,NewCurrentState),
    debug(rosenbrock,"CurrentState 4th order evaluated",[]),

    % Estimate error
    constant_vector(Length, 0, ErrorList0),
    update_state(ErrorList0,E1,G1X,ErrorList1),
    update_state(ErrorList1,E2,G2X,ErrorList2),
    update_state(ErrorList2,E3,G3X,ErrorList3),
    update_state(ErrorList3,E4,G4X,ErrorList4),
    get_vector_as_list(ErrorList4, ErrorList4L),
    get_vector_as_list(TrueCurrentState, TrueCurrentStateL),
    scale_error(ErrorList4L,TrueCurrentStateL,ErrorList),
    max_list(ErrorList,Erro),
    debug(rosenbrock,"Error computed",[]),
    debug(rosenbrock,"Erro = ~w",[Erro]),

    % check error
    nb_getval(step_doubling_error,ErrMax),
    Error is Erro/ErrMax,
    debug(rosenbrock,"Error = ~w",[Error]),

    (
      (
        Error > 1
      ;
        (Error = nan)   % catch the case where Error=nan
      )
    ->
      % failure: reduce the step
      % TODO if not yet at maxtry
      (
        StepSize < ErrMax/1000
      ->
        (
          k_fail
        ->
          true
        ;
          assertz(k_fail),
          debug(warning, "Failure with minimal step, error will not be guaranteed.", [])
        )
      ;
        StepSize1 is SAFE*StepSize*(Error**PSHRINK),
        StepSize2 is SHRINK*StepSize,
        max_list([StepSize1,StepSize2],StepSizeNext),
        nb_setval(h,StepSizeNext),
        debug(rosenbrock,"error ~p, candidates ~p ~p",[Erro,StepSize1,StepSize2]),
        debug(rosenbrock,"step size ~p failed, trying ~p...",[StepSize,StepSizeNext]),
        fail
      )
    ;
      % success, try increasing the step size
      (
        Error > ERRCON*ErrMax
      ->
        StepSizeNext is SAFE*StepSize*(Error**PGROW)
      ;
        StepSizeNext is StepSize*GROW
      ),
      debug(rosenbrock,"step size ~p ok, trying ~p...",[StepSize,StepSizeNext]),
      nb_getval(hmax,StepSizeMAX),
      (
        StepSizeNext < StepSizeMAX
      ->
        nb_setval(h,StepSizeNext)
      ;
        true
      )
    ),
    %%% EVENTS HANDLING PART %%% 
    (
      NewTime is Time+StepSize,
      check_events(NewCurrentState, Parameters, NewTime),
      check_conditional_events(NewCurrentState, Parameters, NewTime),
      nb_getval(events_number,NumberOfEvents),
      NumberOfEvents > 0
    ->
      nb_setval(event_handling, true),
      (
        nb_getval(event_step_size, ESS),
        StepSize =< ESS
      ->
        handle_rosenbrock_events(NewCurrentState, Parameters, NewTime),
        handle_conditional_events(NewCurrentState, Parameters, NewTime),
        nb_setval(event_handling, false),
        nb_getval(h_init, NaturalStepSize),
        nb_setval(h, NaturalStepSize)
      ;
        StepSizeRed is SHRINK*StepSize,
        nb_setval(h, StepSizeRed),
        fail
      )
    ;
      (
        nb_getval(event_handling, true)
      ->
        nb_getval(event_step_size, ESS),
        StepSizeRed is max(SHRINK*StepSize, ESS),
        nb_setval(h, StepSizeRed)
      ;
        true
      )
    ),
  !,
  true.

%! scale_error(+Errors, +Values, -Scaled_errors)
% 
% Scale error vector with absolute value of variable (if |value| is > 1)

scale_error([],[],[]).

scale_error([E|Err],[V|L],[SE|SErr]) :-
  (
    abs(V) < 1
  ->
    SE is abs(E)
  ;
    SE is abs(E/V)
  ),
  scale_error(Err,L,SErr).


/*******************/
/* Events handling */
/*******************/

/* Conditionnal event is seen as true when it goes form False to True only */

:- dynamic(conditional_events_status/2).

:- dynamic(rosenbrock_events_status/2).

%! split_events(+Events, -RegularEvents, -TimeEvents)
%
% Split the events between the time and regular ones, would be deprecated when time_events
% will be correctly handled.

split_events([], [], []).

split_events([t>=Thr->Do|Tail], RegTail, [t>=Thr->Do|TimeTail]) :-
  !,
  split_events(Tail, RegTail, TimeTail).

split_events([t>Thr->Do|Tail], RegTail, [t>=Thr->Do|TimeTail]) :-
  !,
  split_events(Tail, RegTail, TimeTail).

split_events([Head|Tail], [Head|RegTail], TimeTail) :-
  split_events(Tail, RegTail, TimeTail).


%! get_events_lists(-EventsList, -ConditionalEventsList)
%
% Gives acces to the events list and the kinetics conditionnal events list

get_events_lists(EventsList,ConditionalEventsList) :-
  nb_getval(events_list,Events),
  length(Events,NumberOfEvents),
  nb_getval(conditional_events,Number),
  NumberOfConditionalEvents is Number*2,
  Separation is NumberOfEvents - NumberOfConditionalEvents,
  separate_list(Events,Separation,EventsList,ConditionalEventsList).


%! prepare_conditional_events(+State, +Parameters, +Time)
%
% Creates the initial facts concerning theses events

prepare_conditional_events(State,Parameters,Time) :-
  nb_getval(conditional_events,Number),
  NumberOfEvents is Number - 1,
  forall(
    between(0,NumberOfEvents,ParameterIndex),
    (
      EventIndex is 2*ParameterIndex,
      numerical_simulation:conditional_event(IndexedCondition, ParameterIndex),
      test_condition(State,Parameters,Time,IndexedCondition,Result),
      assertz(conditional_events_status(EventIndex,Result)),
      TrueParameterIndex is ParameterIndex + 1,
      OppositeEventIndex is EventIndex + 1,
      (
        Result
      ->
        assertz(conditional_events_status(OppositeEventIndex,false)),
        set_value(Parameters, TrueParameterIndex, 1)
      ;
        assertz(conditional_events_status(OppositeEventIndex,true)),
        set_value(Parameters, TrueParameterIndex, 0)
      )
    )
  ).


%! prepare_rosenbrock_events(+State, +Parameters, +Time)
%
%

prepare_rosenbrock_events(State, Parameters, Time) :-
  get_events_lists(Events,_),
  forall(
    (member(Event, Events),Event =.. [->,Condition,AssignmentList]),
    (
      test_condition(State,Parameters,Time,Condition,Result),
      nth0(EventIndex,Events,Event),
      assertz(rosenbrock_events_status(EventIndex,Result)),
      %format("event new state is ~w ~n",[Result]),
      Result
    ->
      maplist(apply_changes(State,Parameters,Time), AssignmentList),
      test_condition(State,Parameters,Time,Condition,NextResult), % In case the condition have changed (hybrid events for example)
      retract(rosenbrock_events_status(EventIndex,_)),
      assertz(rosenbrock_events_status(EventIndex,NextResult))
    ;
      true
    )
  ).

%! check_conditional_events(+State, +Parameters, +Time)
%
% Check if there is some conditional events which become true during the time step of the
% rosenbrock stimulation.

check_conditional_events(State,Parameters,Time) :-
  get_events_lists(_,Events),
  forall(
    (member(Event, Events),Event =.. [->,Condition|_]),
    (  
      test_condition(State,Parameters,Time,Condition,Result),
      nth0(EventIndex,Events,Event),
      retract(conditional_events_status(EventIndex,EventInfo)),
      assertz(conditional_events_status(EventIndex,Result)),
      %format("Event last state is ~w event new state is ~w ~n",[EventInfo,Result]),
      \+(EventInfo),
      Result,
      retract(conditional_events_status(EventIndex,Result)), % If the event goes from false to true, since we'll get back to the time step before, we get back to old values
      assertz(conditional_events_status(EventIndex,EventInfo))
    ->
      count(events_number,_)
    ;
      true
    )
  ).


%! check_events(+State, +Parameters, +Time)
%
%

check_events(State,Parameters,Time) :-
  get_events_lists(Events,_),
  set_counter(events_number,0),
  forall(
    (member(Event, Events),Event =.. [->,Condition|_]),
    (
      test_condition(State,Parameters,Time,Condition,Result),
      nth0(EventIndex,Events,Event),
      retract(rosenbrock_events_status(EventIndex,EventInfo)),
      assertz(rosenbrock_events_status(EventIndex,Result)),
      %format("Event last state is ~w event new state is ~w ~n",[EventInfo,Result]),
      \+(EventInfo),
      Result,
      % If the event goes from false to true, since we'll get back to the time step 
      % before, we get back to old values
      retract(rosenbrock_events_status(EventIndex,Result)), 
      assertz(rosenbrock_events_status(EventIndex,EventInfo))
    ->
      count(events_number,_)
    ;
      true
    )
  ).


%! handle_time_event(+State, +Parameters, +CurrentTime, -NextTime)
%
% Apply all time events that should be and indicate the next event time.

handle_time_event(State, Parameters, CurrentTime, NextTime) :-
  nb_getval(time_events_list, TimeEvents),
  compute_next_event(TimeEvents, State, Parameters, CurrentTime, Time, Effect),
  (
    Time =< CurrentTime
  ->
    maplist(apply_changes(State,Parameters,CurrentTime), Effect),
    nb_setval(last_time_event, Time),
    handle_time_event(State, Parameters, CurrentTime, NextTime)
  ;
    NextTime = Time
  ).


%! compute_next_event(-TimeEvents, -State, -Parameters, -Time, +NextTime, +NextEvent)
%
% Compute the next time and type of time_event, return a big number if no event

compute_next_event([], _State, _Parameters, _CurrentTime, 999999, -1).

compute_next_event([(t>=ExprTime->Effect2)|Tail], State, Parameters, CurrentTime, Time0, Effect0) :-
  rw_coeff( ExprTime, State,  Parameters,  CurrentTime,  Time2),
  compute_next_event(Tail, State, Parameters, CurrentTime, Time1, Effect1),
  (
    nb_getval(last_time_event, LastTime),
    Time2 > LastTime,
    Time2 < Time1
  ->
    Time0 = Time2,
    Effect0 = Effect2
  ;
    Time0 = Time1,
    Effect0 = Effect1
  ).


handle_conditional_events(State, Parameters, Time) :-
   get_events_lists(_,Events),
   forall(
    (member(Event, Events),Event =.. [->,Condition,AssignmentList]),
    (
      test_condition(State,Parameters,Time,Condition,Result),
      nth0(EventIndex,Events,Event),
      retract(conditional_events_status(EventIndex,EventInfo)),
      assertz(conditional_events_status(EventIndex,Result)),
      %format("Eventtt last state is ~w event new state is ~w ~n",[EventInfo,Result]),
      \+(EventInfo),
      Result
    ->
      maplist(apply_changes(State,Parameters,Time), AssignmentList),
      nb_getval(events_number,EventsNumber),
      NewEventsNumber is EventsNumber - 1,
      nb_setval(events_number,NewEventsNumber)
    ;
      true
    )
  ).


handle_rosenbrock_events(State,Parameters,Time) :-
   get_events_lists(Events,_),
   forall(
    (member(Event, Events),Event =.. [->,Condition,AssignmentList]),
    (
      test_condition(State,Parameters,Time,Condition,Result),
      nth0(EventIndex,Events,Event),
      retract(rosenbrock_events_status(EventIndex,EventInfo)),
      assertz(rosenbrock_events_status(EventIndex,Result)),
      %format("Eventtt last state is ~w event new state is ~w ~n",[EventInfo,Result]),
      \+(EventInfo),
      Result
    ->
      maplist(apply_changes(State,Parameters,Time), AssignmentList),
      nb_getval(events_number,EventsNumber),
      NewEventsNumber is EventsNumber - 1,
      nb_setval(events_number,NewEventsNumber),
      test_condition(State,Parameters,Time,Condition,NextResult), % In case the condition have changed (hybrid events for example)
      retract(rosenbrock_events_status(EventIndex,_)),
      assertz(rosenbrock_events_status(EventIndex,NextResult))
    ;
      true
    )
  ).


%! test_condition(+State, +Parameters, +Time, +Condition, +Result)
%
% Check if the condition to fire an event is true
% Can also be used if conditional changes in events

test_condition(State,Parameters,Time,Condition,Result) :-
  Condition =.. [Op, A, B],
  memberchk(Op, ['=', '<', '<=', '>', '>=']),
  !,
  test_condition(State,Parameters,Time,A,ResultA),
  test_condition(State,Parameters,Time,B,ResultB),
  (
    Op = '<='
  ->
    Op2 = '=<' 
  ;
    true  
  ),
  (
    Op = '=>'
  ->
    Op2 = '>=' 
  ;
    true  
  ),
  (
    var(Op2)
  ->
    TrueOp = Op
  ;
    TrueOp = Op2
  ),
  VCondition =.. [TrueOp, ResultA, ResultB],
  (
    VCondition
  ->
    Result = true
  ;
    Result = false
  ).

test_condition(State,Parameters,Time,not Condition,Result) :-
  test_condition(State,Parameters,Time,Condition,NotResult),
  (
    NotResult
  ->
    Result = false
  ;
    Result = true
  ).

test_condition(State,Parameters,Time,Condition,Result) :-
  rw_coeff(Condition, State, Parameters, Time, Result).

test_condition(_,_,_,true,true).

test_condition(_,_,_,false,false).

test_condition(State,Parameters,Time,Condition1 and Condition2,Result) :-
  test_condition(State,Parameters,Time,Condition1,Result1),
  test_condition(State,Parameters,Time,Condition2,Result2),
  (
    Result1,
    Result2
  ->
    Result = true
  ;
    Result = false
  ).

test_condition(State,Parameters,Time,Condition1 or Condition2,Result) :-
  test_condition(State,Parameters,Time,Condition1,Result1),
  test_condition(State,Parameters,Time,Condition2,Result2),
  (
    (Result1;Result2)
  ->
    Result = true
  ;
    Result = false
  ).


%! apply_changes(State, Parameters, Time, Condition)
%
% to be called when an events is triggered

apply_changes(State, Parameters, Time, Parameter = (if Condition then IfTrue else IfFalse)) :-
  test_condition(State, Parameters, _, Condition, Result),
  (
    Result
  ->
    apply_changes(State,Parameters,Time,Parameter = IfTrue)
  ;
    apply_changes(State,Parameters,Time,Parameter = IfFalse)
  ).
   
apply_changes(State,Parameters,Time, Parameter = Expression) :-
  Index is Parameter + 1,
  eval_coeff(Expression,State,Parameters,Time,Value),
  set_value(Parameters,Index,Value).

/*:- doc('\\begin{example}Ball simulation using event:').
:- biocham_silent(clear_model).
:- biocham(load_biocham('library:examples/RobustMonitoring/ball.bc')).
:- biocham(numerical_simulation(method: rsbk)).
:- doc('\\end{example}').*/

