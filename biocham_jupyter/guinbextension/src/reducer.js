import { combineReducers } from 'redux';
import settings from './reducers/settings';
import outputs from './reducers/outputs';
import page from './reducers/page';

export default combineReducers({
    settings,
    outputs,
    page
});
