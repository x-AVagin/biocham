#!/bin/bash
set -ue

if [[ $(uname -s) == "Darwin" ]]
then
  conda_arch="MacOSX"
  ### homebrew

  if hash brew 2>/dev/null
  then
    echo "Using existing homebrew…"
    brew update
  else
    echo "Installing homebrew…"
    ## xcode-select --install will need a GUI :/
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  fi

  # https://stackoverflow.com/questions/43619480/upgrade-or-install-a-homebrew-formula
  function install_or_upgrade {
    if brew ls --versions "$1" >/dev/null; then
        HOMEBREW_NO_AUTO_UPDATE=1 brew upgrade "$1"
    else
        HOMEBREW_NO_AUTO_UPDATE=1 brew install "$1"
    fi
  }

  brew tap brewsci/science
  brewlings='swi-prolog gsl gnuplot graphviz pkg-config ppl libsbml nusmv node open-mpi'
  for brewling in $brewlings ; do
    install_or_upgrade "$brewling"
  done

  release=

elif [[ -r /etc/debian_version ]]
then
  conda_arch="Linux"
  ### Ubuntu or Debian

  sudo apt-get -qy update
  sudo apt-get -qy install lsb-release curl gcc g++ make pkg-config libgsl0-dev gnuplot libppl-dev graphviz-dev git nodejs npm openmpi-bin libopenmpi-dev

  release=$(lsb_release -sc)

  if [[ $release == "xenial" || $release == "bionic" || $release == "focal" ]]
  then
    sudo apt-get -qy install software-properties-common
    sudo apt-add-repository -y ppa:swi-prolog/stable
    sudo apt-get -qy install swi-prolog libsbml5-dev
    sudo cp /usr/lib/swi-prolog/lib/x86_64-linux/libswipl.* /usr/lib/
    if [[ $release == "bionic" ]]
    then
      sudo apt-get -qy install zlib1g-dev
    fi
  else
    if ! swipl -g '
      call_cleanup(
        (
          current_prolog_flag(version, Version),
          Version >= 80203,
          halt(0)
        ),
        halt(1)
      )'
    then
      # Compiling swi-prolog from source, expecting that a newer version of
      # swi-prolog should be packaged in the next releases of Ubuntu
      sudo apt-get -qy install libreadline-dev libgmp-dev
      if [[ ! -d swipl-8.2.4 ]]
      then
        curl -fsSLO http://www.swi-prolog.org/download/stable/src/swipl-8.2.4.tar.gz
        tar -xf swipl-8.2.4.tar.gz
      fi
      pushd swipl-8.2.4
      ./configure
      make
      sudo make install
      cd packages
      ./configure
      make
      sudo make install
      popd
    fi

    if ! pkg-config --atleast-version 5.11.4 libsbml
    then
      # Compiling libSBML from source, expecting that libSBML should
      # be packaged in the next releases of Ubuntu
      sudo apt-get -qy install libxml2-dev
      if [[ ! -d libsbml-5.13.0 ]]
      then
        curl -fsSLO http://downloads.sourceforge.net/project/sbml/libsbml/5.13.0/stable/libSBML-5.13.0-core-src.tar.gz
        tar -xf libSBML-5.13.0-core-src.tar.gz
      fi
      pushd libsbml-5.13.0
      ./configure
      make
      sudo make install
      sudo ldconfig /usr/local/lib/
      popd
    fi
  fi

  if ! hash NuSMV 2>/dev/null
  then
    if [[ $(uname -i) == "x86_64" ]]
    then
      nusmv_url=http://nusmv.fbk.eu/distrib/NuSMV-2.6.0-linux64.tar.gz
    else
      nusmv_url=http://nusmv.fbk.eu/distrib/NuSMV-2.6.0-linux32.tar.gz
    fi
    nusmv_archive=${nusmv_url##*/}
    if [[ ! -r $nusmv_archive ]]
    then
      curl -fsSLO $nusmv_url
    fi
    pushd /usr/local
    sudo tar --strip-components 1 -xf $(dirs -l +1)/$nusmv_archive
    popd
  fi
else
  echo "Automatic install not available, feel free to use the source of this script."
  exit 1
fi

### Glucose
if [[ ! -f /usr/local/bin/glucose.py ]] ; then
  echo "Copying biocham's glucose.py to /usr/local/bin/"
  if [[ "$conda_arch" == "Linux" ]]
  then
    sudo cp glucose.py /usr/local/bin
  else
    cp glucose.py /usr/local/bin
  fi
fi

### Jupyter and python-sat
packages='python-sat jupyter bokeh ipywidgets'
if python3 -m pip --version 2>/dev/null
then
  echo "Using existing pip for python3…"
else
  if hash conda 2>/dev/null
  then
    echo "Using existing conda install to get pip…"
    conda install -y pip
  else
    if [[ "$conda_arch" == "Linux" ]]
    then
      echo "Using apt to get python3-pip…"
      sudo apt-get -qy install python3-pip
    else
      echo "Installing miniconda3…"
      # Note that miniconda is available through brew cask, but to avoid adding
      # brew cask if not necessary, we just do without
      curl -fsSLO https://repo.continuum.io/miniconda/Miniconda3-latest-${conda_arch}-x86_64.sh
      chmod a+x Miniconda3-latest-${conda_arch}-x86_64.sh
      ./Miniconda3-latest-${conda_arch}-x86_64.sh -b -p ${HOME}/miniconda3
      echo 'export PATH=${HOME}/miniconda3/bin:${PATH}' >> ${HOME}/.profile
      export PATH=${HOME}/miniconda3/bin:${PATH}
      conda install -y pip
    fi
  fi
fi
echo "installing python packages ($packages)…"
python3 -m pip install -U $packages
jupyter nbextension install --py widgetsnbextension
jupyter nbextension enable --py widgetsnbextension

### trust all our notebooks but make them read-only
find . -name '*.ipynb' -print0 | xargs -0 jupyter trust
find . -name '*.ipynb' -print0 | xargs -0 chmod a-w

### install ipywidgets and bokeh extensions for jupyterlab
# jupyter labextension install @jupyter-widgets/jupyterlab-manager
# jupyter labextension install jupyterlab_bokeh

make clean && make

### biocham for jupyter
if ! hash biocham 2>/dev/null
then
  echo
  echo "##########"
  echo "export PATH=$(pwd):\${PATH}" >> ${HOME}/.profile
  echo "Don't forget to source ~/.profile or to manually export PATH=$(pwd):\${PATH}"
  echo "and if necessary export PATH=\${HOME}/miniconda3/bin:\${PATH}"
fi
