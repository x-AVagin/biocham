import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import { createComm } from 'Utils/comms';
import { dashingLowercase } from 'Utils/utils';
import { formatArgs, formatOptions } from 'Utils/formats';
import { addToNotebook } from 'Utils/comms';
import { BIOCHAM_TYPE_WIDGETS } from 'Config/config';
import { BaseInput } from './inputs';


const inputWrapperStyles = theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column'
    },
    span: {
        fontWeight: 350,
        color: 'grey'
    },
});

/** Command inputs container. Fetch correct input type from config file for each 
 * biocham type. 
 */
function CommandInputs(props) {
    const { classes, content, handleChange, handleEnter, type } = props;
    return (
        <div className={classes.root}>
          <div className={classes.span}>{type}</div>
          { content.map((arg, index) => {
              const widget = BIOCHAM_TYPE_WIDGETS[arg.type];
              
              const TypedInput = widget? widget.type: BaseInput;

              return (
                  <TypedInput key={`input-${arg}-${index}`}
                              name={arg.name}
                              ctype={arg.type}
                              doc={arg.doc || null}
                              value={arg.value || arg.value === 0? arg.value: ''}
                              handleChange={handleChange}
                              handleEnter={handleEnter}
                              type={type}
                              completionCommand={widget? widget.complete: null}/>
              );
          })}
        </div>
    );
}

CommandInputs = withStyles(inputWrapperStyles)(CommandInputs);


const styles = theme => ({
    root: {
        paddingTop: '10px',
        paddingBottom: '10px',
        paddingLeft: '15px',
        paddingRight: '15px',
        display: 'flex',
        flexDirection: 'row',
        overflowX: 'hidden',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    },
    title: {
        fontWeight: 'bold',
        marginRight: '30px',
        minWidth: '33%'
    },
    mainWrapper: {
        display: 'flex',
        flexFlow: 'row wrap',
        width: '100%',
        flex: '1 1 0',
    },
    argWrapper: {
        display: 'flex',
        flexFlow: 'row wrap',
        flex: '1 1 0',
        justifyContent: 'space-evenly',
    },
    args: {
        flex : '1 1 auto',
        paddingLeft: '5px',
        paddingRight: '5px'
    },
    button: {
        backgroundColor: '#e6e6e6',
        textTransform: 'none'
    }
});

const mapStateToProps = (state) => {
    return {
        options: state.settings.options
    };
};


/**
 * Biocham command container.
 */
class Command extends Component {

    state = {
        options: [],
        args: []
    }

    /** When mounting, init all the args to empty string in state. */
    componentDidMount() {
        const { command, options } = this.props;
        this.setState(prevState => (
            {
                ...prevState,
                args: command.arguments.map(arg => ({
                    ...arg,
                    value: ''
                }))
            }
        ));

        options.length > 0 && this.fillOptions();
    }

    /** On each update, if global options are different from the ones in command
     * state, update these. 
     */
    componentDidUpdate(prevProps) {
        const { command, options } = this.props;
        if (prevProps.options !== options && options.length > 0) {
            this.fillOptions();
        }
    }

    /** Fill command options with global options value. */
    fillOptions = () => {
        const { command, options } = this.props;
        
        this.setState(prevState => ({
            ...prevState,
            options: command.options.map( opt => {
                return {
                    ...opt,
                    value: options.find(o => o.name === opt.name).value
                };
            })
        }));
    }

    /** Reset args input to empty string (usually after command execution). */
    resetInputs = () => {
        this.setState(prevState => (
            {
                ...prevState,
                args: prevState.args.map(arg => ({
                    ...arg,
                    value: ''
                }))
            }
        ));
    }

    /** Upon execution:
     * * get command options different from global ones, format them in a string.
     * * format command arguments.
     * * create a notebook cell with code previously formatted (options and command + args) 
     * then executes it.
     After these steps, arguments are reset (options are always filled with their global value).
     */
    executeCommand = () => {
        const { command } = this.props;
        const { args, options } = this.state;
        // find options that differ from global ones
        const optDiff = this.props.options.reduce((obj, opt) => {
            const optionFromStore =
                  options.find(o => o.name === opt.name &&
                               o.value !== opt.value);
            if (optionFromStore) obj.push(optionFromStore);
            return obj;
        }, []);
        const optionString = formatOptions(optDiff);
        const commandArgs = formatArgs(args);
        const argsString = commandArgs !== ''? `(${commandArgs})`: commandArgs;
        let code;
        // tiny hack to only execute modified options in the settings
        if (command.name === 'option') code = optionString;
        else code = `${optionString} ${command.name}${argsString}.`;
        addToNotebook(code);
        this.resetInputs();
    }

    /** Update argument/option in component state when change is detected. */
    handleChange = (argName, value, type) => {
        const newState = this.state[type].map(o => {
                if (o.name === argName) {
                    o.value = value;
                }
            return o;
        });
        
        this.setState(prevState => ({
            ...prevState,
            [type]: newState
        }));
    }

    render () {
        const { classes, command, tableCmd } = this.props;
        const { options, args } = this.state;
        const { name, doc } = command;
        let commandDoc = Array.isArray(doc)? doc[0]: doc;
        const hasArgs = args.length > 0;
        const hasOptions = options.length > 0;
        
        return (
            <div className={classes.root}
                 id={name}>
              <div className={classes.mainWrapper}>
                <Tooltip title={doc}
                         placement="top-start">
                  <div className={classes.title}>
                    <Button className={classes.button}
                            color={tableCmd && "primary"}
                            onClick={this.executeCommand}>
                      {name}
                    </Button>
                  </div>
                </Tooltip>
                <div className={classes.argWrapper}>
                  { hasArgs &&
                    <div className={classes.args}>
                      <CommandInputs content={args}
                                     handleChange={this.handleChange}
                                     handleEnter={this.executeCommand}
                                     type="args"/>
                    </div>
                  }
                  { hasOptions &&
                    <div className={classes.args}>
                      <CommandInputs content={options}
                                     handleChange={this.handleChange}
                                     handleEnter={this.executeCommand}
                                     type="options"/>
                    </div>
                  }
                </div>
              </div>
            </div>
        );
    }
}

export default connect(mapStateToProps)(
    withStyles(styles)(Command)
);
