import { isEqual } from 'lodash';
import { CHANGE_WORKFLOW,
         CREATE_WORKFLOW,
         DELETE_WORKFLOW,
         ADD_COMMAND_TO_WORKFLOW,
         REMOVE_COMMAND_FROM_WORKFLOW,
         TOGGLE_NUMERICAL_SIMULATION,
         UPDATE_EXECUTION_COUNT,
         UPDATE_OPTIONS } from '../constants/actionTypes';
import { workflows, listCommands } from 'Utils/sections';
import { getOptionsProps,
         mergeOptionPropsAndValues } from 'Utils/comms';
import { getMetadata } from 'Utils/utils';

const biochamOptionsProps = getOptionsProps(listCommands, 'name');

const initialState = {
    // if user saved with custom workflows, otherwise defaults are
    // loaded
    workflows: getMetadata('settings', 'workflows', workflows),
    workflowSelected: 0,
    numericalSimulation: false,
    executionCount: 0,
    options: mergeOptionPropsAndValues(biochamOptionsProps, [])
};

export default (state = initialState, action) => {
    switch (action.type) {
    case CHANGE_WORKFLOW:
        return {
            ...state,
            workflowSelected: action.index
        };
    case CREATE_WORKFLOW:
        return {
            ...state,
            workflows: [
                ...state.workflows,
                action.workflow
            ]
        };
    case DELETE_WORKFLOW:
        return {
            ...state,
            workflows: 
            state.workflows.filter((wf, index) => index !== action.index)
        };
    case ADD_COMMAND_TO_WORKFLOW:
        return {
            ...state,
            workflows:
            state.workflows.map(wf => {
                if (wf.name === action.wfname) {
                    if (wf.commands.some(cmd => isEqual(cmd, action.command)))
                        return wf;
                    return {
                        ...wf,
                        commands: [...wf.commands, action.command]
                    };
                } else {
                    return wf;
                }
            })
        };
    case REMOVE_COMMAND_FROM_WORKFLOW:
        return {
            ...state,
            workflows:
            state.workflows.map(wf => {
                if (wf.name === action.wfname) {
                    return {
                        ...wf,
                        commands: wf.commands.filter((cmd, index) =>
                                                     index !== action.index)
                    };
                } else {
                    return wf;
                }
            })
        };
    case TOGGLE_NUMERICAL_SIMULATION:
        return {
            ...state,
            numericalSimulation: !state.numericalSimulation
        };
    case UPDATE_EXECUTION_COUNT:
        return {
            ...state,
            executionCount: action.executionCount,
        };
    case UPDATE_OPTIONS:
        return {
            ...state,
            options: mergeOptionPropsAndValues(state.options, action.options)
        };
    default:
        return state;
    }
    
};
