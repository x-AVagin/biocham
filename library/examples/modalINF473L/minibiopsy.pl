% SWI-Prolog program

% Reading Biocham models
% to query their reaction graph (with a tree constraint)

% load_bc('catalog.bc').
% reactants((pathway(glucoseext, G), pathway(acetoneext, A), reaction_schema(G+A => R), pathway(R, resorufin)), Reactants).

% F. Fages, Inria, March 2020

:- module(
  minibiopsy,
  [
    load_bc/1,

    print_crn/0,
    write_bc/1,

    reactants/2,
    pathway/2,
    pathway/3,
    arc/3,

    reaction_schema/1,
    complexation/1,
    decomplexation/1,
    and/3,
    or/3,
    neg/2,

    remove_decomplexation/0,

    clear_table/0,

    % Biocham syntax
    op(1100, xfx, for),
    op(1050, xfx, =>)
  ]
).

lt(X, Y) :- when(?=(X, Y), X @< Y).

%%%%%
% CRN representation in Prolog
%%%%%
:- dynamic(reaction/3). % database of reactions with lists of reactants and products
:- dynamic(species/1).  % database of molecular species
:- dynamic(initial/2).  % database of initial states
:- dynamic(parameter/2).% database of parameters


new_crn:-
  clear_table,
  retractall(reaction(_, _, _)),
  retractall(species(_)),
  retractall(parameter(_, _)),
  retractall(initial(_, _)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parsing Biocham reactions
%%%%%%%%%%%%%%%%%%%%%%%%%%%

load_bc(InputFile):-
  new_crn,
  open(InputFile, read, Stream),
  read_crn(Stream),
  close(Stream).


read_crn(Stream):-
  repeat,     % iteration until stopped by cut predicate "!"
  read_term(Stream, Term, [var_prefix(true), variable_names(VarNameList)]),
  maplist(name_variable, VarNameList), % instanciates the tokens read as Prolog variables to their names
  debug(crn, "read term: ~k", [Term]),
  (
    Term=end_of_file
  ->
    !
  ;
    (
      Term=(L => R)
    ->
      add_reaction('MA(1)' for L => R)
    ;
      Term=(K for L => R)
      ->
        add_reaction(K for L => R)
    ;
      Term=absent(A)
      ->
        assertz(initial(A, 0))
    ;
      Term=present(A)
      ->
        assertz(initial(A, 1))
    ;
      Term=present(A, V)
      ->
        assertz(initial(A, V))
    ;
      Term =.. [parameter | L] % term decomposition in a list [Head | Arguments]
      ->
        forall(member(P=V, L), assertz(parameter(P, V)))
    ;
      write('skiping ')
    ),
    %writeln(Term),
    fail
  ).


% eliminate reversed decomplexation

remove_decomplexation:-
  reaction_schema((X + Y => Z)),
  reaction_schema((Z => X + Y), Reaction),
  retract(reaction(_, _, Reaction)),
  writeln(remove(Reaction)),
  fail.
remove_decomplexation.



name_variable(Name=Var):-
  (atom_prefix(Name, '_') % particular case of the character _ which is read in Prolog as a new variable with a number added
  ->
    Var='_'
  ;
    Var=Name % instanciates the Prolog variable to its name to become a ground term
  ).


add_reaction(Reaction) :-
  Reaction = (_ for Reactants => Products),
  debug(crn, "add_reaction(~w, ~w)", [Reactants, Products]),
  add_solution(Reactants, ReactantsList),
  debug(crn, "add_reactants -> ~w", [ReactantsList]),
  add_solution(Products, ProductsList),
  debug(crn, "add_products -> ~w", [ProductsList]),
  assertz(reaction(ReactantsList, ProductsList, Reaction)).


add_solution(Solution, List):-
  (
    Solution='_'
  ->
    List=[]
  ;
    Solution=S1+S2
    ->
      add_solution(S1, L1),
      add_solution(S2, L2),
      append(L2, L1, L),
      sort(L, List)
  ;
    Solution=_K*Molecule
    ->
      List=[Molecule],
      add_species(Molecule)
  ;
    List=[Solution],
    add_species(Solution)
  ).


add_species(Molecule):-
  (
    species(Molecule)
  ->
    true
  ;
    assertz(species(Molecule))
  ).


%%%%%%%%%%%%%%%%%%
% pretty-print CRN
%%%%%%%%%%%%%%%%%%

print_crn:-
  listing(species/1),
  listing(reaction/3),
  listing(initial/2),
  listing(parameter/2).


write_bc(OutputFile):-
  open(OutputFile, write, Stream),
  forall(reaction(_, _, R), (write(Stream, R), writeln(Stream, '.'))),
  close(Stream).


% Pathway constraint

:- table pathway(_, _, lattice(shortest/3)).

shortest(P1, P2, P):-
  length(P1, L1),
  length(P2, L2),
  (   L1 < L2
  ->  P = P1
  ;   P = P2
  ).

clear_table:-
  abolish_table_subgoals(pathway(_, _, _)).

:- clear_table.

pathway(X, Y, [Reaction]) :-
  arc(X, Y, (_Reactants, Reaction)).

pathway(X, Y, R) :-
  pathway(X, Z, PX),
  pathway(Z, Y, PZ),
  % No reversible reaction
  \+ (
      member(R1, PX),
      member(R2, PZ),
      reaction(A, B, R1),
      reaction(B, A, R2)
    ),
  % No use of what we build
  \+ (
      member(R1, PX),
      reaction(RR1, _, R1),
      member(Y, RR1)
    ),
  append(PX, PZ, R).


pathway(X, Y):-
  pathway(X, Y, _).


arc(X, Y, (React, Reaction)):-
  reaction(Reactants, Products, Reaction),
  dif(X, Y),
  member(X, Reactants),
  member(Y, Products),
  subtract(Reactants,[X], React).


reactants(Goal, Reactants):-
  (
    Goal=(G1, G2)
  ->
    reactants(G1, R1),
    reactants(G2, R2),
    append(R1, R2, R),
    sort(R, Reactants)
  ;
    Goal=pathway(A, B)
  ->
    pathway(A, B, Reactions),
    get_reactants(Reactions, [A], Reactants)
  ;
    Goal=pathway(A, _, Reactions)
  ->
    call(Goal),
    get_reactants(Reactions, [A], Reactants)
  ;
    call(Goal),
    Reactants=[]
  ).


get_reactants([], _Inputs, []).

get_reactants([Reaction | Reactions], Inputs, Reactants) :-
  reaction(Reacs, Prods, Reaction),
  subtract(Reacs, Inputs, Needed),
  append(Inputs, Prods, NewInputs),
  get_reactants(Reactions, NewInputs, OtherNeeded),
  % TODO sort?
  append(Needed, OtherNeeded, Reactants).


% Reaction schemas

reaction_schema(Schema):-
  reaction_schema(Schema, _).

reaction_schema((X + Y => Z), Reaction):-
  lt(X, Y),
  reaction(Reactants, Products, Reaction),
  member(X, Reactants),
  member(Y, Reactants),
  member(Z, Products).

reaction_schema((X => Y + Z), Reaction):-
  lt(Y, Z),
  reaction(Reactants, Products, Reaction),
  member(X, Reactants),
  member(Y, Products),
  member(Z, Products).

complexation((X+Y=>Z)):-
  dif(X, Z),
  dif(Y, Z),
  reaction_schema((X+Y=>Z)).

decomplexation((X=>Y+Z)):-
  dif(X, Y),
  dif(X, Z),
  reaction_schema(X=>Y+Z).


% Design



and(A, B, C):-
  pathway(A, AA),
  pathway(B, BB),
  complexation(AA+BB=>CC),
  pathway(CC, C).

or(A, _B, C):-
  pathway(A, C).

or(_A, B, C):-
  pathway(B, C).

neg(A, B):-
  dif(B, C),
  reaction_schema(A+B=>C).
