:- module(
  numerical_simulation,
  [
    % Grammars
    method/1,
    time/1,
    filter/1,
    % Commands
    numerical_simulation/0,
    continue/0,
    % Public API
    prepare_numerical_simulation_options/1
  ]
).

:- dynamic(rosenbrock_running/0).

:- dynamic(rosenbrock_running2/0).

% Only for separate compilation/linting
:- use_module(biocham).
:- use_module(doc).
:- use_module(objects).


:- devdoc('\\section{Grammars}').


:- doc('
  Biocham v5 is interfaced to the GNU Scientific Library (GSL \\url{http://www.gnu.org/software/gsl/}) to perform numerical simulations.
  The page
  \\url{http://www.gnu.org/software/gsl/manual/html_node/Stepping-Functions.html#Stepping-Functions}
  gives a detailed description of the numerical integration methods and options listed below.
      The implicit method \\texttt{bsimp} is the default one.
').

:- doc(' The ODE simulation of a Biocham model proceeds by creating an ODE system if there is none, and deleting it after the simulation.
It is worth noting that if there is an ODE system already present (e.g. created by import_ode), it is the current ODE system that is simulated, not the Biocham model.').

:- doc('The stochastic simulation of a Biocham model is specific to reaction and influence models which can be interpreted by a Continuous Time Markov Chain (CTMC).
      The stochastic simulation algorithm cannot be directly used on an ODE system, but on an equivalent reaction system (e.g. automatically inferred with command \\command{load_reactions_from_ode/1}).').


:- grammar(method).

% Note: a list of GSL supported methods is given in gsl:gsl_methods/1
method(rk2).
:- grammar_doc('\\emphright{Explicit embedded Runge-Kutta (2, 3) method}').

method(rk4).
:- grammar_doc('\\emphright{Explicit 4th order (classical) Runge-Kutta. Error estimation is carried out by the step doubling method. For more efficient estimate of the error, use the embedded methods described below}').

method(rkf45).
:- grammar_doc('\\emphright{Explicit embedded Runge-Kutta-Fehlberg (4, 5) method. This method is a good general-purpose integrator}').

method(rkck).
:- grammar_doc('\\emphright{Explicit embedded Runge-Kutta Cash-Karp (4, 5) method.}').

method(rk8pd).
:- grammar_doc('\\emphright{Explicit embedded Runge-Kutta Prince-Dormand (8, 9) method.}').

method(rk1imp).
:- grammar_doc('\\emphright{Implicit Gaussian first order Runge-Kutta. Also known as implicit Euler or backward Euler method. Error estimation is carried out by the step doubling method.}').

method(rk2imp).
:- grammar_doc('\\emphright{Implicit Gaussian second order Runge-Kutta. Also known as implicit mid-point rule. Error estimation is carried out by the step doubling method.}').

method(rk4imp).
:- grammar_doc('\\emphright{Implicit Gaussian 4th order Runge-Kutta. Error estimation is carried out by the step doubling method.}').

method(bsimp).
:- grammar_doc('Implicit Bulirsch-Stoer method of Bader and Deuflhard. The method is generally suitable for stiff problems.}').

method(msadams).
:- grammar_doc('\\emphright{A variable-coefficient linear multistep Adams method in Nordsieck form. This stepper uses explicit Adams-Bashforth (predictor) and implicit Adams-Moulton (corrector) methods in P(EC)^m functional iteration mode. Method order varies dynamically between 1 and 12.}').

method(msbdf).
:- grammar_doc('\\emphright{\\emph{Perhaps the most robust method but may be slow}, a variable-coefficient linear multistep backward differentiation formula (BDF) method in Nordsieck form. This stepper uses the explicit BDF formula as predictor and implicit BDF formula as corrector. A modified Newton iteration method is used to solve the system of non-linear equations. Method order varies dynamically between 1 and 5.}').

method(ssa).
:- grammar_doc('\\emphright{Stochastic simulation of a Continuous-Time Markov Chain,
  defined as per Gillespie''s algorithm \\cite{Gillespie76jcp}. Note that
  the initial concentrations are converted in molecule numbers by using the \\command{stochastic\\_conversion} option (default 100) and rounding to the nearest
  integer.}').

method(spn).
:- grammar_doc('\\emphright{Stochastic Petri net simulation. Similar to the SSA
  algorithm above but with a discrete/logical time.}').

method(pn).
:- grammar_doc('\\emphright{Random Petri net simulation run. All transitions are
  equiprobable.}').

method(sbn).
:- grammar_doc('\\emphright{Stochastic Boolean net simulation. Similar to the SSA
  algorithm above but with a discrete/logical time and Boolean states.}').

method(bn).
:- grammar_doc('\\emphright{Random Boolean net simulation run. All transitions are
  equiprobable.}').

method(rsbk).
:- grammar_doc('\\emphright{Experimental implementation of Rosenbrock method (not compatible with \\command{search_parameters} and \\command{robustness} and \\command{sensitivity} commands).}').



:- grammar(time).


time(Number) :-
  number(Number).


%%% filters are defined in library/filter.inc and forward declared in
%%% library/gsl_solver.hh
:- grammar(filter).


filter(no_filter).
:- grammar_doc('\\emphright{Does no filtering at all.}').

filter(only_extrema).
:- grammar_doc('\\emphright{Only keeps the points that are an extremum for some variable.}').

:- initial(option(method: bsimp)). % though not robust % rsbk still too slow % msbdf too slow
:- initial(option(error_epsilon_absolute: 1e-6)).
:- initial(option(error_epsilon_relative: 1e-6)).
:- initial(option(initial_step_size: 1e-6)).
:- initial(option(maximum_step_size: 1e-2)).
:- initial(option(minimum_step_size: 1e-5)).
:- initial(option(precision: 6)).
:- initial(option(time: 20)).

:- initial(option(stochastic_conversion: 100)).
:- initial(option(stochastic_bound: 1e6)).
:- initial(option(stochastic_thresholding: 1000)).
:- initial(option(filter: no_filter)).
:- initial(option(stats: no)).

:- dynamic(last_method/1).

:- devdoc('\\section{Commands}').

numerical_simulation :-
  biocham_command,
  option(time, time, Time, 'time horizon of the numerical integration'),
  option(stats, yesno, Stats, 'display computation time'),
  option(method, method, Method, 'method for the numerical solver'),
  option(
    error_epsilon_absolute, number, _ErrorEpsilonAbsolute,
    'absolute error for the numerical solver'
  ),
  option(
    error_epsilon_relative, number, _ErrorEpsilonRelative,
    'relative error for the numerical solver'
  ),
  option(
    initial_step_size, number, _InitialStepSize,
    'initial step size for the numerical solver'
  ),
  option(
    maximum_step_size, number, _MaximumStepSize,
    'maximum step size for the numerical solver, as a fraction of the total time'
  ),
  option(
    minimum_step_size, number, _MinimumStepSize,
    'minimum step size, as a fraction of the total time (used to trigger events)'
  ),
  option(
    precision, number, _Precision,
    'precision for the numerical solver'
  ),
  option(
    stochastic_conversion, number, _,
    'Conversion factor used to scale 1 mole to the given number of molecules (default 100).'
  ),
  option(
    stochastic_bound, number, _,
    'Maximum number of molecules of one molecular species allowed in stochastic simulations (default 1e6).'
  ),
  option(
    stochastic_thresholding, number, _,
    'Do not write (but still compute) stochastic steps below one fraction of
    the total time (default 1000)'
  ),
  option(
    filter, filter, _Filter,
    'filtering function for the trace'
  ),
  doc('performs a numerical simulation from time 0 up to a given time.'),
  retractall(last_method(_)),
  debug(numsim, "numerical simulation method: ~w", [Method]),
  assertz(last_method(Method)),
  statistics(runtime,_),
  (
    Method = ssa
  ->
    stochastic_simulation(Time, continuous, continuous, false)
  ;
    Method = spn
  ->
    stochastic_simulation(Time, continuous, discrete, false)
  ;
    Method = pn
  ->
    stochastic_simulation(Time, boolean, discrete, false)
  ;
    Method = sbn
  ->
    stochastic_simulation(Time, continuous, discrete, true)
  ;
    Method = bn
  ->
    stochastic_simulation(Time, boolean, discrete, true)
  ;
    Method = rsbk
  ->
    assertz(rosenbrock_running),
    with_current_ode_system(
      with_clean(
        [
          numerical_simulation:variable/2,
          numerical_simulation:equation/2,
          numerical_simulation:parameter_index/2,
          numerical_simulation:conditional_event/2,
          numerical_simulation:conditional_parameter_value/2,
          rosenbrock:conditional_events_status/2,
          rosenbrock:rosenbrock_events_status/2
        ],
        solve2
      )
    ),
    retractall(rosenbrock_running),
    retractall(rosenbrock_running2)
  ;
    with_current_ode_system(
      with_clean(
        [
          numerical_simulation:variable/2,
          numerical_simulation:equation/2,
          numerical_simulation:parameter_index/2,
          numerical_simulation:conditional_event/2,
          numerical_simulation:conditional_parameter_value/2
        ],
        solve
      )
    )
  ),
  statistics(runtime,[_,T]),
  (
    Stats = yes
  ->
    format("Simulation time: ~pms~n",[T])
  ;
    true
  ).


continue :-
  biocham_command,
  option(time, time, Time, 'time to add to the numerical integration'),
  doc('Continues the numerical simulation by extending the time horizon by option "time" units.'),
  get_table_headers([_| Variables]),
  get_table_data(OldData),
  last(OldData, LastRow),
  LastRow=..[row, LastTime | Values],
  get_initial_values(Variables, InitialValues),
  set_initial_values(Variables, Values),
  % FIXME uses global options, not the ones used for previous simulation
  (
    last_method(rsbk)
  ->
    nb_setval(rsbk_continue,yes),
    with_option([time: Time,method: rsbk], numerical_simulation),
    nb_delete(rsbk_continue)
  ;
    with_option(time: Time, numerical_simulation),
    get_table_data(NewData),
    translate_data(LastTime, NewData, ContinuedData),
    append(OldData, ContinuedData, FullData),
    HeaderRow =.. [row, '#''Time' | Variables],
    add_table(numerical_simulation, [HeaderRow | FullData])
  ),
  set_initial_values(Variables, InitialValues).


function_to_parameters :- % Add functions to parameters in order to allow them in event changes
  all_items([kind: function],Functions),
  forall(
          member(function(Function),Functions),
          (Function =.. [=,Head,Expression],
          (
            rosenbrock:is_kinetics_function(Head)
          ->
            true
          ;
            numerical_simulation:convert_expression(Expression,IndexedExpression),
            change_item([], parameter, Head, parameter(Head = IndexedExpression)) % parameters:set_parameter/2 does not allow to create a parameter which has a function name
          )
        )
  ).

prepare_numerical_simulation_options(Options) :-
  get_option(time, Time),
  get_option(method, Method),
  get_option(error_epsilon_absolute, ErrorEpsilonAbsolute),
  get_option(error_epsilon_relative, ErrorEpsilonRelative),
  get_option(initial_step_size, InitialStepSize),
  get_option(maximum_step_size, MaximumStepSize),
  get_option(minimum_step_size, MinimumStepSize),
  get_option(precision, Precision),
  get_option(filter, Filter),
  set_counter(conditional_events,0),
  check_no_free_identifiers,
  enumerate_variables,
  convert_ode,
  gather_equations(Equations),
  debug(numsim, "ODEs: ~w", [Equations]),
  (
    rosenbrock_running
  ->
    assertz(rosenbrock_running2),
    function_to_parameters,
    all_items([kind:parameter],Parameters), % Creates indexes for all parameters
    forall(
          member(parameter(Parameter = _),Parameters),
          convert_identifier(Parameter,_)
    )
  ;
    true
  ),
  gather_initial_values(InitialValues),
  debug(numsim, "Initial values: ~w", [InitialValues]),
  gather_events(RegularEvents, event),
  gather_events(TimeEvents, time_event),
  append(RegularEvents, TimeEvents, Events),
  debug(numsim, "Events: ~w", [Events]),
  gather_fields(Events, Fields),
  gather_initial_parameter_values(InitialParameterValues),
  debug(numsim, "Parameter values: ~w", [InitialParameterValues]),
  jacobian(Equations, Jacobian),
  debug(numsim, "Jacobian: ~w", [Jacobian]),
  Options = [
    fields: Fields,
    equations: Equations,
    initial_values: InitialValues,
    initial_parameter_values: InitialParameterValues,
    events: Events,
    method: Method,
    error_epsilon_absolute: ErrorEpsilonAbsolute,
    error_epsilon_relative: ErrorEpsilonRelative,
    initial_step_size: InitialStepSize,
    maximum_step_size: MaximumStepSize,
    minimum_step_size: MinimumStepSize,
    precision: Precision,
    filter: Filter,
    time_initial: 0,
    time_final: Time,
    jacobian: Jacobian
  ].
  %format("Jacobian is ~w ~n", [Jacobian]).

:- devdoc('\\section{Private predicate}').


:- dynamic(variable/2).


:- dynamic(equation/2).


:- dynamic(parameter_index/2).


:- dynamic(conditional_event/2).


:- dynamic(conditional_parameter_value/2).


solve2 :-
  prepare_numerical_simulation_options(Options),
  ode_solver(Options),
  nb_getval(numerical_table,Data),
  add_table('numerical_simulation',Data).

solve :-
  prepare_numerical_simulation_options(Options),
  debug(numsim, "simulation options: ~w", [Options]),
  solve(Options, Table),
  debug(numsim, "Done", []),
  add_table('numerical_simulation', Table).


gather_fields(Events, ['Time':t|Fields_tail]) :-
  findall(
    Field,
    (
      enumerate_variables(Field)
    ;
      enumerate_nonconstant_parameters(Events, Field)
    ;
      enumerate_nonparametric_functions(Field)
    ),
    Fields_unsorted
  ),
  sort(Fields_unsorted, Fields_tail).


enumerate_variables(Header: x(VariableIndex)) :-
  peek_count(variable_counter, VariableCount),
  VariableMax is VariableCount - 1,
  between(0, VariableMax, VariableIndex),
  variable(Molecule, VariableIndex),
%  format(atom(Header), '~a', [Molecule]).
  format(atom(Header), '~w', [Molecule]).


enumerate_nonconstant_parameters(Events, Header: Parameter) :-
  setof(
    ParameterIndex,
    Cond^Action^Value^(
      member(Cond -> Action, Events),
      member(ParameterIndex = Value, Action)
    ),
    NCParameters
  ),
  debug(numsim, "NC parameters ~w", [NCParameters]),
  member(ParameterIndex, NCParameters),
  Parameter = p(ParameterIndex),
  convert_identifier(Header, Parameter).


enumerate_nonparametric_functions(Header: expression(Expr)) :-
  % (item([kind: function, item: function(Header = Body)]) ;
  get_ode_function(Header, Body),
  atomic(Header),
  debug(numsim, "NP function ~w with body ~w", [Header, Body]),
  ode:substitute_functions(Body, NoFuncBody),
  debug(numsim, "substituted body ~w", [NoFuncBody]),
  convert_expression(NoFuncBody, Expr).


gather_equations(Equations) :-
  peek_count(variable_counter, VariableCount),
  VariableMax is VariableCount - 1,
  findall(
    Equation,
    (
      between(0, VariableMax, VariableIndex),
      equation(VariableIndex, Equation)
    ),
    Equations
  ).


gather_initial_values(InitialValues) :-
  peek_count(variable_counter, VariableCount),
  VariableMax is VariableCount - 1,
  findall(
    InitialValue,
    (
      between(0, VariableMax, VariableIndex),
      variable(Molecule, VariableIndex),
      (
        % try first in the model in case Concentration is a parameter
        get_initial_concentration(Molecule, Concentration)
      ->
        State = initial_state(Molecule = Concentration)
      ;
        % otherwise use the ODE system
        get_ode_initial_state(Molecule, State)
      ),
      (
        State = initial_state(Molecule = Concentration)
      ->
        (
          parameter_index(Concentration, Index)
        ->
          InitialValue = p(Index)
        ;
          InitialValue = Concentration
        )
      ;
        InitialValue = 0
      )
    ),
    InitialValues
  ).

:- devdoc('Conditional kinetics are treated with events and virtual parameters.').

gather_initial_parameter_values(InitialParameterValues) :-
  peek_count(parameter_counter, ParameterCount),
  ParameterMax is ParameterCount - 1,
  debug(numsim, "~d parameters", [ParameterCount]),
  findall(
    InitialParameterValue,
    (
      between(0, ParameterMax, ParameterIndex),
      parameter_index(Parameter, ParameterIndex),
      debug(numsim, "Parameter #~d: ~w", [ParameterIndex, Parameter]),
      (
        % use ode parameters first
        ode_parameter_value(Parameter, InitialParameterValue)
      ->
        true
      ;
        % if not present try model parameter then
        parameter_value(Parameter, InitialParameterValue)
      ->
        true
      ;
        % then virtual event parameters for conditional kinetics
        conditional_parameter_value(ParameterIndex,InitialParameterValue) 
      )
    ),
    InitialParameterValues
  ).


gather_events(Events, Type) :-
  findall(
    Event,
    (
      bagof(
        ParameterValue,
        ParameterValues ^ (
          item([kind: Type, item: event(Condition, ParameterValues)]),
          member(ParameterValue, ParameterValues)
        ),
        ParameterValues
      ),
      convert_condition(Condition, ConditionIndexed),
      maplist(convert_parameter, ParameterValues, IndexedParameterValues),
      Event = (ConditionIndexed -> IndexedParameterValues)
    ;
      conditional_event(ConditionIndexed, ParameterIndex),
      (
        Event = (ConditionIndexed -> [ParameterIndex = 1])
      ;
        Event = (not ConditionIndexed -> [ParameterIndex = 0])
      )
    ),
    Events
  ).


convert_parameter(Parameter = Value, ParameterIndex = ValueIndexed) :-
  parameter_index(Parameter, ParameterIndex),
  convert_expression(Value, ValueIndexed).


enumerate_variables :-
  set_counter(variable_counter, 0),
  \+ (
    ode(Molecule, _),
    \+ (
      count(variable_counter, VariableIndex),
      assertz(variable(Molecule, VariableIndex))
    )
  ).


convert_ode :-
  set_counter(parameter_counter, 0),
  \+ (
    ode(Molecule, Expression),
    \+ (
      ode:substitute_functions(Expression, NoFuncExpr),
      variable(Molecule, VariableIndex),
      convert_expression(NoFuncExpr, IndexedExpression),
      assertz(equation(VariableIndex, IndexedExpression)),
      % also consider as parameters those used in initial values
      % so that they can be searched
      get_initial_state(Molecule, Initial),
      (
        Initial = initial_state(_=Parameter),
        parameter_value(Parameter, _)
      ->
        convert_identifier(Parameter, _)
      ;
        true
      )
    )
  ).


convert_condition(Expression, IndexedExpression) :-
  grammar_map(
    condition,
    [
      condition: (numerical_simulation:convert_condition),
      arithmetic_expression: (numerical_simulation:convert_expression)
    ],
    Expression,
    IndexedExpression
  ).


convert_identifier('Time', 't') :-
  !.

convert_identifier(floor(Element), floor(ConvertedElement)) :-
  convert_expression(Element,ConvertedElement),
  !.

convert_identifier('random_float','random') :-
  !.

convert_identifier('inf','infinity') :-
  !.

convert_identifier('step_size','step_size') :-
  !.


convert_identifier(Molecule, [VariableIndex]) :-
  variable(Molecule, VariableIndex),
  !.

convert_identifier(Expression,FinalIndexExpression) :-
  Expression =.. [log,Rest],
  convert_identifier(Rest,IndexExpression),
  FinalIndexExpression =.. [log,IndexExpression],
  !.

convert_identifier(Parameter, p(ParameterIndex)) :-
  atom(Parameter),
  !,
  (
    parameter_index(Parameter, ParameterIndex)
  ->
    true
  ;
    count(parameter_counter, ParameterIndex),
    assertz(parameter_index(Parameter, ParameterIndex))
  ).

convert_expression([Object], IndexedExpression) :-
  !,
  convert_identifier(Object, IndexedExpression).


convert_expression(if Condition then IfTrue else IfFalse, IndexedExpression) :-
  \+(rosenbrock_running2),
  !,
  convert_condition(Condition, IndexedCondition),
  (
    conditional_event(IndexedCondition, ParameterIndex)
  ->
    true
  ;
    count(conditional_events,_),
    count(parameter_counter, ParameterIndex),
    assertz(parameter_index(condition_parameter(ParameterIndex),ParameterIndex)),
    assertz(conditional_parameter_value(ParameterIndex,0)),
    assertz(conditional_event(IndexedCondition, ParameterIndex))
  ),
  convert_expression(IfTrue, IndexedIfTrue),
  convert_expression(IfFalse, IndexedIfFalse),
  IndexedExpression =
    p(ParameterIndex) * IndexedIfTrue
    + (1 - p(ParameterIndex)) * IndexedIfFalse.

convert_expression(if Condition then IfTrue else IfFalse,IndexedExpression) :- % This allows to convert expressions in hybrid events
  rosenbrock_running2,
  !,
  convert_condition(Condition,IndexedCondition),
  convert_expression(IfTrue,IndexedIfTrue),
  convert_expression(IfFalse,IndexedIfFalse),
  IndexedExpression = (if IndexedCondition then IndexedIfTrue else IndexedIfFalse).


convert_expression(Expression, IndexedExpression) :-
  grammar_map(
    arithmetic_expression,
    [
      number: (=),
      identifier: (numerical_simulation:convert_identifier),
      arithmetic_expression: (numerical_simulation:convert_expression)
    ],
    Expression,
    IndexedExpression
  ).
