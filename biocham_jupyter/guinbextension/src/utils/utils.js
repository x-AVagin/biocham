/**
 * Generic helper functions.
 * @module utils  
 * 
 */

import { range } from 'lodash';
import { commTarget } from 'Constants/gui';
import { getJupyter,
         addToNotebook } from './comms';


const Jupyter = getJupyter();


/**
 * Concatenate all argument names and types for a biocham command.
 * @param {object} command - the biocham command object.
 * @return {string} the arguments type and name concatenated.
 */
export function placeHolder(command) {
    //format a placeholder if input
    const args = command.arguments;
    return args.length > 0 &&
        args.map(argument => {
            return (`${argument.type} ${argument.name}`);
        }).join(', ');
};

/**
 * Remove empty spaces in the 
 */
function cleanEmpty(array) {
    return array.filter(ix => ix);
};

export function dashingLowercase(title) {
    return title.replace(new RegExp(' ', 'g'), '-').toLowerCase();
};

/** Computes slider step for a value. 
 * @param {string} value - the value.
 * @return {number} step value. 
 */
export function step(value) {
    return parseFloat(`1e-${precision(value)}`);
}

/** Computes slider precision for a value. 
 * Notice that this method is not so great as Javascript
 * is not so great for handling numbers...
 * @param {string} value - the value as a string.
 * @return {number} step value. 
*/
export function precision(value) {
    // ugly af precision but at least it computes fast
    const a = String(value);
    if (a.includes('.')) {
        const n = a.split('.');
        if (n[1].includes('e')) {
            const g = n[1].split('e');
            const t = g[0].length;
            const e = g[1];
            if (e < 0) {
                return t - e;
            } 
            else {
                return e - t;
            }  
        }
        return n[1].length;
    }
    return 0;
}

/** 
 * Computes rounded value for slider (to avoid point decimal problems.
 * @param {number} value - the slider value.
 * @param {number} precision - the slider precision.
 * @return {number} rounded value to precision.
 */
export function round(value, precision) {
    return precision > 0 ?
        parseFloat(parseFloat(value).toFixed(precision)) :
        parseInt(value);
}


/** Method used to get parts of the metadata to populate the redux store.
 * @param {string} state - the state part which is need (key).
 * @param {string} key - the key in the state part.
 * @param {Object} def - default value.
 * @return {Object} the part of the metadata or default value. 
 */
export function getMetadata(state, key, def) {
    if (Jupyter.notebook.metadata.hasOwnProperty('gui_state'))
        return JSON.parse(Jupyter.notebook.metadata.gui_state)[state][key];
    return def;
}


/** Clear all notebook cells (called when launching a notebook 
 * that was saved in the GUI).
*/
export function clearNotebookCells() {
    const len = Jupyter.notebook.get_cells().length;
    const cell_range = range(len);
    Jupyter.notebook.delete_cells(cell_range);
}


//---------------- HTML helpers ---------------------

/**
 * 
 * Hide an html node.
 * @param {object} element - HTML element
 */
export function hideElement(element) {
    if (element.style.display !== 'none') {
        element.style.display = 'none';
    }
}

/**
 * Show an html node with given style (i.e. block, inline, ...)
 *
 *
 */
export function showElement(element, style) {
    if (element.style.display === 'none') {
        element.style.display = style;
    }
}

/**
 * Check if an html node is visible
 */
export function isVisible(element) {
    return element.offsetWidth > 0 && element.offsetHeight > 0;
}

