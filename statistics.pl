:- module(
  statistics,
  [   
   normal_random/1,
   clear_stat/1,
   clear_distribution/1,
   clear_min/1,
   clear_max/1,
   compute_distribution/2,
   compute_min/1,
   compute_max/1,
   new_sample/2,
   new_sample/1,
   new_min/2,
   new_max/2,
   same_min/2,
   same_max/2,
   stat_distribution/4,
   stat_min/2,
   stat_max/2
   ]).


:- devdoc('Box-Muller method for random sampling a normal distribution').
:- dynamic(done_normal/1).


normal_random(X) :-
  retract(done_normal(X)),
  !.

normal_random(X) :-
  random(U),
  random(V),
  LN is sqrt(- 2 * log(U)),
  X is LN * cos(2*pi*V),
  Y is LN * sin(2*pi*V),
  assertz(done_normal(Y)).



:- devdoc('Incremental computation of minimum, maximum, mean and standard deviation values.').

:- devdoc('compute_distribution(name, N) asks to compute the mean, variance  and standard deviation of variable name with N samples').
:- devdoc('get_distribution(name, mean, deviation)').

:- devdoc('compute_min(name) asks to compute the minimum value of variable name').
:- devdoc('compute_max(name) asks to compute the maximum value of variable name').

:- devdoc('new_sample(name, value) enters a new sample value for name').
:- devdoc('new_sample(goal) executes a goal in this module, e.g. (new_max(name, value) -> new_sample(name2,value2)').

:- devdoc('new_min(name,value) true if new minimum value ').
:- devdoc('new_max(name,value)').

:- devdoc('same_min(name,value) true if same minimum value ').
:- devdoc('same_max(name,value)').

:- devdoc('get_min(name,Min)').
:- devdoc('get_max(name,Max)').


:- dynamic stat_distrib/4.
:- dynamic stat_min/2.
:- dynamic stat_max/2.

clear_stat(Name) :-
  clear_distribution(Name),
  clear_min(Name),
  clear_max(Name).

clear_distribution(Name) :-
  retractall(stat_distrib(Name, _, _, _)).

compute_distribution(Name, N) :-
  clear_distribution(Name),
  assertz(stat_distrib(Name, N, 0, 0)).


clear_min(Name) :-
  retractall(stat_min(Name, _)).

clear_max(Name) :-
  retractall(stat_max(Name, _)).

compute_min(Name) :-
  clear_min(Name),
  assertz(stat_min(Name, _)).

compute_max(Name) :-
  clear_max(Name),
  assertz(stat_max(Name, _)).


new_min(Name, Value):-
  stat_min(Name, Min),
  (var(Min) ; Value<Min), 
  !.

new_max(Name, Value):-
  stat_max(Name, Max),
  (var(Max) ; Value>Max),
  !.


same_min(Name, Value):-
  stat_min(Name, Min),
  nonvar(Min),
  Value=:=Min, 
  !.

same_max(Name, Value):-
  stat_max(Name, Max),
  nonvar(Max),
  Value=:=Max,
  !.


new_sample(Name, Value):-
  stat_distrib(Name, N, Mean, NVar),
  M is Mean+(Value-Mean)/N,
  V is NVar+(Value-M)*(Value-Mean),
  clear_distribution(Name),
  assertz(stat_distrib(Name, N, M, V)),
  fail.

new_sample(Name, Value) :-
   new_min(Name, Value),
   clear_min(Name),
   assertz(stat_min(Name, Value)),
   fail.

new_sample(Name, Value) :-
  new_max(Name, Value),
  clear_max(Name),
  assertz(stat_max(Name, Value)),
  fail.

new_sample(_Name, _Value).


new_sample(Goal) :-
  call(Goal).


stat_distribution(Name, Mean, Variance, Deviation):-
  stat_distrib(Name, N, Mean, NVar),
  Variance is NVar/N,
  Deviation is sqrt(Variance).

