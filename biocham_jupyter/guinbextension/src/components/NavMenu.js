import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Divider from '@material-ui/core/Divider';
import { TOGGLE_MENU_SECTION } from 'Constants/actionTypes';
import { dashingLowercase } from 'Utils/utils';


const styles = theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    menu: {
        marginRight: '12px'
    },
    item: {
        paddingLeft: '10px',
        paddingRight: '0'
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
        paddingTop: '6px',
        paddingBottom: '6px',
        fontSize: '12px',
    },
    nestedSelected: {
        backgroundColor: '#F0F0F0'
    },
    drawer: {
        height: '100%',
        width: '160px',
        flexShrink: 0,
    },
    drawerPaper: {
        width: '160px',
        zIndex: 99,
        position: 'relative',
        transform: 'none !important'
    },
    drawerHeader: {
        display: 'flex',
        minHeight: '39px',
        height: '39px',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
});

const mapStateToProps = state => {
    return {
        navMenu: state.page.navMenu,
        mainApp: state.page.mainApp,
        workflows: state.settings.workflows
    };
};

const mapDispatchToProps = dispatch => ({
    onSectionClicked: name =>
        dispatch({ type: TOGGLE_MENU_SECTION, name })
});


/** Navigation menu container. */
class NavMenu extends Component {

    state = {
        lastClicked: ''
    }

    /** Handle click on chapter section to update it in Redux store 
     * and highlight it in the menu.
     */
    handleClick = (id) => {
        this.props.onSectionClicked(id);
    }

    /** Handle section click (inside chapter) to update central container 
     * scroll position and highlight current one selected in menu.
     */
    handleSectionClick = (id) => {
        this.setState({ lastClicked: id });
        // scroll into section;
        const elem = document.getElementById(id);
        elem.scrollIntoView();
    }

    /**
     * Generate section items (inside chapter sections).
     */
    generateSectionItems = (section) => {
        const { classes } = this.props;
        const { lastClicked } = this.state;
        
        return section.map((section, index) => {
            const name = dashingLowercase(section.name);
            const selected = lastClicked === name;
            return (
                <ListItem button
                          className={selected ?
                                     classNames(classes.nested,
                                                classes.nestedSelected):
                                     classes.nested}
                          key={`section-menu-${index}`}
                          onClick={() => this.handleSectionClick(name)}>
                  <ListItemText primary={section.name}
                                disableTypography/>
                </ListItem>
            );
        });
    }

    /**
     * Generate chapters section containers.
     */
    generateMenu = () => {
        const { classes, sections, navMenu } = this.props;
        
        return sections.map((chapter, index) => {
            const title = dashingLowercase(chapter.name);
            return (
                <div key={`chapter-menu-${index}`}>
                  <ListItem id={title}
                            button
                            selected={navMenu.hasOwnProperty(title)}
                            className={classes.item}
                            onClick={() => this.handleClick(title)} >
                    <ListItemText primary={chapter.name}
                                  disableTypography/>
                  </ListItem>
                  <Collapse in={navMenu[title]}
                            unmountOnExit>
                    <List component="div"
                          disablePadding>
                      { this.generateSectionItems(chapter.children) }
                    </List>
                  </Collapse>
                </div>
            );
        });
    }

    render() {
        const { classes, modelSections, workflows, navMenu } = this.props;

        return (
            <Drawer className={classes.drawer}
                    variant="persistent"
                    anchor="left"
                    classes={{
                        paper: classes.drawerPaper,
                    }}>
              <List component="nav"
                    disablePadding>
                <ListItem id="model"
                          button
                          selected={navMenu.hasOwnProperty('model')}
                          className={classes.item}
                          onClick={() => this.handleClick('model')}>
                  <ListItemText primary="Model"
                                disableTypography/>
                </ListItem>
                <Collapse in={navMenu.model}
                          unmountOnExit>
                  <List component="div"
                        disablePadding>
                    { workflows &&
                      <ListItem button
                                className={classes.nested}
                                onClick={() => this.handleSectionClick(
                                    dashingLowercase('workflows')
                                )}>
                        <ListItemText primary={'Workflows'}
                                      disableTypography/>
                      </ListItem>
                    }
                    { this.generateSectionItems(modelSections) }
                  </List>
                </Collapse>
                { this.generateMenu() }
                <ListItem id="settings"
                          button
                          selected={navMenu.hasOwnProperty('settings')}
                          className={classes.item}
                          onClick={() => this.handleClick('settings')}>
                  <ListItemText primary="Settings"
                                disableTypography/>
                </ListItem>
              </List>
            </Drawer>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(
    withStyles(styles)(NavMenu)
);
