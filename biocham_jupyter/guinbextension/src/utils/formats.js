/**
 * String and outputs formatting methods for biocham code/outputs.
 * @module formats
 */

/**
 * Format code response from the kernel into an array.
 * This function is used for formatting each CRUD list of the model.
 * @param {string} str - the code sent by the kernel.
 * @param {string} type - the section of the model.
 * @param {boolean} [display=false] - true if only names and not values
 * are needed.
 * @return {Array} array of formatted strings (or objects {name, value}
 * in case of initial state and parameters).
 */
export function format({str, type, display=false}) {
    if (type === 'list_reactions') {
        return handleOutputString({str});
    }
    else if (type === 'list_initial_state') {
        return sortOnKey(handleListInitialState(str), 'name');
    }
    else if (type === 'list_parameters') {
        return sortOnKey(handleOutputString({str: str,
                                             sep: '=',
                                             sort: true}), 'name');
    }
    else if (type === 'list_molecules') {
        return handleOutputString({str});
    }
    else if (type === 'list_influences') {
        return handleOutputString({str});
    }
    else if (type === 'list_options') {
        return handleOutputString({str: str,
                                   sep: ':',
                                   display: display,
                                   header: true});
    }
    else if (type === 'list_functions') {
        return handleOutputString({str: str, header: true});
    }
    return handleOutputString({str});
}


/**
 * Handle the output string 
 */
function handleOutputString({str, sep='', display=false, header=false}) {
    if (str === '') {
        return [];
    }
    const stringArray = str.split(/\r\n/);
    // we replace the line number and get rid of empty strings in the array
    let cleaned = stringArray.map(str => str.replace(/\[\d+]/, '')).filter(x => x);
    // if the first str is 'From inherited initial:'
    if (header) {
        cleaned = cleaned.slice(1, cleaned.length);
    }
    
    // display is the case when we only need to display names
    // for completion purposes
    if (display) {
        return getNames(cleaned, sep);
    }
    
    if (sep && !display) {
        return splitOutputs(cleaned, sep);
    }
    
    return cleaned;
}


/**
 * Some outputs have a separator (e.g. = for parameters, ',' for species),
 * meaning they can be split as {name: name of biocham object, value: value 
 * of biocham objects} (this is used to create inputs and sliders in the GUI).
 * @param {Array} arr - array of string outputs.
 * @param {string} sep - the separator to use to split the string.
 */
function splitOutputs(arr, sep) {
    return arr.map(str => {
        const name = str.match(new RegExp(`(?<=\\()(.*)(?=${sep})`))[0];
        let value = str.match(new RegExp(`(?<=${sep})(.*?)(?=\\))`))[0];
        value = formatValue(value);
        // case where option is an empty set
        return { name: name, value: value };
    });
}

/**
 * This is used in order to correctly format the value for the GUI:
 * * empty string if it's empty or [{}].
 * * string value if it's NaN.
 * * cast to number if its not NaN.
 * @param {string} value - the string value
 * @return {string|number} the value 
 */
function formatValue(value) {
    if (!value || value === '[{}]') return '';
    else if (isNaN(value)) return value;
    return parseFloat(value);
}

/**
 * Extract biocham object names (i.e. for initial state or parameters).
 * @param {Array} arr - array of string (i.e. present(A,2).).
 * @param {string} sep - separator to use in the regexp.
 * @return {Array} array of names.
 */
function getNames(arr, sep) {
    return arr.map(str => str.match(new RegExp(`(?<=\\()(.*)(?=${sep})`))[0]);
}

/**
 * This one is defined on its own because it has several conditions
 * and values cannot be handled like for other lists (i.e. when 
 * an object is present without an initial value, or absent).
 * @param {string} - the result of `list_initial_state.` .
 * @return {Array} array of formatted outputs {name, value}.
 */
function handleListInitialState(str) {
    const arr = handleOutputString({str});
    const sep = ',';
   
    return arr.map(string => {
        const command = string.match(/\w+(?=\()/)[0];
        const fullValue = string.match(/(?<=\()(.*)(?=\))/)[0];
        
        if (command === 'present') {
            // Fix me: this is in the case only ',' is provided to present
            // to avoid splitting
            if (fullValue === ',') {
                return {name: fullValue, value: 'present'};
            }
            const split = fullValue.split(sep);
            // if there is no value (default value of 1 in biocham)
            if (split.length === 1) {
                return {name: split[0], value: 'present'};
            }
            else if (split.length > 2) {
                return {name: split.join(), value: 'present'};
            }
            return {name: split[0], value: isNaN(split[1]) ?
                    split[1]: parseFloat(split[1])};
        }

        return {name: fullValue, value: 'absent'};
    });
}


/**
 * Format command arguments by joining them.
 * @param {Array} args - command arguments.
 * @return {string} arguments string.
 */
export function formatArgs(args) {
    const argsStr = args.length > 0 ?
          args.filter(arg => arg.value.trim() !== '')
          .map(arg => arg.value)
          .join(', ') : '';
    return argsStr;
};

/** 
 * Format options to be stored in notebook's metadata.
 * @param {Array} options - unformatted options.
 * @return {string} all global options in a string.
 */
export function formatOptions(options) {
    const optStr = options.length > 0 ?
          options.map(opt => {
              let value = opt.value;
              if (isSet(opt.type)) {
                  value = `{${opt.value}}`;
              }
              // add 2 space and newline for markdown
              return `option(${opt.name}:${value}).  
`;
          }).join(''): '';
    return optStr;
}

/**
 * Check if type of parameter is a biocham set.
 * @param {string} type - type of biocham object.
 * @return {boolean} true if type is a biocham set 
 */
function isSet(type) {
    return !!type.match(/{(.*?)}/);
}


/**
 * Sort an array of biocham objects (e.g. [{name: k1, value: 0}], 
 * to get the same order each time, otherwise when updating a part 
 * of the model (e.g a single parameter or specie) the last updated 
 * would be place at the end of the array automatically. We don't
 * really care about the order as long as it stays the same throughout 
 * the GUI use.
 * @param {Array} arr - an array of biocham objects
 * @param {string} key - the property to sort on.
 * @return {Array} sorted array.
 */
function sortOnKey(arr, key){
    return arr.sort(function(a, b){
        if(a[key] < b[key]){
            return -1;
        }else if(a[key] > b[key]){
            return 1;
        }
        return 0;
    });
}
