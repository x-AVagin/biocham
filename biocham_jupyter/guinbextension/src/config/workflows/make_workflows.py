"""a small file to convert bc files to UI workflows in the notebook, represented
by command lists in file workflows.js.
"""
import json
import os
import argparse
from commands import commands

def clean(command_list):
    """clean the commands extracted from a .bc file"""
    # get rid of line breaks
    tmp = [x.strip() for x in command_list]
    # get rid of empty and comments
    tmp = [x for x in tmp if x and not x.startswith('%')]
    return tmp

def get_biocham_command(cmd, command_list, acc): #pylint:disable=W0621
    """method to separate commands if several are provided, and check if 
    it actually is a biocham command (in a script we could have reactions
    or other things described without the usage of a command directly"""
    # TODO should have list of commands to ignore (e.g. option)
    # check if several commands (naive way)
    if cmd.count('.') > 1:
        cmds = [x.replace('.', '') for x in cmd.split()]
        cmds = [x.split('(')[0] if '(' in x else x for x in cmds]
        for c in cmds:
            if c in command_list:
                acc.append(c)
    elif '(' in cmd:
        c = cmd.split('(')[0]
        if c in command_list:
            acc.append(c)
    elif cmd[-1] == '.':
        c = cmd.replace('.', '')
        if c in command_list:
            acc.append(c)

if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-i', '--input', help='directory containing bc files')
        parser.add_argument('-o', '--output', help='output file for workflows')
        args = parser.parse_args()
        path = args.input
        outpath = args.output
        gen = []

        for file in os.listdir(path):
            if file.split('.')[-1] == 'bc':
                acc = []
                with open(os.path.join(path, file), 'r') as f:
                    lines = f.readlines()
                    lines = clean(lines)
                    for cmd in lines:
                        get_biocham_command(cmd, commands, acc)
                    fileName = file.split('.')[0]
                    gen.append({'name': fileName, 'commands': list(dict.fromkeys(acc))})

        with open(outpath, 'w+') as outfile:
            outfile.write('// Auto generated file, please do not edit manually \n')
            outfile.write('export default')
            json.dump(gen, outfile)

    except FileNotFoundError as error:
        print(error)
