/**
 * Configuration file for the GUI. 
 * @module config
 */

import {
    BaseInput,
    CheckboxInput,
    FileInput,
    AutocompleteInput } from "Components/inputs";
import { createComm } from 'Utils/comms';


/** Object mapping biocham types to React components.*/
export const BIOCHAM_TYPE_WIDGETS = {
    "option": {
        type: AutocompleteInput,
        complete: 'list_options'
    },
    "reaction": {
        type: AutocompleteInput,
        complete: 'list_reactions'
    },
    "yesno": {
        type: CheckboxInput
    },
    "input_file": {
        type: FileInput
    },
    "{input_file}": {
        type: FileInput
    },
};

/** Biocham model sections (to extract from the manual), with their crud part.*/
export const CRUD_SECTIONS = [
    {
        name: 'Molecules and initial state',
        crud: {
            read: 'list_initial_state', create: 'present', del: 'undefined',
            commandSep: ','
        },
    },
    {
        name: 'Parameters',
        crud: {
            read: 'list_parameters', create: 'parameter', del: 'delete_parameter',
            commandSep: '='
        },
    },
    {
        name: 'Functions',
        crud: {
            read: 'list_functions', create: 'function', del: 'delete_function',
        },
    },
    {
        name: 'Reaction editor',
        crud: {
            read: 'list_reactions', create: 'add_reaction', del: 'delete_reaction',
        },
    },
    {
        name: 'Influence editor',
        crud: {
            read: 'list_influences', create: 'add_influence'
        },
    },
    {
        name: 'Events',
        crud: {
            read: 'list_events', create: 'add_event'
        },
    },
    {
        name: 'Conservation laws and invariants',
        crud: {
            read: 'list_conservations', create: 'add_conservation', del: 'delete_conservation'
        },
    },
];


/** List of chapters to ignore in the biocham manual.*/

export const CHAPTERS_TO_IGNORE = [
    'Overview',
    'Index',
    'Bibliography',
    'Toplevel'
];
