:- module(
  types,
  [
    type/2,
    predicate_info/5,
    check_type/3,
    function_application/2,
    untyped/1
  ]
).


type(L, { _ }) :-
  !,
  (
    list(L)
  ->
    true
  ;
    throw(error(type_error(list, L)))
  ).

type(L, '*'(_)) :-
  !,
  (
    list(L)
  ->
    true
  ;
    throw(error(type_error(list, L)))
  ).

type(_, _).


predicate_info((Head :- Body), ArgumentTypes, Options, BiochamCommand, Doc) :-
  functor(Head, Functor, Arity),
  Head =.. [Functor | Arguments],
  length(ArgumentTypes, Arity),
  collect_info(Body, Arguments, ArgumentTypes, Options, BiochamCommand0, Doc),
  (
    var(BiochamCommand0)
  ->
    BiochamCommand0 = no
  ;
    true
  ),
  BiochamCommand = BiochamCommand0.

predicate_info(Functor/Arity, ArgumentTypes, Options, BiochamCommand, Doc) :-
  functor(Head, Functor, Arity),
  (
    catch(
      (
        clause(Head, Body),
        predicate_info(
          (Head :- Body), ArgumentTypes, Options, BiochamCommand, Doc
        ),
        (
          BiochamCommand = yes
        ;
          BiochamCommand = variantargs
        )
      ),
      _E,
      fail
    )
  ->
    true
  ;
    BiochamCommand = no
  ).


collect_info(biocham_command, _Arguments, _ArgumentTypes, [], yes, []) :-
  !.

collect_info(
  biocham_command(*), _Arguments, _ArgumentTypes, [], variantargs, []
) :-
  !.

collect_info(type(Argument, Type), Arguments, ArgumentTypes, [], _, []) :-
  !,
  once(nth0_eqq(Index, Arguments, Argument)),
  nth0(Index, ArgumentTypes, Type).

collect_info(
  option(Option, OptionType, _Value, Doc),
  _Arguments,
  _ArgumentTypes,
  [option(Option, OptionType, Doc)],
  _,
  []
) :-
  !.

collect_info(
  DocItem, _Arguments, _ArgumentTypes, [], _BiochamCommand, [DocItem]
) :-
  (
    DocItem = doc(_)
  ;
    DocItem = devdoc(_)
  ;
    DocItem = biocham(_)
  ;
    DocItem = biocham_silent(_)
  ),
  !.

collect_info((A, B), Arguments, ArgumentTypes, Options, BiochamCommand, Doc) :-
  !,
  collect_info(A, Arguments, ArgumentTypes, OptionsA, BiochamCommand, DocA),
  append(DocA, DocB, Doc),
  append(OptionsA, OptionsB, Options),
  collect_info(B, Arguments, ArgumentTypes, OptionsB, BiochamCommand, DocB).

collect_info(_, _Arguments, _ArgumentTypes, [], _BiochamCommand, []).

check_type(untyped, Object, Object) :-
  !.

check_type(atom, Object, NewObject) :-
  !,
  check_atom(Object, NewObject).

check_type(number, Number, NewNumber) :-
  !,
  check_number(Number, NewNumber).

check_type(term, Term, Term) :-
  !.

check_type(integer, Number, NewNumber) :-
  !,
  check_number(Number, NewNumber),
  integer(NewNumber).

check_type('='(SubType), Equals, NewList) :-
  !,
  Equals =.. ['=' | _],
  equals_to_list(Equals, List),
  check_type([SubType], List, NewList).

check_type('='(LeftType, RightType), Equal, NewLeft = NewRight) :-
  !,
  Equal = (Left = Right),
  check_type(LeftType, Left, NewLeft),
  check_type(RightType, Right, NewRight).

check_type(':'(LeftType, RightType), Equal, (NewLeft: NewRight)) :-
  !,
  Equal = (Left: Right),
  check_type(LeftType, Left, NewLeft),
  check_type(RightType, Right, NewRight).

check_type({SubType}, Set, NewList) :-
  !,
  set_to_list(Set, List),
  check_type([SubType], List, NewList).

check_type('*'(SubType), List, NewList) :-
  !,
  check_type([SubType], List, NewList).

check_type([SubType], List, NewList) :-
  !,
  check_type_list(List, SubType, NewList).

check_type(Grammar, Item, NewItem) :-
  (
    Grammar = solution
  ->
    patch_solution(Item, Item0)
  ;
    Grammar = reactants
  ->
    patch_reactants(Item, Item0)
  ;
    Grammar = inputs
  ->
    patch_inputs(Item, Item0)
  ;
    Grammar = arithmetic_expression
  ->
    patch_arithmetic_expression(Item, Item0)
  ;
    Item0 = Item
  ),
  Head =.. [Grammar, Item0],
  NewHead =.. [Grammar, NewItem],
  (
    clause(NewHead, NewBody),
    copy_term((NewHead, NewBody), (Head, Body)),
    catch(
      check_grammar_body(Body, NewBody),
      error(expected(_, _)),
      fail
    )
  ->
    true
  ;
    throw(error(expected(Grammar, Item)))
  ).


check_type_list([], _Type, []).

check_type_list([HeadIn| TailIn], Type, [HeadOut | TailOut]) :-
  check_type(Type, HeadIn, HeadOut),
  check_type_list(TailIn, Type, TailOut).

% check_atom does perform coercions from compound terms to names
check_atom(Atom, NewAtom) :-
  (
   compound(Atom)
  ->
   functor(Atom,F,_),  % very superficial sanity check...
   member(F,
	  ['@',         % locations
	   ':','/','.', % filenames
	   '-','~']     % previous Biocham syntax for complexes and phosphorylations
	 )
  ;
   true
  ),
  format(atom(NewAtom), '~w', [Atom]).


check_number(Number, NewNumber) :-
  number(Number),
  catch(NewNumber is Number, error(_, _), fail).


check_grammar_body((A, B), (NewA, NewB)) :-
  !,
  check_grammar_body(A, NewA),
  check_grammar_body(B, NewB).

check_grammar_body(list(Grammar, Item), list(Grammar, NewItem)) :-
  !,
  check_type([Grammar], Item, NewItem).

check_grammar_body(
  function_application(Grammar, Item),
  function_application(Grammar, NewItem)
) :-
  !,
  functor(Item, Functor, Arity),
  identifier_name(Functor),
  Item =.. [Functor | Arguments],
  length(NewArguments, Arity),
  NewItem =.. [Functor | NewArguments],
  check_type([Grammar], Arguments, NewArguments).

check_grammar_body(true, true) :-
  !.

check_grammar_body(Goal, NewGoal) :-
  Goal =.. [Grammar, Item],
  NewGoal =.. [Grammar, NewItem],
  check_type(Grammar, Item, NewItem).


untyped(_Kind).


function_application(Kind, Term) :-
  Term =.. [Function | Arguments],
  identifier_name(Function),
  Arguments = [_ | _],
  maplist(Kind, Arguments).
