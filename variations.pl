:- module(
  variations,
  [
    % Commands
    variation/3,
    logarithmic_variation/3,
    clear_variation/1,
    clear_variations/0,
    dose_response/3,
    logarithmic_dose_response/3,
    bifurcations/3,
    logarithmic_bifurcations/3,
    change_parameter_to_variable/1,
    restore_parameter/1
  ]
       ).


:- doc('This section describes commands for continuously varying the values of molecular species or parameters.').

:- doc('
This can be used for drawing dose-response diagrams
under the hypothesis that the \\emph{input dose is a catalyst} not affected by the system.
It can also be used for drawing some simple forms of bifurcation diagrams
obtained by just varying forth and back an input.').


:- dynamic(initial_value/2).


variation(Object, Value1, Value2):-
  biocham_command,
  type(Object, object),
  type(Value1, arithmetic_expression),
  type(Value2, arithmetic_expression),
  option(time, time, Duration, 'duration of the variation.'),
  doc('Makes a molecule continuously vary from Value1 to Value2 in the time option duration, under the hypothesis that the object is not affected by the other reactions on this time scale. This is achieved by adding a synthesis reaction with appropriate kinetics, removing any previous variation reaction on the object, and setting its initial concentration to Value1.'),
  get_initial_state(Object, Init),
  assertz(initial_value(Object, Init)),
  present([Object], Value1),
  atom_concat('variation_', Object, Name),
  delete_reaction_named(Name),
  add_reaction(Name -- (Value2-Value1)/Duration for _=>Object).


logarithmic_variation(Object, Value1, Value2):-
  biocham_command,
  type(Object, object),
  type(Value1, arithmetic_expression),
  type(Value2, arithmetic_expression),
  option(time, time, Duration, 'duration of the variation.'),
  doc('Makes a logarithmic variation. Value1 must be non zero.'),
  get_initial_state(Object,Init),
  assertz(initial_value(Object, Init)),
  present([Object],Value1),
  atom_concat('variation_', Object, Name),
  delete_reaction_named(Name),
  add_reaction(Name -- (Object*log((1e-20+Value2)/(1e-20+Value1))/Duration) for Object=>2*Object).


restore_init(Object) :-
  (
    initial_value(Object, Init)
  ->
    set_initial_state(Object, Init),
    retractall(initial_value(Object, _))
  ;
    true
  ).



clear_variation(Object) :-
  biocham_command,
  type(Object, object),
  doc('Deletes any variation of the object and restores its initial state.'),
% restore_parameter(Object),
  restore_init(Object),
  atom_concat(variation_, Object, Name),
  delete_reaction_named(Name).


clear_variations :-
  biocham_command,
  doc('Deletes all variations.'),
  \+ (
    find_reaction_prefixed('variation_',Name),
    atom_concat('variation_',Object,Name),
    \+ (
      clear_variation(Object)
    )
  ).


:- doc('\\begin{example}').
:- biocham_silent(clear_model).
:- biocham(option(time:6)).
:- biocham(variation(a, 1e-7, 1e-1)).
:- biocham(logarithmic_variation(b, 1e-7, 1e-1)).
:- biocham(numerical_simulation).
:- biocham(plot).
:- biocham(plot(logscale:y)).
:- biocham(list_model).
:- doc('\\end{example}').


dose_response(Dose, Value1, Value2):-
  biocham_command,
  type(Dose, object),
  type(Value1, arithmetic_expression),
  type(Value2, arithmetic_expression),
  option(time, time, Duration, 'time horizon necessary to approximate the steady state response; the variation of the dose is performed over a time horizon of 10 fold that value.'),
  option(
    show, {object}, ResponseSet,
    'Defines a set of objects for the response; everything will be
    plotted if the show option is empty'
  ),
  doc('Draws a dose-response diagram by linear variation of the initial concentration (the dose) of the input object which must be a \\emph{catalyst} not affected by the system, from Value1 to Value2, and plotting the output object (the response) in relation to the dose. 
A set of output objects can be given to draw their respective responses.
The time option should be greater or equal to the simulation time horizon necessary to approximate the steady state response. 
The variation of the dose is performed over a time horizon of 10 fold the given duration.'),
  Dur is 10*Duration,
  with_option(time: Dur, (variation(Dose,Value1,Value2), numerical_simulation)),
  with_option([against: Dose, show: ResponseSet], plot),
  clear_variation(Dose).

logarithmic_dose_response(Dose, Value1, Value2):-
  biocham_command,
  type(Dose, object),
  type(Value1, arithmetic_expression),
  type(Value2, arithmetic_expression),
  option(time, time, Duration, ''),
  option(show, {object}, ResponseSet, ''),
  doc('Draws a similar dose-response diagram by logarithmic variation and plots it on a logarithmic scale for both the dose and the responses. Value1 must be non zero.'),
  Dur is 10*Duration,
  with_option(time: Dur, (logarithmic_variation(Dose,Value1,Value2), numerical_simulation)),
  with_option([logscale:xy, against: Dose, show: ResponseSet], plot),
  clear_variation(Dose).


:- doc('\\begin{example}This example shows the dose-response diagrams of the MAPK model of Huang and Ferrell in BiomModels revealing the amplifier and analog-digital converter functions
of this network at the second and third level of the cascade. ').
:- biocham_silent(clear_model).
:- biocham('load_sbml(library:biomodels/BIOMD0000000009.xml)').
:- biocham('option(time:100, show:{P_KKK,PP_KK,PP_K})').
:- biocham('dose_response(E1,1e-6,1e-4)').
%:- biocham('logarithmic_dose_response(E1,1e-6,1e-4)').
:- doc('\\end{example}').


%:- doc('\\begin{example}
%\\clearmodel
%\\trace{
%biocham: load_sbml(library:examples/mapk/BIOMD0000000009.xml).
%biocham: option(time:100, show:{P_KKK,PP_KK,PP_K}).
%biocham: dose_response(E1,1e-9,1e-4).
%biocham: logarithmic_dose_response(E1,1e-9,1e-4).
%}
%\\end{example}').




change_parameter_to_variable(Object):-
  biocham_command,
  type(Object, parameter_name),
  doc('Transforms a parameter in a variable initialized to the parameter value (and creates a parameter named variation_Object). This transformation is reversible.'),
  (
    catch(parameter_value(Object,Value), _, fail)
  ->
    atom_concat('varying_parameter_',Object,Parameter),
    set_parameter(Parameter, Value),
    delete_parameter([Object]),
    present([Object],Value)
  ;
    true
  ).




restore_parameter(Object):-
  biocham_command,
  type(Object, object),
  doc('Restores a varying parameter to a fixed parameter.'),
  atom_concat('varying_parameter_', Object, Parameter),
  (
    parameter_value(Parameter, Value)
  ->
    delete_parameter([Parameter]),
    undefined_object(Object),
    set_parameter(Object, Value)
  ;
    true
  ).





bifurcations(Dose, Value1, Value2):-
  biocham_command,
  type(Dose, object),
  type(Value1, arithmetic_expression),
  type(Value2, arithmetic_expression),
  option(time, time, Duration, ' '),
  option(show, {object}, ResponseSet, ' '),
  doc('Draws a dose-response diagram by linear variation from Value1 to Value2 and then from Value2 to Value1 (to see memory effects) of the input object which must be a catalyst not affected by the system, and plots the output object responses.
The time duration should be greater or equal to the simulation time horizon necessary to get the response at steady state (in both the increasing forward and decreasing backward variations which may be different).
The increasing and decreasing variations of the dose are performed over a time horizon of 10 fold duration each.
Unlike a real bifurcation diagram that draws the stable and unstable zeros of the differential function of the response, the steady states are not computed here but approximated by simulation.'),
  Dur is 10*Duration,
  with_option(time: Dur, (variation(Dose,Value1,Value2), numerical_simulation, variation(Dose,Value2,Value1))),
  get_last_value(Dose, Value3),
  present([Dose],Value3),
  with_option(time: Dur, continue),
  with_option([against: Dose, show: ResponseSet], plot),
  clear_variation(Dose).


logarithmic_bifurcations(Dose, Value1, Value2):-
    biocham_command,
    type(Dose, object),
    type(Value1, arithmetic_expression),
    type(Value2, arithmetic_expression),
    option(time, time, Duration, ''),
    option(show, {object}, ResponseSet, ''),
    doc('Draws a similar dose-response diagram by logarithmic variation using a logarithmic scale for both the dose and the response. Value1 and Value2 must be non zero.'),
    Dur is 10*Duration,
    with_option(time: Dur, (logarithmic_variation(Dose,Value1,Value2), numerical_simulation, logarithmic_variation(Dose,Value2,Value1))),
    get_last_value(Dose, Value3),
    present([Dose],Value3),
    continue(Dur),
    with_option(logscale:xy, with_option(against: Dose, with_option(show: ResponseSet, plot))),
    clear_variation(Dose).


:- doc('\\begin{example}This example of the MAPK network shows a memory effect in one single level of two phosphorylation cycles. 
The hysteresis corresponds to the existence of two stable states in the rate equation.
\\clearmodel
\\trace{
biocham: load_sbml(library:biomodels/BIOMD0000000026.xml).
biocham: dose_response(MAPKK, 0, 100, time:1e4, show:Mpp).
biocham: bifurcations(MAPKK, 0, 100, time:1e4, show:Mpp).
}
\\end{example}').


%:- doc('\\begin{example}This example shows a memory effect (and erroneous dose-response diagrams) in the MAPK network when using too short simulation times.
%\\clearmodel
%\\trace{
%biocham: load_sbml(library:examples/mapk/BIOMD0000000009.xml).
%biocham: option(time:100,show:{PP_KK,PP_K}).
%biocham: dose_response(E1,1e-6,1e-4).
%biocham: bifurcations(E1,1e-6,1e-4).
%biocham: bifurcations(E1,1e-6,1e-4,time:1000000).
%}
%\\end{example}').



    
%:- doc('\\begin{example}
%\\clearmodel
%\\trace{
%biocham: load(library:examples/cell_cycle/Qu_et_al_2003.bc).
%biocham: change_parameter_to_variable(k1).
%biocham: list_model.
%biocham: dose_response(k1,1,1000,time:200,show:CycB-CDK~{p1}).
%}
%\\end{example}').
