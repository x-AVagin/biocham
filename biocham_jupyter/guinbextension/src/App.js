import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { MuiThemeProvider,
         createMuiTheme,
         withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';

import { OutputTabs } from 'Components/OutputTabs';
import NavMenu from 'Components/NavMenu';
import Chapter from 'Components/Chapter';
import Model from 'Components/Model';
import Settings from 'Components/Settings';
import {
    ADD_GRAPHICAL_OUTPUT,
    DELETE_GRAPHICAL_OUTPUT,
    ADD_STRING_OUTPUT,
    UPDATE_EXECUTION_COUNT,
    UPDATE_OPTIONS,
    CHANGE_TAB } from './constants/actionTypes';
import { commTarget } from './constants/gui';
import { biochamSections,
         crudSections,
         workflows } from 'Utils/sections';
import { dashingLowercase,
         clearNotebookCells } from 'Utils/utils';
import { getJupyter,
         createComm,
         getOptionsValues,
         storeOutput,
         loadModel } from 'Utils/comms';
import { format, formatOptions } from 'Utils/formats';
import { store } from './store';


/**
 * MaterialUI custom theme to redefine app fonts (the same used by
 * the notebook).
 * @ignore
 */
const theme = createMuiTheme({
    typography: {
        fontFamily: [
            'Helvetica Neue',
            'Helvetica',
            'Arial',
            'sans-serif'
        ].join(','),
        fontSize: '13px',
        // https://github.com/mui-org/material-ui/issues/12741
        useNextVariants: true,
    }
});

/**
 * App css.
 * @ignore
 */
const styles = theme => ({
    root: {
        right: 0,
        left: 0,
        bottom: 0,
        top: 0,
        display: 'flex',
        flexDirection: 'row',
        flexGrow: 1,
        // in case another extension (e.g. nbpresent) is installed
        zIndex: 99,
        position: 'absolute',
        backgroundColor: 'white'
    },
    menu: {
        minWidth: '140px',
        width: '160px',
        overflow: 'auto'
    },
    main: {
        flex: 1,
        flexGrow: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    inputCol: {
        flex: 2,
        alignItems: 'flex-start',
        overflow: 'auto'
    },
    resCol: {
        flex: 3,
        alignItems: 'flex-start',
        borderLeft: '1px solid rgb(0, 0, 0, 0.12)'
    },
    hidden: {
        display: 'none'
    }
});

const mapStateToProps = (state) => {
    return {
        graphicalOutputs: state.outputs.graphicalOutputs,
        consoleOutputs: state.outputs.consoleOutputs,
        executionCount: state.settings.executionCount,
        mainApp: state.page.mainApp,
        options: state.settings.options,
        numericalSimulation: state.settings.numericalSimulation,
    };
};

const mapDispatchToProps = dispatch => ({
    onGraphicalOutputReceived: data =>
        dispatch({ type: ADD_GRAPHICAL_OUTPUT, data }),
    onGraphicalOutputDeleted: index =>
        dispatch({ type: DELETE_GRAPHICAL_OUTPUT, index }),
    onStringOutputReceived: data =>
        dispatch({ type: ADD_STRING_OUTPUT, data }),
    onChangeTab: (tabType, index) =>
        dispatch({ type: CHANGE_TAB, tabType, index }),
    onUpdateExecCount: executionCount =>
        dispatch({ type: UPDATE_EXECUTION_COUNT, executionCount }),
    onUpdateOptions: options =>
        dispatch({ type: UPDATE_OPTIONS, options })
});

/**
 * React app component for the biocham GUI in the Jupyter notebook. 
 */
class App extends Component {
    state = {
        model: '',
        options: ''
    }

    /**
     * Several step occurs when mounting the App component:
     * * It registers a comm target in the Jupyter comms manager, in order
     * to handle kernel responses.
     * * It checks if a previous model was stored in the notebook's metadata,
     * and if that is the case it clears the notebook cells, create a markdown
     * cell to show the last model and options, and then load the model and options.
     * * It links a method to the notebook saving event, in order to store the app state,
     * model and options in the notebook's metadata before saving.
     * * It fetches biocham global options to populate the biocham commands with 
     * their value.
     */
    componentDidMount() {
        const { Jupyter } = this.props;
        // this comm is opened from the kernel 
        Jupyter.notebook.kernel.comm_manager
            .register_target('to_gui',(comm, msg) => {
                comm.on_msg( (msg) => {
                    this.handleKernelResponse(msg.content);
                });
                comm.on_close((msg) => {
                });
            });

        const prevModel = Jupyter.notebook.metadata.biocham_model;
        const prevOptions = Jupyter.notebook.metadata.biocham_options;

        // if in GUI mode when loading, clean the notebook, insert previous
        // model and options in a markdown cell in the notebook, then execute
        // the previous model and options.
        if (prevModel && Jupyter.notebook.metadata.biocham_gui) {
            clearNotebookCells();
            Jupyter.notebook.insert_cell_at_index('markdown', 0)
                .set_text(
                    `### Model:\n${prevModel}\n\n ### Options:\n${prevOptions}`
                );
            loadModel(prevModel, prevOptions);
        }
        
        Jupyter.notebook.events.on('before_save.Notebook', () => {
            if (Jupyter.notebook.metadata.biocham_gui) {
                this.saveStateToMetadata();
                Jupyter.notebook.metadata.biocham_model = this.state.model;
                Jupyter.notebook.metadata.biocham_options = this.state.options;
            }
        });
        
        // trigger event to get global options
        getOptionsValues(this.props.onUpdateOptions);
    }

    /**
     * When component updates, check if the current section opened is the
     * same as the last one. If not, it scrolls to the top of the section.
     * @param {object} prevProps - The previous component props. 
     */
    componentDidUpdate(prevProps) {
        const { mainApp } = this.props;
        // scroll to top of main container
        if (prevProps.mainApp !== mainApp) {
            document.getElementById('biocham-container').scrollTop = 0;
        }
    }

    /**
     * Saves the current app state to the notebook's metadata.
     */
    saveStateToMetadata = () => {
        const { Jupyter } = this.props;
        Jupyter.notebook.metadata.gui_state =
            JSON.stringify(store.getState());
    }

    /**
     * Update the app's model and options.
     */
    updateModelStr = () => {
        const comm = createComm(commTarget);
        comm.send({ code: 'list_model.' });
        comm.on_msg(msg => {
            if (msg.content.data.status === 'ok')
                this.setState({ model: msg.content.data.output,
                                options: formatOptions(this.props.options)
                              });
        });
    }

    /** 
     * Generate the current section to display with a Chapter component.
     */
    generateLayout = () => {
        const { sections, kernelTarget, mainApp, options } = this.props;

        return sections.map((container, index) => {
            const name = dashingLowercase(container.name);
            return (
                container.children.length > 0 && mainApp[name] &&
                    <Chapter key={`chapter-container-${index}`}
                             content={container.children}
                             kernelTarget={kernelTarget}
                             options={options}
                             handleKernelResponse={this.handleKernelResponse}
                             appendResults={this.appendResults}/>
            );
        });
    }

    /** 
     * Handle kernel messages which can be either strings or graphical outputs, and
     * updates accordingly which output tab is displayed to the user.
     * @param {object} msg - The full response object.
     */
    handleKernelResponse = (msg) => {
        const data = msg.data;
        const { type, code, show, graphical } = data;
        
        if (data.execution_count) {
            this.props.onUpdateExecCount(data.execution_count);
            getOptionsValues(this.props.onUpdateOptions);
            this.updateModelStr();
        }

        switch (type) {
        case 'plot':
        case 'image':
            this.props.onGraphicalOutputReceived(data);
            this.props.onChangeTab('mainTab', 0);
            break;
        case 'str':
            this.props.onStringOutputReceived(data);
            // case its a command for a graphical output
            // it doesn't automatically change to console tab
            if (!graphical) {
                this.props.onChangeTab('mainTab', 1);
            }
            break;
        default:
            break;
        }
    }

    /**
     * Render class method containing the JSX structure of the App.
     */
    render() {
        const { classes, sections, modelSections,
                kernelTarget, mainApp } = this.props;
        
        return (
            <MuiThemeProvider theme={theme}>
              <div className={classes.root}>
                <NavMenu className={classes.menu}
                         modelSections={modelSections}
                         sections={sections}
                         handleSelect={this.showContainer}/>
                <div className={classes.inputCol}
                     id="biocham-container">
                  <div className={mainApp.model ? '': classes.hidden}>
                    <Model kernelTarget={kernelTarget}
                           handleKernelResponse={this.handleKernelResponse}/>
                  </div>
                  { this.generateLayout() }
                  { mainApp.settings &&
                    <Settings handleChange={this.handleState}/>
                  }
                </div>
                <div className={classes.resCol}>
                  <OutputTabs className={classes.resCol}/>
                </div>
              </div>
            </MuiThemeProvider>
        );
    }
}

App.propTypes = {
    /** The sections of the manual (model sections excluded).*/
    sections: PropTypes.array.isRequired,
    /** The model sections defined in config file.*/
    modelSections: PropTypes.array.isRequired,
    /** Array containing lists of biocham commands.*/
    workflows: PropTypes.array.isRequired,
    /** The kernel target for Jupyter comms. The string should be identical in the kernel.*/
    kernelTarget: PropTypes.string.isRequired,
};

App.defaultProps = {
    sections: biochamSections,
    modelSections: crudSections,
    workflows: workflows,
    kernelTarget: 'from_gui'
};

export default connect(mapStateToProps, mapDispatchToProps)(
    withStyles(styles)(App)
);
