:- module(
  sepi_infos,
  [
    % Commands
    get_graphs_infos/4,
    get_graph_info/5,
    species/3
  ]
).

:- use_module(biocham).

%% Read Graph from file and create internal representation

%! get_graphs_infos(+FileName1, +FileName2, -G1, -G2)
%
% represent each graph as a list
% each vertex is represented by an integer
% Specie vertices: [[0, Nb_species - 1]] 
% Reaction vertices: [[Nb_species, Nb_vertices - 1]]
% Edges_list example: [(0,3), (1,3), (3,2)]
%
% @arg FileName1  input_file for graph 1
% @arg FileName2  input_file for graph 2
% @arg G1         graph 1 [Nb_vertices, Nb_species, Edges_list, Id]
% @arg G2         graph 2 [Nb_vertices, Nb_species, Edges_list, Id]
get_graphs_infos(FileName1, FileName2, G1, G2) :-
  once(get_graph_info(FileName1, Ns1, E1, N1, Id1)),
  once(get_graph_info(FileName2, Ns2, E2, N2, Id2)),
  G1 = [N1, Ns1, E1, Id1],
  G2 = [N2, Ns2, E2, Id2],
  N1 >= N2,
  Ns1 >= Ns2.


get_graph_info(FileName, Ns, Edges, N, Id) :-
  load(FileName),
  reaction_graph,
  get_current_graph(Id),
  number_species(Id),
  number_reactions(Id),
  list_edges(Id),
  peek_count(species, Ns),
  peek_count(reactions, Nr),
  N is Ns + Nr,
  peek_list(edges, LabEdges),
  unlable_edges(LabEdges, Edges, Id).


:- dynamic(species/3).


number_species(Id) :-
  set_counter(species, 0),
  retractall(species(_, _, Id)),
  forall((item([parent: Id, kind: vertex, item: VertexName]), not(get_attribute(Id, VertexName, kind = transition))),
    (count(species, Count), assertz(species(VertexName, Count, Id)))); true.


number_reactions(Id) :-
  peek_count(species, Nsp),
  set_counter(reactions, 0),
  forall((item([parent: Id, kind: vertex, item: Item]), get_attribute(Id, Item, kind = transition)),
    (count(reactions, Count), N is Nsp + Count, assertz(species(Item, N, Id)))); true.


list_edges(Id) :-
  set_list(edges, []),
  forall(item([parent: Id, kind: edge, item: Edge, id: _]),
    add_to_list(edges, Edge)); true.


set_list(ListName, InitialList) :-
  nb_setval(ListName, InitialList).


add_to_list(ListName, AddToList) :-
  nb_getval(ListName, List),
  NextList = [AddToList | List],
  nb_setval(ListName, NextList).


peek_list(ListName, List) :-
  nb_getval(ListName, List).


unlable_edges(LabledEdges, Edges, Id) :-
  unlable_edges(LabledEdges, [], Edges, Id),
  length(Edges, _).


unlable_edges([], Edges, E, _) :-
  E = Edges.
  
unlable_edges([(VertexA -> VertexB)|LabledEdges], Edges, E, Id) :-
  species(VertexA, Na, Id),
  species(VertexB, Nb, Id),
  unlable_edges(LabledEdges, [(Na, Nb)|Edges], E, Id).
