:- module(
  aliases,
  [
    % Commands
    alias/1,
    canonical/1,
    list_aliases/0,
    delete_alias/1,
    % Public API
    canonical/2
  ]
).

:- use_module(doc).

:- devdoc('\\section{Commands}').


:- biocham_silent(clear_model).

alias(ObjectList) :-
  biocham_command,
  type(ObjectList, =(object)),
  doc('
    makes \\argument{Objects} be alternative names for the
    same object.
\\clearmodel
\\trace{
biocham: a=>b.
biocham: b=>c.
biocham: c=>a.
biocham: list_reactions.
[0] a=>b
[1] b=>c
[2] c=>a
biocham: alias(a=c).
biocham: list_reactions.
[0] a=>b
[1] b=>a
[2] _=[a]=>_
}
'),
  setof(
    Object,
    instance_member_delete(Object, ObjectList),
    EquivalenceClassList),
  add_equivalence_class_list(EquivalenceClassList).


add_equivalence_class_list(EquivalenceClassList) :-
  (
    EquivalenceClassList = [_]
  ->
    true
  ;
    list_to_equals(EquivalenceClassList, EquivalenceClass),
    add_item([
      kind: alias, key: EquivalenceClassList, item: alias(EquivalenceClass)
    ]),
    simplify_all_reactions,
    check_aliased_states(EquivalenceClassList)
  ).


canonical(Object) :-
  biocham_command,
  type(Object, object),
  doc('
    makes \\argument{Object} be the canonical representant
    for all its aliases.'),
  find_alias(Object, Id, EquivalenceClassList),
  delete_item(Id),
  delete(EquivalenceClassList, Object, RemainingClassList),
  add_equivalence_class_list([Object | RemainingClassList]).


list_aliases :-
  biocham_command,
  doc('shows the values of all known aliases.'),
  list_items([kind: alias]).


delete_alias(Object) :-
  biocham_command,
  type(Object, object),
  doc('makes \\argument{Object} distinct from all other objects.'),
  find_alias(Object, Id, EquivalenceClassList),
  delete_item(Id),
  delete(EquivalenceClassList, Object, RemainingClassList),
  add_equivalence_class_list(RemainingClassList).


:- devdoc('\\section{Public API}').


canonical(Object, Canonical) :-
  devdoc('
    canonical(Object, Canonical) unifies Canonical with the canonical
    representant of the class of Object.
  '),
  catch(
    find_alias(Object, _, [Canonical | _]),
    error(not_an_alias(_)),
    Canonical = Object
  ).


:- devdoc('\\section{Private predicates}').


find_alias(Instance, Id, EquivalenceClassList) :-
  (
    item([id: Id, kind: alias, key: Instance, item: alias(EquivalenceClass)]),
    equals_to_list(EquivalenceClass, EquivalenceClassList)
  ->
    true
  ;
    throw(error(not_an_alias(Instance)))
  ).


instance_member_delete(Object, ObjectList) :-
  member(Instance, ObjectList),
  catch(
    (
      find_alias(Instance, Id, EquivalenceClassList),
      delete_item(Id),
      member(Object, EquivalenceClassList)
    ),
    error(not_an_alias(_)),
    Object = Instance
  ).
