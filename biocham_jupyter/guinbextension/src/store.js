import { applyMiddleware, createStore, compose } from 'redux';
import logger from 'redux-logger';
import reducer from './reducer';

const middlewares = [];

if (process.env.NODE_ENV === `development`) {
  const { logger } = require(`redux-logger`);
  middlewares.push(logger);
}

export const store = compose(applyMiddleware(...middlewares))(createStore)(reducer);

