:- module(
  functions,
  [
    % Grammar
    functor/1,
    % Commands
    'function'/1,
    list_functions/0,
    delete_function/1,
    % Public API
    set_macro/3,
    macro_apply/3,
    delete_macro/2,
    function_apply/2,
    list_model_macros/1,
    op(1010, fx, function)
  ]
).


:- devdoc('\\section{Grammar}').


functor(Functor/Arity) :-
  name(Functor),
  integer(Arity).


functor(Functor) :-
  name(Functor).


:- devdoc('\\section{Commands}').


function(FunctionList) :-
  biocham_command(*),
  type(FunctionList, '*'(function_prototype = arithmetic_expression)),
  doc('sets the definition of functions.'),
  \+ (
    member(Function = Value, FunctionList),
    \+ (
      set_macro(function, Function, Value)
    )
  ).


list_functions :-
  biocham_command,
  doc('lists all known functions.'),
  list_items([kind: function]).


delete_function(FunctorSet) :-
  biocham_command(*),
  type(FunctorSet, '*'(functor)),
  doc('deletes some functions.  Either arity is given, or all functions with
  the given functor are deleted.'),
  \+ (
    member(Functor, FunctorSet),
    \+ (
      delete_macro(function, Functor)
    )
  ).


:- devdoc('\\section{Public API}').


set_macro(Kind, Head, Body) :-
  Head =.. [Functor | _Arguments],
  check_identifier_kind(Functor, Kind),
  check_valid_identifier_name(Functor),
  macro_key(Head, Key),
  Macro =.. [Kind, Head = Body],
  change_item([], Kind, Key, Macro).


macro_apply(Kind, FunctionApplication, NewBody) :-
  callable(FunctionApplication),
  functor(FunctionApplication, Functor, Arity),
  macro_key(Functor, Arity, Key),
  item([kind: Kind, key: Key, item: Macro]),
  !,
  Macro =.. [Kind, Head = Body],
  Head =.. [Functor | Parameters],
  FunctionApplication =.. [Functor | Arguments],
  substitute(Parameters, Arguments, Body, NewBody).


delete_macro(Kind, Functor/Arity) :-
  !,
  macro_key(Functor, Arity, Key),
  delete_item([kind: Kind, key: Key]).


delete_macro(Kind, Functor) :-
  findall(
    Id,
    (
      item([kind: Kind, item: Item, id: Id]),
      Item =.. [Kind, Head = _Body],
      functor(Head, Functor, _Arity)
    ),
    Ids
  ),
  (
    Ids = []
  ->
    throw(error(unknown_macro(Kind, Functor)))
  ;
    \+ (
      member(Id, Ids),
      \+ (
        delete_item(Id)
      )
    )
  ).



function_apply(FunctionApplication, NewBody) :-
  macro_apply(function, FunctionApplication, NewBody).


list_model_macros(Kind) :-
  devdoc('
    lists all the macros of \\argument{Kind} in a loadable syntax
    (auxiliary predicate of list_model).
  '),
  (
    item([no_inheritance, kind: Kind])
  ->
    write(Kind),
    write('(\n'),
    write_successes(
      (
        item([no_inheritance, kind: Kind, item: Item]),
        Item =.. [Kind, Head = Body]
      ),
      write(',\n'),
      format('  ~w = ~w', [Head, Body])
    ),
    write('\n).\n')
  ;
    true
  ).


:- devdoc('\\section{Private predicates}').


macro_key(Function, Key) :-
  functor(Function, Functor, Arity),
  macro_key(Functor, Arity, Key).


macro_key(Functor, Arity, Key) :-
  format(atom(Key), '~a/~d', [Functor, Arity]).


% redefine the way things are printed by prolog
% necessary since we go through portray to avoid quoting capital letters 'A'

user:portray(function(Thing)) :-
  format("function ~p",[Thing]).
